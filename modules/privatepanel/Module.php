<?php

namespace app\modules\privatepanel;
//use yii\filters\AccessControl;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\privatepanel\controllers';
    
    public function init()
    {
        parent::init();
        if(\yii::$app->id != 'console')
        {
            $modules = \yii::$app->getModules();

            foreach($modules as $moduleID => $module)
            {
                if($moduleID == $this->getUniqueId())
                    continue;
                if(!is_object($module))
                {
                    $module = (\Yii::$app->hasModule($moduleID))?\Yii::$app->getModule($moduleID):null;
                }

                if($module && method_exists($module, 'privateControllerMap'))
                {
                    $this->controllerMap = array_merge($this->controllerMap, $module->privateControllerMap());
                }
            }
            \yii::$app->user->loginUrl = [$this->getUniqueId().'/default/login'];
        }
        // custom initialization code goes here
        self::registerTranslations();
    }
    
    public static function registerTranslations()
    {
        
        \Yii::$app->i18n->translations['modules/privatepanel/*'] = [
            'class' => '\yii\i18n\PhpMessageSource',
            'sourceLanguage' => 'en-US',
            'basePath' => '@app/modules/privatepanel/messages',
            'fileMap' => [
                'modules/privatepanel/app' => 'app.php',
            ],
        ];
        
    }
    
    public static function t($category, $message, $params = [], $language = null)
    {
        if($category == 'app')
            return \Yii::t('modules/privatepanel/' . $category, $message, $params, $language);
        else {
            
            return \Yii::t($category, $message, $params, $language);
        }
    }
    
    public function getMenus()
    {
        $recallCount = \app\models\Recall::find()->where('is_viewed=0')->count();
        $result = [
            'home' => 
                ['label' => self::t('app', 'Main panel'), 'url'=> ['/private/default/index'], 'visible' => true, 'icon' => 'home']
            ,
            'users' => ['label' => \yii::t('modules/user/app', 'Users'), 'url'=> ['/privateuser/user'], 'icon' => 'users', 'visible' => \yii::$app->user->can('/privateuser/user/admin',[], true), 'items'=>[
                ['label' => \yii::t('modules/user/app', 'Users'), 'url'=> ['/privateuser/user'], 'visible' => \yii::$app->user->can('/privateuser/user/admin',[], true)],
                ['label' => \yii::t('modules/user/app', 'Roles'), 'url'=> ['/privateuser/role'], 'visible' => \yii::$app->user->can('/privateuser/role/admin',[], true)],
                ['label' => \yii::t('modules/user/app', 'Permissions'), 'url'=> ['/privateuser/permission'], 'visible' => \yii::$app->user->can('/privateuser/permission/admin',[], true)],
            ]],
            'content' => ['label' => Module::t('app', 'Content'), 'url'=> '#', 'icon' => 'file', 'items'=>[
                ['label' => \yii::t('app', 'Pages'), 'url'=> ['/private/page'], 'visible' => \yii::$app->user->can('/private/page/admin',[], true)],
                ['label' => \yii::t('app', 'Text place contents'), 'url'=> ['/private/text-place-content'], 'visible' => \yii::$app->user->can('/private/text-place-content/admin',[], true)],
                ['label' => 'Слайдер', 'url'=> ['/private/slider'], 'visible' => \yii::$app->user->can('/private/slider/admin',[], true)],
                ['label' => 'Блоки "О нас"', 'url'=> ['/private/about'], 'visible' => \yii::$app->user->can('/private/about/admin',[], true)],
                ['label' => 'Подразделения', 'url'=> ['/private/departments'], 'visible' => \yii::$app->user->can('/private/departments/admin',[], true)],
            ]],
            'donors' => ['label' => 'Информация о донорах', 'url'=> '#', 'items'=>[
                ['label' => 'Доноры', 'url'=> ['/private/donor'], 'visible' => \yii::$app->user->can('/private/donor/admin',[], true)],
                ['label' => 'Список с датой', 'url'=> ['/private/donorlist/list1'], 'visible' => \yii::$app->user->can('/private/donor/admin',[], true)],
                ['label' => 'Карточки доноров', 'url'=> ['/private/dcard'], 'visible' => \yii::$app->user->can('/private/dcard/admin',[], true)],
                ['label' => 'Статусы', 'url'=> ['/private/donorstatus'], 'visible' => \yii::$app->user->can('/private/donorstatus/admin',[], true)],
                ['label' => 'История', 'url'=> ['/private/donorhistory'], 'visible' => \yii::$app->user->can('/private/donorhistory/admin',[], true)],
                ['label' => 'Записать', 'url'=> ['/private/donorlist/create/'], 'visible' => \yii::$app->user->can('/private/donorlist/create',[], true)],
            ]],
            'donate-table' => ['label' => 'Таблицы сдающих', 'url' => ['/private/donorlist/full-list'], 'visible' => true, true],
            //'donates' => ['label' => 'Сдача крови', 'url'=> ['/private/donates'], 'visible' => \yii::$app->user->can('/private/donates/admin',[], true)],
            'times' => ['label' => 'Время сдачи - список', 'url'=> ['/private/times'], 'visible' => \yii::$app->user->can('/private/times/admin',[], true)],
            'recall' => ['label' => "Обратная связь ({$recallCount})", 'url'=> ['/private/recall'], 'visible' => \yii::$app->user->can('/private/recall/admin',[], true)],
            'mail' => ['label' => 'Сообщения', 'url'=> '#', 'items'=>[
                ['label' => 'Сообщения', 'url'=> ['/private/mail'], 'visible' => \yii::$app->user->can('/private/mail/admin',[], true)],
                ['label' => 'Отправка', 'url'=> ['/private/mail/send'], 'visible' => \yii::$app->user->can('/private/mail/send',[], true)],
                ['label' => 'Лог отправленных сообщений', 'url'=> ['/private/maillog'], 'visible' => \yii::$app->user->can('/private/maillog/admin',[], true)],
                ]]
        ];
        $modules = \yii::$app->getModules();

        foreach($modules as $moduleID => $module)
        {
            if(!is_object($module))
            {
                $module = (\Yii::$app->hasModule($moduleID))?\Yii::$app->getModule($moduleID):null;
            }
            if($module && method_exists($module, 'privateMenuLinks'))
            {
                $result = \yii\helpers\ArrayHelper::merge($result, $module->privateMenuLinks());
            }
        }
        $result = \yii\helpers\ArrayHelper::merge($result, ['settings' => ['label' => Module::t('app', 'Settings'), 'url'=> '#', 'icon' => 'cogs', 'items'=>[
                ['label' => 'Документы', 'url'=> ['/private/document'], 'visible' => \yii::$app->user->can('/private/document/admin',[], true)],
                //['label' => \yii::t('app', 'Menus'), 'url'=> ['/private/menu-type'], 'visible' => \yii::$app->user->can('/private/menu-type/admin',[], true)],
                ['label' => \yii::t('app', 'Mail templates'), 'url'=> ['/private/mail-template'], 'visible' => \yii::$app->user->can('/private/mail-template/admin',[], true)],
                ['label' => \yii::t('app', 'Constants'), 'url'=> ['/private/constants'], 'visible' => \yii::$app->user->can('/private/constants/admin',[], true)],
                ['label' => \yii::t('app', 'Text places'), 'url'=> ['/private/text-place'], 'visible' => \yii::$app->user->can('/private/text-place/admin',[], true)],
                ['label' => \yii::t('app', 'Settings'), 'url'=> ['/private/project-settings'], 'visible' => \yii::$app->user->can('/project-settings/admin',[], true)],
            ]],
            'file-manager' => ['label' => \yii::t('app', 'File Manager'), 'url'=> ['/private/file-manager'], 'visible' => \yii::$app->user->can('/private/file-manager/admin',[], true), 'icon' => 'folder-open'],
        ]);
        
        return $result;
    }
    
    public function getQuickMenus()
    {
        $result = [
            ['label' => \yii::t('app', 'Pages'), 'url'=> ['/private/page'], 'icon' => 'file-o', 'sort' => 10, 'visible' => \yii::$app->user->can('/private/page/admin',[], true)],
            ['label' => \yii::t('modules/user/app', 'Users'), 'url'=> ['/privateuser/user'], 'icon' => 'users', 'class' => 'primary', 'sort' => 20, 'visible' => \yii::$app->user->can('/privateuser/user/admin',[], true)],
        ];
        $modules = \yii::$app->getModules();

        foreach($modules as $moduleID => $module)
        {
            if($moduleID == $this->id)
                continue;
            if(!is_object($module))
            {
                $module = (\Yii::$app->hasModule($moduleID))?\Yii::$app->getModule($moduleID):null;
            }
            if($module && method_exists($module, 'getQuickMenus'))
            {
                $result = \yii\helpers\ArrayHelper::merge($result, $module->getQuickMenus());
            }
        }
        
        $result = \yii\helpers\ArrayHelper::merge($result, [
            ['label' => self::t('app', 'Instructions'), 'url'=> ['/private/page'], 'icon' => 'book', 'class' => 'green', 'sort' => 1000],
        ]);
        
        return $result;
    }
}
