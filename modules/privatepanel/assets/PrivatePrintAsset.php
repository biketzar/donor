<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\modules\privatepanel\assets;

use yii\web\AssetBundle;

class PrivatePrintAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/privatepanel/assets';
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        //'css/private/style/style.css',
        'web/css/private/style/print.css',
    ];
    public $js = [
        
    ];
    public $cssOptions = [
        'media' => 'print',
    ];
    public $depends = [
        'app\modules\privatepanel\assets\PrivateAsset',
    ];
}
