<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\modules\privatepanel\assets;

use yii\web\AssetBundle;

class GoogleMapAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/privatepanel/assets';
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
    ];
    public $jsOptions = [
        'position' => \yii\web\View::POS_HEAD,
    ];
    public $js = [
        'https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false',
        'web/css/private/js/GoogleMapLocation.js'
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'app\modules\privatepanel\assets\PrivateAsset',
    ];
}
