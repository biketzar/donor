<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\modules\privatepanel\assets;

use yii\web\AssetBundle;

class PrivateAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/privatepanel/assets';
    /*public $basePath = '@webroot2/private';
    public $baseUrl = '@web2/private';*/
    public $css = [
        'styles/plugins.css',
        'styles/main.css',
        //'css/private/style/print.css',
    ];
    public $js = [
        'scripts/vendor.js',
        'scripts/main.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
    ];
}
