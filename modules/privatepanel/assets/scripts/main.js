$(window).load(function() {

// menu accordion
    // jQuery('.page-sidebar ul > li.has-sub > a').click(function () {
    //     var last = jQuery('.has-sub.open', $('.page-sidebar'));
    //     last.removeClass("open");
    //     jQuery('.arrow', last).removeClass("open");
    //     jQuery('.sub', last).slideUp(200);

    //     var sub = jQuery(this).next();
    //     if (sub.is(":visible")) {
    //         jQuery('.arrow', jQuery(this)).removeClass("open");
    //         jQuery(this).parent().removeClass("open");
    //         sub.slideUp(200);
    //     } else {
    //         jQuery('.arrow', jQuery(this)).addClass("open");
    //         jQuery(this).parent().addClass("open");
    //         sub.slideDown(200);
    //     }
    // });
$(".accordion").accordion({
        accordion:true,
        speed: 500,
        closedSign: '<span class="arrow"></span>',
        openedSign: '<span class="arrow act"></span>'
    });

// $('.accordion ul > li.has-sub > a').click(function() {

//     var checkElement = $(this).next();

//     $('.accordion li').removeClass('active');
//     $(this).closest('li').addClass('active');

//     if((checkElement.is('ul')) && (checkElement.is(':visible'))) {
//         $(this).closest('li').removeClass('active');
//         checkElement.slideUp('normal');
//     }

//     if((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
//         $('.accordion ul ul:visible').slideUp('normal');
//         checkElement.slideDown('normal');
//     }

//     if (checkElement.is('ul')) {
//         return false;
//     } else {
//         return true;
//     }
// });
/*
    function showMenu() {
        var screen_width = Math.max(document.documentElement.clientWidth, window.innerWidth || 0)
        if (screen_width >= 991) {
            $('body').removeClass('page-sidebar-closed');
        }
        if (screen_width <= 990) {
            $('body').addClass('page-sidebar-closed');
        }
    }
    $(window).resize(function(){
        showMenu();
    });
*/

// sidebar show/hide
    $('body').on('click', '.sidebar-toggler', function (e) {
        var sidebar = $('.page-sidebar');
        var sidebarMenu = $('.page-sidebar-menu');
        var body = $('body');
        //$(".sidebar-search", sidebar).removeClass("open");
        $(this).toggleClass('active');
        if (body.hasClass("page-sidebar-closed")) {
            body.removeClass("page-sidebar-closed");
            sidebarMenu.removeClass("page-sidebar-menu-closed");
            if ($.cookie) {
                $.cookie('sidebar_closed', '0');
            }
        } else {
            body.addClass("page-sidebar-closed");
            sidebarMenu.addClass("page-sidebar-menu-closed");
            if (body.hasClass("page-sidebar-fixed")) {
                sidebarMenu.trigger("mouseleave");
            }
            if ($.cookie) {
                $.cookie('sidebar_closed', '1');
            }
        }
        e.preventDefault();
        $(window).trigger('resize');
    });

// sorting
    $('th.sorting').click(function(e) {
        if(e.target.tagName != 'A')
        {
            /*$(this).find('a').trigger('click');
            e.preventDefault();
            return false;*/
            /*var link = $(this).find('a')[0];
            var linkEvent = null;
            if (document.createEvent) {
              linkEvent = document.createEvent('MouseEvents');
              linkEvent.initEvent('click', true, true);
              link.dispatchEvent(linkEvent);
            }
            else if (document.createEventObject) {
              linkEvent = document.createEventObject();
              link.fireEvent('onclick', linkEvent);
            }
            */
           location.href = $(this).find('a[data-sort]').attr('href');
            e.preventDefault();
        }
        
        /*if ($(this).hasClass("sorting_asc")) {
            $(this).removeClass("sorting_asc");
            $(this).addClass("sorting_desc");
        } else if ($(this).hasClass("sorting_desc")) {
            $(this).addClass("sorting_asc");
            $(this).removeClass("sorting_desc");
        } else {
            $(this).addClass("sorting_asc");
        };*/
    });

// admin-bar
    $('.pass_remember').click(function() {
        $('.admin__bar-inner-password').slideToggle();
        $(this).find('i').toggleClass('active');
    });

// select
    if ($("select").length) {
        $('select').select2({
                formatNoMatches: function () {return "Нет результатов";},
                formatMatches: function(a){return 1===a?"One result is available, press enter to select it.":a+" results are available, use up and down arrow keys to navigate."},
                formatAjaxError:function(){return"Ошибка загрузки"},
                formatInputTooShort:function(a,b){var c=b-a.length;return"Введите "+c+"  или больше символов";},
                formatInputTooLong:function(a,b){var c=a.length-b;return"Удалите "+c+" символов";},
                formatSelectionTooBig:function(a){return"Вы можете выбрать "+a+" элемент(ов)";},
                formatLoadMore:function(){return"Слишком много результатов\u2026"},
                formatSearching:function(){return"Поиск\u2026"},
        });
    }
    
});