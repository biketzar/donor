<?php

namespace app\modules\privatepanel\components\fields;

use yii\base\Object;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use app\modules\privatepanel\components\fields\RDbBase;

//use app\modules\privatepanel\components\PrivateModelAdmin;

class RDbDateTime extends RDbBase
{
	protected $_model;
	protected $_admin;
	protected $_field;
	protected $_field_origin;
	protected $_args = array();

	public function __construct( $model, $field, $args = array() )
	{
		$this->_admin = $model;
		$this->_model = $model->getModel();
		$this->_field = $field;
		$this->_field_origin = isset($args['field'])?$args['field']:$field;
		$this->_args = $args;
	}
	
	public function __toString()
	{
            $value = (isset($this->_model->{$this->_field.'Text'}))?$this->_model->{$this->_field.'Text'}:$this->_model->{$this->_field};
            if(is_array($value))
            {
                return implode(', ',$value);
            }
            return (string)$value;
	}

	public function getFieldFilter()
	{
		return null;
	}

	public function asGridColumn($filter = true)
	{
		return array(
                    'class' => 'app\modules\privatepanel\components\CustomDataColumn',
                    'format'=>'raw',
                    //'attribute' => $this->_field,
                    'filter' => ($filter ? $this->getFieldFilter() : false),
                    'attribute' => $this->_field,
                    'filterInputOptions' => ['class'=>''],
                    'options' => ['class' => ''], 
                    'value' => function ($data) {
                        $admin = \Yii::createObject(['class' => 'app\modules\privatepanel\components\PrivateModelAdmin'],[$data]);
                        return (string)$admin->getField($this->_field);
                    },
		);

	}

	public function asViewAttribute()
	{
		return array(
			'label' => (isset($this->_args['label']))?$this->_args['label']:$this->_model->getAttributeLabel($this->_field),
                        'format' => 'html',
			'name' => $this->_field,
			'value' => (string)$this,
		);
	}

	public function asActiveForm($activeForm, $options = array('template' => '{label} {input} {error}',
                    'htmlOptions' => array(),'labelOptions' => array()) )
	{
            if(!in_array($this->_field, $this->_admin->preInitFields))
            {
		$template = array_key_exists('template', $options) ? $options['template'] :'{label} {input} {error}' ;
		$htmlOptions = array_key_exists('htmlOptions',$options) ? $options['htmlOptions'] : array();
		$labelOptions = array_key_exists('labelOptions',$options) ? $options['labelOptions'] : array();
                
                if(isset($this->_args['htmlOptions'])){
			$htmlOptions = ArrayHelper::merge($htmlOptions, $this->_args['htmlOptions']);
		}
                $template = str_replace('{label}', $this->renderLabel($labelOptions), $template);
		$template = str_replace('{help}', $this->renderHelp(), $template);
		$template = str_replace('{input}', $this->renderInput($activeForm, $htmlOptions), $template);
		$template = str_replace('{error}', Html::error($this->_model, $this->_field, []), $template);
                
		return $template;
            } else {
                return $activeForm->field($this->_model, $this->_field, ['template'=>'{input}', 'options' => ['class' => '']])->hiddenInput();
            }
            
	}

	protected function renderInput($activeForm, $htmlOptions = array())
	{
            \Yii::$app->view->registerCssFile('/web/js/datetimepicker-master/jquery.datetimepicker.css');
            \Yii::$app->view->registerJsFile('/web/js/datetimepicker-master/build/jquery.datetimepicker.full.min.js', ['depends' => \yii\web\JqueryAsset::className()]);
            $function = new \ReflectionClass(get_class($this->_model));
            $id = strtolower($function->getShortName()).'-'.strtolower($this->_field);
            \Yii::$app->view->registerJs("jQuery(document).ready("
                    . "function () {"
                    . "jQuery.datetimepicker.setLocale('ru');"
                    . "jQuery('input#{$id}').datetimepicker({"
                            . "startTime: '7:00', "
                            . "format:'Y-m-d H:i', "
                            . "inline:false, "
                            . "lang:'ru',"
                            . "allowTimes:[
                                '07:00','07:10','07:20','07:30','07:40','07:50',
                                '08:00','08:10','08:20','08:30','08:40','08:50',
                                '09:00','09:10','09:20','09:30','09:40','09:50',
                                '09:00','09:10','09:20','09:30','09:40','09:50',
                                '09:00','09:10','09:20','09:30','09:40','09:50',
                                '10:00','10:10','10:20','10:30','10:40','10:50',
                                '11:00','11:10','11:20','11:30','11:40','11:50',
                                '12:00','12:10','12:20','12:30','12:40','12:50',
                                '13:00','13:10','13:20','13:30','13:40','13:50',
                                '14:00','14:10','14:20','14:30','14:40','14:50',
                                '15:00','15:10','15:20','15:30','15:40','15:50',
                                '16:00','16:10','16:20','16:30','16:40','16:50',
                                '17:00','17:10','17:20','17:30','17:40','17:50',
                               ],"
                            . "onChangeDateTime:function(dp,input){
                                    jQuery('input#{$id}').val(input.val());
                                    console.log(jQuery('input#{$id}').val());
                                  }"
                            . "});});");
            $htmlOptions['size'] = 80;
            return $activeForm->field($this->_model, $this->_field, ['template'=>'{input}', 'options' => ['class' => '', 'id' => $id, 'autocomplete'=>'off', 'size'=>"80"]])->textInput($htmlOptions);
	}

	protected function renderHelp()
	{
            $help = $this->_model->getAttributeHint($this->_field);
            
            if($help){
                    $a = $this->_field;
                    $htmlOptions = array();
                    Html::resolveNameID($this->_model, $a, $htmlOptions);
                    //Yii::app()->clientScript->registerCoreScript('jquery.ui');
                    //return '<a class="helpButton" rel="help_'.$htmlOptions['id'].'">&nbsp;</a><div class="helpConteiner"><div class="helpMessage" id="help_'.$htmlOptions['id'].'">'.$help.'</div></div>';
                    return ''.$help.'';
            }
            
            return '';
	}
        protected function renderLabel($options = [])
        {
            if(isset($this->_args['label']))
            {
                $options['label'] = $this->_args['label'];
            }
            return Html::activeLabel($this->_model, $this->_field, $options);
        }


	public function asSearchForm($activeForm, $options)
        {
            $template = isset($options['template']) ? $options['template'] :'{label} {input}' ;
            $htmlOptions = isset($options['htmlOptions']) ? $options['htmlOptions'] : array();

            $template = str_replace('{label}', $activeForm->label($this->_model, $this->_field), $template);
            $template = str_replace('{input}', $this->renderInput($activeForm,$htmlOptions), $template);
            return $template;
        }
        
        public function prepareForSave($data, $check = false)
        {
            if(isset($data[$this->_field]))
            {
                $data[$this->_field] = $data[$this->_field];
            }
            return $data;
        }
        public function afterSave($data)
        {
        }
        
        public function getFilterWhere($value)
        {
            return ['like', $this->_model->tableName().'.'.$this->_field, $value];
        }
}