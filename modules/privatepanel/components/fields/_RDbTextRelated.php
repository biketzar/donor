<?php

/**
 * Используется для вывода неизменяемого текста, без фильтрации.
 * 
 * Характерный пример применения - вывод поля из связанной записи:
 * Пусть есть модель Child и связанная модель Parent.
 * Требуется вывести в админке Child поле external_field из Parent.
 * Для этого в Child добавляется магический метод getExternal_Field(),
 * который возвращает значение из связанной модели.
 * Таким образом у Child появляется "магическое" поле external_field, которое
 * и выводится с помощью данного класса.
 */
class RDbTextRelated extends RDbText
{

	/**
	 * Формирует поле для ввода текста.
	 * Если атрибут модели имеет в БД тип text или longtext то выводится визуальный редактор. 
	 * В остальных случаях выводиться простой input. Вывод textarea можно задать вручную с помощью спец.опции.
	 */
	protected function renderInput($activeForm, $htmlOptions)
	{
		if ($this->_model) {
			return (string)$this;
		}
		return null;
	}

	public function asSearchForm($activeForm, $options)
	{
		return '';
	}
	
	
	public function getFieldFilter()
	{
		return false;
	}

}