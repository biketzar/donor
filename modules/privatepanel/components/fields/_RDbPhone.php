<?php
/**
 * Элемент формы административной части для ввода телефона.
 * @author Lvov
 */
class RDbPhone extends RDbText
{
	public function __toString() {
		$t = parent::__toString();
		return ($t) ? '+'.$t : '';
	}
	
	/**
	 * Рендерим html для формы.
	 * HTML генериться в отдельном виджите. Это делает возможным использовать этот виджет и в клиентской части
	 * @param CActiveForm $activeForm
	 * @param array $htmlOptions
	 * @return string 
	 */
	protected function renderInput($activeForm,$htmlOptions)
	{
		return Yii::app()->controller->widget('private.widgets.PPhoneInput', array(
			'model' => $this->_model,
			'attribute' => $this->_field,		
		), true);
	}
}