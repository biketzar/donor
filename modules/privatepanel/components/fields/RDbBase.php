<?php

namespace app\modules\privatepanel\components\fields;

use yii\base\Object;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;

//use app\modules\privatepanel\components\PrivateModelAdmin;

class RDbBase extends Object
{
	protected $_model;
	protected $_admin;
	protected $_field;
	protected $_field_origin;
	protected $_args = array();

	public function __construct( $model, $field, $args = array() )
	{
		$this->_admin = $model;
		$this->_model = $model->getModel();
		$this->_field = $field;
		$this->_field_origin = isset($args['field'])?$args['field']:$field;
		$this->_args = $args;
	}
	
	public function __toString()
	{
            $value = (isset($this->_model->{$this->_field.'Text'}))?$this->_model->{$this->_field.'Text'}:$this->_model->{$this->_field};
            if(is_array($value))
            {
                return implode(', ',$value);
            }
            return (string)$value;
	}

	public function getFieldFilter()
	{
		return null;
	}

	public function asGridColumn($filter = true)
	{
		return array(
                    'class' => 'app\modules\privatepanel\components\CustomDataColumn',
                    'format'=>'raw',
                    //'attribute' => $this->_field,
                    'filter' => ($filter ? $this->getFieldFilter() : false),
                    'attribute' => $this->_field,
                    'filterInputOptions' => ['class'=>''],
                    'options' => ['class' => ''], 
                    'value' => function ($data) {
                        $admin = \Yii::createObject(['class' => 'app\modules\privatepanel\components\PrivateModelAdmin'],[$data]);
                        return (string)$admin->getField($this->_field);
                    },
		);

	}

	public function asViewAttribute()
	{
		return array(
			'label' => (isset($this->_args['label']))?$this->_args['label']:$this->_model->getAttributeLabel($this->_field),
                        'format' => 'html',
			'name' => $this->_field,
			'value' => (string)$this,
		);
	}

	public function asActiveForm($activeForm, $options = array('template' => '{label} {input} {error}',
                    'htmlOptions' => array(),'labelOptions' => array()) )
	{
            if(!in_array($this->_field, $this->_admin->preInitFields))
            {
		$template = array_key_exists('template', $options) ? $options['template'] :'{label} {input} {error}' ;
		$htmlOptions = array_key_exists('htmlOptions',$options) ? $options['htmlOptions'] : array();
		$labelOptions = array_key_exists('labelOptions',$options) ? $options['labelOptions'] : array();
                
                if(isset($this->_args['htmlOptions'])){
			$htmlOptions = ArrayHelper::merge($htmlOptions, $this->_args['htmlOptions']);
		}
                $template = str_replace('{label}', $this->renderLabel($labelOptions), $template);
		$template = str_replace('{help}', $this->renderHelp(), $template);
		$template = str_replace('{input}', $this->renderInput($activeForm, $htmlOptions), $template);
		$template = str_replace('{error}', Html::error($this->_model, $this->_field, []), $template);
                
		return $template;
            } else {
                return $activeForm->field($this->_model, $this->_field, ['template'=>'{input}', 'options' => ['class' => '']])->hiddenInput();
            }
            
	}

	protected function renderInput($activeForm, $htmlOptions = array())
	{
		return $activeForm->textField($this->_model, $this->_field, $htmlOptions);
	}

	protected function renderHelp()
	{
            $help = $this->_model->getAttributeHint($this->_field);
            
            if($help){
                    $a = $this->_field;
                    $htmlOptions = array();
                    Html::resolveNameID($this->_model, $a, $htmlOptions);
                    //Yii::app()->clientScript->registerCoreScript('jquery.ui');
                    //return '<a class="helpButton" rel="help_'.$htmlOptions['id'].'">&nbsp;</a><div class="helpConteiner"><div class="helpMessage" id="help_'.$htmlOptions['id'].'">'.$help.'</div></div>';
                    return ''.$help.'';
            }
            
            return '';
	}
        protected function renderLabel($options = [])
        {
            if(isset($this->_args['label']))
            {
                $options['label'] = $this->_args['label'];
            }
            return Html::activeLabel($this->_model, $this->_field, $options);
        }


	public function asSearchForm($activeForm, $options)
        {
            $template = isset($options['template']) ? $options['template'] :'{label} {input}' ;
            $htmlOptions = isset($options['htmlOptions']) ? $options['htmlOptions'] : array();

            $template = str_replace('{label}', $activeForm->label($this->_model, $this->_field), $template);
            $template = str_replace('{input}', $this->renderInput($activeForm,$htmlOptions), $template);
            return $template;
        }
        
        public function prepareForSave($data, $check = false)
        {
            if(isset($data[$this->_field]))
            {
                $data[$this->_field] = $data[$this->_field];
            }
            return $data;
        }
        public function afterSave($data)
        {
        }
        
        public function getFilterWhere($value)
        {
            return ['like', $this->_model->tableName().'.'.$this->_field, $value];
        }
}