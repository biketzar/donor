<?php

/**
 * Элемент-поле для файла, защищеного от скачивания.
 */
class RDbSecureFile extends RDbFile
{
	public function getFieldFilter() {
		return array(
			'doc' => 'Word',
			'docx' => 'Word 2011',
			'xsl' => 'Excel',
			'pdf' => 'PDF',
			'jpg' => 'Изображение JPG',
		);
	}
	
	public function asGridValue()
	{
		$buffer = $this->_model->{$this->_field};
		if(isset($this->_model->{$this->_field.'Path'})){
			$file = $this->_model->{$this->_field.'Path'};
			
			if($file){
				if($this->_model instanceof SupplierDoc){
					$url = '/'.Yii::app()->controller->module->id.'/supplierDoc/download/doc/'.$this->_model->primaryKey;
					$url .= '/supp/1';
				}else{
					$url = '/'.Yii::app()->controller->module->id.'/orderDoc/download/doc/'.$this->_model->primaryKey;
				}
				$buffer = Html::link('Формат &laquo;'.pathinfo($file, PATHINFO_EXTENSION).'&raquo<br/>Скачать', $url, array('target' => '_blank'));
			}
		}
		return $buffer;
	}
}