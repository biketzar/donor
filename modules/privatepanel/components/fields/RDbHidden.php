<?php
/**
 * Элемент формы административной части для невидимых элементов. 
 */

namespace app\modules\privatepanel\components\fields;

use app\modules\privatepanel\components\fields\RDbBase;

use yii\helpers\StringHelper;


class RDbHidden extends RDbBase
{
	
	/**
	 * Формирует поле для ввода текста.
	 * Если атрибут модели имеет в БД тип text или longtext то выводится визуальный редактор. 
	 * В остальных случаях выводиться простой input. Вывод textarea можно задать вручную с помощью спец.опции.
	 */
	protected function renderInput($activeForm, $htmlOptions = array())
	{
		return $activeForm->field($this->_model, $this->_field, ['template'=>'{input}'])->hiddenInput();
	}
        
        protected function renderLabel($options = [])
        {
            return '';
        }

	public function __toString()
	{
            $result = StringHelper::truncateWords( parent::__toString(), 255, $suffix = '...');
            if(!empty($this->_args['escape']))
                $result = HtmlPurifier::process($result);
                
            return $result;
	}

}