<?php


class RDbMassiveRelation extends RDbRelation
{
	public function getFieldFilter() 
	{
		$html_id = CHtml::resolveName($this->_model, $this->_field);
		
		Yii::app()->clientScript
				->registerCoreScript('jquery.ui')
				->registerCssFile(Yii::app()->clientScript->getCoreScriptUrl().'/jui/css/base/jquery-ui.css')
				// виджет .combox() ищи в файле /css/private/js/main.js
				->registerScript($html_id.'sxc81299x', 'jQuery(".filters select[name*=id_good]").combobox()');
		;
		
		$values = array();
		if($this->_model->getRelated($this->_args[0], true)){
			$values[$this->_model->getRelated($this->_args[0], false)->primaryKey] = $this->_model->getRelated($this->_args[0], false)->admin->getRepr();
		}
		return $values;
	}
	
	/**
	 * Возвращает непростой комбобокс
	 * @param CActiveForm $activeForm
	 * @param array $htmlOptions
	 * @return string 
	 */
	protected function renderSimpleDropList($activeForm, $htmlOptions)
	{
		CHtml::resolveNameID($this->_model, $this->_field, $htmlOptions);
		$html_id = $htmlOptions['id'];
		$values = array();
		if($this->_model->getRelated($this->_args[0], true)){
			$values[$this->_model->getRelated($this->_args[0], false)->primaryKey] = $this->_model->getRelated($this->_args[0], false)->admin->getRepr();
		}
		$htmlOptions['style'] = 'width: 400px;';
		$buffer  = $activeForm->dropDownList($this->_model, $this->_field, $values, $htmlOptions);
		Yii::app()->clientScript
				->registerCoreScript('jquery.ui')
				->registerCssFile(Yii::app()->clientScript->getCoreScriptUrl().'/jui/css/base/jquery-ui.css')
				// виджет .combox() ищи в файле /css/private/js/main.js
				->registerScript($html_id.'sxc81299x', 'jQuery("#'.$html_id.'").combobox()');
		;
		return $buffer;
	}
	
	protected function renderMultipleDropList($activeForm, $htmlOptions) 
	{
		//Note: для того что бы можно было снять выделение и сохранить результат, нужно добавлять скрытое поле с именем ModelClass[relationName][] и с пустым значением
		//Но если поле обязательное и ничего не выделено, то ошибки не будет, поэтому hidden-поле выводим только для не обязательны полей
		$buffer = ((!$this->_model->isAttributeRequired($this->_field)) ? $activeForm->hiddenField($this->_model, $this->_field.'[]', array('id' => 'a'.rand(), 'value'=>null)) : '');
		
		CHtml::resolveNameID($this->_model, $this->_field, $htmlOptions);
		$html_id = $htmlOptions['id'];
		$relOpts = $this->_model->getActiveRelation($this->_args[0]);
		
		$values = array();
		foreach($this->_model->{$this->_args[0]} as $relModel){
			if(is_object($relModel)){
				$values[$relModel->primaryKey] = $relModel->admin->getRepr();
			}else{
				$relModel = CActiveRecord::model($relOpts->className)->findByPk($relModel);
				if($relModel !== null){
					$values[$relModel->primaryKey] = $relModel->admin->getRepr();
				}
			}
		}
		$htmlOptions['multiple'] = 'multiple';
		$htmlOptions['style'] = 'width: 400px;';
		$buffer .= $activeForm->dropDownList($this->_model, $this->_field, $values, $htmlOptions);
		Yii::app()->clientScript
				->registerCoreScript('jquery.ui')
				->registerCssFile(Yii::app()->clientScript->getCoreScriptUrl().'/jui/css/base/jquery-ui.css')
				->registerScript($html_id.'9902mxcmz', 'jQuery("#'.$html_id.'").multicombobox()');
		;
		
		return $buffer;
	}
}