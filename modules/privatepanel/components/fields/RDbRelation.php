<?php
namespace app\modules\privatepanel\components\fields;

use app\modules\privatepanel\Module;
use app\modules\privatepanel\components\fields\RDbBase;
use app\modules\privatepanel\components\PrivateModelAdmin;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

class RDbRelation extends RDbBase
{
	/**
         * Связь между таблицами
         * @var object
         */
        protected $_relation = null;
        
        /**
         * Название связи между таблицами
         * @var string
         */
        
        protected $_relationName = '';
        
        
        protected $_condition = array();
	/**
	 * Флаг означающий что у нас множественное отношение,
	 * определяется автоматически
	 * @var boolean
	 */
	protected $_multiple = false;
	
	protected $_multipleBr = '<br/>';
        
        
	/**
	 * Флаг режима древовидного отображения
	 * @var boolean 
	 */
	protected $_treeModeRelation = false;
	/**
	 *
	 * @var string 
	 */
	protected $_treeParentRel = null;
	/**
	 * 
	 * @var string 
	 */
	protected $_treeChildRel = null;
	/**
	 * Флаг работы только с конечными нодами.
	 * @var boolean 
	 */
	protected $_treeModeOnlyLeaf = false;
	/**
	 * атрибут модели, который выводим вкачестве названия
	 * @var string 
	 */
	protected $_labelTemplate = 'admin.repr';

	/**
	 * дополнительные критерии для выборки
	 * @var null|CDbCriteria 
	 */
	protected $_criteria = null;
	
	/**
	 * Флаг того, что поле недоступно при редактировании
	 * @var boolean 
	 */
	//protected $_disableOnEdit = false;


	public function __construct( $model, $field, $args = array() )
	{
            parent::__construct($model, $field, $args);

            // работа возможно если заданы оба параметра
            if(isset($this->_args['treeParentRel']) && isset($this->_args['treeChildRel'])){
                    $this->_treeModeRelation = true;
                    $this->_treeParentRel = $this->_args['treeParentRel'];
                    $this->_treeChildRel = $this->_args['treeChildRel'];
            }
            if(isset($this->_args['treeModeOnlyLeaf'])){
                    $this->_treeModeOnlyLeaf = $this->_args['treeModeOnlyLeaf'];
            }
            /*if(isset($this->_args['disableOnEdit'])){
                    $this->_disableOnEdit = $this->_args['disableOnEdit'];
            }
            */
            $this->_relationName = $this->_args[0];//get relation name
            
            $this->_relation = $this->_model->getRelation($this->_relationName);
            
            if($this->_relation->multiple){
                $this->_multiple = true;
            }

            if(isset($this->_args['labelTemplate'])){
                $this->_labelTemplate = $this->_args['labelTemplate'];
            }
            if(isset($this->_args['condition'])){
                $this->_condition = $this->_args['condition'];
            }

	}
	
	protected function getRelatedModel()
	{
            $relModelName = $this->_relation->modelClass;
            
            return \Yii::createObject(['class' => $relModelName]);
	}
        
        public function getRelationName()
	{
            return $this->_relationName;
	}

	public function getRelation()
	{
            return $this->_relation;
	}

	public function getFieldFilter()
	{
		 return $this->getData();
	}
        
        
        public function getData()
	{
		$model = $this->getRelatedModel();
                
		$pk = $model->getTableSchema()->primaryKey ;//get primary key info
                
		if ( is_array($pk) && count($pk) > 1)
                    throw new \yii\web\BadRequestHttpException(Module::t("Composed primary key in related record not supported"));//@todo: make nice
                else {
                    $pk = $pk[0];    
                }
		$data = array();
		if($this->_treeModeRelation){
                    $data = $this->buildTree();
		}else{
                    $repr = PrivateModelAdmin::getReprNameOfObject($model);
                    $rows = $model->find()->all();
                    foreach($rows as $row)
                    {
                        if(method_exists($row, 'getAdminReprString'))
                        {
                            $data[$row->{$pk}] = $row->getAdminReprString();
                        }
                        elseif(isset($row->adminnRepresentation))
                        {
                            $data[$row->{$pk}] = $row->adminnRepresentation;
                        }
                        else{
                            $data[$row->{$pk}] = $row->{$repr};
                        }
                        
                    }
			
                    
		}
                
                return $data;
	}

	/**
	 * В случае отношения MANY_MANY отключаем фильтрацию по полю и задаем тип данных "raw" что бы отображались &lt;br/&gt;
	 * @return array 
	 */
	public function asGridColumn($filter = true)
	{
		$result = parent::asGridColumn($filter);
                $result['class'] = 'app\modules\privatepanel\components\CustomDataColumn';
                /*foreach($this->_relation->link as $a=>$b)
                    $result['attribute'] = $b;*/
		return $result;
                
	}
	
	/**
	 * В случае отношения MANY_MANY задаем тип данных "raw" что бы отображались &lt;br/&gt;
	 * @return array 
	 */
	public function asViewAttribute()
	{
		$buffer = (string)$this;
		$opts = $this->_admin->extendRelations();
		$urlPrefix = $this->_admin->urlPrefix;
		
		if(!$this->_multiple){
                    	if($this->_model->{$this->_relationName} !== null && isset($opts[$this->_relationName])){
				$opt = $opts[$this->_relationName];
                                if($opt['controller']){
					$url = Url::to([ArrayHelper::merge($opt['controller'].'/view', $this->_model->{$this->_relationName}->primaryKey())]);
					$buffer = '<a href="'.$url.'">'.$buffer.'</a>';
				}else{
					
				}
			}
                        
		} elseif($this->_multiple && !$this->_treeModeRelation && isset($opts[$this->_relationName]) ){
			$opt = $opts[$this->_relationName];
			$buffer = array();
			foreach($this->_model->{$this->_relationName} as $relModel){
                                $fieldName = PrivateModelAdmin::getReprNameOfObject($relModel);
                                $str = $relModel->{$fieldName};
                                if($opt['controller']){
					$url = Url::to([ArrayHelper::merge($opt['controller'].'/view', $relModel->primaryKey())]);
                                        $buffer[] = '<a href="'.$url.'">'.$str.'</a>';
				} else {
					$buffer[] = $str;
				}
			}
			$buffer = implode($this->_multipleBr, $buffer);
		} elseif($this->_multiple && $this->_treeModeRelation && isset($opts[$this->_relationName])) {
			$opt = $opts[$this->_relationName];
			$buffer = array();
			foreach($this->_model->{$this->_relationName} as $relModel){
				$fieldName = PrivateModelAdmin::getReprNameOfObject($relModel);
                                $str = $relModel->{$fieldName};
				
                                $url = Url::to([ArrayHelper::merge($opt['controller'].'/view', $relModel->primaryKey())]);
				
                                $childRelModel = $relModel;
				while($childRelModel->{$this->_treeParentRel} !== null){
					$childRelModel = $childRelModel->{$this->_treeParentRel};
					$str = $childRelModel->{$fieldName}.$this->_lvlSeparator.$str;
				}
				if($opts['controller']){
					$buffer[] = '<a href="'.$url.'">'.$str.'</a>';
				}else{
					$buffer[] = $str;
				}
			}
			$buffer = implode($this->_multipleBr, $buffer);
		}
                
		return array(
                        'label' => $this->_model->getAttributeLabel($this->_field),
			'name' => $this->_field,
			'format' => 'html',
			'value' => $buffer,
		);
	}
	
	/**
	 * Получение текстового представления поля, для отображения в GridView и DetailView
	 * @return string 
	 */
	public function __toString()
	{
                if($this->_treeModeRelation && $this->_multiple){
                        return (string)$this->renderTreeModeMultipleGridView();
                }
                if($this->_treeModeRelation){
                        return (string)$this->renderTreeModeGridView();
                }
                if($this->_multiple){
                        return (string)$this->renderMultipleGridView();
                }
                return (string)$this->renderSimpleGridView();
		
	}
	
	protected function renderSimpleGridView()
	{
            
                $model = $this->_model->{$this->_relationName};
                if(method_exists($model, 'getAdminReprString'))
                {
                    return $model->getAdminReprString();
                }
                if(isset($model->adminnRepresentation))
                {
                    return $model->adminnRepresentstion;
                }
                if($model === null) return '';
                
                $fieldName = PrivateModelAdmin::getReprNameOfObject($model);
                return (isset($model->{$fieldName}))?$model->{$fieldName}:'';
	}

	/**
	 * результат для отношения MANY_MANY
	 * @return string 
	 */
	protected function renderMultipleGridView($returnAsArray = false)
	{
            $values = array();
            if(isset($this->_model->{$this->_relationName}))
            {
                foreach($this->_model->{$this->_relationName} as $model)
                {
                    if(!is_object($model)) 
                    {
                        $modelClass = $this->_relation->modelClass;
                        $model = $modelClass::findOne($model);
                    }
                    if($model !== null)
                    {
                        $fieldName = PrivateModelAdmin::getReprNameOfObject($model);
                        $str = $model->{$fieldName};
                        $values[] = $str;

                    }
                }
            }
            if($returnAsArray){
                    return $values;
            }
            return implode($this->_multipleBr, $values);
	}
        
        protected function renderTreeModeMultipleGridView()
	{
            $values = array();
            foreach($this->_model->{$this->_relationName} as $model){
                if(!is_object($model)) 
                {
                    $modelClass = $this->_relation->modelClass;
                    $model = $modelClass::findOne($model);
                }
                if($model !== null){
                        $fieldName = PrivateModelAdmin::getReprNameOfObject($model);
                        $str = $model->{$fieldName};
                        // почти рекурсия :)
                        $childRelModel = $model;
                        while($childRelModel->{$this->_treeParentRel} !== null){
                                $childRelModel = $childRelModel->{$this->_treeParentRel};
                                $str = $childRelModel->{$fieldName}.$this->_lvlSeparator.$str;
                        }
                        $values[] = $str;
                }
            }
            return implode($this->_multipleBr, $values);
		
	}

	/**
	 * Получение отображения для древовидного режима.
	 * Рекурсивно поднимается вверх по дереву и выводим имя из модели
	 * @return string 
	 */
	protected function renderTreeModeGridView()
	{
            $model = $this->_model->{$this->_relationName};
            if($model === null) return '';
            
            $fieldName = PrivateModelAdmin::getReprNameOfObject($model);
            $str = $model->{$fieldName};
            $childRelModel = $model;
            while($childRelModel->{$this->_treeParentRel} !== null){
                    $childRelModel = $childRelModel->{$this->_treeParentRel};
                    $str = $childRelModel->{$fieldName}.$this->_lvlSeparator.$str;
            }
            return $str;
	}
	
	/*
	 * Получение HTML для формы
	 * В зависимости от настроек поля вызывает нужный метод рендеринга 
	 * @param CActiveForm $activeForm
	 * @param array $htmlOptions
	 * @return void 
	 */
	protected function renderInput($activeForm, $htmlOptions = array())
	{
		/*if ($this->_disableOnEdit && !$this->_model->isNewRecord) {
			if($this->_treeModeRelation){
				return $this->renderTreeModeGridView();
			}
			if($this->_multiple){
				return $this->renderMultipleGridView();
			}
			return $this->renderSimpleGridView();
		}*/
		if($this->_treeModeRelation && $this->_multiple){
			return $this->renderTreeMultipleDropList($activeForm, $htmlOptions);
		}
		if($this->_treeModeRelation){
			return $this->renderTreeDropList($activeForm, $htmlOptions);
		}
		if($this->_multiple){
			return $this->renderMultipleDropList($activeForm, $htmlOptions);
		}
		return $this->renderSimpleDropList($activeForm, $htmlOptions);
	}

	/**
	 * Возвращает простой комбобокс
	 * @param CActiveForm $activeForm
	 * @param array $htmlOptions
	 * @return string 
	 */
	protected function renderSimpleDropList($activeForm, $htmlOptions)
	{
		!isset($htmlOptions['unselect']) && $htmlOptions['unselect'] = '';
                $data = $this->getData();
                
                if(!$this->_model->isAttributeRequired($this->_field))
                {
                    $data = \yii\helpers\ArrayHelper::merge(['0'=>' --- '], $data);
                }
                
		return $activeForm->field($this->_model, $this->_field, ['template'=>'{input}', 'options' => ['class' => '']])->dropDownList($data, $htmlOptions);
	}
	
	/**
	 * Возвращает мульти-комбобокс
	 * @param CActiveForm $activeForm
	 * @param array $htmlOptions
	 * @return string 
	 */
	protected function renderMultipleDropList($activeForm, $htmlOptions)
	{
		//Note: для того что бы можно было снять выделение и сохранить результат, нужно добавлять скрытое поле с именем ModelClass[relationName][] и с пустым значением
		//Но если поле обязательное и ничего не выделено, то ошибки не будет, поэтому hidden-поле выводим только для не обязательны полей
		$htmlOptions['multiple'] = 'multiple';
		$text = '';
                if(!$this->_model->isAttributeRequired($this->_field))
                {
                    $text .= $activeForm->field($this->_model, $this->_field.'[]', ['template'=>'{input}', 'options' => ['class' => '']])->hiddenInput($htmlOptions);
                }
                $text .= $activeForm->field($this->_model, $this->_field, ['template'=>'{input}', 'options' => ['class' => 'padleftright0 col-xs-12 col-sm-12 col-md-12 col-lg-7']])->dropDownList($this->getFieldFilter(), $htmlOptions);
                return $text;
	}
	
	protected $_treeChildRelField = 'id_parent';

	/**
	 * Вывод древовитного комбобокса для одиночного режима и multiple
	 * @param CActiveForm $activeForm
	 * @param array $htmlOptions
	 * @return string 
	 */
	protected function renderTreeDropList($activeForm, $htmlOptions)
	{
            	!isset($htmlOptions['empty']) && $htmlOptions['empty'] = '';
		return $activeForm->field($this->_model, $this->_field, ['template'=>'{input}', 'options' => ['class' => '']])->dropDownList($this->buildTree(), $htmlOptions);
		
	}
        
        protected function renderTreeMultipleDropList($activeForm, $htmlOptions)
	{
		$htmlOptions['multiple'] = 'multiple';
                $text = '';
                if(!$this->_model->isAttributeRequired($this->_field))
                {
                    $text .= $activeForm->field($this->_model, $this->_field.'[]', ['template'=>'{input}', 'options' => ['class' => '']])->hiddenInput($htmlOptions);
                }
                $text .= $activeForm->field($this->_model, $this->_field, ['template'=>'{input}', 'options' => ['class' => '']])->dropDownList($this->buildTree(), $htmlOptions);		
                return $text;
	}
	
	/**
	 * Разделитель уровней в дереве
	 * @example категория1->категория1.1->категория1.1.1
	 * @var string 
	 */
	protected $_lvlSeparator = ' -> ';

	/**
	 * "Строит" дерево и возвращает массив key=>value для комбобоксов
	 * @see CHtml::listData
	 * @param array $parentModel
	 * @param integer $lvl
	 * @param string $nodePrefix
	 * @return array 
	 */
	protected function buildTree($parentModel = null, $lvl = 0, $nodePrefix = '')
	{
            static $objects = [];
            
            if(!count($objects))
            {
                $className = $this->_relation->modelClass;
                $objects = $className::find()->andWhere($this->_relation->where, $this->_relation->params);
                
                $objects = $objects->all();
            }
            $relModel = \Yii::createObject(['class'=>$this->_relation->modelClass]);
            
            $className = $this->_relation->modelClass;
            
            if($parentModel === null){
                $rel = $relModel->getRelation($this->_treeParentRel);
                if($rel)
                {
                    if(!$this->_relation->multiple)
                    {
                        /*$models = $className::find()->andWhere($this->_relation->where, $this->_relation->params);
                        foreach($rel->link as $a=>$b)
                            $models->andWhere($b.'=0 OR '.$b.' IS NULL');
                        $models = $models->all();*/
                        $models = [];
                        foreach($objects as $object)
                        {
                            $found = true;
                            foreach($rel->link as $a=>$b)
                            {
                                if($object->$b == 0 || $object->$b === null)
                                {
                                    
                                } else {
                                    $found = false;
                                }
                            }
                            if($found)
                            {
                                $models[] = $object;
                            }
                        }
                    }
                } else {
                    //$models = $className::find()->andWhere($this->_relation->where, $this->_relation->params)->all();
                    $models = $objects;
                }

            } else {
                /*$models = $parentModel->{$this->_treeChildRel};
                if(is_object($models)){
                        $models = array($models);
                }
                
                if($models === null){
                        $models = array();
                }*/
                $models = [];
                $rel = $relModel->getRelation($this->_treeChildRel);
                if($rel)
                {
                    foreach($objects as $object)
                    {
                        $found = true;
                        foreach($rel->link as $a=>$b)
                        {
                            if($object->$a == $parentModel->$b)
                            {

                            } else {
                                $found = false;
                            }
                        }
                        if($found)
                        {
                            $models[] = $object;
                        }
                    }
                }
            }
            
            $result = array();

            if(!$this->_model->isAttributeRequired($this->_field) && $lvl == 0)
            {
                $f = false;
                $validators = $this->_model->getValidators();
                foreach($validators as $validator)
                {
                    if(in_array($this->_field, $validator->attributes))
                    {
                        if($validator instanceof NumberValidator)
                        {
                            if(!isset($result[0]))
                                $result[0] = ' --- ';
                            $f = true;
                        }
                    }
                }
                if(!$f)
                    $result = array_merge(['0'=>' --- '], $result);
            }
            foreach($models as $model){
                $nodeName = $nodePrefix.$this->_admin->getReprOfObject($model);
                $childNodes = $this->buildTree($model, $lvl+1, $nodeName.$this->_lvlSeparator);

                if($this->_treeModeOnlyLeaf == false){
                        $result[$model->primaryKey] = $nodeName;
                } else {
                    if(sizeof($childNodes) == 0){
                            $result[$model->primaryKey] = $nodeName;
                    }
                }
                $result = ArrayHelper::merge($result, $childNodes);
            }
            return $result;
	}
        public function afterSave($data)
        {
            if(isset($data[$this->_field]))
            {
                if($this->_relation->via instanceof \yii\db\ActiveQuery)
                {
                    $this->_model->unlinkAll($this->_field, true);
                    
                    $className = $this->_relation->modelClass;
                    if(!is_array($data[$this->_field]))
                    {
                        if($data[$this->_field])
                            $data[$this->_field] = array($data[$this->_field]);
                        else
                            $data[$this->_field] = array();
                    }
                    foreach($data[$this->_field] as $pk)
                    {
                        $find = $className::find()->limit(1);
                        foreach ($this->_relation->link as $a => $b) {
                            $find->andWhere($a.' = :pk', ['pk' => $pk]);
                        }
                        $relModel = $find->one();
                        
                        $this->_model->link($this->_field, $relModel);
                    }
                }
            }
        }
}
