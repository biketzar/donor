<?php

namespace app\modules\privatepanel\components\fields;

use app\modules\privatepanel\components\fields\RDbBase;
use app\modules\privatepanel\Module;

class RDbBoolean extends RDbBase
{

	public function __toString()
	{
		return $this->_model->{$this->_field} ? Module::t('app', 'yes') : Module::t('app', 'no');
	}

	public function getFieldFilter()
	{
            return array('0' => Module::t('app', 'no'), '1' => Module::t('app', 'yes'));
	}


	protected function renderInput($activeForm, $htmlOptions = array())
	{
            	//return $activeForm->checkBox($this->_model, $this->_field,$htmlOptions);
            return $activeForm->field($this->_model, $this->_field, ['template'=>'{input}', 'options' => ['class' => '']])->checkbox($htmlOptions, false);
	}
        public function getFilterWhere($value)
        {
            return [$this->_model->tableName().'.'.$this->_field => $value];
        }
}