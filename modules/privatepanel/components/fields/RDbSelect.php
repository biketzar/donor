<?php
namespace app\modules\privatepanel\components\fields;

use app\modules\privatepanel\components\fields\RDbBase;
use yii\validators\NumberValidator;

class RDbSelect extends RDbBase
{
    
    protected $data = array();
    protected $dataFilter = array();
    
	//protected $_disableOnEdit = false;
	
    public function __construct( $model, $field, $args = array() )
    {
        parent::__construct($model, $field, $args);
        
        $this->data = $args['data'];
        $this->dataFilter = $args['data'];
        
        if(!$this->_model->isAttributeRequired($this->_field))
        {
            $f = false;
            $validators = $this->_model->getValidators();
            foreach($validators as $validator)
            {
                if(in_array($this->_field, $validator->attributes))
                {
                    if($validator instanceof NumberValidator)
                    {
                        if(!isset($this->data[0]))
                            $this->data[0] = '';
                        $f = true;
                    }
                }
            }
            if(!$f)
                $this->data = \yii\helpers\ArrayHelper::merge([''=>' --- '], $this->data);
        }
            /*if(isset($this->_args['disableOnEdit'])){
                    $this->_disableOnEdit = $this->_args['disableOnEdit'];
            }*/
    }
	
    public function getFieldFilter()
    {
        return $this->dataFilter;
    }
	
    public function __toString()
    {
        $fieldName = $this->_field;
        if(strpos($this->_field ,'[') !== false)
        {
            $fieldName = substr($this->_field, 0,strpos($this->_field ,'[') );
        }
        if(isset($this->_args['htmlOptions']) && isset($this->_args['htmlOptions']['multiple']) || is_array($this->_model->{$fieldName}))
        {
            $result = array();
            foreach($this->_model->{$fieldName} as $v)
            {
                if(isset($this->data[$v]))
                    $result[] = $this->data[$v];
                else
                    $result[] = $v;
            }
            
            return implode('<br/>', $result);
        } else {
            return (isset($this->data[$this->_model->{$this->_field}]))
                    ? (string) $this->data[$this->_model->{$this->_field}]
                    : (string) $this->_model->{$this->_field};
        }
    }
    public function asGridColumn($filter = true)
    {
            return array(
                'class' => 'app\modules\privatepanel\components\CustomDataColumn',
                'format'=>(isset($this->_args['htmlOptions']) && isset($this->_args['htmlOptions']['multiple']))?'raw':'text',
                //'attribute' => $this->_field,
                'filter' => ($filter ? $this->getFieldFilter() : false),
                'attribute' => $this->_field,
                'filterInputOptions' => ['class'=>'', 'prompt' => ' --- '],
                'value' => function ($data) {
                    $admin = \Yii::createObject(['class' => 'app\modules\privatepanel\components\PrivateModelAdmin'],[$data]);
                    return (string)$admin->getField($this->_field);
                },
            );

    }

    protected function renderInput($activeForm, $htmlOptions = array())
    {
            /*if ($this->_disableOnEdit && !$this->_model->isNewRecord) {
                    return $this->__toString();
            }*/
        //$htmlOptions = array_merge($htmlOptions, $this->htmlOptions);
        !isset($htmlOptions['empty']) && $htmlOptions['empty'] = '';
        return $activeForm->field($this->_model, $this->_field, ['template'=>'{input}', 'options' => ['class' => 'padleftright0 col-xs-12 col-sm-12 col-md-12 col-lg-7']])->dropDownList($this->data, $htmlOptions);
    }
    
    public function prepareForSave($data, $check = false)
    {
        if(isset($this->_args['htmlOptions']) && isset($this->_args['htmlOptions']['multiple']) && !$check)
        {
            $data[$this->_field] = json_encode($data[$this->_field]);
        } elseif(isset($this->_args['htmlOptions']) && isset($this->_args['htmlOptions']['multiple']) && $check)
        {
            $data[$this->_field] = $data[$this->_field];
        } elseif(isset($data[$this->_field]))
        {
            $data[$this->_field] = $data[$this->_field];
        }
        
        return $data;
    }
    
    public function getFilterWhere($value)
    {
        if(isset($this->_args['htmlOptions']) && isset($this->_args['htmlOptions']['multiple']))
        {
            return ['like', $this->_model->tableName().'.'.$this->_field, '"'.$value.'"'];
        } else {
            return parent::getFilterWhere($value);
        }
    }

}