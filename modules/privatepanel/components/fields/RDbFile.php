<?php
namespace app\modules\privatepanel\components\fields;

use app\modules\privatepanel\components\fields\RDbBase;
use app\modules\privatepanel\Module;

use yii\validators\Validator;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\validators\ImageValidator;
use app\components\CustomImageValidator;
use app\components\SiteHelper;
//use app\models\UploadForm;
use yii\web\UploadedFile;

class RDbFile extends RDbBase
{
        protected $maxUpload = null;
        public function __construct($model, $field, $args = array()) {
            parent::__construct($model, $field, $args);
            $validator_exists = false;
            /*for($k = 0; $k < count($this->_model->getValidators()); $k++)
            {
                if(!$this->_model->getValidators()->offsetExists($k))
                    continue;
                $validator = $this->_model->getValidators()->offsetGet($k);
                if(in_array($this->_field.'_attr', $validator->attributes))
                {
                    $validator_exists = true;
                    break;
                }
            }*/
            $iterator = $this->_model->getValidators()->getIterator();

            while($iterator->valid()) {
                
                $validator = $iterator->current();
                if(in_array($this->_field.'_attr', $validator->attributes))
                {
                    $validator_exists = true;
                    break;
                }

                $iterator->next();
            }
            if(!$validator_exists)
            {
                $validator = Validator::createValidator( 'safe', $this->_model, $this->_field.'_attr');
                $this->_model->getValidators()->append($validator);
            }
            if($this->maxUpload === null){
                $this->maxUpload = min((int) str_replace("M", "", ini_get('post_max_size')), (int) str_replace("M", "", ini_get('upload_max_filesize')));
            }
        }
        public function getFilesDirectory()
        {
            $func = 'get'.ucfirst($this->_field).'Directory';
            if(method_exists($this->_model, $func))
                    $path = \Yii::getAlias ('@webroot').'/'.$this->_model->$func();
            elseif(method_exists($this->_model, 'getFilesDirectory'))
                    $path = \Yii::getAlias ('@webroot').'/'.$this->_model->getFilesDirectory();
            /*elseif (\Yii::getAlias ('@files'))
                    $path = \Yii::getAlias ('@files');*/
            else 
                $path = \Yii::getAlias ('@webroot2');

            if(substr($path, -1) != '/')
                $path .= '/';
            return $path;
        }
        public function getFilesDirectoryURL()
        {
            $func = 'get'.ucfirst($this->_field).'Directory';
            if(method_exists($this->_model, $func))
                    $path = $this->_model->$func();
            elseif(method_exists($this->_model, 'getFilesDirectory'))
                    $path = $this->_model->getFilesDirectory();
            /*elseif (\Yii::getAlias ('@filesweb'))
                    $path = \Yii::getAlias ('@filesweb');*/
            else 
                $path = '';
            if(substr($path, -1) != '/')
                $path .= '/';

            return $path;
        }
        
        public function getFilePath()
        {
            if($this->_model->{$this->_field})
            {
                $dir = $this->getFilesDirectory();
                return $dir.$this->_model->{$this->_field};
            } else {
                return false;
            }
        }
        public function getFilePathURL()
        {
            if($this->_model->{$this->_field})
            {
                $dir = $this->getFilesDirectoryURL();
                if(substr($dir, -1) != '/')
                        $dir .= '/';
                return $dir.$this->_model->{$this->_field};
            } else {
                return false;
            }
        }
        
	protected function renderInput($activeForm, $htmlOptions = array())
	{
            $text = '';
            if($this->_model->{$this->_field})
            {
                $f = false;
                $validators = $this->_model->getValidators();
                foreach($validators as $validator)
                {
                    if(in_array($this->_field, $validator->attributes))
                    {
                        if($validator instanceof ImageValidator)
                        {
                            $text .= Html::a(Html::img(\app\components\ImageHelper::thumbnail($this->getFilePathURL(), 150, 150)), $this->getFilePathURL(), array('target' => '_blank'));
                            $f = true;
                        }
                    }
                }
                if(!$f)
                {
                    $text .= Html::a($this->_model->{$this->_field}, $this->getFilePathURL(), array('target' => '_blank'));
                }
            }
            $text .= $activeForm->field($this->_model, $this->_field, ['template'=>'{input}', 'options' => ['class' => '']])->fileInput($htmlOptions);
            $inputName = Html::getInputName($this->_model, ''.$this->_field.'_attr');
            $text .= Html::input('hidden', $inputName.'[file]', 1, array());
            $text .= '<br />'.Module::t('app', 'Max allowed size').' '.$this->maxUpload.' MB';
            if($this->_model->{$this->_field})
            {
                $text .= '<br />'.Html::label(Module::t('app', 'Delete file').' '.Html::input('checkbox', $inputName.'[del]', 0, array()));
            }
            $validators = $this->_model->getValidators();
            foreach($validators as $validator)
            {
                if(in_array($this->_field, $validator->attributes))
                {
                    if($validator instanceof CustomImageValidator)
                    {
                        if($validator->watermark)
                            $text .= '<br />'.Html::label(Module::t('app', 'Add watermark').' '.Html::input('checkbox', $inputName.'[watermark]', 0, array()));
                        //$text .= Html::input('checkbox', $inputName.'[watermark]', 0, array());
                    }
                }
            }
            return $text;
	}
	
	public function getFieldFilter() {
		return false;
	}
	
	public function asGridColumn($filter = true)
	{
                return array(
                    'class' => 'app\modules\privatepanel\components\CustomDataColumn',
                    //'attribute' => $this->_field,
                    'format'=>'raw',
                    'attribute' => $this->_field,
                    'filterInputOptions' => ['class'=>''],
                    'value' => function ($data) {
                        $admin = \Yii::createObject(['class' => 'app\modules\privatepanel\components\PrivateModelAdmin'],[$data]);
                        return (string)$admin->getField($this->_field)->asGridValue();
                    },
		);

	}
	
	public function asGridValue()
	{

                $buffer = '';
                
		if($this->_model->{$this->_field})
                {
                    $f = false;
                    $validators = $this->_model->getValidators();
                    foreach($validators as $validator)
                    {
                        if(in_array($this->_field, $validator->attributes))
                        {
                            if($validator instanceof ImageValidator)
                            {
                                $buffer .= Html::a(Html::img(\app\components\ImageHelper::thumbnail($this->getFilePathURL(), 150, 150)), $this->getFilePathURL(), array('target' => '_blank'));
                                $f = true;
                            }
                        }
                    }
                    if(!$f)
                    {
                        $buffer .= Html::a($this->_model->{$this->_field}, '/'.$this->getFilePathURL(), array('target' => '_blank'));
                    }
                }
		return $buffer;
	}

	public function asViewAttribute() 
	{


		return array(
                        'label' => $this->_model->getAttributeLabel($this->_field),
			'name' => $this->_field,
			'format' => 'html',
			'value' => $this->asGridValue(),
		);
	}
        
        public function prepareForSave($data, $check = false)
        {
            if(isset($data[$this->_field]) && !$check)
            {
                
                /*$file = UploadedFile::getInstance($this->_model, $this->_field);
                
                if($file)
                {
                    $path = $this->getFilesDirectory();
                    
                    if(!is_dir($path))
                    {
                        $path_parts = str_replace(\Yii::getAlias('@webroot').'/', '', $path);
                        $path_parts = explode('/',$path_parts);

                        $path_temp = '';
                        for($i = 0; $i<count($path_parts); $i++)
                        {
                            $path_temp.= $path_parts[$i].'/';
                            if(!file_exists(\Yii::getAlias('@webroot').'/'.$path_temp))
                            {
                                mkdir(\Yii::getAlias('@webroot').'/'.$path_temp, 0755);
                            }
                        }
                    }
                    
                    $filename = $this->translit($file->baseName);
                
                    $k = 1;

                    while(file_exists($path.'/'.$filename. '.' . $file->extension))
                    {
                        $filename = $this->translit(trim($file->baseName)).'-'.($k++);
                    }
                    $file->saveAs($this->getFilesDirectory().$filename . '.' . $file->extension);

                    $data[$this->_field] = $file->baseName . '.' . $file->extension;
                } else {
                    if(isset($data[$this->_field]) && $data[$this->_field])
                        $data[$this->_field] = $data[$this->_field];
                    elseif($this->_model->getOldAttribute($this->_field))
                        $data[$this->_field] = $this->_model->getOldAttribute($this->_field);
                    else
                        $data[$this->_field] = '';
                }
                if(isset($data[$this->_field.'_attr']['watermark']) && $data[$this->_field])
                {
                    $watermark = \Yii::$app->constants->get('watermark');
                    if($watermark)
                    {
                        \app\components\ImageHelper::watermark($this->getFilesDirectory().$data[$this->_field], 
                                \Yii::$app->constants->get('watermark'),
                                $this->getFilesDirectory().$data[$this->_field]
                                );
                    }
                }*/
                if(isset($data[$this->_field]) && $data[$this->_field])
                    $data[$this->_field] = $data[$this->_field];
                elseif($this->_model->getOldAttribute($this->_field))
                    $data[$this->_field] = $this->_model->getOldAttribute($this->_field);
                else
                    $data[$this->_field] = '';
                if(isset($data[$this->_field.'_attr']['del']))
                    $data[$this->_field] = '';
                
            }
            
            if(isset($data[$this->_field.'_attr']))
                unset($data[$this->_field.'_attr']);
            
            $iterator = $this->_model->getValidators()->getIterator();

            while($iterator->valid()) {
                
                $validator = $iterator->current();
                if(in_array($this->_field.'_attr', $validator->attributes))
                {
                    $newattributes = array();
                    foreach($validator->attributes as $attr)
                    {
                        if($attr != $this->_field.'_attr')
                              $newattributes[] = $attr;  
                    }
                    
                    if(!count($newattributes))
                    {
                        $this->_model->getValidators()->offsetUnset($iterator->key());
                        $iterator->rewind();
                    } else {
                        $validator->attributes = $newattributes;
                    }
                    
                }

                $iterator->next();
            }
            /*$count = count($this->_model->getValidators());
            for($k = 0; $k < $count; $k++)
            {
                if(!$this->_model->getValidators()->offsetExists($k))
                    continue;
                $validator = $this->_model->getValidators()->offsetGet($k);
                
                if(in_array($this->_field.'_attr', $validator->attributes))
                {
                    var_dump(in_array($this->_field.'_attr', $validator->attributes));
                    $newattributes = array();
                    foreach($validator->attributes as $attr)
                    {
                        if($attr != $this->_field.'_attr')
                              $newattributes[] = $attr;  
                    }
                    
                    if(!count($newattributes))
                    {
                        $this->_model->getValidators()->offsetUnset($k);
                        var_dump($k);
                        $k--;
                    } else {
                        $validator->attributes = $newattributes;
                    }
                    
                }
            }*/
            
            return $data;
        }
        public function afterSave($data)
        {
                    
            $file = UploadedFile::getInstance($this->_model, $this->_field);
            
            $path = $this->getFilesDirectory();
			
            $path = $path.((substr($path,-1) != '/')?'/':'');
            
            //$path = str_replace(\Yii::getAlias('@web2'), \Yii::getAlias('@webroot2'), $path);

            if($file)
            {
                SiteHelper::checkFolderPath($path);
                /*if(!is_dir($path))
                {

                    $path_parts = str_replace(\Yii::getAlias('@webroot').'', '', $path);

                    $path_parts = explode('/',$path_parts);
                    $path_temp = '';
                    for($i = 0; $i<count($path_parts); $i++)
                    {
                        $path_temp.= $path_parts[$i].'/';
                        if(!file_exists(\Yii::getAlias('@webroot').''.$path_temp))
                        {
                            mkdir(\Yii::getAlias('@webroot').''.$path_temp, 0755);
                        }
                    }
                }*/

                $filename = SiteHelper::translit($file->baseName);

                $k = 1;
                
                while(file_exists($path.$filename. '.' . $file->extension))
                {
                    $filename = SiteHelper::translit(trim($file->baseName)).'-'.($k++);
                }
                $file->saveAs($path.$filename . '.' . $file->extension);

                $data[$this->_field] = $filename . '.' . $file->extension;
            } else {
                if(isset($data[$this->_field]) && $data[$this->_field])
                    $data[$this->_field] = $data[$this->_field];
                elseif($this->_model->getOldAttribute($this->_field))
                    $data[$this->_field] = $this->_model->getOldAttribute($this->_field);
                else
                    $data[$this->_field] = '';
            }
            if(isset($data[$this->_field.'_attr']['watermark']) && $data[$this->_field])
            {
                $watermark = \Yii::$app->constants->get('watermark');
                if($watermark)
                {
                    \app\components\ImageHelper::watermark($this->getFilesDirectory().$data[$this->_field], 
                            \Yii::$app->constants->get('watermark'),
                            $path.$data[$this->_field]
                            );
                }
            }
            
            if(isset($data[$this->_field.'_attr']['del']))
                $data[$this->_field] = '';
            $this->_model->{$this->_field} = $data[$this->_field];
             $this->_model->update(false, [$this->_field]);
        }
        public function translit($str)
	{
		$tr = array(
			"А"=>"A","Б"=>"B","В"=>"V","Г"=>"G",
			"Д"=>"D","Е"=>"E","Ж"=>"J","З"=>"Z","И"=>"I",
			"Й"=>"Y","К"=>"K","Л"=>"L","М"=>"M","Н"=>"N",
			"О"=>"O","П"=>"P","Р"=>"R","С"=>"S","Т"=>"T",
			"У"=>"U","Ф"=>"F","Х"=>"H","Ц"=>"TS","Ч"=>"CH",
			"Ш"=>"SH","Щ"=>"SCH","Ъ"=>"","Ы"=>"YI","Ь"=>"",
			"Э"=>"E","Ю"=>"YU","Я"=>"YA","а"=>"a","б"=>"b",
			"в"=>"v","г"=>"g","д"=>"d","е"=>"e","ж"=>"j",
			"з"=>"z","и"=>"i","й"=>"y","к"=>"k","л"=>"l",
			"м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
			"с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"h",
			"ц"=>"ts","ч"=>"ch","ш"=>"sh","щ"=>"sch","ъ"=>"y",
			"ы"=>"yi","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya",
	   "."=>"-"," "=>"-","?"=>"-","/"=>"-","\\"=>"-",
	   "*"=>"-",":"=>"-","*"=>"-","\""=>"","<"=>"-",
	   ">"=>"-","|"=>"-"
		);
		return strtr($str,$tr);
	}
}
