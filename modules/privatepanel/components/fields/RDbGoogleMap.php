<?php
namespace app\modules\privatepanel\components\fields;

use app\modules\privatepanel\components\fields\RDbBase;

use yii\helpers\StringHelper;
use yii\helpers\Html;

use yii\web\JsExpression;

class RDbGoogleMap extends RDbBase
{
    protected $related_fields = [];
    
    public function __construct( $model, $field, $args = array() )
    {
        parent::__construct($model, $field, $args);
        
        if(isset($args['related_fields']))
        {
            if(!is_array($args['related_fields']))
                $args['related_fields'] = explode(',',$args['related_fields']);
            foreach($args['related_fields'] as $rf)
            {
                if(trim($rf))
                    $this->related_fields[] = $model->formName().'_'.trim($rf);
            }
        }
    }
	
	/**
	 * Формирует поле для ввода текста.
	 * Если атрибут модели имеет в БД тип text или longtext то выводится визуальный редактор. 
	 * В остальных случаях выводиться простой input. Вывод textarea можно задать вручную с помощью спец.опции.
	 */
	protected function renderInput($activeForm, $htmlOptions = array())
	{
                \app\modules\privatepanel\assets\GoogleMapAsset::register(\yii::$app->getView());
                
                $itemvalue = $this->_model->{$this->_field};
                
                $itemvalue = json_decode($itemvalue, true);
                if(!isset($itemvalue['map']))
                {
                    if(isset($itemvalue['mark']))
                        $itemvalue['map'] = $itemvalue['mark'];
                    else
                        $itemvalue['map'] = [0,0];
                }
                $centervalue = $itemvalue['map'];
                if(!$centervalue[0])
                        $centervalue[0] = 0;
                if(!isset($centervalue[1]) || !$centervalue[1])
                        $centervalue[1] = 0;
                if(!isset($itemvalue['mark']))
                    $itemvalue['mark'] = [0,0];
                $value = $itemvalue['mark'];
                if(!$value[0])
                        $value[0] = 0;
                if(!isset($value[1]) || !$value[1])
                        $value[1] = 0;
                $mapid = $this->_model->formName().'_'.$this->_field.'_map';
                $id = $this->_model->formName().'_'.$this->_field;
                
                $buffer = '';
                
                $buffer .= '<div>';
                $buffer .= Html::label(\yii::t('modules/privatepanel/app', 'Latitude')).' ';
                $buffer .= Html::textInput($this->_field.'temp[latitude]', $value[0], ['id' => $id.'_latitude']).' ';
                $buffer .= Html::label(\yii::t('modules/privatepanel/app', 'Longitude')).' ';
                $buffer .= Html::textInput($this->_field.'temp[longitude]', $value[1], ['id' => $id.'_longitude']).' ';
                $buffer .= $activeForm->field($this->_model, $this->_field, ['template'=>'{input}', 'options' => ['class' => '']])->hiddenInput(['id' => $id]);
                $buffer .= '</div>';
                $buffer .= '<div>';
                $buffer .= Html::label(\yii::t('modules/privatepanel/app', 'Search location')).' ';
                $buffer .= Html::textInput($this->_field.'temp[search]', '', ['id' => $id.'_search']).' ';
                $buffer .= Html::button(\yii::t('modules/privatepanel/app', 'Search button'), ['id' => $id.'_searchbutton']);
                $buffer .= '</div>';
                $buffer .= '<div id="'.$mapid.'" style="width:100%; height:300px;"></div>';
                $buffer .= '<script type="text/javascript">
           initGoogleMap("'.$mapid.'", "'.$id.'", "'.$id.'", '.(float)$centervalue[0].', '.(float)$centervalue[1].', '.(float)$value[0].', '.(float)$value[1].',"'.implode(',',$this->related_fields).'");
        </script>';
                $buffer .= '</div>';
                return $buffer;
	}

}