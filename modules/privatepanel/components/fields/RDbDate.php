<?php
namespace app\modules\privatepanel\components\fields;

use app\modules\privatepanel\components\fields\RDbBase;
//use yii\i18n\Formatter;
//use yii\jui\DatePicker;

class RDbDate extends RDbBase
{
	
	protected $_format = 'yyyy-MM-dd';
	protected $_inputFormat = 'yyyy-MM-dd';
	protected $_required = false;
	
	public function __construct( $model, $field, $args = array() )
	{
		parent::__construct($model, $field, $args);

		if(isset($this->_args['format'])){
			$this->_format = $this->_args['format'];
		}
                if(isset($this->_args['inputFormat'])){
			$this->_inputFormat = $this->_args['inputFormat'];
		}
                $iterator = $this->_model->getValidators()->getIterator();

                while($iterator->valid()) {
                    $validator = $iterator->current();
                    if(in_array($this->_field, $validator->attributes))
                    {
                        if($validator instanceof \yii\validators\RequiredValidator)
                        {
                            $this->_required = true;
                            break;
                        }
                    }

                    $iterator->next();
                }
	}
	
	protected function renderInput($activeForm, $htmlOptions = array())
	{
            if(!$this->_model->{$this->_field} && $this->_required)
                $this->_model->{$this->_field} = date('Y-m-d');
                return $activeForm->field($this->_model, $this->_field, ['template'=>'{input}', 'options' => ['class' => '']])->widget(\yii\jui\DatePicker::classname(), [
                    'language' => 'ru',
                    'dateFormat' => $this->_inputFormat,
                    'clientOptions' => [
                        'changeMonth' => '1',
                        'changeYear' => '1',
                        'showOn'=> "both",
                        'buttonImageOnly'=> 1,
                        'buttonImage'=> \Yii::getAlias('@web2')."/../assets/privatepanel/calendar.png",
                        //'buttonText'=> "Calendar"
                    ]
                ]);
	}
	
	public function __toString()
	{
		return \Yii::$app->formatter->asDate($this->_model->{$this->_field}, $this->_format);
	}
}