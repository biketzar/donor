<?php

/**
 * Элемент формы административной части для ввода адреса.
 * @see RDbRelation
 * @see PAddressInput
 * @author Lvov
 */
class RDbAddress extends RDbRelation
{

	/**
	 * Для отображения вызываем у модели адреса специальный метод, который собирает все поля в строчку
	 * @return string
	 */
	public function __toString()
	{
		try
		{
			$relName = $this->_args[0];
			if ( $this->_model->$relName === null || !($this->_model->$relName instanceof Address))
				return "";
			return $this->_model->$relName->getAddressStr();
		}
		catch ( Exception $e)
		{
			return "RAdmin error: ".$e->getMessage();
		}
	}

	public function asViewAttribute()
	{
		return array(
			'name' => $this->_field,
			'type' => 'raw',
			'value' => (string) $this,
		);
	}
	
	/**
	 * Отключаем фильтрацию по полю
	 * @return boolean 
	 */
	public function getFieldFilter()
	{
		return false;
	}

	/**
	 * Рендерим html для формы.
	 * HTML генериться в отдельном виджите. Это делает возможным использовать этот виджет и в клиентской части
	 * @param CActiveForm $activeForm
	 * @param array $htmlOptions
	 * @return string 
	 */
	protected function renderInput($activeForm,$htmlOptions)
	{
		return Yii::app()->controller->widget('private.widgets.PAddressInput', array(
			'model' => $this->_model,
			'attribute' => $this->_field,		
			'address' => $this->_model->{$this->_args[0]},
		), true);
	}
}