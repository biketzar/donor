<?php
/**
 * Элемент формы административной части для ввода текста. 
 * В зависимости от условий формирует простой input, textare или область с визуальным редактором.
 * Дополнительные параметры:
 * forceTextInput - принудительно вывоводит input;
 * forceTextArea - принудительно вывовдит textarea;
 * suggest - принудительно выводит input с подсказками значений;
 * escape - выводить текст в безопасном формате;
 */

namespace app\modules\privatepanel\components\fields;

use app\modules\privatepanel\components\fields\RDbBase;

use yii\helpers\StringHelper;

use dosamigos\tinymce\TinyMce;
use mihaildev\elfinder\ElFinder;
use yii\web\JsExpression;

class RDbText extends RDbBase
{
    
        protected $truncateWords = 255;
	
        public function __construct( $model, $field, $args = array() )
	{
            parent::__construct($model, $field, $args);
            if(isset($args['truncateWords']))
                $this->truncateWords = $args['truncateWords'];
	}
	protected function preRenderInput()
	{
            if(isset($this->_model->tableSchema->columns[$this->_field_origin]))
            {
		$type = $this->_model->tableSchema->columns[$this->_field_origin]->dbType;
		if(preg_match("/int|float/i", $type) && preg_match("/e/i", $this->_model->{$this->_field})){
			$this->_model->{$this->_field} = number_format($this->_model->{$this->_field},null,'.','');
			//$this->_model->{$this->_field} = sprintf("%f", $this->_model->{$this->_field});
		}
            }
	}
        
        protected function getFieldType()
        {
            if(!empty($this->_args['forceTextArea'])){
                return 'textarea';
            }
            if(!empty($this->_args['forceTextInput'])){
                return 'text';
            }
            if(isset($this->_model->tableSchema->columns[$this->_field_origin]))
            {
                if($this->_model->tableSchema->columns[$this->_field_origin]->dbType == 'text' || 
                            $this->_model->tableSchema->columns[$this->_field_origin]->dbType == 'longtext'){
                    return 'html';
                }
            }
            return 'text';
        }
	/**
	 * Формирует поле для ввода текста.
	 * Если атрибут модели имеет в БД тип text или longtext то выводится визуальный редактор. 
	 * В остальных случаях выводиться простой input. Вывод textarea можно задать вручную с помощью спец.опции.
	 */
	protected function renderInput($activeForm, $htmlOptions = array())
	{
		
		$this->preRenderInput();
		
                $type = $this->getFieldType();
                
		isset($htmlOptions['size']) || $htmlOptions['size'] = 80;
		if($type == 'text'){
			return $activeForm->field($this->_model, $this->_field, ['template'=>'{input}', 'options' => ['class' => '']])->textInput($htmlOptions);
		}
		if($type == 'textarea'){
			$htmlOptions['cols'] = $htmlOptions['size'];
                        return $activeForm->field($this->_model, $this->_field, ['template'=>'{input}', 'options' => ['class' => '']])->textarea($htmlOptions);
		}
		/*if(!empty($this->_args['suggest'])){
			!isset($this->_args['param']) && $this->_args['param'] = array();
			!isset($this->_args['param']['field']) && $this->_args['param']['field'] = $this->_field;
			!isset($this->_args['url']) && $this->_args['url'] = Yii::app()->getController()->createUrl('suggestField', $this->_args['param']);
			
			return Yii::app()->controller->widget('zii.widgets.jui.CJuiAutoComplete', array(
				'model'=>$this->_model,
				'attribute'=>$this->_field,
				'sourceUrl'=>$this->_args['url'],
				// additional javascript options for the autocomplete plugin
				'options'=>array(
					'minLength'=>'1', //The minimum number of characters a user has to type before the Autocomplete activates
					'delay'=>'300', //The delay in milliseconds the Autocomplete waits after a keystroke to activate itself
				),
				'htmlOptions'=>array(
					//'style'=>'height:20px;',
					'size' => $htmlOptions['size'],
				),
			), true).'&nbsp;...';
		}*/
		
		if($type == 'html'){
			
			/*return Yii::app()->controller->widget('private.widgets.PTinyMce', array(
				'model'=>$this->_model,
				'attribute'=>$this->_field, //Model attribute name. Nome do atributo do modelo.
				'options'=>array(),
			), true);*/
                        
                        return $activeForm->field($this->_model, $this->_field, ['template'=>'{input}'])->widget(TinyMce::className(), [
                            'options' => ['rows' => 8],
                            'language' => 'ru',
                            'clientOptions' => [
                                /*'plugins' => [
                                    "advlist autolink lists link charmap print preview anchor",
                                    "searchreplace visualblocks code fullscreen",
                                    "insertdatetime media table contextmenu paste"
                                ],*/
                                'content_css' => isset(\Yii::$app->params['projectSettings']['css_for_visual_editor']) ? \Yii::$app->params['projectSettings']['css_for_visual_editor']['value'] : \Yii::getAlias('@web2').'/css/template_editor.css',
                                'image_advtab' => new JsExpression('true'),
                                'image_description'=> new JsExpression('true'),
                                'image_title'=> new JsExpression('true'),
                                'image_dimensions'=> new JsExpression('true'),
                                'plugins' => [
                                    "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                                    "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                                    "noindex save table contextmenu directionality emoticons template paste textcolor importcss"
                                ],
                                'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
                                'toolbar1' => "newdocument fullpage | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect",
                                'toolbar2' => "cut copy paste | searchreplace | bullist numlist | outdent indent blockquote | undo redo | link unlink anchor image media code | inserttime preview | forecolor backcolor",
                                'toolbar3' => "table | hr removeformat | subscript superscript | charmap emoticons | print fullscreen | ltr rtl | spellchecker | visualchars visualblocks nonbreaking template pagebreak restoredraft",
                                'relative_urls' => new JsExpression('false'),
                                'convert_urls' => new JsExpression('false'),
                                'remove_script_host' => new JsExpression('true'),
                                'image_advtab' => new JsExpression('true'),
                                'custom_elements' => 'noindex',
                                'extended_valid_elements' => 'noindex',
                                'file_browser_callback' => new JsExpression('function(field_name, url, type, win) { 
                                    
                                    var filemanager_url = {
                                        image: \''.ElFinder::getManagerUrl('elfinder', ['callback' => 'tinymce_callback' ,'filter' => 'image']).'\',
                                        media: \''.ElFinder::getManagerUrl('elfinder', ['callback' => 'tinymce_callback' ,'filter' => 'video']).'\',
                                        file: \''.ElFinder::getManagerUrl('elfinder', ['callback' => 'tinymce_callback' ,'filter' => ['image', 'video', 'application', 'text/plain']]).'\',
                                        common: \''.ElFinder::getManagerUrl('elfinder', ['callback' => 'tinymce_callback' ,'filter' => ['image', 'video', 'application', 'text/plain']]).'\',
                                    }
                                    if(typeof(filemanager_url[type]) != "undefined")
                                        url = filemanager_url[type];
                                    else
                                        url = filemanager_url.common;
                                    tinymce.activeEditor.windowManager.open({
                                      file: url,// use an absolute path!
                                      title: \'file manager\',
                                      width: 900,  
                                      height: 450,
                                      resizable: \'yes\'
                                    }, {
                                      oninsert: function (file) {
                                            win.document.getElementById(field_name).value = file.url;
                                              var event = new Event("change")
                                              win.document.getElementById(field_name).dispatchEvent(event);
                                      }
                                    });
                                    return false;
                                    }'),
                                'extended_valid_elements'=> new JsExpression("'noindex,script,img[E|rel=|rel|src|alt=|title|name|longdesc|width|height|usemap|ismap|align|border|hspace|vspace|real_width|real_height|preview|preview_desc|design][]'"),
                                'setup' => new JsExpression(
                                        "function(ed) {
                                            ed.addMenuItem('example', {
                                                text: 'noindex',
                                                context: 'insert',
                                                onclick: function() {
                                                //вставка тега noindex
                                                    var node  = document.createElement('noindex');
                                                    node.innerHTML = ed.selection.getNode().outerHTML;
                                                    ed.selection.getNode().parentNode.replaceChild(node, ed.selection.getNode());
                                                }
                                             });
                                         }"
                                        ),
                            ]
                        ]);
		}
		return '';
	}

	public function asViewAttribute()
	{
		$buffer = (string) $this->_model->{$this->_field};
		
		if($this->getFieldType() == 'html'){
			
			$buffer = '<div class="wisywig_value">'.$buffer.'</div>';
		}
		return array(
                        'label' => $this->_model->getAttributeLabel($this->_field),
			'name' => $this->_field,
			'format' => ($this->getFieldType() == 'html')?'html':'text',
			'value' => $buffer,
		);
	}
	
	public function __toString()
	{
            
            $result = StringHelper::truncateWords( parent::__toString(), $this->truncateWords, '...', $this->getFieldType() == 'html');
            if(!empty($this->_args['escape']))
                $result = HtmlPurifier::process($result);
                
            return $result;
	}

}