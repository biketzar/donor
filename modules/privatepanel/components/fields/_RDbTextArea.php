<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class RDbTextArea extends RDbText
{
   
    protected function renderInput($activeForm,$htmlOptions)
    {
        $htmlOptions['rows'] = '6';
        $htmlOptions['cols'] = '60';
        return $activeForm->textArea($this->_model, $this->_field, $htmlOptions);
    }
}
