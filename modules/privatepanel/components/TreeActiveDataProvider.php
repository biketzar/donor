<?php
namespace app\modules\privatepanel\components;

use yii\data\ActiveDataProvider;
use yii\db\ActiveQueryInterface;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\db\Connection;
use yii\db\QueryInterface;
use yii\di\Instance;


class TreeActiveDataProvider extends ActiveDataProvider
{
    
    public $is_tree = false;
    
    public $treeParentAttribute = '';
    public $treePKAttribute = '';
    public $treeChildRelation = '';
    
    protected function prepareModels()
    {
        if(!$this->is_tree)
            return parent::prepareModels();
        else {
            
            $result = array();
            if (!$this->query instanceof QueryInterface) {
                throw new InvalidConfigException('The "query" property must be an instance of a class that implements the QueryInterface e.g. yii\db\Query or its subclasses.');
            }
            $query = clone $this->query;
            if (($pagination = $this->getPagination()) !== false) {
                $pagination->totalCount = $this->getTotalCount();
                $query->limit($pagination->getLimit())->offset($pagination->getOffset());
            }
            if (($sort = $this->getSort()) !== false) {
                $query->addOrderBy($sort->getOrders());
            }
            $query->andWhere($this->treeParentAttribute.' = 0 OR '.$this->treeParentAttribute.' IS NULL');
            $data = $query->all($this->db);
            
            foreach($data as $object)
            {
                
                $result[] = $object;
                $result = array_merge($result, $this->prepareChildModels($object));
            }
            return $result;
        }
    }
    
    protected function prepareChildModels($parent, $level = 1)
    {
        static $objects = [];
        if(!count($objects))
        {
            $query = clone $this->query;
            $objects = $query->all($this->db);
        }
        $result = array();
        if($this->treePKAttribute)
        {
            foreach($objects as $object)
            {
                if($object->{$this->treeParentAttribute} == $parent->{$this->treePKAttribute})
                {
                    $fieldName = PrivateModelAdmin::getReprNameOfObject($object);
                    $object->$fieldName = str_repeat('&nbsp;&nbsp;&nbsp;&nbsp;', $level).'<i class="fa fa-long-arrow-right"></i> '.$object->$fieldName;
                    $result[] = $object;
                    $result = array_merge($result, $this->prepareChildModels($object, $level + 1));
                }
            }
        } else {
            foreach($parent->{$this->treeChildRelation} as $object)
            {
                $fieldName = PrivateModelAdmin::getReprNameOfObject($object);
                $object->$fieldName = str_repeat('&nbsp;&nbsp;&nbsp;&nbsp;', $level).'<i class="fa fa-long-arrow-right"></i> '.$object->$fieldName;
                $result[] = $object;
                $result = array_merge($result, $this->prepareChildModels($object, $level + 1));
            }
        }
        return $result;
        
    }

    /**
     * @inheritdoc
     */
    protected function prepareTotalCount()
    {
        if(!$this->is_tree)
            return parent::prepareTotalCount();
        else {
            if (!$this->query instanceof QueryInterface) {
                throw new InvalidConfigException('The "query" property must be an instance of a class that implements the QueryInterface e.g. yii\db\Query or its subclasses.');
            }
            $query = clone $this->query;
            $query->andWhere($this->treeParentAttribute.' = 0 OR '.$this->treeParentAttribute.' IS NULL');
            return (int) $query->limit(-1)->offset(-1)->orderBy([])->count('*', $this->db);
        }
    }
    
    public function getCount()
    {
        if(!$this->is_tree)
            return count($this->getModels());
        else {
            $query = clone $this->query;
            $query = $query->andWhere($this->treeParentAttribute.' = 0 OR '.$this->treeParentAttribute.' IS NULL');
            if (($pagination = $this->getPagination()) !== false) {
                $pagination->totalCount = $this->getTotalCount();
                $query->limit($pagination->getLimit())->offset($pagination->getOffset());
            }
            
            return count($query->all($this->db));
        }
    }

    /**
     * @inheritdoc
     */
    /*public function setSort($value)
    {
        parent::setSort($value);
        if (($sort = $this->getSort()) !== false && $this->query instanceof ActiveQueryInterface) {
            
            $model = new $this->query->modelClass;
            if (empty($sort->attributes)) {
                foreach ($model->attributes() as $attribute) {
                    $sort->attributes[$attribute] = [
                        'asc' => [$attribute => SORT_ASC],
                        'desc' => [$attribute => SORT_DESC],
                        'label' => $model->getAttributeLabel($attribute),
                    ];
                }
            } else {
                foreach($sort->attributes as $attribute => $config) {
                    if (!isset($config['label'])) {
                        $sort->attributes[$attribute]['label'] = $model->getAttributeLabel($attribute);
                    }
                }
            }
        }
    }*/
}
?>
