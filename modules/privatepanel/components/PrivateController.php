<?php

namespace app\modules\privatepanel\components;

use yii\web\Controller;
use \app\modules\privatepanel\Module;

use app\components\AccessControl;
use yii\data\ActiveDataProvider;
//use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\modules\privatepanel\components\PrivateModelAdmin;
use app\modules\privatepanel\components\TreeActiveDataProvider;
use app\modules\privatepanel\components\fields;

class PrivateController extends Controller
{
    
    public $defaultAction = 'admin';

	/**
	 * Название обрабатываего класса
	 * @var string 
	 */
    protected $_modelName = '';
    
    protected $_modelPrimaryKeys = [];
	
    /**
     * @var CActiveRecord the currently loaded data model instance.
     */
    //private $_model;
    
    /**
    * Шаблон для отображения списка
    * @var string 
    */
    public $actionAdminTemplate = '@app/modules/privatepanel/views/default/admin';
    /**
    * Шаблон для отображения страницы записи
    * @var string 
    */
    public $actionViewTemplate = '@app/modules/privatepanel/views/default/view';
    /**
    * Шаблон для отображения страницы редактирования
    * @var string 
    */
    public $actionUpdateTemplate = '@app/modules/privatepanel/views/default/update';
    /**
    * Шаблон для отображения страницы клонирования
    * @var string 
    */
    public $actionCloneTemplate = '@app/modules/privatepanel/views/default/clone';
    /**
    * Шаблон для отображения формы добавления/редактирования
    * @var string 
    */
    public $formTemplate = '@app/modules/privatepanel/views/default/_form';
    /**
    * Шаблон для отображения страницы добавления
    * @var string 
    */
    public $actionCreateTemplate = '@app/modules/privatepanel/views/default/create';
    /**
    * Шаблон для отображения расширеной формы поиска для списка
    * @var string 
    */
    public $searchTemplate = '@app/modules/privatepanel/views/default/_search';

    /**
    * Включение валидации форм на ajax
    * @var boolean 
    */
    public $enableAjaxValidation = true;
    /**
    * Включение переадресации на добавление если список пуст
    * @var boolean 
    */
    protected $createIfEmptyList = true;
    /**
    * Текст сообщения для пустого списка
    * @var string 
    */
    protected $createIfEmptyListMessage = '';

    protected $createSuccessMessage = false;
    protected $updateSuccessMessage = true;
    protected $cloneSuccessMessage = true;
    protected $deleteSuccessMessage = null;
    
    
    protected $isAjaxSorting;
    
    public $isTreeAdminView = false;
    public $treeParentAttribute = '';
    public $treeChildRelation = '';
    
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    //public $layout='/layouts/column2';

    private $_logAction = true;
    
    /*public function actionIndex()
    {
        return $this->render('index');
    }*/
    public function init(){
        parent::init();
        
        $this->createIfEmptyListMessage = Module::t('app', 'no records in list');
        $this->updateSuccessMessage = Module::t('app', 'Success update');
        $this->createSuccessMessage = Module::t('app', 'Success create');
        $this->cloneSuccessMessage = Module::t('app', 'Success clone');
        $this->deleteSuccessMessage = Module::t('app', 'Success delete');
        
    }
    
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    ['class' => 'app\components\ActionRule']
                ]
                
            ],
        ];
    }
    
    /*
     * экспорт данных в файл csv
     * в общем, этот метод аналог actionAdmin,  только с выводом в файл и без пагинации
     */
    public function actionExport() {
       $model = \Yii::createObject(['class' => $this->_modelName, 'scenario' => 'search']);	
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename='.$this->_modelName.'.csv');
        $output = fopen('php://output', 'w');
        $columnsModel = \app\models\ShowColumns::find()->where('tabl_name=:tabl_name', [':tabl_name'=>$model->tableName()])->one();
        if($columnsModel == NULL)
        {
			$allColumns = $model->getAdminFields();
            $columnsModel = new \app\models\ShowColumns;
            $columnsModel->setScenario('insert');
            $columnsModel->column_names = implode(',', $allColumns);
            $columnsModel->tabl_name = $model->tableName();
            $columnsModel->save();
        }
        if($columnsModel)
            $columns = explode(',', $columnsModel->column_names);    
        else
            $columns = array_keys($model->tableSchema->columns);
        $labels = $model->attributeLabels();
        $columnDescription = $model->getFieldsDescription();
        $columnsName = [];
        foreach ($columns as $column)
        {
            $columnsName[] = isset($labels[$column]) ? $labels[$column] : $column;
        }
        fputcsv($output, $columnsName);
        
  
        $session_key = 'navigation_'.$this->module->id.'_'.$this->id.'_'.$this->action->id;
        $session = \Yii::$app->session->get($session_key, array());
	$model->load(\Yii::$app->request->get());
        $data = [];
        if(count(\Yii::$app->request->get($model->formName()))) {
             $data = \Yii::$app->request->get($model->formName());
             $session['filter'] = $data;
        } elseif(isset($session['filter'])) {
             $data = $session['filter'];
        }
        $query = $model->searchQuery($data);        
        $privateModelAdmin = \Yii::createObject(['class' => 'app\modules\privatepanel\components\PrivateModelAdmin'],[$model]);        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);	
        
        $arrayWithModels = $dataProvider->getModels();
        //$arrayWithModels = $model->find()->all();
        foreach($arrayWithModels as $data)
        {
            $insertRow = [];
            foreach ($columns as $column)
            {
                $privateModelAdmin = new PrivateModelAdmin($data);
                $privateModelAdmin->setModel($data);
                if(isset($columnDescription[$column]) && $columnDescription[$column]!='RDbText' && !(is_array($columnDescription[$column]) && $columnDescription[$column][0]=='RDbText'))
                {
                    {
                        $field = $privateModelAdmin->getField($column);
                        $insertRow[] = isset($data->$column) ? $field->__toString() : '';
                    }
                }
                else
                {
                    $insertRow[] = isset($data->$column) ? $data->$column : '';
                }
            }
            fputcsv($output, $insertRow);
        }
    }
    
    /*
     * настройки выводимых колонок
     * название всех колонок вытягиваем из модели с помощью getAdminFields
     * храним в БД в виде название_таблицы => колонка1,колонка2,...,колонкаN
     */
    public function actionSettings() {
        $model = \Yii::createObject(['class' => $this->_modelName]);
        $privateModelAdmin = new PrivateModelAdmin($model);
        $privateModelAdmin->setModel($model);
        $allColumns =$model->getAdminFields();
        $columnsModel = \app\models\ShowColumns::find()->where('tabl_name=:tabl_name', [':tabl_name'=>$model->tableName()])->one();
        if(isset($_POST['Settings']))
        {
            $arrayWiwhColumns = array();
            foreach($_POST['Settings'] as $key=>$data)
            {
                if(!in_array($key, $allColumns))
                {
                    unset($_POST['Settings'][$key]);
                }
                else
                {
                    $arrayWiwhColumns[] = $key;
                }
            }
            $columnsModel->column_names = implode(',', $arrayWiwhColumns);
            if($columnsModel->save())
            {
                $params = ['admin'];
                $params = array_merge($params, $privateModelAdmin->additionalUrlParams);
                $this->redirect($params);
                return;
            }
        }
        if($columnsModel == NULL)
        {
            $columnsModel = new \app\models\ShowColumns;
            $columnsModel->setScenario('insert');
            $columnsModel->column_names = implode(',', $allColumns);
            $columnsModel->tabl_name = $model->tableName();
            $columnsModel->save();
        }
        $columns = explode(',', $columnsModel->column_names);
        //формируем порядок
        foreach($allColumns as $key=>$column)
        {
            if(in_array($column, $columns))
            {
                unset($allColumns[$key]);
            }
        }
        $allColumns = array_merge($columns, $allColumns);
        return $this->render('@app/modules/privatepanel/views/default/setting',
        [
            'columns'=>$columns,
            'allColumns'=>$allColumns,
            'model'=>$model,
            'privateModelAdmin'=>$privateModelAdmin,
        ]);
    }
    
    /**
     * Установка сообщения для пользователя
     * @param string $msg
     * @param boolean $ctrl 
     */
    public function setAdminMessage($msg, $type = 'info')
    {
        \Yii::$app->session->setFlash('admin_message_'.$type, $msg);
    }

    /**
     * Получение сообщения для пользователя
     * @param boolean $ctrl 
     * @return string
     */
    public function getAdminMessage($type = 'info')
    {
        if ( $this->hasAdminMessage($type) )
            return \Yii::$app->session->getFlash('admin_message_'.$type);
        else
            return '';
    }
	
	/**
     * Проверка наличия сообщения для польщователя
     * @param boolean $ctrl
     * @return boolean 
     */
    public function hasAdminMessage($type = 'info')
    {
        return \Yii::$app->session->hasFlash('admin_message_'.$type);
    }
    
    public function actionIndex()
    {
        return $this->actionAdmin();
    }
    
    public function actionAdmin()
    {
        $modelName = $this->_modelName;
        
        $model = \Yii::createObject(['class' => $modelName, 'scenario' => 'search']);
        
        $session_key = 'navigation_'.$this->module->id.'_'.$this->id.'_'.$this->action->id;
        $session = \Yii::$app->session->get($session_key, array());
	
        $model->load(\Yii::$app->request->get());
        $data = [];
        if(count(\Yii::$app->request->get($model->formName()))) {
             $data = \Yii::$app->request->get($model->formName());
             $session['filter'] = $data;
        } elseif(isset($session['filter'])) {
             $data = $session['filter'];
        }
		
        $session = $this->initAdminSession($session);
        
        $countOnPage = $session['per-page'];
        
        $query = $model->searchQuery($data);
        
        $privateModelAdmin = \Yii::createObject(['class' => 'app\modules\privatepanel\components\PrivateModelAdmin'],[$model]);
        
        /*$getParams = \Yii::$app->request->getQueryParams(); 
        
        //$query = $modelName::find();
        
        if(isset($getParams))
        {
            foreach($getParams as $param=>$value)
            {
                if(isset($value))
                {
                    if($relation = $model->getRelation($param, false))
                    {
                        $p = array_values($relation->link);
                        if(!$relation->multiple)
                        {
                            foreach($relation->link as $a=>$b)
                                $query->andFilterWhere([$b => $value]);
                            
                        } else {
                            
                            $query->with($param)->andFilterWhere([$param => $value]);
                            
                        }
                    } else {
                        if($model->hasAttribute($param) && $f = $privateModelAdmin->getField($param)->getFilterWhere($value))
                            $query->andFilterWhere($f);
                        
                    }
                }
            }
        }
        */
        $use_filter = false;
        
        if(\Yii::$app->request->get($privateModelAdmin->getModel()->formName()))
        {
            $data = \Yii::$app->request->get($privateModelAdmin->getModel()->formName());
            foreach($data as $k => $v)
            {
                if($v)
                    $use_filter = true;
            }
            
        }
        $providerOptions = [
            'query' => $query,
            'is_tree' => $this->isTreeAdminView && !$use_filter,
            'treeParentAttribute' => $this->treeParentAttribute,
            'treeChildRelation' => $this->treeChildRelation,
            'pagination' => [
                'pageSize' => $countOnPage,
            ],
            
        ];
        if($model::find() instanceof \app\components\CommonActiveQuery && $model::find()->defaultOrder)
        {
            $providerOptions['sort'] = [
                'defaultOrder' => $model::find()->defaultOrder
            ];
        }
        $dataProvider = new TreeActiveDataProvider($providerOptions);
	
        \Yii::$app->session->set($session_key, $session);
        
        if($this->createIfEmptyList && !isset($data) && $dataProvider->getTotalCount() == 0 && \Yii::$app->user->can($this->getAccessOperationName('create'))){
                $this->createIfEmptyListMessage && $this->setAdminMessage($this->createIfEmptyListMessage);
                $params = ['create'];
                $params = array_merge($params, $privateModelAdmin->additionalUrlParams);
                $this->redirect($params);
                return;
        }
        
        
        return $this->render($this->actionAdminTemplate,array(
            'privateModelAdmin'=>$privateModelAdmin,
            'dataProvider'=>$dataProvider,
            'use_tree'=>$this->isTreeAdminView && !$use_filter,
            
        ));
    }
    
    public function actionCreate()
    {
        $this->logOff();
        
	$modelName = $this->_modelName;

        $model = \Yii::createObject(['class' => $modelName, 'scenario' => 'insert']);
        
        $model->load(\Yii::$app->request->get());
        
        $getParams = \Yii::$app->request->getQueryParams(); 
        
        if(isset($getParams))
        {
            foreach($getParams as $param=>$value)
            {
                if(isset($value))
                {
                    if($relation = $model->getRelation($param, false))
                    {
                    } else {
                        if($model->hasAttribute($param))
                                $model->$param = $value;
                    }
                }
            }
            foreach($getParams as $param=>$value)
            {
                if(isset($value))
                {
                    if($relation = $model->getRelation($param, false))
                    {
                        if(!$relation->multiple)
                        {
                            foreach($relation->link as $a=>$b)
                                $model->$b = $value;
                        } 
                    }
                }
            }
        }
        
        if($this->enableAjaxValidation && \Yii::$app->request->isAjax && \Yii::$app->request->post('ajax', null) === strtolower($model->formName()).'-form')
        {
            return $this->performAjaxValidation($model);
        }

        if(\Yii::$app->request->isPost)
        {
            $privateModelAdmin = new PrivateModelAdmin($model);
            
            $data = $privateModelAdmin->prepareForSave(\Yii::$app->request->post(), false);
            
            $model->load($data);
            
            if($model->save())
            {
                $privateModelAdmin->setModel($model);
                $privateModelAdmin->afterSave($data);
                
                $this->createSuccessMessage && $this->setAdminMessage($this->createSuccessMessage, 'success');
                
                $this->_modelPrimaryKeys = $model->getPrimaryKey(true);
        	
                $this->logOn();
                
                if(\Yii::$app->request->post('savetype', '')  == 'apply')
                {
                    $params = ArrayHelper::merge(['update'], $model->getPrimaryKey(true));
                    $params = array_merge($params, $privateModelAdmin->additionalUrlParams);
                        
                    $this->redirect($params);
                    return;
                } else {
                    $params = ['admin'];
                    $params = array_merge($params, $privateModelAdmin->additionalUrlParams);
                    $this->redirect($params);
                    return;
                }
            }
        }
        
        $privateModelAdmin = new PrivateModelAdmin($model);
        if(\Yii::$app->request->isAjax)
        {
            return $this->renderPartial($this->actionCreateTemplate,
                [
                    'privateModelAdmin'=>$privateModelAdmin,
                ]
            );
        }
        return $this->render($this->actionCreateTemplate,
            [
                'privateModelAdmin'=>$privateModelAdmin,
            ]
        );
    }
    
    
    public function actionUpdate()
    {
	$this->logOff();

        $modelName = $this->_modelName;
        
        $keys = [];
        foreach($modelName::primaryKey() as $key)
        {
            if(\Yii::$app->request->get($key))
                $keys[$key] = \Yii::$app->request->get($key);
        }
        if(!count($keys))
            $keys['id'] = -1;
        
        $model = $modelName::find()->andWhere($keys)->one();
        
        if($model)
        {
            if(array_key_exists('update', $model->scenarios()))
                $model->setScenario('update');
	
            $privateModelAdmin = new PrivateModelAdmin($model);

            if(\Yii::$app->request->get($model->formName()))
            {
                $data = $privateModelAdmin->prepareForSave(\Yii::$app->request->get(), true);
                $model->load($data);
            }
            
            if($this->enableAjaxValidation && \Yii::$app->request->isAjax && \Yii::$app->request->post('ajax', null) === strtolower($model->formName()).'-form')
            {
                return $this->performAjaxValidation($model);
            }

            if(\Yii::$app->request->isPost)
            {
                //$privateModelAdmin = new PrivateModelAdmin($model);
                $data = $privateModelAdmin->prepareForSave(\Yii::$app->request->post(), false);

                $model->load($data);
                
                if($model->save())
                {
                    $privateModelAdmin->setModel($model);
                    $privateModelAdmin->afterSave($data);
                    
                    $this->updateSuccessMessage && $this->setAdminMessage($this->updateSuccessMessage, 'success');
                    
                    $this->_modelPrimaryKeys = $model->getPrimaryKey(true);
                    
                    $this->logOn();
                    
                    if(\Yii::$app->request->post('savetype', '')  == 'apply')
                    {
                        $params = ArrayHelper::merge(['update'], $model->getPrimaryKey(true));
                        $params = array_merge($params, $privateModelAdmin->additionalUrlParams);
                        $this->redirect($params);
                        return;
                    } else {
                        $params = ['admin'];
                        $params = array_merge($params, $privateModelAdmin->additionalUrlParams);
                        $this->redirect($params);
                        return;
                    }
                } else {
                    $privateModelAdmin->setModel($model);
                }
            }

            if(\Yii::$app->request->isAjax)
            {
                return $this->renderPartial($this->actionCreateTemplate,
                    [
                        'privateModelAdmin'=>$privateModelAdmin,
                    ]
                );
            }
            return $this->render($this->actionUpdateTemplate,
                [
                    'privateModelAdmin'=>$privateModelAdmin,
                ]
            );
        } else {
            $this->setAdminMessage(Module::t('app', 'There is no record with this primary key', 'error'));
            $params = ['admin'];
            $params = array_merge($params, $privateModelAdmin->additionalUrlParams);
            $this->redirect($params);
        }
    }
    
    public function actionView()
    {
        $this->logOn();
        
	$modelName = $this->_modelName;
        
        $keys = [];
        foreach($modelName::primaryKey() as $key)
        {
            if(\Yii::$app->request->get($key))
                $keys[$key] = \Yii::$app->request->get($key);
        }
        if(!count($keys))
            $keys['id'] = -1;
        
        $model = $modelName::find()->andWhere($keys)->one();
        
        $privateModelAdmin = new PrivateModelAdmin($model);

        if($model)
        {
            $this->_modelPrimaryKeys = $model->getPrimaryKey(true);
            
            return $this->render($this->actionViewTemplate,
                [
                    'privateModelAdmin'=>$privateModelAdmin,
                ]
            );
        } else {
            $this->setAdminMessage(Module::t('app', 'There is no record with this primary key', 'error'));
            $params = ['admin'];
            $params = array_merge($params, $privateModelAdmin->additionalUrlParams);
            $this->redirect($params);
        }
    }
    
    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     */
    public function actionDelete()
    {
        $this->logOff();
        if(\Yii::$app->request->isPost || \Yii::$app->request->isGet && \Yii::$app->request->validateCsrfToken(\Yii::$app->request->get('token', '')))
        {
            // we only allow deletion via POST request
            $modelName = $this->_modelName;

            $keys = [];
            foreach($modelName::primaryKey() as $key)
            {
                if(\Yii::$app->request->get($key))
                    $keys[$key] = \Yii::$app->request->get($key);
            }
            if(!count($keys))
                $keys['id'] = -1;

            $model = $modelName::find()->andWhere($keys)->one();
			
            $privateModelAdmin = new PrivateModelAdmin($model);
            $params = ['admin'];
            $params = array_merge($params, $privateModelAdmin->additionalUrlParams);
            
            if(!$model)
            {
                throw new \yii\web\NotFoundHttpException(Module::t('app', 'Invalid request. Please do not repeat this request again.'));
            }
            
            $this->_modelPrimaryKeys = $model->getPrimaryKey(true);
            
            $privateModelAdmin = new PrivateModelAdmin($model);
            $params = ['admin'];
            $params = array_merge($params, $privateModelAdmin->additionalUrlParams);
            
            $model->delete();
            
            $this->logOn();
		
            if(\Yii::$app->request->get('ajax', ''))
            {
            } else {
                $this->redirect($params);
            }
            
        }
        else
            throw new \yii\web\NotFoundHttpException(Module::t('app', 'Invalid request. Please do not repeat this request again.'));
    }
    
    public function actionClone()
    {
        $this->logOff();
        
        $modelName = $this->_modelName;

        $keys = [];
        foreach($modelName::primaryKey() as $key)
        {
            if(\Yii::$app->request->get($key))
                $keys[$key] = \Yii::$app->request->get($key);
        }
        if(!count($keys))
            $keys['id'] = -1;
        
        $model = $modelName::find()->andWhere($keys)->one();
        
        if($model)
        {
            $privateModelAdmin = new PrivateModelAdmin($model);
            // Если POST
            if(\Yii::$app->request->isPost)
            {
                /*$this->setAdminMessage('Клонирование модели успешно завершено!');
                $this->logOn();*/
                $cloneObject = new CloneObjectClass($model);
                /*$cloneObject->modelName = $this->_modelName;
                $cloneObject->targetId = $_GET['id'];

                // Получаем Админ модель
                $model_admin_name = $this->_modelName.'Admin';
                $adminModel = new $model_admin_name($model);

                // Если метод существует
                if (method_exists($adminModel, 'getProhibitedRelationsForClone'))
                // Тогда указываем запрещающие связи
                    $cloneObject->prohibited_relations = $adminModel->getProhibitedRelationsForClone();
                */
                // Начинаем клонирование
                $new_model = $cloneObject->cloneObject();
                
                $this->_modelPrimaryKeys = $model->getPrimaryKey(true);
                
                $this->logOn();
                //$this->whiteLog();
                $params = ArrayHelper::merge(['view'], $new_model->getPrimaryKey(true));
                $params = array_merge($params, $privateModelAdmin->additionalUrlParams);
                $this->redirect($params);
                return;
                
            }
            return $this->render($this->actionCloneTemplate,
                [
                    'privateModelAdmin'=>$privateModelAdmin,
                ]
            );
        } else {
            $this->setAdminMessage(Module::t('app', 'There is no record with this primary key', 'error'));
            $params = ['admin'];
            $params = array_merge($params, $privateModelAdmin->additionalUrlParams);
            $this->redirect($params);
        }
        
    }
    
    /*public function actionSuggest()
    {
        $this->logOff();
        if(\Yii::$app->request->get('field') || \Yii::$app->request->get('term'))
        {
            $field = \Yii::$app->request->get('field');
            $term = \Yii::$app->request->get('term');
            $modelName = $this->_modelName;
            
            $model = \Yii::createObject(['class' => $modelName, 'scenario' => 'search']);
	
            $query = $modelName::find();
        
            $query->andFilterWhere(['like', $field, $term]);
            
            $dataProvider = new ActiveDataProvider([
                'query' => $query,
                'pagination' => [
                    'pageSize' => 100,
                ],
            ]);
            $result = [];
            foreach($dataProvider->getModels() as $object)
            {
                $result[] = array(
                        'id' => $object->primaryKey,
                        'label' => $object->$field,
                        'value' => $object->$field,
                );
            }
            
            \Yii::$app->response->format = \Yii\web\Response::FORMAT_JSON;
            return $result;
            
        } else {
            throw new \yii\web\NotFoundHttpException(Module::t('app', 'Invalid request. Please do not repeat this request again.'));
        }
    }
    
    public function actionSorting()
    {
        $this->logOff();
        $modelName = $this->_modelName;

        $model = \Yii::createObject(['class' => $modelName]);


        if($this->isAjaxSorting && $model->hasAttribute('order')){
                $session_key = 'navigation_'.$this->module->id.'_'.$this->id.'_admin';
                $session = \Yii::$app->session->get($session_key, array());
                $session = $this->initAdminSession($session);
                $count = $session['per-page'];
                $min = 0;
                //максимум берем исходя из кол-ва записей на страницу
                $max = 10*($count != -1 ? $count : 100);
                
                $pk = array(-1);
                $pk = array_merge($pk, \Yii::$app->request->post('id', array()));
                
                $query = $modelName::find();
        
                $query->andFilterWhere(['in', $model->tableSchema->primaryKey[0], $pk]);
                
                $query->select('sort');
                
                $dataProvider = new ActiveDataProvider([
                    'query' => $query,
                    //'sort' => ['attributes' => ['sort'=> SORT_ASC]],
                ]);
                foreach($dataProvider->getModels() as $object)
                {
                    $min = min($min, $object->order);
                    $max = max($max, $object->order);
                }
                
                for($i = 1; $i < count($pk); $i++)
                {
                    $modelName::updateAll(['order' => (($i-1) * ($max-$min) / (count($pk)-1))], ['id'=>$pk[$i]]);
                }
                
        }
        \Yii::$app->response->format = \Yii\web\Response::FORMAT_JSON;
        return ['result'=>'success'];
    }*/
    
    public function afterAction($action, $result)
    {
        $result = parent::afterAction($action, $result);

        if($this->_logAction){
                $this->whiteLog();
        }
        return $result;
    }
    
    protected function performAjaxValidation($model)
    {
        
        $privateModelAdmin = new PrivateModelAdmin($model);
        $data = $privateModelAdmin->prepareForSave(\Yii::$app->request->post(), true);
        $model->load($data);

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return ActiveForm::validate($model);
    }
    
    protected function initAdminSession($session)
    {
        $pageParam = 'page';
        $page = \Yii::$app->request->get($pageParam);
        if($page)
        {
            $session[$pageParam] = $page;
        } else {
            if(isset($session[$pageParam])){
                $getParams = \yii::$app->request->getQueryParams();
                $getParams[$pageParam] = $session[$pageParam];
                \yii::$app->request->setQueryParams($getParams);
            } else {
            }
        }
        
        $count = \Yii::$app->request->get('per-page', null);
        if(isset($count) && ($count > 0 || $count == -1)){
            $countOnPage = min((int)$count, 999);
            $session['per-page'] = $countOnPage;
        }else{
            if(isset($session['per-page'])){
                $countOnPage = $session['per-page'];
            } else {
                $countOnPage = 20;
                $session['per-page'] = $countOnPage;
            }
        }
        return $session;
    }
    
    public function getAccessOperationName($action = null, $controller = null, $module = null)
    {
            if(!$action) $action = $this->getAction()->id;
            $action = ($action == 'index') ? 'admin' : $action;
            if(!$controller)
                $controller = $this->id;
            $controller = strtolower($controller);
            if(!$module)
                $module = ($this->module instanceof \yii\base\Module)?$this->module->id:'';
            $module = strtolower($module);
            return '/'.($module?$module.'/':''). $controller.'/'.$action;
    }
    
    public function getModelName()
    {
        return $this->_modelName;
    }
    
    protected function whiteLog()
    {
        $keys = $this->_modelPrimaryKeys;
        
        if(count($keys) == 0)
        {
            $keys = 0;
        } elseif(count($keys) == 1)
        {
            $keys = array_pop($keys);
        } else {
            $keys = json_encode($keys);
        }
        
        \Yii::$app->db->createCommand()->insert('{{%private_log}}',[
            'user_id' => \Yii::$app->user->getId(),
            'controller' => $this->id,
            'action' => $this->action->id,
            'model' => $this->_modelName,
            'item_id' => $keys,
            'is_post' => \Yii::$app->request->isPost?1:0,
            'datetime' => date('Y-m-d H:i:s'),
        ])->execute();
    }
    
    protected function logOff()
    {
            $this->_logAction = false;
    }

    protected function logOn()
    {
            $this->_logAction = true;
    }
    
    public function modelName()
    {
        return $this->_modelName;
    }
    
}
?>
