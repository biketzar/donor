<?php

namespace app\modules\privatepanel\components;

class Metrika
{
    private $key = '';//цифровая подпись для получения метрики
    private $id = '';// id домена
    private $url = 'http://www.alkodesign.ru/metrikainfo/'; //сервис для работы с метрикой
    
    /*
     * получение настроек
     * если чего-то нет, возвращает ложь
     * return bool
     */
    public function init()
    {
        if(!isset(\Yii::$app->params['projectSettings']))
        {
            return FALSE;
        }
        $config = \Yii::$app->params['projectSettings'];
        if(!isset($config['metrikaKey']['value']) || !isset($config['metrikaID']['value']))
        {
            return FALSE;
        }
        $this->key = $config['metrikaKey']['value'];
        $this->id = $config['metrikaID']['value'];
        return TRUE;
    }
    
    /*
     * получение кол-ва визитов и посетителей
     * при неудаче или ошибке возвращает ложь
     * return bool/array
     */
    public function getTodayInfo()
    {
        if($this->init()===FALSE)
        {
            return FALSE;
        }
        
        $query=array(
            'id'=>$this->id,
            'sign'=>$this->key,
            'metrics'=>'ym:s:visits,ym:s:users,ym:s:pageviews',//Суммарное количество визитов. + Количество уникальных посетителей.
            'date1'=>'today',
            'date2'=>'today',
        );
        
        $out = $this->sendQuery($query);
        
        if($out === FALSE)
        {
            return FALSE;
        }
        try {
            $outArray = json_decode(trim($out), TRUE);
        } catch (Exception $e) {
            return FALSE;
        }
        if(isset($outArray['data'][0]['metrics'][0]) && isset($outArray['data'][0]['metrics'][1]))
        {
            return array('visits'=>$outArray['data'][0]['metrics'][0],
                        'users'=>$outArray['data'][0]['metrics'][1],
                        'views'=>$outArray['data'][0]['metrics'][2],
                        'id'=>$this->id);
        }
        elseif(isset($outArray['error']) || isset($outArray['errors']))
        {
            return array('error'=>isset($outArray['error']) ? $outArray['error'] : $outArray['errors']);
        }
        else
        {
            return FALSE;
        }
    }
    
    /*
     * отправка запроса
     * возвращает результат запроса
     * params array - массив с get-параметрами
     */
    private function sendQuery($query)
    {
        $ch = curl_init();
        $this->url.'?'.http_build_query($query);
        curl_setopt($ch, CURLOPT_URL, $this->url.'?'.http_build_query($query));
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $out = curl_exec($ch);
        curl_close($ch);
        return $out;
    }
    
    
}