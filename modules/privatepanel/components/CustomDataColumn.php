<?php
namespace app\modules\privatepanel\components;

use yii\base\Model;
use yii\helpers\Html;

class CustomDataColumn extends \yii\grid\DataColumn
{
    protected function renderFilterCellContent()
    {
        if (is_string($this->filter)) {
            return $this->filter;
        }

        $model = $this->grid->filterModel;
        if ($this->filter !== false && $model instanceof Model && $this->attribute !== null && ($model->isAttributeActive($this->attribute) || $model->getRelation($this->attribute, false))) {
            if ($model->hasErrors($this->attribute)) {
                Html::addCssClass($this->filterOptions, 'has-error');
                $error = ' ' . Html::error($model, $this->attribute, $this->grid->filterErrorOptions);
            } else {
                $error = '';
            }
            if($model->getRelation($this->attribute, false))
            {
                $options = $this->filterInputOptions;
                $name = isset($options['name']) ? $options['name'] : Html::getInputName($model, $this->attribute);
                if(isset($model->filterVars) && isset($model->filterVars[$this->attribute]))
                    $selection = $model->filterVars[$this->attribute];
                else
                    $selection = Html::getAttributeValue($model, $this->attribute);
                
                if (!array_key_exists('unselect', $options)) {
                    $options['unselect'] = '';
                }
                if (!array_key_exists('id', $options)) {
                    $options['id'] = Html::getInputId($model, $this->attribute);
                }
                if (is_array($this->filter)) {
                    $options = array_merge(['prompt' => ' --- '], $options);
                    return Html::dropDownList($name, $selection, $this->filter, $options) . $error;
                } else {
                    return Html::textInput($name, $selection, $options) . $error;
                }
            } else {
                if (is_array($this->filter)) {
                    $options = array_merge(['prompt' => ' --- '], $this->filterInputOptions);
                    return Html::activeDropDownList($model, $this->attribute, $this->filter, $options) . $error;
                } else {
                    return Html::activeTextInput($model, $this->attribute, $this->filterInputOptions) . $error;
                }
            }
        } else {
            return parent::renderFilterCellContent();
        }
    }
    public function renderHeaderCell()
    {
        $provider = $this->grid->dataProvider;
        
        $options = $this->headerOptions;
        if ($this->attribute !== null && $this->enableSorting &&
            ($sort = $provider->getSort()) !== false && $sort->hasAttribute($this->attribute)) {
            
            Html::addCssClass($options, 'sorting');
        }
        if ($this->attribute !== null && $this->enableSorting &&
            ($sort = $provider->getSort()) !== false && $sort->hasAttribute($this->attribute)) {
            if(!count($sort->getOrders()))
            {
                if(isset($provider->query->orderBy) && isset($provider->query->orderBy[$this->attribute]))
                {
                    Html::addCssClass($options, 'sorting_'.(($provider->query->orderBy[$this->attribute] == SORT_ASC)?'asc':'desc'));
                }
            }
            if($sort->getAttributeOrder($this->attribute))
                Html::addCssClass($options, 'sorting_'.(($sort->getAttributeOrder($this->attribute) == SORT_ASC)?'asc':'desc'));
        }
        
        return Html::tag('th', $this->renderHeaderCellContent(), $options);
    }
}
?>
