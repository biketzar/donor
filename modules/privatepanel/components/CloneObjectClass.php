<?php
namespace app\modules\privatepanel\components;

use yii\helpers\ArrayHelper;

use app\modules\privatepanel\Module as Module;

/*
 * Класс для клонирования объектов
 *
 * 
 * Пример (В модели можно использовать функции):
 *
 * Возвращает список запрещенный связей
 * @return array 
 *
 * public function getProhibitedAttributesForClone() {
 *     return array(
 *         'attr1',
 *         'shows',
 *         'children',
 * *  public function getProhibitedAttributesForClone() {    );
 * }
 *
 * Добавление в конце столбца подписей
 * @return array 
 *
 * public function getChangedFieldsForClone() {
 *     return array(
 *         'header' => '_clone',
 *         'title' => '_clone',
 *     );
 * }
 * 
 * Добавление кнопки:
 * public function getGridButtonColumns($columnParams = array('buttons'=>array(), 'template'=>'')) {
 *            //$columnParams = parent::getGridButtonColumns();
 *            $columnParams['template'] .= ' {clone}';
 *            $columnParams['buttons']['clone'] = function ($url, $model, $key) 
 *            {
 *                $options = array_merge([
 *                     'title' => Module::t('yii', 'Clone'),
 *                     'aria-label' => Module::t('yii', 'Clone'),
 *                    'data-pjax' => $key,
 *                ], []);
 *                return Html::a('<i class="fa fa-copy"></i>', $url, $options);
 *            };
 * 
 *            return $columnParams;
 *    }
 * 
 * @author Suvorov & Meshcheryakova
 */
class CloneObjectClass {
    
    /**
     * id объекта клонирования
     * @var integer
     */
    //public $targetId;
    
    /**
     * Имя модели объекта клонирования
     * @var string
     */
    //public $modelName;
    
    /**
     * Запрещенные связи (они не будут клонироваться)
     * @var string
     */
    public $prohibited_attributes = array();
    public $extended_attributes = array();
    public $extended_relations = array();


    /**
     * Модель объекта клонирования
     * @var model
     */
    private $_model;
    
    /**
     * Модель склонированый объект
     * @var model
     */
    //private $_modelCloned;
    
    public function __construct(\yii\db\ActiveRecord $model) {
        $this->_model = $model;
        $this->prohibited_attributes = array();
        if(method_exists($this->_model,  'getProhibitedAttributesForClone'))
             $this->prohibited_attributes = $this->_model->getProhibitedAttributesForClone();
        $this->extended_attributes = array();
        if(method_exists($this->_model,  'geExtendedAttributesForClone'))
             $this->extended_attributes = $this->_model->geExtendedAttributesForClone();
        $this->extended_relations = array();
        if(method_exists($this->_model,  'getRelationsForClone'))
             $this->extended_relations = $this->_model->getRelationsForClone();
    }

    public function cloneObject($new_attributes = array()) {
        
        if(!$this->_model)
            return null;
        $modelName = $this->_model->className();
        
        $modelCloned = \yii::createObject(['class' => $modelName, 'scenario' => 'insert']);
        
        $attributes = $this->_model->attributes;
        
        $clone_attributes = array();
        
        $behaviors = $modelCloned->behaviors();
        
        foreach($behaviors as $behavior)
        {
            if($behavior['class'] == 'yii\behaviors\TimestampBehavior')
            {
                foreach($behavior['attributes'] as $attrs)
                {
                    $this->prohibited_attributes = ArrayHelper::merge($this->prohibited_attributes, $attrs);
                }
            }
        }
        
        foreach($attributes as $key=>$value)
        {
            if(in_array($key, $this->prohibited_attributes))
                continue;
            
            if(!$modelCloned->getBehavior('ml') || !in_array($key, $modelCloned->getBehavior('ml')->localizedAttributes))
            {
                $clone_attributes[$key] = $value;
            } else {
                foreach(\Yii::$app->params['translatedLanguages'] as $l=>$lang):
                        if($l !== \Yii::$app->params['defaultLanguage']) 
                            $suffix = '_' . $l;
                        else 
                            $suffix = '_' . $l;
                    $clone_attributes[$key.$suffix] = $this->_model->{$key.$suffix};
                endforeach;
            }
            
        }
        foreach($this->extended_attributes as $key)
        {
            $clone_attributes[$key] = $this->_model->$key;
        }
        
        $privateModelAdmin = new PrivateModelAdmin($modelCloned);
        $privateModelAdminOrigin = new PrivateModelAdmin($this->_model);
        
        $attrs = $privateModelAdmin->getFieldsDescription();
        
        $relations = array();
        
        foreach($attrs as $key=>$type)
        {
            if(in_array($key, $this->prohibited_attributes))
                    continue;
            $args = array();
            if ( is_array($type) )
            {
                    $args = $type;
                    $type = array_shift($args);
            }
            $fieldname = 'app\\modules\\privatepanel\\components\\fields\\'.$type;

            $type = \Yii::createObject(['class' => $fieldname], [$privateModelAdmin, $key, $args]);
            
            if($type && $type instanceof fields\RDbFile && $this->_model->$key)
            {
                $typeOrigin = \Yii::createObject(['class' => $fieldname], [$privateModelAdminOrigin, $key, []]);
                $path = $type->getFilesDirectory();
                
                \app\components\SiteHelper::checkFolderPath($path);

                /*if(!is_dir($path))
                {
                    $path_parts = str_replace(\Yii::getAlias('@web2').'/', '', $path);
                    $path_parts = explode('/',$path_parts);

                    $path_temp = '';
                    for($i = 0; $i<count($path_parts); $i++)
                    {
                        $path_temp.= $path_parts[$i].'/';
                        if(!file_exists(\Yii::getAlias('@web2').'/'.$path_temp))
                        {
                            mkdir(\Yii::getAlias('@web2').'/'.$path_temp, 0755);
                        }
                    }
                }*/
                $pi = pathinfo($this->_model->$key);
                $filename = $type->translit($pi['filename']);

                $k = 1;

                while(file_exists($path.'/'.$filename. '.' . $pi['extension']))
                {
                    $filename = $type->translit(trim($pi['filename'])).'-'.($k++);
                }
                if (copy($typeOrigin->getFilePath(),  $path.$filename . '.' . $pi['extension'])) {
                    $clone_attributes[$key] = $filename. '.' . $pi['extension'];
                }
            }
            
            if($type && $type instanceof fields\RDbRelation && $this->_model->$key)
            {
                //$relations[] = $type->getRelationName();
                /*$relation = $type->getRelation();
                if($relation->multiple)
                {
                    if($relation->via)
                    {
                        $clone_attributes[$key] = [];
                        foreach($relation->via->link as $a => $b)
                        {
                            foreach($this->_model->$key as $r)
                                $clone_attributes[$key][] = $r->$b;
                        }
                    }
                }*/
            }
        }
        
        $relations = array_merge($relations, $this->extended_relations);
        foreach($relations as $relation_name)
        {
            if($relation = $this->_model->getRelation($relation_name))
            {
                if($relation->multiple)
                {
                    $clone_attributes[$relation_name] = [];
                    if($relation->via)
                    {
                        
                        foreach($relation->via->link as $a => $b)
                        {
                            foreach($this->_model->$relation_name as $r)
                                $clone_attributes[$relation_name][] = $r->$b;
                        }
                    } else {
                        
                    }
                } else {
                    foreach($relation->link as $a=>$b)
                        $clone_attributes[$b] = $this->_model->$b;
                }
            }
        }
        
        $clone_attributes = $privateModelAdmin->prepareForSave([$modelCloned->formName() => $clone_attributes], false);
        
        $modelCloned->load($clone_attributes, null);
        
        $primary = $modelCloned->tableSchema->primaryKey;
        
        foreach($primary as $p)
            $modelCloned->$p = null;
        
        foreach ($new_attributes as $key => $attr) {
            $modelCloned->$key = $attr;
        }
        
        if ($modelCloned->hasAttribute('alias'))
            $modelCloned->alias .= '_'.time();
        
        if ($modelCloned->validate()) {
            
            /*if ($modelCloned->hasAttribute('sort_order')) {
                $query = $modelCloned->getDbConnection()->createCommand();
                $modelCloned->sort_order = $query->select('MAX(sort_order)')->from($modelCloned->tableName())->queryScalar()+10;
            }*/
            if (!$modelCloned->save()) {
                throw new \yii\web\NotFoundHttpException(Module::t('app', 'Error of saving of clonned object.'));
            }
            $privateModelAdmin->setModel($modelCloned);
            $privateModelAdmin->afterSave($clone_attributes);
            foreach($relations as $relation_name)
            {
                if($relation = $this->_model->getRelation($relation_name))
                {
                    if($relation->multiple)
                    {
                        if(!$relation->via)
                        {
                            $rel_params = [];
                            foreach($relation->link as $a => $b)
                            {
                                $rel_params[$a] = $modelCloned->$b;
                            }
                            foreach($this->_model->$relation_name as $rel)
                            {
                                $clone_object_rel = new CloneObjectClass($rel);
                                $clone_object_rel->cloneObject($rel_params);
                            }
                        } else {

                        }
                    } 
                }
            }
        } else {
            throw new \yii\web\NotFoundHttpException(Module::t('app', 'Error of cloning object.').' '.implode("\n", $modelCloned->getFirstErrors()));
        }
        
        return $modelCloned;
    }
    
    /**
     * Возвращает модель, которая базируется на имени модели и его id
     * @return model
     */
    /*private function _loadModel() {
        if($this->_model === null)
        {
            $inst = CActiveRecord::model($this->modelName);
            if(isset($this->targetId))
                $this->_model = $inst->findbyPk($this->targetId);
            if($this->_model === null)
                throw new CHttpException(404,'The requested page does not exist.');
        }
        return $this->_model;
    }*/
    
    /**
     * Клонируем модель
     * @param model $modelObject Модель с которой клонируют
     * @param string $modelName Имя модели с которой клонируют
     * @param array $masChangedAttr Массив атрибутов, которые нужно проставить
     * @return model
     */
    /*private function _cloneModel($modelObject, $modelName, $masChangedAttr = array()) {
        $modelCloned = new $modelName();
        $modelCloned->setAttributes($modelObject->attributes, false);
        $primary = $modelCloned->tableSchema->primaryKey;
        $modelCloned->$primary = NULL;
        foreach ($masChangedAttr AS $key => $attr) {
            $modelCloned->$key = $attr;
        }
        if (!empty($modelCloned->alias))
            $modelCloned->alias .= '_'.time();
        if ($modelCloned->validate()) {
            //хардкод, вечер пятницы, с лету CRUDActiveRecort -> init() не принял setScenario('insert');
            if ($modelCloned->hasAttribute('sort_order')) {
                $query = $modelCloned->getDbConnection()->createCommand();
                $modelCloned->sort_order = $query->select('MAX(sort_order)')->from($modelCloned->tableName())->queryScalar()+10;
            }
            if (!$modelCloned->save()) {
                throw new CHttpException(404,'The requested page does not exist.');
            }
        } else {
            throw new CHttpException(404,'The requested page does not exist.');
        }
        return $modelCloned;
    }
    */
    /**
     * Клонируем все связи модели
     * @param array $relations Имена связей у модели.
     * @return model
     */
    /*private function _cloneRelations($relations) {
        foreach ($relations AS $key => $rel) {
            if (!in_array($key, $this->prohibited_relations)) {
                // удаляем все прошлые записи (если они как-то случайно пришли)
                if (!empty($this->_modelCloned->$key))
                    foreach ($this->_modelCloned->$key AS $del_model) {
                        $primary = $del_model->tableSchema->primaryKey;
                        $primary_id = $del_model->$primary;
                        $sql = "DELETE FROM {$del_model->tableName()} WHERE {$primary} = {$primary_id}";
                        $connection = Yii::app()->db;
                        $command = $connection->createCommand($sql);
                        $command->execute(); 
                    }
                // добавляем новые записи
                $changedAttr = array();
                if (!empty($this->_model->$key))
                    foreach ($this->_model->$key AS $rel_model) {
                        // Добавляем связь с основной таблицей
                        $primary = $this->_modelCloned->tableSchema->primaryKey;
                        $primary_id = $this->_modelCloned->$primary;
                        $changedAttr[$rel[2]] = $primary_id;

                        // Клонируем
                        $rel_cloned_model = self::_cloneModel($rel_model, $rel[1], $changedAttr);

                        // получаем модель админа
                        $model_admin_name = $rel[1].'Admin';
                        $modelAdmin = new $model_admin_name($rel_model);
                        // получаем все поля
                        $fields_arr = $modelAdmin->getFieldsDescription();
                        // ищем файлы
                        foreach ($fields_arr AS $key => $file) {
                            // Если это файл
                            if ($file == 'RDbFile') {
                                $fileFunction = 'get'.$key.'Path';
                                $dirFunction = 'get'.$key.'Directory';
                                $old_file_url = $rel_model->$fileFunction();
                                $new_file_url = $rel_cloned_model->$fileFunction();

                                // создаем директорию под файлы
                                $server_root = $_SERVER['DOCUMENT_ROOT'].'/';
                                if (!file_exists($server_root.$rel_cloned_model->$dirFunction()))
                                    mkdir($server_root.$rel_cloned_model->$dirFunction());
                                // Перемещаем туда файлы
                                if (!copy($server_root.$old_file_url, $server_root.$new_file_url)) {
                                    echo "не удалось скопировать $file...\n";
                                }
                            }
                        }
                    }
            }
        }
    }*/
}