<?php
namespace app\modules\privatepanel\components;

use yii\helpers\Url;
use yii\helpers\Html;

class Helper extends \yii\base\Object
{
    public static function getPrivateMenu($menus, $route = '')
    {
        $result = [];
        foreach($menus as $k => $menu)
        {
            if(isset($menus[$k]['visible']) && !$menus[$k]['visible'])
                continue;
            if(isset($menus[$k]['icon']))
                $menus[$k]['label'] = '<i class="fa fa-'.$menus[$k]['icon'].' fa-lg"></i>'.$menus[$k]['label'];
            $menus[$k]['active'] = (isset($menu['url']))?((strpos($route, trim($menu['url'][0], '/').'/') === 0) || ($route == trim($menu['url'][0], '/'))):false;

            if(isset($menus[$k]['items']) && count($menus[$k]['items']))
            {
                Html::addCssClass($menus[$k]['options'], 'has-sub');

                $menus[$k]['label'] = ''.$menus[$k]['label'].'';
                $menus[$k]['url'] = 'javascript:;';
                $menus[$k]['items'] = static::getPrivateMenu($menus[$k]['items'], $route);
                if(!count($menus[$k]['items']))
                    continue;
                foreach($menus[$k]['items'] as $item)
                    $menus[$k]['active'] = $menus[$k]['active'] || $item['active'];
                /*if($menus[$k]['active'])
                    Html::addCssClass($menus[$k]['options'], 'open');*/
            }
            $result[] = $menus[$k];
        }
        return $result;
    }
}
