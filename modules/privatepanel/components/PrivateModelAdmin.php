<?php

namespace app\modules\privatepanel\components;

use Yii;
use \app\modules\privatepanel\Module;
use yii\base\Object;
//use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use app\modules\privatepanel\components\fields\RDbBase;
use app\modules\privatepanel\components\fields\RDbText;
use yii\helpers\Html;
use yii\helpers\Url;

class PrivateModelAdmin extends Object
{
	protected $_model;
        //protected $_query;
	protected $_adminName = '';
	protected $_pluralName = '';

	protected $_repr = '';
        
        protected $_fieldsManager = null;

	
	/**
	 * Включение обработки родительско-дочерних связей 
	 * @var boolean
	 */
	public $enableRelationReference = true;
	/**
	 * Массив родителей где ключ это название родительского relation-a, а значение это ID родителя
	 * @var array 
	 */
	public $relationReferenceParents = array();
	
	/**
	 * Название поля таблицы для использования внутри класса.
	 * @var string
	 */
	private $relationReferenceParentField = null;

	public $urlPrefix = '';
        
        public $additionalUrlParams = array();
        public $additionalParentUrlParams = array();
        
        /**
	 *
	 * @var boolean 
	 */
	public $isTreeAdminView = false;
	/**
	 * Названия релейшина, ссылающегося на родителя
	 * @var string
	 */
	public $treeViewParentField = null;
	/**
	 * Названия релейшина, ссылающегося на дочернии записи
	 * @var string
	 */
	public $treeViewChildRelation = null;
        
        
        public $preInitFields = [];


    	/*-------------------------*/
	
	public function __construct(\yii\db\ActiveRecord $model )
	{
            $this->_model = $model;
            $className = $model::className();
            //$this->_query = $className::find();
            $this->enableRelationReference && $this->initRelationReference();
            
	}
        
        
        public function prepareForSave($data, $check = false)
        {
            $fields = $this->getFieldsDescription();
            
            foreach ( $fields as $field => $class )
            {
                    $args = array();
                    if ( is_array($class) )
                    {
                            $args = $class;
                            $class = array_shift($args);
                    }
                    $classname = 'app\\modules\\privatepanel\\components\\fields\\'.$class;
                    $fieldClass = Yii::createObject(['class' => $classname], [$this, $field, $args]);
                    $data[$this->_model->formName()] = $fieldClass->prepareForSave($data[$this->_model->formName()], $check);
            }
            return $data;
        }
        public function afterSave($data)
        {
            include_once $_SERVER['DOCUMENT_ROOT'].'/components/PageCache.php';
            $pageCache = new \PageCache();
            $pageCache->saveLastUpdateTime(time());
            
            
            $fields = $this->getFieldsDescription();
            
            foreach ( $fields as $field => $class )
            {
                    $args = array();
                    if ( is_array($class) )
                    {
                            $args = $class;
                            $class = array_shift($args);
                    }
                    $classname = 'app\\modules\\privatepanel\\components\\fields\\'.$class;
                    $fieldClass = Yii::createObject(['class' => $classname], [$this, $field, $args]);
                    $fieldClass->afterSave($data[$this->_model->formName()]);
            }
            
        }
	
	/**
	 * Returns list of fields to be excluded from html form (update, create)
	 *
	 * @return array
	 */
	public function getFormExcludedFields()
	{
            if(method_exists($this->_model, 'getFormExcludedFields'))
            {
                return $this->_model->getFormExcludedFields();
            }
            $result = array($this->_model->tableSchema->primaryKey[0], 'visible');
            foreach($this->additionalUrlParams as $param=>$value)
            {
                if($relation = $this->_model->getRelation($param, false))
                {
                    if(!$relation->multiple)
                    {
                        foreach($relation->link as $a=>$b)
                            $result[] = $b;
                    }
                }
            }
           
            return $result;
	}

	/**
	 * Returns list of fields to be showed in html (update,created). Fields described in {@link getFormExcludedFields()} will be excluded from this list
	 *
	 * @return array
	 */
	public function filterFormFields()
	{
		$out = array();
		$excludedFields = $this->getFormExcludedFields();
                
		if ( is_string($excludedFields ) )
			$excludedFields = array($excludedFields);
		
		foreach ( $this->getFormFields() as $field)
		{
                    
                    if(!$this->_model->getBehavior('ml') || !in_array($field, $this->_model->getBehavior('ml')->localizedAttributes))
                    {
                        if ( ! in_array($field, $excludedFields))
				$out[] = $field;
                    } else {
                        foreach(\Yii::$app->params['translatedLanguages'] as $l=>$lang):
                            if($l !== \Yii::$app->params['defaultLanguage']) 
                                $suffix = '_' . $l;
                            else 
                                $suffix = '_' . $l;
                            if ( ! in_array($field, $excludedFields) && ! in_array($field.$suffix, $excludedFields))
				$out[] = $field.$suffix;
                        endforeach;
                    }
		}

		return $out;
	}

	/**
	 * Returns columns for CGridView widget
	 *
	 * @return array
	 */
	public function getGridColumns()
	{
		$columns = array();

		$excludeFields = array();
                
                if(method_exists($this->_model, 'getAdminExcludedFields'))
                    $excludeFields = $this->_model->getAdminExcludedFields();
		if ( is_string($excludeFields ) )
                    $excludeFields = array($excludeFields);
                $columnsModel = \app\models\ShowColumns::find()->where('tabl_name=:tabl_name', [':tabl_name'=>$this->_model->tableName()])->one();
                if($columnsModel)
                {
                    $adminFields = explode(',', $columnsModel->column_names);
                }
                elseif(method_exists($this->_model, 'getAdminFields'))
                    $adminFields = $this->_model->getAdminFields();
                else {
                    $adminFields = array();
                    foreach($this->_model->attributes as $k => $v)
                    {
                        $adminFields[] = $k;
                    }
                }
                $relations = $this->extendParentRelations();
                
                foreach($relations as $relname => $opts)
                {
                    $value = isset($this->additionalUrlParams[$relname])?$this->additionalUrlParams[$relname]:null;
                    if(isset($value))
                    {
                        if(isset($opts['relation']) && $opts['relation'] instanceof \yii\db\ActiveQuery){
                            $rel = $opts['relation'];
                        }
                        foreach($rel->link as $a => $b)
                        {
                            $excludeFields[] = $b;
                        }
                    }
                }
                
		foreach ( $adminFields as $field )
		{
			if ( in_array($field, $excludeFields ) )
				continue;
			if ( is_array($field) )
			{
				$columns[] = $field;
				continue;
			}
                        
			$columns[] = $this->getField($field)->asGridColumn();

		}
                
		/*if ( $last )
			$columns[] = $last;*/
		
		$buttons = $this->getGridButtonColumns();
                $buttons['urlCreator'] = function($action, $model, $key, $index)
                {
                    //$params = is_array($key) ? $key : ['id' => (string) $key];
                    $params = $model->getPrimaryKey(true);
                    $params[0] = Yii::$app->controller ? Yii::$app->controller->id . '/' . $action : $action;
                    
                    $params = array_merge($params, $this->additionalUrlParams);
                    
                    return Url::toRoute($params);
                };
		if($buttons && $buttons['template'])
                    $columns[] = $buttons;
		
		return $columns;
	}

	public function getGridButtonColumns()
	{
                $template = [];
                /*if(Yii::$app->user->can(Yii::$app->controller->getAccessOperationName('view'), ['id'=>$this->_model->primaryKey]))
                    $template[] = '{view}';
                if(Yii::$app->user->can(Yii::$app->controller->getAccessOperationName('update'), ['id'=>$this->_model->primaryKey]))
                    $template[] = '{update}';
                if(Yii::$app->user->can(Yii::$app->controller->getAccessOperationName('delete'), ['id'=>$this->_model->primaryKey]))
                    $template[] = '{delete}';*/
                $template = '{view} {update} {delete}';
		$columnParams = array(
			'class' => 'yii\grid\ActionColumn',
			//'header'=>Module::t('app', 'Actions'),
			'template'=> $template,
			'buttons'=> [
			],
                        'contentOptions' => ['class' => 'grid-actions'],
                    'buttonOptions'=>[]
		);
                
                $columnParams['buttons'] = [
                            'view' => function ($url, $model, $key) {
                                if(!Yii::$app->user->can(Yii::$app->controller->getAccessOperationName('view'), $model->primaryKey, true))
                                    return;
                                $options = array_merge([
                                    'title' => Module::t('app', 'View'),
                                    'aria-label' => Module::t('app', 'View'),
                                    'data-pjax' => '0',
                                    'class' => 'btn btn-info btn-sm',
                                ], []);
                                return Html::a('<i class="fa fa-eye"></i>', $url, $options);
                            },
                            'update' => function ($url, $model, $key) {
                                if(!Yii::$app->user->can(Yii::$app->controller->getAccessOperationName('update'), $model->primaryKey, true))
                                    return;
                                $options = array_merge([
                                    'title' => Module::t('app', 'Update'),
                                    'aria-label' => Module::t('app', 'Update'),
                                    'data-pjax' => '0',
                                    'class' => 'btn btn-success btn-sm',
                                ], []);
                                return Html::a('<i class="fa fa-edit"></i>', $url, $options);
                            },
                            'delete' => function ($url, $model, $key) {
                                if(!Yii::$app->user->can(Yii::$app->controller->getAccessOperationName('delete'), $model->primaryKey, true))
                                    return;
                                $fieldName = $model->getTableSchema()->primaryKey[0];
                                if (isset($model->_privateAdminRepr))
                                    $fieldName = $model->_privateAdminRepr;
                                $str = $model->getOldAttribute($fieldName);
                                $options = array_merge([
                                    'title' => Module::t('app', 'Delete'),
                                    'aria-label' => Module::t('app', 'Delete'),
                                    'data-pjax' => '0',
                                    'class' => 'btn btn-danger btn-sm',
                                    'onclick'=>'if(!confirm("'.Module::t("app",'Are you sure you want to delete {this_item}?', ['this_item'=>$str]).'")) return false;',
                                ], []);
                                return Html::a('<i class="fa fa-trash-o"></i>', $url, $options);
                            }
                ];
		
                $columnParams = $this->showChildReferenceButtons($columnParams);
                if(Yii::$app->user->can(Yii::$app->controller->getAccessOperationName('admin', 'privatelog'), [], true))
                {
			$columnParams['template'] .= ' {log}';
			
                        $columnParams['buttons']['log'] = function ($url, $model, $key) 
                        {
                            $options = array_merge([
                                'title' => Module::t('app', 'Change log'),
                                'aria-label' => Module::t('app', 'Change log'),
                                'data-pjax' => $key,
                                'class' => 'btn btn-warning btn-sm',
                            ], []);
                            $primaryKey = $model->primaryKey();
                            $url = Url::to(['privatelog/admin', 'model'=>get_class($this->_model), 'item'=>$model->{$primaryKey[0]}]);
                            return Html::a('<i class="fa fa-file-code-o"></i>', $url, $options);
                        };
		}
                
		if(method_exists($this->_model, 'getGridButtonColumns'))
                {
                    $columnParams = $this->_model->getGridButtonColumns($columnParams);
                }
		
		return $columnParams;
	}
        
        public function resolveParent($attribute, $name)
        {
            $result = [];
            if($privatemodule = \yii::$app->getModule('private'))
            {
                
                $folder = \yii::getAlias('@'.str_replace('\\', '/', $privatemodule->controllerNamespace));
                if(file_exists($folder))
                {
                    $files = scandir($folder);

                    foreach($files as $file)
                    {
                        if($file == '.' || $file == '..')
                            continue;
                        if(!is_file($folder.'/'.$file))
                            continue;
                        $pi = pathinfo($file);
                        if(!preg_match("/^([a-zA-Z0-9]*)Controller$/s", $pi['filename'], $m))
                            continue;
                        $conrtollerfile = $pi['filename'];
                        $className = $privatemodule->controllerNamespace.'\\'.$conrtollerfile;
                        
                        
                        //$controller = new $className($privatemodule->controllerNamespace.'\\'.$conrtollerfile, $privatemodule);
                        $controller = \yii::createObject($className, [$privatemodule->controllerNamespace.'\\'.$conrtollerfile, $privatemodule]);
                        
                        if(method_exists($controller, 'modelName') && $controller->modelName() == $attribute)
                        {
                            $conrtollerfile = str_replace('Controller', '', $pi['filename']);
                            $controller = '';
                            for($i = 0; $i < strlen($conrtollerfile); $i++)
                            {
                                if($i > 0 && strtoupper($conrtollerfile[$i]) == $conrtollerfile[$i])
                                    $controller .= '-';
                                $controller .= strtolower($conrtollerfile[$i]);
                            }
                            $result[$name] = array('controller' => $controller, 'relationName'=>$controller, 'icon'=>'book');
                        }
                        

                    }
                }
                foreach($privatemodule->controllerMap as $controller => $conrtollerfile)
                {
                    $controllerObject = \yii::createObject($conrtollerfile, [$conrtollerfile, $privatemodule]);
                    if(method_exists($controllerObject, 'modelName') && $controllerObject->modelName() == $attribute)
                    {
                        $result[$name] = array('controller' => $controller, 'relationName'=>$controller, 'icon'=>'book');
                    }
                }
            }
            return $result;
        }

	/**
	 * Returns list of attributes for CDetailView widget. Fields in {@link getViewExcludedFields()} will be excluded.
	 *
	 * @return array
	 */
	public function getViewAttributes()
	{
		$attributes = array();

		$excludeAttributes = $this->getViewExcludedFields();

		if ( is_string($excludeAttributes) )
			$excludeAttributes = array($excludeAttributes);

		foreach ( $this->getViewFields() as $field )
		{
			if ( in_array($field, $excludeAttributes ) )
				continue;
			if ( is_array($field) )
			{
				$attributes[] = $field;
				continue;
			}
                        if(!$this->_model->getBehavior('ml') || !in_array($field, $this->_model->getBehavior('ml')->localizedAttributes))
                        {
                            $attributes[] = $this->getField($field)->asViewAttribute();
                        } else {
                            foreach(\Yii::$app->params['translatedLanguages'] as $l=>$lang):
                                    if($l !== \Yii::$app->params['defaultLanguage']) 
                                        $suffix = '_' . $l;
                                    else 
                                        $suffix = '_' . $l;
                                $attributes[] = $this->getField($field.$suffix)->asViewAttribute();
                            endforeach;
                        }
			
		}
                
		return $attributes;

	}
        
        /**
	 *
	 * @param type $action
	 * @return type 
	 */	
	public function checkAccess($action, $controller = null, $module = null)
	{
		return Yii::$app->user->can(Yii::$app->controller->getAccessOperationName($action, $controller, $module), true);
	}


	

	/**
	 * Return breadcrumbs data for actionAdmin 
	 *
	 * @return array for breadcrumbs
	 */
	public function getAdminBreadcrumbs()
	{
		return ArrayHelper::merge($this->getParentBreadcrumbs(), [
			['label' => $this->getPluralName(), 'url'=> array_merge(['admin'], $this->additionalUrlParams)],
			Module::t('app', 'All')
		]);
	}

	/**
	 * Returns breadcrumbs data for actionView 
	 *
	 * @return array
	 */
	public function getViewBreadcrumbs()
	{
                return ArrayHelper::merge($this->getParentBreadcrumbs(), [
			['label' => $this->getPluralName(), 'url'=> array_merge(['admin'], $this->additionalUrlParams)],
                        ['label' => Module::t("app",'View') .' «'.$this->getRepr().'»']
		]);

	}

	/**
	 * Returns breadcrumbs data for actionUpdate
	 *
	 * @return array
	 */
	public function getUpdateBreadcrumbs()
	{
            $model = $this->_model;
            
            return ArrayHelper::merge($this->getParentBreadcrumbs(), [
			['label' => $this->getPluralName(), 'url'=> array_merge(['admin'], $this->additionalUrlParams)],
                        ['label' => $this->getRepr(), 'url'=> array_merge(array_merge(['view'],$this->_model->getPrimaryKey(true)), $this->additionalUrlParams)],
			Module::t('app', 'Update')
		]);
	}

	/**
	 * Returns breadcrumbs data for actionCreate
	 *
	 * @return array
	 */
	public function getCreateBreadcrumbs()
	{
                return ArrayHelper::merge($this->getParentBreadcrumbs(), [
			['label' => $this->getPluralName(), 'url'=> array_merge(['admin'], $this->additionalUrlParams)],
			Module::t('app', 'Create')
		]);
	}
	
	/**
	 * Returns breadcrumbs data for actionDelete
	 *
	 * @return array
	 */
	public function getDeleteBreadcrumbs()
	{
                return ArrayHelper::merge($this->getParentBreadcrumbs(), [
			['label' => $this->getPluralName(), 'url'=> array_merge(['admin'], $this->additionalUrlParams)],
			['label' => $this->getRepr(), 'url'=> array_merge(array_merge(['view'],$this->_model->getPrimaryKey(true)), $this->additionalUrlParams)],
			Module::t('app', 'Delete')
		]);
	}
        
        public function getCloneBreadcrumbs()
	{
		return ArrayHelper::merge($this->getParentBreadcrumbs(), [
			['label' => $this->getPluralName(), 'url'=> array_merge(['admin'], $this->additionalUrlParams)],
			['label' => $this->getRepr(), 'url'=> array_merge(array_merge(['view'],$this->_model->getPrimaryKey(true)), $this->additionalUrlParams)],
			Module::t('app', 'Clone')
		]);
	}
	
	/**
	 * Returns menu items for actionAdmin
	 *
	 * @return array
	 */
	public function getAdminMenu()
	{
		$menu = [
			//['label'=> Module::t("app", 'Create').': ' . $this->getAdminName(), 'url'=>array_merge(['create'], $this->additionalUrlParams), 'visible' => $this->checkAccess('create'), 'template' => '<a href="{url}" class="btn btn-primary"><i class="fa fa-plus"></i> {label}</a>'],
			['label'=> Module::t("app", 'Create'), 'url'=>array_merge(['create'], $this->additionalUrlParams), 'visible' => $this->checkAccess('create'), 'template' => '<a href="{url}" class="btn btn-primary"><i class="fa fa-plus"></i> {label}</a>'],
		];
                foreach($menu as &$menuitem)
                {
                    if(array_key_exists('url', $menuitem) && is_array($menuitem['url']))
                    {
                        $menuitem['url'] = array_merge($menuitem['url'], $this->additionalUrlParams);
                    }
                }
                $menu[] = ['label'=> Module::t("app", 'Settings'), 'url'=>['settings'], 'visible' => $this->checkAccess('settings'), 'template' => '<a href="{url}" class="btn btn-orange"><i class="fa fa-cog"></i> {label}</a>'];
                $menu[] = ['label'=> Module::t("app", 'Export CSV'), 'url'=>array_merge(['export'], $this->additionalUrlParams), 'visible' => $this->checkAccess('export'), 'template' => '<a href="{url}" class="btn btn-lilac"><i class="fa fa-cloud-download"></i> {label}</a>'];
                
                if(method_exists($this->_model, __FUNCTION__))
                {
                    $method_name = __FUNCTION__;
                    $menu = $this->_model->$method_name($this, $menu);
                }
                
                return $menu;
	}
	
	/**
	 * Returns menu data for actionCreate 
	 *
	 * @return array
	 */
	public function getCreateMenu()
	{
		$menu = [
			//['label'=> Module::t("app", 'Manage').': ' . $this->getPluralName(), 'url'=>['admin'], 'visible' => $this->checkAccess('admin'), 'template' => '<a href="{url}" class="btn btn-lilac"><i class="fa fa-bars"></i> {label}</a>'],
			['label'=> Module::t("app", 'Manage'), 'url'=>['admin'], 'visible' => $this->checkAccess('admin'), 'template' => '<a href="{url}" class="btn btn-lilac"><i class="fa fa-bars"></i> {label}</a>'],
		];
                foreach($menu as &$menuitem)
                {
                    if(array_key_exists('url', $menuitem) && is_array($menuitem['url']))
                    {
                        $menuitem['url'] = array_merge($menuitem['url'], $this->additionalUrlParams);
                    }
                }
                if(method_exists($this->_model, __FUNCTION__))
                {
                    $method_name = __FUNCTION__;
                    $menu = $this->_model->$method_name($this, $menu);
                }
                return $menu;
	}

	/**
	 * Returns menu data for actionUpdate
	 * @return array
	 */
	public function getUpdateMenu()
	{
                $menu = [
			/*['label'=> Module::t("app", 'Manage').': ' . $this->getPluralName(), 'url'=>['admin'], 'visible' => $this->checkAccess('admin'), 'template' => '<a href="{url}" class="btn btn-lilac"><i class="fa fa-bars"></i> {label}</a>'],
			['label'=> Module::t("app", 'Create').': ' . $this->getAdminName(), 'url'=>['create'], 'visible' => $this->checkAccess('create'), 'template' => '<a href="{url}" class="btn btn-success"><i class="fa fa-plus"></i> {label}</a>'],
			['label'=> Module::t("app", 'View').': ' . $this->getRepr(), 'url'=>array_merge(['view'],$this->_model->getPrimaryKey(true)), 'visible' => $this->checkAccess('view'), 'template' => '<a href="{url}" class="btn btn-primary"><i class="fa fa-eye"></i> {label}</a>'],
                        ['label'=> Module::t("app", 'Delete').': ' . $this->getRepr(), 'url'=>array_merge(['delete','token'=>Yii::$app->request->getCsrfToken()],$this->_model->getPrimaryKey(true)), 'options'=>['onclick'=>'if(!confirm("'.Module::t("app",'Are you sure you want to delete {this_item}?', ['this_item'=>$this->getRepr()]).'")) return false;'], 'visible' => $this->checkAccess('delete'), 'template' => '<a href="{url}" class="btn btn-danger"><i class="fa fa-remove"></i> {label}</a>'],*/
                        ['label'=> Module::t("app", 'Manage'), 'url'=>['admin'], 'visible' => $this->checkAccess('admin'), 'template' => '<a href="{url}" class="btn btn-lilac"><i class="fa fa-bars"></i> {label}</a>'],
			['label'=> Module::t("app", 'Create'), 'url'=>['create'], 'visible' => $this->checkAccess('create'), 'template' => '<a href="{url}" class="btn btn-success"><i class="fa fa-plus"></i> {label}</a>'],
			['label'=> Module::t("app", 'View'), 'url'=>array_merge(['view'],$this->_model->getPrimaryKey(true)), 'visible' => $this->checkAccess('view'), 'template' => '<a href="{url}" class="btn btn-primary"><i class="fa fa-eye"></i> {label}</a>'],
                        ['label'=> Module::t("app", 'Delete'), 'url'=>array_merge(['delete','token'=>Yii::$app->request->getCsrfToken()],$this->_model->getPrimaryKey(true)), 'options'=>['onclick'=>'if(!confirm("'.Module::t("app",'Are you sure you want to delete {this_item}?', ['this_item'=>$this->getRepr()]).'")) return false;'], 'visible' => $this->checkAccess('delete'), 'template' => '<a href="{url}" class="btn btn-danger"><i class="fa fa-remove"></i> {label}</a>'],
		];
                
                $menu = array_merge($menu, $this->getRelationMenu());
                
                foreach($menu as &$menuitem)
                {
                    if(array_key_exists('url', $menuitem) && is_array($menuitem['url']))
                    {
                        $menuitem['url'] = array_merge($menuitem['url'], $this->additionalUrlParams);
                    }
                }
		
		if(method_exists($this->_model, __FUNCTION__))
                {
                    $method_name = __FUNCTION__;
                    $menu = $this->_model->$method_name($this, $menu);
                }
                return $menu;
		
	}

	/**
	 * Returns menu items for actionView. Appending menu items with data from {@link getExtendedViewMenu()}
	 *
	 * @return array
	 */
	public function getViewMenu()
	{
                $menu = [
			/*['label'=> Module::t("app", 'Manage').': ' . $this->getPluralName(), 'url'=>['admin'], 'visible' => $this->checkAccess('admin'), 'template' => '<a href="{url}" class="btn btn-lilac"><i class="fa fa-bars"></i> {label}</a>'],
			['label'=> Module::t("app", 'Create').': ' . $this->getAdminName(), 'url'=>['create'], 'visible' => $this->checkAccess('create'), 'template' => '<a href="{url}" class="btn btn-success"><i class="fa fa-plus"></i> {label}</a>'],
			['label'=> Module::t("app", 'Update').': ' . $this->getRepr(), 'url'=>array_merge(['update'],$this->_model->getPrimaryKey(true)), 'visible' => $this->checkAccess('update'), 'template' => '<a href="{url}" class="btn btn-primary"><i class="fa fa-edit"></i> {label}</a>'],
			//['label'=> Module::t("app", 'Delete').': ' . $this->getRepr(), 'url'=>[['view'], ['id'=>$this->_model->primaryKey()]], 'visible' => $this->checkAccess('view')],
			['label'=> Module::t("app", 'Delete').': ' . $this->getRepr(), 'url'=>array_merge(['delete','token'=>Yii::$app->request->getCsrfToken()], $this->_model->getPrimaryKey(true)), 'options'=>['onclick'=>'if(!confirm("'.Module::t("app",'Are you sure you want to delete {this_item}?', ['this_item'=>$this->getRepr()]).'")) return false;'], 'visible' => $this->checkAccess('delete'), 'template' => '<a href="{url}" class="btn btn-danger"><i class="fa fa-remove"></i> {label}</a>'],*/
                        ['label'=> Module::t("app", 'Manage'), 'url'=>['admin'], 'visible' => $this->checkAccess('admin'), 'template' => '<a href="{url}" class="btn btn-lilac"><i class="fa fa-bars"></i> {label}</a>'],
			['label'=> Module::t("app", 'Create'), 'url'=>['create'], 'visible' => $this->checkAccess('create'), 'template' => '<a href="{url}" class="btn btn-success"><i class="fa fa-plus"></i> {label}</a>'],
			['label'=> Module::t("app", 'Update'), 'url'=>array_merge(['update'],$this->_model->getPrimaryKey(true)), 'visible' => $this->checkAccess('update'), 'template' => '<a href="{url}" class="btn btn-primary"><i class="fa fa-edit"></i> {label}</a>'],
			['label'=> Module::t("app", 'Delete'), 'url'=>array_merge(['delete','token'=>Yii::$app->request->getCsrfToken()], $this->_model->getPrimaryKey(true)), 'options'=>['onclick'=>'if(!confirm("'.Module::t("app",'Are you sure you want to delete {this_item}?', ['this_item'=>$this->getRepr()]).'")) return false;'], 'visible' => $this->checkAccess('delete'), 'template' => '<a href="{url}" class="btn btn-danger"><i class="fa fa-remove"></i> {label}</a>'],
		];
		/*$menu = array(
			array('label'=>Yii::t("YiisyCrudAdmin", 'Manage').': ' .($this->getPluralName()), 'url'=>(array('admin')), 'visible' => $this->checkAccess('admin')),
			array('label'=>Yii::t('YiisyCrudAdmin', 'Create'), 'url'=>(array('create')), 'visible' => $this->checkAccess('create')),
			array('label'=>Yii::t("YiisyCrudAdmin",'Update'), 'url'=>(array('update', 'id'=>$this->_model->primaryKey())), 'visible' => $this->checkAccess('update')),
			array('label'=>Yii::t("YiisyCrudAdmin",'Delete'), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$this->_model->primaryKey()),'confirm'=>Yii::t("YiisyCrudAdmin",'Are you sure you want to delete {this_item}?', array('{this_item}'=>$this->getRepr()))), 'visible' => $this->checkAccess('delete')),
		);*/
		$menu = array_merge($menu, $this->getRelationMenu());
		foreach($menu as &$menuitem)
                {
                    if(array_key_exists('url', $menuitem) && is_array($menuitem['url']))
                    {
                        $menuitem['url'] = array_merge($menuitem['url'], $this->additionalUrlParams);
                    }
                }
                
		if(method_exists($this->_model, __FUNCTION__))
                {
                    $method_name = __FUNCTION__;
                    $menu = $this->_model->$method_name($this, $menu);
                }
                return $menu;
	}
	
	public function getDeleteMenu()
	{
                $menu = [
			/*['label'=> Module::t("app", 'Manage').': ' . $this->getPluralName(), 'url'=>['admin'], 'visible' => $this->checkAccess('admin'), 'template' => '<a href="{url}" class="btn btn-lilac"><i class="fa fa-bars"></i> {label}</a>'],
			['label'=> Module::t("app", 'Create').': ' . $this->getAdminName(), 'url'=>['create'], 'visible' => $this->checkAccess('create'), 'template' => '<a href="{url}" class="btn btn-success"><i class="fa fa-plus"></i> {label}</a>'],
			['label'=> Module::t("app", 'Update').': ' . $this->getRepr(), 'url'=>array_merge(['update'],$this->_model->getPrimaryKey(true)), 'visible' => $this->checkAccess('update'), 'template' => '<a href="{url}" class="btn btn-primary"><i class="fa fa-edit"></i> {label}</a>'],*/
                        ['label'=> Module::t("app", 'Manage'), 'url'=>['admin'], 'visible' => $this->checkAccess('admin'), 'template' => '<a href="{url}" class="btn btn-lilac"><i class="fa fa-bars"></i> {label}</a>'],
			['label'=> Module::t("app", 'Create'), 'url'=>['create'], 'visible' => $this->checkAccess('create'), 'template' => '<a href="{url}" class="btn btn-success"><i class="fa fa-plus"></i> {label}</a>'],
			['label'=> Module::t("app", 'Update'), 'url'=>array_merge(['update'],$this->_model->getPrimaryKey(true)), 'visible' => $this->checkAccess('update'), 'template' => '<a href="{url}" class="btn btn-primary"><i class="fa fa-edit"></i> {label}</a>'],
		];
		
                $menu = array_merge($menu, $this->getRelationMenu());
		foreach($menu as &$menuitem)
                {
                    if(array_key_exists('url', $menuitem) && is_array($menuitem['url']))
                    {
                        $menuitem['url'] = array_merge($menuitem['url'], $this->additionalUrlParams);
                    }
                }
                
		if(method_exists($this->_model, __FUNCTION__))
                {
                    $method_name = __FUNCTION__;
                    $menu = $this->_model->$method_name($this, $menu);
                }
                return $menu;
	}
        
        public function getCloneMenu()
	{
		$menu = [
			//['label'=> Module::t("app", 'Manage').': ' . $this->getPluralName(), 'url'=>['admin'], 'visible' => $this->checkAccess('admin'), 'template' => '<a href="{url}" class="btn btn-lilac"><i class="fa fa-bars"></i> {label}</a>'],
			['label'=> Module::t("app", 'Manage'), 'url'=>['admin'], 'visible' => $this->checkAccess('admin'), 'template' => '<a href="{url}" class="btn btn-lilac"><i class="fa fa-bars"></i> {label}</a>'],
		];
                foreach($menu as &$menuitem)
                {
                    if(array_key_exists('url', $menuitem) && is_array($menuitem['url']))
                    {
                        $menuitem['url'] = array_merge($menuitem['url'], $this->additionalUrlParams);
                    }
                }
                if(method_exists($this->_model, __FUNCTION__))
                {
                    $method_name = __FUNCTION__;
                    $menu = $this->_model->$method_name($this, $menu);
                }
                return $menu;
                return $menu;
	}
	/**
	 * Returns text for header in actionAdmin
	 *
	 * @return string
	 */
	public function getAdminHeader()
	{
		return ($this->getPluralName());
	}


	/**
	 * Returns list of fields will be excluded in actionView 
	 *
	 * @return array
	 */
	public function getViewExcludedFields()
	{
            $result = ['visible'];
            if(method_exists($this->_model, 'getViewExcludedFields'))
                $result = array_merge($result, $this->_model->getViewExcludedFields());
            return $result;
	}

	public function getAdminExcludedFields()
	{
		$f = array('visible', $this->relationReferenceParentField);
		if(!in_array($this->_model->tableSchema->primaryKey, $this->getAdminFields())){
			$f[] = $this->_model->tableSchema->primaryKey;
		}
		/*if($this->isAjaxSorting){
			$f[] = 'sort_order';
			$f[] = 'ordering';
		}*/
		return $f;
	}

	public function getViewFields()
	{
            if(method_exists($this->_model, 'getViewFields'))
                    return $this->_model->getViewFields();
		//return array_keys($this->_model->getAttributes());
		return array_keys($this->getFieldsDescription());
	}

    public function getSearchFields()
    {
        return array_keys($this->_model->getAttributes());
    }

    public function getAdminFields()
    {
            return array_keys($this->_model->getAttributes());
    }


    public function getFormFields()
    {
        //return array_keys($this->_model->getAttributes());        
        return array_keys($this->getFieldsDescription());        
    }

	
	/**
	 * Дополняет данные из CActiveRecord::relations().
	 * Можно указывать дополнительные параметры отношений родитель(BELONGS_TO)->текущий объект->дочерние(HAS_MANY)
	 * <ul>
	 * <li>controller -(string) ID контроллера, отвечающего за работу с моделью. по-умолчанию вычисляется как имя класса связаной модели, основываясь на том что класс модели и контроллера будут называться одинаково</li>
	 * <li>displayOnView -(boolean) флаг отображения связи на странице модели(действие view ID)</li>
	 * <li>icon -(string) иконка для списка. можно задать полный путь, начинающийся со "/", или имя файла в папке с иконками админки. если не задано и идет поиск иконки в папке иконок админки по имени отношения "icon_'.strtolower($name).'.gif". по умолчанию = "icon_contents.gif"</li>
	 * <li>label -(string) описательное название для модели, по-умолчанию берется из CRUDModelAdmin::getPluralName() от соотвествующей модели </li>
	 * <li>visible -(string) PHP выражение для CGridView вычисляющее факт отображения ссылки. </li>
	 * <li>relationName -(string) назвение relation-а дочернего объекта, указывающий нужную нам родительскую модель. Вычисляется автиматически на основании названий классов.</li>
	 * </ul>
	 * 
	 * @example
	 * return array(
	 *		'contents' => array('controller' => 'page-content', 'relationName'=>'page', 'icon'=>'book'),
	 *	);
	 * @return array 
	 */
	public function relationReferenceOptions()
	{
		return [];
	}
	
	/**
	 * Обработка отношений/связей c родителями (связей типа CActiveRecord::BELONGS_TO).
	 * Если параметр отвечающий за отношение обнаружен в GET, то к модели в её DbCriteria добавляется соотвествующие условие для фильтрации записей
	 */
	protected function initRelationReference()
	{
            $getParams = Yii::$app->request->getQueryParams(); 
            
            if(isset($getParams))
            {
                foreach($getParams as $param=>$value)
                {
                    if($value)
                    {
                        if($relation = $this->_model->getRelation($param, false))
                        {
                            $this->additionalUrlParams[$param] = $value;
                            if(!$relation->multiple)
                            {
                                if($this->_model->isNewRecord) 
                                {
                                    foreach($relation->link as $a=>$b)
                                    {
                                        //$this->_model->$b = $value;
                                        $this->preInitFields[] = $b;
                                    }
                                }
                            } else {
                                //$this->_query->with($param)->filterWhere([$param => $value]);
                                if($this->_model->isNewRecord) 
                                {
                                    //$this->_model->{$param} = $value;
                                    $this->preInitFields[] = $param;
                                }
                            }
                        } else {
                            if($this->_model->hasAttribute($param) && !in_array($param, $this->_model->primaryKey()))
                            {
                                $this->additionalUrlParams[$param] = $value;
                                if($this->_model->isNewRecord) 
                                {
                                    //$this->_model->{$param} = $value;
                                    $this->preInitFields[] = $param;
                                }
                            }
                        }
                    }
                }
            }
            /*foreach($this->_model->getRelatedRecords() as $name => $relation){
                    if($relation[0] == CActiveRecord::BELONGS_TO && isset($getParams[$name])){
                            // сохраняем значение инфу о родителе
                            $this->relationReferenceParents[$name] = $getParams[$name];
                            // и в модель записываем значение (пригодиться для добавления )
                            if($this->_model->isNewRecord()) $this->_model->{$relation[2]} = $this->relationReferenceParents[$name];
                            // запомнили поле, его исключим из отображения
                            $this->relationReferenceParentField = $relation[2];
                            // к существующим критериям(описанным в defaultScopes) добавляем новое условие
                            $criteria = $this->_model->getDbCriteria();
                            $criteria->addColumnCondition(array($relation[2] => $this->relationReferenceParents[$name]));
                            $this->_model->setDbCriteria($criteria);

                    }elseif($relation[0] == CActiveRecord::MANY_MANY && isset($getParams[$name])){
                            /
                            $criteria = $this->_model->getDbCriteria();
                            //добавляем к выборки отношение MANY_MANY
                            $criteria->with[$name] = array(
                                    'select' => false,	//не выбираем связаные модели, делаем только JOIN для фильтрации. 
                                                                            //если оставить значение по умолчанию, то при отображении списка товаров будет видна только одна категория
                                    'together' => true, //принудительное соединение таблицы
                                    'joinType' => 'JOIN', //по умолчанию LEFT OUTER JOIN выведет и пустые результаты
                                    'condition' => CActiveRecord::model($relation[1])->getTableAlias().'.id_category=:param1',
                                    'params' => array(':param1' => $getParams[$name]),
                            );
                            $this->_model->setDbCriteria($criteria);
                            //сохраняем значение для передачи в действия и дочернии страницы
                            $this->relationReferenceParents[$name] = $getParams[$name];
                            //для новых зеписей (форма добавления) можно задать значение по умолчанию
                            if($this->_model->isNewRecord()){
                                    $this->_model->{$name} = array($getParams[$name]);
                            }
                    }
             
             
            }* */
		
	}
	
	/**
	 * Массив relations от модели, но с дополнительными опциями
	 * Для опций назначены значения по-умолчанию, что бы не беспокоится о несуществующих индексах массива
	 * @var array
	 */
	private $_extendRelations;
	
	
	/**
	 * Получения массива relations с доп.опциями.
	 * Сливаем два массива: дополнительные опции и оригинальный CActiveRecord::relations(). Проверяем наличие всех необходимых опций.
	 * @see CRUDModelAdmin::relationReferenceOptions()
	 * @return array 
	 */
	public function extendRelations()
	{
            if($this->_extendRelations === null){
                $this->_extendRelations = array();
                if(method_exists($this->_model, 'relationReferenceOptions'))
                {
                    foreach($this->_model->relationReferenceOptions() as $name => $opts)
                    {
                        if($relation = $this->_model->getRelation($name, false))
                        {
                            if(empty($opts['disable'])){
                                    $opts['relation'] = $relation;
                                    /* задаем значения по умолчанию и убеждаемся что все задано */
                                    isset($opts['controller']) || $opts['controller']=$opts[1];
                                    isset($opts['displayOnList']) || $opts['displayOnList']=true;
                                    isset($opts['displayOnView']) || $opts['displayOnView'] = false /*($opts[0]==CActiveRecord::HAS_MANY)*/;
                                    isset($opts['icon']) || $opts['icon'] = null;
                                    isset($opts['label']) || $opts['label'] = null;
                                    isset($opts['visible']) || $opts['visible'] = 'true';
                                    //isset($opts['childRelation']) || $opts['childRelation'] = $this->findChildReferenceName($name, $opts);
                                    isset($opts['accessOperation']) || $opts['accessOperation'] = ((Yii::$app->controller->module instanceof \yii\base\Module)?Yii::$app->controller->module->id:'').'.'.$opts['controller'].'.admin';

                                    /****/
                                    $this->_extendRelations[$name] = $opts;
                            }
                        }
                    }
                }
            }
            return $this->_extendRelations;
	}
        
        private $_extendParentRelations;
        
        public function extendParentRelations()
	{
            if($this->_extendParentRelations === null)
            {
                $this->_extendParentRelations = array();
                
                if(method_exists($this->_model, 'relationParentReferenceOptions'))
                {
                    foreach($this->_model->relationParentReferenceOptions() as $name => $opts)
                    {
                        if($relation = $this->_model->getRelation($name, false))
                        {
                            if(empty($opts['disable'])){
                                    $opts['relation'] = $relation;
                                    /* задаем значения по умолчанию и убеждаемся что все задано */
                                    isset($opts['controller']) || $opts['controller']=$opts[1];
                                    isset($opts['displayOnList']) || $opts['displayOnList']=true;
                                    isset($opts['displayOnView']) || $opts['displayOnView'] = false /*($opts[0]==CActiveRecord::HAS_MANY)*/;
                                    isset($opts['icon']) || $opts['icon'] = null;
                                    isset($opts['label']) || $opts['label'] = null;
                                    isset($opts['visible']) || $opts['visible'] = 'true';
                                    //isset($opts['childRelation']) || $opts['childRelation'] = $this->findChildReferenceName($name, $opts);
                                    isset($opts['accessOperation']) || $opts['accessOperation'] = ((Yii::$app->controller->module instanceof \yii\base\Module)?Yii::$app->controller->module->id:'').'.'.$opts['controller'].'.admin';

                                    /****/
                                    $this->_extendParentRelations[$name] = $opts;
                            }
                        }
                    }
                }
            }
            return $this->_extendParentRelations;
	}

	/**
	 * Добавление кнопок для дочерних связей
	 * @param array $columnParams
	 * @return string 
	 */
	protected function showChildReferenceButtons(array $columnParams)
	{
            //$baseUrl = Yii::getAlias('@web');
            $relations = $this->extendRelations();
            //VarDumper::dump($relations);
            $columnTemplate = '';

            if(!is_array($relations)) return $columnParams;

            foreach($relations as $name => $opts){
                if(isset($opts['relation']) && $opts['relation'] instanceof \yii\db\ActiveQuery)
                {
                    /*if($opts['relation']->multiple)
                    {*/
                    if(!empty($opts['disable']) || !$opts['displayOnList']) continue;

                    if(!isset($opts['relationName']))	continue;
                    if(!$opts['label'])
                    {
                            //$opts['label'] = $name;
                            $className = $opts['relation']->modelClass;
                            $model = Yii::createObject(['class' => $className]);
                            $opts['label'] = PrivateModelAdmin::getPluralNameOfObject($model);
                    }

                    if(!isset($opts['icon']) || !$opts['icon']){
                        $opts['icon'] = 'th-list';
                    }
                    if(!isset($opts['color']) || !$opts['color']){
                        $opts['color'] = 'green';
                    }
                    $visible = $this->checkAccess('admin', $opts['controller']);
                    if($visible)
                    {
                        eval('$visible = '.$opts['visible'].';');

                    }
                    if(!$visible)
                        continue;
                    $urlParams = array();
                    /*foreach($this->relationReferenceParents as $key => $id){
                            $urlParams[$key] = $id;
                    }*/
                    $urlParams = $this->additionalUrlParams;
                    //$urlParams[$name] = '$model->primaryKey';
                    if(isset($opts['urlOptions']))
                        $urlParams = array_merge($opts['urlOptions']);
                    array_walk($urlParams, function(&$item, $key){ $item = str_replace('\\', '\\\\', $item);});
                    $urlParams = json_encode($urlParams);

                    //$url = Url::to(array_merge([$opts['controller'].'/admin'], $urlParams));
                    $columnTemplate .= '{'.$name.'} ';
                    /*$columnParams['buttons'][$name] = array(
                            'label'=>$opts['label'], // text label of the button
                            'url'=>$url,   // a PHP expression for generating the URL of the button
                            'imageUrl'=>$icon,  // image URL of the button. If not set or false, a text link is used
                            'options'=>array(), // HTML options for the button tag
                            'visible'=>$opts['visible'].(Yii::$app->user->can($opts['accessOperation']) ? '&& true' : '&& false'),   // a PHP expression for determining whether the button is visible
                    );*/

                    $text_function = <<<FUNC
                        \$options = array_merge([
                            'title' => "{$opts['label']}",
                            'aria-label' => "{$opts['label']}",
                            'data-pjax' => \$key,
                            'class' => 'btn btn-{$opts['color']} btn-sm',
                        ], []);
                        \$urlParams = json_decode('{$urlParams}', true);
                        \$urlParams['{$opts['relationName']}'] = \$model->primaryKey;
                        \$url = \yii\helpers\Url::to(array_merge(['{$opts['controller']}/admin'], \$urlParams));
                        return \yii\helpers\Html::a('<i class="fa fa-{$opts['icon']}"></i>', \$url, \$options);
FUNC;
                        //var_dump($text_function);

                    $function = create_function('$url, $model, $key', $text_function);

                    $columnParams['buttons'][$name] = $function;

                    //}
                }
            }
            $columnParams['template'] = $columnTemplate.$columnParams['template'];
            return $columnParams;
	}
        public function getRelationButton($name, $opts, $url)
        {
            return function ($url, $model, $key) 
            {
                $options = array_merge([
                    'title' => $opts['label'],
                    'aria-label' => Module::t('app', 'Change log'),
                    'data-pjax' => $key,
                ], []);
                $url = Url::to(['privatelog/admin', 'model'=>get_class($this->_model), 'item'=>$model->primaryKey]);
                return Html::a('<i class="fa fa-database"></i>', $url, $options);
                //return Html::a(Html::img($baseUrl.'/modules/privatepanel/assets/icon_logs.gif', ['alt'=>Module::t('app', 'View')]), $url);
            };
        }
	
	/**
	 * Получение родительских "хлебных крошек"
	 * В оригинальных методах добавляются пункты, списка и действия. 
	 * В этом методе мы добавляем в "историю" родителя, и опрашиваем этот же медот у него - который нам вернет "крошки" родителей родителя.
	 * @return array 
	 */
        
	public function getParentBreadcrumbs($deep = 0, $parentClassRel = null)
	{
                if(!$this->enableRelationReference || !count($this->additionalUrlParams) || $deep > 5){
			return array();
		}
		
		$breadCrumbs = array();
		$relations = $this->extendParentRelations();
                
                if(!$parentClassRel)
                    $parentClassRel = $this->_model->className();
                
		//foreach($this->additionalUrlParams as $name => $value){
                        foreach($relations as $relname => $opts){
                            
                            if(isset($opts['relation']) && $opts['relation'] instanceof \yii\db\ActiveQuery){
                                $rel = $opts['relation'];
                                if($rel->modelClass == $parentClassRel)
                                    continue;
                                
                            }
                            $relClassName = $rel->modelClass;
                            $value = isset($this->additionalUrlParams[$relname])?$this->additionalUrlParams[$relname]:0;
                            if(is_array($value))
                                $value = array_shift($value);
                            $relModel = $relClassName::findOne($value);
                            
                            if($relModel && $relModel instanceof \yii\db\ActiveRecord)
                            {
                                $breadCrumbs = array_merge($this->getParentBreadcrumbs($deep+1, $relClassName), $breadCrumbs);
                                $label = PrivateModelAdmin::getPluralNameOfObject($relModel);
                                $urlParams = array_merge([$relname => $value], $this->additionalUrlParams);
                                $url = [$opts['controller'].'/admin'];
                                $url = array_merge($url, $urlParams);
                                $breadCrumbs[] = ['label' => $label, 'url' => $url];
                                $label = PrivateModelAdmin::getReprOfObject($relModel);
                                $urlParams = array_merge([$relname => $value], $this->additionalUrlParams);
                                $url = ArrayHelper::merge([$opts['controller'].'/view'], $relModel->getPrimaryKey(true));
                                $url = array_merge($url, $urlParams);
                                $breadCrumbs[] = ['label' => $label, 'url' => $url];
                            }
                        }
		//}
		return $breadCrumbs;
	}
	
	/**
	 * Перехватывает генерацию ссылок контроллера и вставляем туда все параметры отношений с родителями.
	 * Перехват осуществлен в базовом контроллере.
	 * @param array $params
	 * @return array 
	 */
	/*public function processPrivateLink($params)
	{
		if($this->enableRelationReference){
			foreach($this->relationReferenceParents as $parentRel=>$parentKey){
				$params[$parentRel] = $parentKey;
			}
		}
		return $params;
	}*/
	
	/**
	 * Добавляет к меню действий (create, update, delete) кнопки перехода по дочерним отношениям
	 * @return array 
	 */
	public function getRelationMenu()
	{
		$result = [];
                $relations = $this->extendRelations();
		
		if(!is_array($relations)) return $result;
                
                foreach($relations as $name => $opts){
                    if(isset($opts['relation']) && $opts['relation'] instanceof \yii\db\ActiveQuery){
                        /*if($opts['relation']->multiple)
                        {*/
                            $visible = $this->checkAccess('admin', $opts['controller']);
                            if($visible)
                            {
                                eval('$visible = '.$opts['visible'].';');
                            }
                            if(!$visible)
                                continue;
                            if(!$opts['label'])
                            {
                                    $className = $opts['relation']->modelClass;
                                    $model = Yii::createObject(['class' => $className]);
                                    $opts['label'] = PrivateModelAdmin::getPluralNameOfObject($model);
                            }
                                   
                            $urlParams = array();
                            //$urlParams = $this->additionalUrlParams;
                            
                            $urlParams[$opts['relationName']] = $this->_model->primaryKey;
                            if(isset($opts['urlOptions']))
                                $urlParams = array_merge($urlParams, $opts['urlOptions']);
                            $url = array_merge([$opts['controller'].'/admin'], $urlParams);
                            $icon = '';
                            if(isset($opts['icon']) && $opts['icon'])
                                $icon = '<i class="fa fa-'.$opts['icon'].'"></i>';
                            $class = 'btn btn-green';
                            if(isset($opts['color']) && $opts['color'])
                                $class = 'btn btn-lilac';
                            
                            $result[] = 
                                ['label'=> $opts['label'], 'url'=>$url, 'visible' => $this->checkAccess('admin', $opts['controller']),
                                    'template' => '<a href="{url}" class="'.$class.'">'.$icon.' {label}</a>']
                            ;
                        //}
                    }
                }
		return $result;
	}
        
        public function getModel()
        {
            return $this->_model;
        }
        public function setModel(\yii\db\ActiveRecord $model)
        {
            $this->_model = $model;
        }
        /*public function getQuery()
        {
            return $this->_query;
        }*/
        
        public function getFieldsDescription()
        {
            $result = [];
            if(method_exists($this->model, 'getFieldsDescription'))
                    $result = array_merge($result, $this->model->getFieldsDescription());
            else {
                foreach($this->_model->attributes as $k => $v)
                {
                    $result[$k] = 'RDbText';
                }
                
            }
            
            return $result;
            
        }
        
        public function getField( $fieldName )
	{
		return $this->getFieldsManager()->getField($fieldName);
	}

	/**
	 * Возвращает объект менеджера полей.
	 * @return CRUDFieldsManager 
	 */
	public function getFieldsManager()
	{
		if ( $this->_fieldsManager == null )
			$this->_fieldsManager = new PrivateFieldsManager($this);
		return $this->_fieldsManager;
	}
	
	//public $isAjaxSorting = false;
        
        
        
	/**
	 * Name of entity used to display in admin. If $_adminName not set, try to get information from model.  
	 *
	 * @return string
	 */
	public function getAdminName($intl = true)
	{
            if (isset($this->_model->_privateAdminName))
                $name = $this->_model->_privateAdminName;
            else
                $name = get_class($this->_model);
            return ($intl)?Module::t('app', $name):$name; 
	}


	/**
	 * Name of entity set used to display in admin. If $_pluralName not set trying to get information from $_adminName
	 *
	 * @return string
	 */
	public function getPluralName($intl = true)
	{
            if (isset($this->_model->_privatePluralName))
                $name = $this->_model->_privatePluralName;
            else {
                $name = $this->getAdminName(false);
                if ( $name[strlen($name) - 1] != 's' )
                    $name .= 's';
            }
            $modelClass = $this->_model;
            
            return ($intl)?Module::t('app', $name):$name; 
	}
        
        public static function getAdminNameOfObject(\yii\db\ActiveRecord $object, $intl = true)
	{
            if (isset($object->_privateAdminName))
                $name = $object->_privateAdminName;
            else
                $name = get_class($object);
            return ($intl)?Module::t('app', $name):$name; 
	}
        
        public static function getPluralNameOfObject(\yii\db\ActiveRecord $object, $intl = true)
	{
            if (isset($object->_privatePluralName))
                $name = $object->_privatePluralName;
            else {
                $name = PrivateModelAdmin::getAdminNameOfObject($object, false);
                if ( $name[strlen($name) - 1] != 's' )
                    $name .= 's';
            }
            return ($intl)?Module::t('app', $name):$name;
	}
        
        
	/**
	 * Returns object string representation for admin.
	 * @return
	 */
	public function getRepr()
	{
                $fieldName = $this->_model->getTableSchema()->primaryKey[0];
                if (isset($this->_model->_privateAdminRepr))
                    $fieldName = $this->_model->_privateAdminRepr;
		$str = $this->_model->{$fieldName};
		return $str;
		//return RDbText::LimitedTextWithoutHtml($str, $limit);
	}
        public function getReprName()
	{
                $fieldName = $this->_model->getTableSchema()->primaryKey[0];
                if (isset($this->_model->_privateAdminRepr))
                    $fieldName = $this->_model->_privateAdminRepr;
		return $fieldName;
		//return RDbText::LimitedTextWithoutHtml($str, $limit);
	}
        public static function getReprNameOfObject(\yii\db\ActiveRecord $object)
	{
                $fieldName = $object->getTableSchema()->primaryKey[0];
                if (isset($object->_privateAdminRepr))
                    $fieldName = $object->_privateAdminRepr;
		return $fieldName;
	}
        
        public function getReprOfObject(\yii\db\ActiveRecord $object)
	{
                $fieldName = $object->getTableSchema()->primaryKey[0];
                if (isset($object->_privateAdminRepr))
                    $fieldName = $object->_privateAdminRepr;
		$str = $object->{$fieldName};
		return $str;
	}
        
        
	/**
	 * Returns text for header in actionCreate 
	 *
	 * @return string
	 */
	public function getCreateHeader()
	{
		return Module::t('app', 'Create').' '. $this->getAdminName();
	}

	/**
	 * Return text for header in actionUpdate
	 * @return string
	 */
	public function getUpdateHeader()
	{
                return Module::t('app', 'Update').': '. $this->getRepr().'';
	}

	/**
	 * Returns text for header in actionView
	 *
	 * @return string
	 */
	public function getViewHeader()
	{
		return Module::t('app', 'View').': '. $this->getRepr().'';
	}
	
	public function getDeleteHeader()
	{
		return Module::t('app', 'Delete').': '. $this->getRepr().'';
	}
        
        public function getCloneHeader()
	{
		return Module::t('app', 'Clone').': '. $this->getRepr().'';
	}

        

	
}


class PrivateFieldsManager
{
	public function __construct( $model )
	{
            $fields = $model->getFieldsDescription();
            
            $translated = [];
            if($model->getModel()->getBehavior('ml'))
            {
                $translated = $model->getModel()->getBehavior('ml')->localizedAttributes;
            }
            
            foreach ( array_diff( array_keys($fields),array_keys($model->getModel()->getAttributes())) as $field)
            {
                    $this->_fields[$field] = new RDbText( $model, $field, []);
            }
            foreach ( array_diff( array_keys($model->getModel()->getAttributes()),array_keys($fields)) as $field)
            {
                    $this->_fields[$field] = new RDbText( $model, $field, []);
            }

            foreach ( $fields as $field => $class )
            {
                    $args = array();
                    if ( is_array($class) )
                    {
                            $args = $class;
                            $class = array_shift($args);
                    }
                    $classname = 'app\\modules\\privatepanel\\components\\fields\\'.$class;
                    /*$this->_fields[$field ] = new $classname( $model, $field, $args);*/
                    $this->_fields[$field ] = Yii::createObject(['class' => $classname], [$model, $field, $args]);
                    if(in_array($field, $translated))
                    {
                        foreach(Yii::$app->params['translatedLanguages'] as $l=>$lang):
                            $suffix = '_' . $l;
                            $args2 = array_merge($args, ['field' => $field]);
                            $this->_fields[$field.$suffix ] = Yii::createObject(['class' => $classname], [$model, $field.$suffix, $args2]);
                        endforeach;
                    }
            }
            
	}

	public function getField($fieldName)
	{
            
		return (isset($this->_fields[$fieldName])?$this->_fields[$fieldName]:null);
	}
        
        
        
        
}