<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\privatepanel\Module as Module;

$this->params['breadcrumbs'][0] = ["label"=>'Настройки', 'url'=>['admin']];
$this->params['breadcrumbs'][1] = ["label"=>'Добавление'];
?>
<?php $form = ActiveForm::begin([
	'id'=>strtolower('form'),
	'enableAjaxValidation' => false,
	'enableClientValidation' => false,
	'options' => array(
		'enctype' => 'multipart/form-data',
	)
]); ?>

	<table cellspacing="1" cellpadding="3" border="0" class="table table-striped table-editing">
            <thead>
                <tr>
                    <th>&nbsp;</th>
                    <th>Добавление настройки</th>
                </tr>
            </thead>
            <tbody>

        <tr>
            <td>Название</td>
            <td>
                <input type="text" name="ProjectSettings[key]" size="80"/>
            </td>
        </tr>
        <tr>
            <td>Тип</td>
            <td>
                <select name="ProjectSettings[type]">
                    <option value="text" selected="">text</option>
                    <option value="checkbox">checkbox</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>Значение</td>
            <td >
                <input type="text" name="ProjectSettings[value]" size="80"/>
            </td>
        </tr>
        <tr>
            <td>Описание</td>
            <td>
                <input type="text" name="ProjectSettings[name]" size="80"/>
            </td>
        </tr>
        
<tr>
        <td colspan="2" style="text-align: center;">
<?= Html::submitButton('Сохранить', ['class' => 'btn btn-success', 'name'=>'save']) ?>&nbsp;
        </td>
</tr>
</tbody></table>

<?php echo Html::hiddenInput('savetype', 'save');?>

<?php ActiveForm::end() ?>