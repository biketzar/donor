<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\privatepanel\Module as Module;
$this->params['menu'][] = ['label'=>'Добавить', 'url'=>['create'], 'visible'=>TRUE];
$this->params['breadcrumbs'][0] = ["label"=>'Настройки'];
$this->title = 'Настройки';
?>
<?php $form = ActiveForm::begin([
	'id'=>strtolower('form'),
	'enableAjaxValidation' => false,
	'enableClientValidation' => false,
	'options' => array(
		'enctype' => 'multipart/form-data',
	)
]); ?>
<table cellspacing="1" cellpadding="3" border="0" class="table table-striped table-editing">
        <thead>
            <tr>
                <th>&nbsp;</th>
                <th>Настройки</th>
                <th>&nbsp;</th>
            </tr>
        </thead>
    <tbody>

<?php 
$class = '';
foreach ( $config as $key=>$data) : 
    $class = ($class != 'dark') ? 'dark' : 'light';
        ?>
        <tr>
            <td><?=isset($data['name']) ? $data['name'] : $key?>
            </td>
            <td>
                <input type="<?=$data['type']?>" name="ProjectSettings[<?=$key?>]" <?=$data['type'] == 'checkbox' &&  boolval($data['value']) ? 'checked="checked"' : ''?> <?=$data['type'] != 'checkbox'  ?  'value="'.$data['value'].'" size="80"' : ''?>/>
            </td>
            <td>
                <a href="/private/project-settings/delete?key=<?=$key?>" title="Удалить" aria-label="Удалить" data-pjax="0"><i class="fa fa-trash-o"></i></a> 
            </td>
        </tr>
        <?php
endforeach;?>
    <tfoot>
        <tr>
            <td colspan="3" style="text-align: center;">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success', 'name'=>'ProjectSettings[save]']) ?>&nbsp;
            </td>
        </tr>
    </tfoot>       

</tbody></table>

<?php echo Html::hiddenInput('savetype', 'save');?>

<?php ActiveForm::end() ?>