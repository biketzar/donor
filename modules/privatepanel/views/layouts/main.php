<?php
use yii\bootstrap\Alert;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\modules\privatepanel\assets\PrivateAsset;
use app\modules\privatepanel\assets\PrivatePrintAsset;
use app\modules\privatepanel\components\Helper;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $content string */
//AppAsset::register($this);
PrivateAsset::register($this);
PrivatePrintAsset::register($this);

$controller = $this->context;
$menus = \yii::$app->getModule('private')->menus;
$route = $controller->route;
$menus = Helper::getPrivateMenu($menus, $route);
if(!isset($this->params['breadcrumbs']))
{
    $this->params['breadcrumbs'] = [];
}

$assetsmanager = $this->getAssetManager();
$baseUrl = $assetsmanager->bundles['app\modules\privatepanel\assets\PrivateAsset']->baseUrl;

?>
<?php $this->beginPage() ?>
<!DOCTYPE html><!--[if lt IE 7]>      <html class="no-js lt-ie10 lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie10 lt-ie9 lt-ie8 ie7"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie10 lt-ie9 ie8"> <![endif]-->
<!--[if IE 9]>         <html class="no-js lt-ie10 ie9"> <![endif]-->
<!--[if gt IE 9]>      <html class="no-js gt-ie9"> <![endif]-->
<!--[if !IE] <![IGNORE[--><!--[IGNORE[]]--> <html class="no-js noie">
<!-- миксины-->
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="description" content="">
    <!--meta(name="viewport", content="width=device-width, initial-scale=1")-->
    <link rel="shortcut icon" href="/web/favicon.ico">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="inner-body<?php if(!empty($_COOKIE['sidebar_closed'])) echo ' page-sidebar-closed';?>">
<?php $this->beginBody() ?>
<style>
.col-xs-6.text-left img {
    max-height: 32px;
}
</style>
    <aside class="page-sidebar-wrapper">
        <div class="page-sidebar">
          <nav role="navigation" class="navbar accordion" style="overflow-y: auto;">
            <div class="logo text-center"><a href="http://www.alkodesign.ru/rus/alchemy_yii/" target="_blank"><img src="<?php echo $baseUrl;?>/images/logo.png" alt="Alchemy"></a></div>
            <?php
            echo \yii\widgets\Menu::widget([
                'options' => ['class' => 'nav navbar-nav nav-stacked'],
                'items' => $menus,
                'submenuTemplate' => "\n<ul class=\"sub list-unstyled\">\n{items}\n</ul>\n",
                'encodeLabels' => false,

            ]);
            ?>
          </nav>
          <div class="copy text-center">
            <p class="text-info">Разработано в&nbsp;<a href="http://www.alkodesign.ru" target="_blank" class="text-info">AlkoDesign</a></p>
          </div>
        </div>
      </aside>
      <main class="page-content-wrapper">
        <div class="page-content">
          <header class="padtop20 padbot20">
              <div class="col-xs-6 text-left"><a href="#" class="sidebar-toggler margright20 text-info<?php if(!empty($_COOKIE['sidebar_closed'])) echo ''; else echo '  active';?>"><i class="fa fa-bars fa-lg"></i></a><a href="<?php echo Url::home();?>"><img src="/web/favicon.ico" alt="<?= \Yii::$app->name?>">&nbsp;<b><?= \Yii::$app->name?> (<?php echo preg_replace("/http[s]*:\/\//si", "", \yii::$app->getUrlManager()->getHostInfo());?>)</b></a></div>
            <div class="col-xs-6 text-right">
                <?php
                if(\yii::$app->user->getIdentity()):
                    $username = \yii::$app->user->getIdentity()->username;
                    $user_roles = [];
                    $roles = \yii::$app->authManager->getRolesByUser(\yii::$app->user->getIdentity()->getId());

                    foreach($roles as $role=>$obj)
                    {
                        $r = \yii::$app->authManager->getRole($role);
                        $user_roles[] = ($r->description)?$r->description:$r->name;
                    }
                    $username .= (count($user_roles))?' ('.implode(', ', $user_roles).')':'';
                    echo Html::a('<i class="fa fa-user fa-lg text-info"> &nbsp;</i><span class="text-info">Пользователь:</span> '.$username, ['/privateuser/user/update', 'id' => \yii::$app->user->getIdentity()->getId()], ['class' => 'margright30']);
                endif;
                echo Html::a('<i class="fa fa-sign-out fa-lg">&nbsp;</i>Выход', ['/private/default/logout'], ['class' => 'text-info']);
                 ?>
            </div>
            <div class="clearfix"></div>
          </header>
          <div class="breadcrumbs clearfix">
            <div class="middle-block">
              <h4><?php echo $this->title; ?></h4>
              <?= Breadcrumbs::widget([
                    'links' => $this->params['breadcrumbs'],
                    'homeLink' => ['label'=>\Yii::t('yii', 'Home'), 'url' => ['/private/default/index']]
                ]) ?>
            </div>
          </div>
          <div id="main">
              <?php
                if (!empty($this->params['top-menu']) && isset($this->params['nav-items'])) {
                    echo \yii\widgets\Menu::widget([
                        'options' => ['class' => 'nav navbar-nav nav-stacked'],
                        'items' => $this->params['nav-items'],
                    ]);
                }
                ?>
              <?php
            $types = ['info', 'error', 'success'];
            $csstypes = ['info' => 'info', 'error' => 'danger', 'success' => 'success'];

            foreach($types as $type)
            {
                if(method_exists($this->context, 'hasAdminMessage') && $this->context->hasAdminMessage($type))
                {
                    $messages = $this->context->getAdminMessage($type);
                    if(is_array($messages))
                        $messages = implode('<br/>', $messages);
                    echo '<div id="admin-message-'.$type.'" class="alert-'.$csstypes[$type].' alert fade in">';
                    //echo '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
                    echo $messages.'</div>';
                    /*echo Alert::widget([
                        'options' => ['class' => 'alert-'.$type],
                        'body' => $this->context->getAdminMessage($type),
                    ]);*/
                }
            }
            if (!empty($this->params['menu']))
            {
                echo '<div class="col-xs-12 text-right">';
                echo \yii\widgets\Menu::widget([
                    'options' => ['class' => 'list-inline'],
                    'itemOptions' => ['class' => ''],
                    'linkTemplate' => '<a href="{url}" class="btn btn-gray">{label}</a>',
                    'items' => $this->params['menu'],
                ]);
                echo '</div>';
            }
            ?>
                <div class="col-xs-12">
                <?php echo $content; ?>
                </div>
            <div class="clearfix"></div>
          </div>
          <!--p= index.footer-->
          <footer></footer>
        </div>
      </main>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>