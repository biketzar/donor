<?php
use yii\bootstrap\Alert;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\modules\privatepanel\assets\PrivateAsset;
use app\modules\privatepanel\assets\PrivatePrintAsset;
use app\modules\privatepanel\components\Helper;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $content string */
//AppAsset::register($this);
PrivateAsset::register($this);
PrivatePrintAsset::register($this);

$controller = $this->context;
$menus = \yii::$app->getModule('private')->menus;
$route = $controller->route;
$menus = Helper::getPrivateMenu($menus, $route);
if(!isset($this->params['breadcrumbs']))
{
    $this->params['breadcrumbs'] = [];
}

$assetsmanager = $this->getAssetManager();
$baseUrl = $assetsmanager->bundles['app\modules\privatepanel\assets\PrivateAsset']->baseUrl;

?>
<?php $this->beginPage() ?>
<!DOCTYPE html><!--[if lt IE 7]>      <html class="no-js lt-ie10 lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie10 lt-ie9 lt-ie8 ie7"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie10 lt-ie9 ie8"> <![endif]-->
<!--[if IE 9]>         <html class="no-js lt-ie10 ie9"> <![endif]-->
<!--[if gt IE 9]>      <html class="no-js gt-ie9"> <![endif]-->
<!--[if !IE] <![IGNORE[--><!--[IGNORE[]]--> <html class="no-js noie">
<!-- миксины-->
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="description" content="">
    <!--meta(name="viewport", content="width=device-width, initial-scale=1")-->
    <link rel="shortcut icon" href="/web/favicon.ico">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>

<body>
<?php $this->beginBody() ?>
      <main>
    <div class="admin__bar">
      
      
          <?php echo $content; ?>
        
      
    </div>
  </main>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>