<?php

use yii\bootstrap\NavBar;
use yii\bootstrap\Nav;
use yii\bootstrap\Alert;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

/* @var $this \yii\web\View */
/* @var $content string */
AppAsset::register($this);

$controller = $this->context;
$menus = \yii::$app->getModule('private')->menus;
$route = $controller->route;
foreach ($menus as $i => $menu) {
    $menus[$i]['active'] = strpos($route, trim($menu['url'][0], '/')) === 0;
}

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body>
        <?php $this->beginBody() ?>
        <div class="wrap">
        <?php
        NavBar::begin([
            'brandLabel' => false,
            'options' => ['class' => 'navbar-inverse navbar-fixed-top'],
        ]);

        if (!empty($this->params['top-menu']) && isset($this->params['nav-items'])) {
            echo Nav::widget([
                'options' => ['class' => 'nav navbar-nav'],
                'items' => $this->params['nav-items'],
            ]);
        }
        
        /*echo Nav::widget([
            'options' => ['class' => 'nav navbar-nav navbar-right'],
            'items' => $this->context->module->navbar,
         ]);*/
        NavBar::end();
        ?>
        
        <div class="container">
            <div class="row">
                
            <?php
            if (!empty($this->params['menu'])) {
                echo Nav::widget([
                    'options' => ['class' => 'nav navbar-nav navbar-right'],
                    'items' => $this->params['menu'],
                ]);
            }
            ?>
                
            </div>
            <div class="row">
                <div class="col-lg-3">
                    <div id="manager-menu" class="list-group">
                        <?php
                        echo \yii\widgets\Menu::widget([
                            'options' => ['class' => ''],
                            'items' => $menus,
                        ]);
                        /*foreach ($menus as $menu) {
                            $label = Html::tag('i', '', ['class' => 'glyphicon glyphicon-chevron-right pull-right']) .
                                Html::tag('span', Html::encode($menu['label']), []);
                            $active = $menu['active'] ? ' active' : '';
                            echo Html::a($label, $menu['url'], [
                                'class' => 'list-group-item' . $active,
                            ]);
                        }*/
                        ?>
                    </div>
                </div>
                <div class="col-lg-9">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?php
            $types = ['info', 'error', 'success'];
            foreach($types as $type)
            {
                if(method_exists($this->context, 'hasAdminMessage') && $this->context->hasAdminMessage($type))
                {

                    echo Alert::widget([
                        'options' => ['class' => 'alert-'.$type],
                        'body' => $this->context->getAdminMessage($type),
                    ]);
                }
            }
            ?>
            <?= $content ?>
                </div>
            </div>
        </div>
        </div>

        <footer class="footer">
            <div class="container">
                <p class="pull-right"><?= Yii::powered() ?></p>
            </div>
        </footer>

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
