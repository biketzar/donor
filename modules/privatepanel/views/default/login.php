<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \app\modules\privatepanel\Module;
use app\modules\privatepanel\assets\PrivateAsset;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

PrivateAsset::register($this);

$this->title = Module::t('app', 'Login');
$this->params['breadcrumbs'][] = $this->title;

$assetsmanager = $this->getAssetManager();
$baseUrl = $assetsmanager->bundles['app\modules\privatepanel\assets\PrivateAsset']->baseUrl;

?>
<div class="admin__bar-inner text-center">
    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'options' => ['class' => ''],
        'fieldConfig' => [
            'template' => "{input}\n",
            'labelOptions' => ['class' => ''],
            //'inputOptions' => ['class' => '', 'size' => 10, 'maxlength'=>30],
        ],
    ]); ?>

    <?php echo $form->errorSummary($model, ['header' => '']); ?>
	<?php
            $types = ['info', 'error', 'success'];
            $csstypes = ['info' => 'info', 'error' => 'danger', 'success' => 'success'];
            
            foreach($types as $type)
            {
                if(\Yii::$app->session->hasFlash('login_message_'.$type))
                {
                    $messages = \Yii::$app->session->getFlash('login_message_'.$type);
                    if(is_array($messages))
                        $messages = implode('<br/>', $messages);
                    echo '<div id="admin-message-'.$type.'" class="alert1-'.$csstypes[$type].' alert1 fade1 in1">';
                    //echo '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
                    echo $messages.'</div>';
                }
            }
            ?>
    <div class="middle-block"><a href="http://www.alkodesign.ru/alchemy_yii/" target="_blank"><img src="<?php echo $baseUrl;?>/images/logo.png" alt=""></a></div>
    <div class="middle-block">
        <div class="form__element">

            <?= $form->field($model, 'username', ['options' => ['class' => 'form-group margtop10'], 'inputOptions' => ['placeholder' => $model->getAttributeLabel('username'), 'class' => 'form-control']]) ?>

            <?= $form->field($model, 'password', ['inputOptions' => ['placeholder' => $model->getAttributeLabel('password'), 'class' => 'form-control']])->passwordInput() ?>
            <div class="form-group">
                <?= Html::submitButton(Module::t('app', 'Login button'), ['class' => 'btn btn-blue btn-block', 'name' => 'login-button']) ?>
            </div>
            <div class="form-group">
                <div class="pass_remember"><a href="javascript: void(0);"><?php echo \yii::t('modules/privatepanel/app', 'Forget password');?></a><i class="fa fa-angle-down"></i></div>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

        <div class="admin__bar-inner-password text-left" style="display: none;">
          <?php $form = ActiveForm::begin([
                'id' => strtolower($forgetmodel->formName()).'-form',
                'options' => ['class' => ''],
                'fieldConfig' => [
                    'template' => "{input}\n",
                    'labelOptions' => ['class' => ''],
                ],
                'enableAjaxValidation' => true,
                'enableClientValidation' => true,
            ]); ?>
            <?php echo $form->errorSummary($forgetmodel); ?>
            <h4><?php echo \yii::t('modules/privatepanel/app', 'Recovery of password');?></h4>
            <div class="text-center">
              <div class="form-group col-xs-1"></div>
              <div class="form-group col-xs-5">

              <?= $form->field($forgetmodel, 'email', ['options'=>['class' => 'row'],  'inputOptions' => ['placeholder' => $forgetmodel->getAttributeLabel('email'), 'class' => 'form-control']]) ?>

              </div>
              <div class="form-group col-xs-5">
                <?= Html::submitButton(\yii::t('modules/privatepanel/app', 'Send'), ['class' => 'btn btn-blue btn-block']) ?>
              </div>
              <div class="clearfix"></div>
            </div>
          <?php ActiveForm::end(); ?>
        </div>
</div>
    <?php

    if($forgetmodel->hasErrors())
    {
        $js = <<<JS
      jQuery('.pass_remember').trigger('click');
JS;
        $this->registerJs($js, \yii\web\View::POS_LOAD);
    }
    ?>