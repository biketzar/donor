<?php
$this->params['breadcrumbs'] = $privateModelAdmin->updateBreadcrumbs;
$this->params['menu'] = $privateModelAdmin->updateMenu;

$this->title = $privateModelAdmin->updateHeader;

echo $this->context->renderPartial($this->context->formTemplate, ['privateModelAdmin'=>$privateModelAdmin]); 
?>