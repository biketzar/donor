<?php

use yii\widgets\DetailView;

$this->params['breadcrumbs'] = $privateModelAdmin->viewBreadcrumbs;
$this->params['menu'] = $privateModelAdmin->viewMenu;

$this->title = $privateModelAdmin->viewHeader;
?>

<?php /*$this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=> $model->admin->viewAttributes,
	'itemCssClass' => array('dark', 'light'),
	'itemTemplate' => '<tr><th>{label}</th><td class="{class}">{value}</td></tr>'
));*/
echo DetailView::widget([
    'model' => $privateModelAdmin->getModel(),
    'attributes' => 
        $privateModelAdmin->getViewAttributes()
    ,
]);
?>

<?php
/*foreach($model->getAdmin()->extendRelations() as $key => $opts){
	if($opts['displayOnView']){
		$dataProvider=new CActiveDataProvider($opts[1], array(
			'criteria'=>new CDbCriteria(array(
				'condition' => $opts[2].'='.$model->primaryKey
			)),
			'pagination' => false,
		));
		$fields = ($dataProvider->model->getAdmin()->getAdminFields());
		$pos = array_search($opts[2], $fields);
		if($pos !== false){
			unset($fields[$pos]);
			$fields = array_merge($fields);
		}
		echo '<h4 align="center">'.$dataProvider->model->getAdmin()->pluralName.'</h4>';
		$this->widget('private.widgets.PSimpleListView', array(
			'dataProvider' => $dataProvider,
			'fields' => $fields,
			'relName' => $key,
			'relOpts' => $opts,
			'parentModel' => $model,
		));
	}
}*/
?>