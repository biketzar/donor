<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \app\modules\privatepanel\Module;
use app\modules\privatepanel\assets\PrivateAsset;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

PrivateAsset::register($this);

$this->title = Module::t('app', 'Login');
$this->params['breadcrumbs'][] = $this->title;

$assetsmanager = $this->getAssetManager();
$baseUrl = $assetsmanager->bundles['app\modules\privatepanel\assets\PrivateAsset']->baseUrl;
if($model):
?>
<div class="admin__bar-inner-password admin__bar-inner text-center">
<h4><?php echo \yii::t('modules/privatepanel/app', 'Recovery of password');?></h4>
<?php $form = ActiveForm::begin([
         'id' => strtolower($model->formName()).'-form',
                'options' => ['class' => ''],
                'fieldConfig' => [
                    'template' => "{input}\n",
                    'labelOptions' => ['class' => ''],
                ],
                'enableAjaxValidation' => false,
                'enableClientValidation' => true,
    ]); ?>
    <?php echo $form->errorSummary($model); ?>
    <div class="middle-block"><a href="http://www.alkodesign.ru/alchemy_yii/" target="_blank"><img src="<?php echo $baseUrl;?>/images/logo.png" alt=""></a></div>
    <div class="middle-block">
        <div class="form__element">
        <?= $form->field($model, 'password', ['inputOptions' => ['placeholder' => $model->getAttributeLabel('password'), 'class' => 'form-control']])->passwordInput(['value' => '']) ?>

        <?= $form->field($model, 'password_repeat', ['inputOptions' => ['placeholder' => $model->getAttributeLabel('password_repeat'), 'class' => 'form-control']])->passwordInput(['value' => '']) ?>

        
        <div class="form-group">
          <?= Html::submitButton(\yii::t('modules/privatepanel/app', 'Save'), ['class' => 'btn btn-blue btn-block']) ?>
        </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>
<?php
endif;
?>