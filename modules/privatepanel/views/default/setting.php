<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Json;
use app\modules\privatepanel\Module as Module;

mihaildev\elfinder\AssetsCallBack::register($this);
$this->registerJs("mihaildev.elFinder.register(".Json::encode("tinymce_callback").",".Json::encode(new \yii\web\JsExpression('function(file){top.tinymce.activeEditor.windowManager.getParams().oninsert(file); top.tinymce.activeEditor.windowManager.close();}')).");");
$this->params['breadcrumbs'] = $privateModelAdmin->adminBreadcrumbs;
$this->params['breadcrumbs'][1] = ["label"=>'Настройки'];
?>
<?php $form = ActiveForm::begin([
	'id'=>strtolower($model->formName()).'-form',
	'enableAjaxValidation' => $this->context->enableAjaxValidation,
	'enableClientValidation' => false,
	'options' => array(
		'enctype' => 'multipart/form-data',
	)
]); ?>

	<table class="table table-striped table-hover" id="viewTable">
            <tbody><!--tr>
		<td class="dark">&nbsp;</td>
		<th>Показывать</th>
	</tr-->

<?php 
$class = '';
$labels = $model->attributeLabels();
$counter = 0;
foreach ( $allColumns as $field) : 
    $class = ($class != 'dark') ? 'dark' : 'light';
        ?>
        <tr id="<?=$counter?>">
            <th><?=isset($labels[$field]) ? $labels[$field] : $field?>
            </th>
            <td class="<?=$class?>" style="text-align: center" width="50">
                <input type="checkbox" name="Settings[<?=$field?>]" <?=in_array($field, $columns) ? 'checked="checked"' : ''?>/>
            </td>
            <td class="<?=$class?>" style="text-align: center" width="100">
                <span style="cursor: pointer;font-size: 12px;" class="btn btn-lilac btn-sm" onclick="ordering(this.parentNode.parentNode, 'down')">&#9660;</span>&#160;
                <span style="cursor: pointer;font-size: 12px;" class="btn btn-lilac btn-sm" onclick="ordering(this.parentNode.parentNode, 'up')">&#9650;</span>
            </td>
        </tr>
        <?php
        $counter++;
endforeach;?>
        
<tr>
        <td class="dark submit-row" colspan="3" style="text-align: center;">
<?= Html::submitButton('<i class="fa fa-floppy-o"></i> '.Module::t('app', 'Save'), ['class' => 'btn btn-success', 'name'=>'save']) ?>&nbsp;
        </td>
</tr>
</tbody></table>

<?php echo Html::hiddenInput('savetype', 'save');?>

<?php ActiveForm::end() ?>
<script>
function ordering(elem, direction)
{
    var table = document.getElementById('viewTable');
    var rows = table.getElementsByTagName('TR');
    //table.innerHTML = '';
    for(var i=0;i<rows.length;i=i+1)
    {
        if(direction == 'up' && i > 0 && elem === rows[i])
        {
            var temp1 = rows[i-1].cloneNode(true);
            var temp2 = rows[i].cloneNode(true);
            rows[i].parentNode.replaceChild(temp1, rows[i]);
            rows[i].parentNode.replaceChild(temp2, rows[i-1]);
        }
        else if(direction == 'down' && i < rows.length-2 && elem === rows[i])
        {
            var temp1 = rows[i+1].cloneNode(true);
            var temp2 = rows[i].cloneNode(true);
            rows[i].parentNode.replaceChild(temp1, rows[i]);
            rows[i].parentNode.replaceChild(temp2, rows[i+1]);
        }
    }    
}

</script>