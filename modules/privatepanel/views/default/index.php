<?php
use app\modules\privatepanel\Module as Module;
$this->title = Module::t('app', 'Main panel');
?>

<div class="col-xs-12">
    <div class="col-xs-4 margbot20"><a href="/private/page" class="panel btn-success text-center">
        <p><i class="fa fa-file-o fa-3x margbot20"></i><br>Страницы</p></a>
    </div>
    <div class="col-xs-4 margbot20"><a href="/privateuser/user" class="panel btn-primary text-center">
        <p><i class="fa fa-users fa-3x margbot20"></i><br>Пользователи</p></a>
    </div>
    <div class="col-xs-4 margbot20"><a href="/private/page" class="panel btn-green text-center">
        <p><i class="fa fa-book fa-3x margbot20"></i><br>Инструкции</p></a>
    </div>
    <div class="col-xs-4 margbot20"><a href="/private/error-log/show" class="panel btn-danger text-center">
            <p><i class="fa fa-cube fa-3x margbot20"></i><br>Ошибка записи логов</p></a>
    </div>
    <?php if($metrikaData !== FALSE && !isset($metrikaData['error'])){ ?>
    <div class="col-xs-4 margbot20">
        <a href="https://metrika.yandex.ru/dashboard?id=<?=$metrikaData['id']?>" target="_blank" style="text-decoration:none;">
            <div class="panel panel-unlink btn-default">
                <div class="middle-block">
                  <p><span class="font-xmd">Статистика</span><br><span class="font-sm">за <?=date('d.m.Y')?></span></p>
                </div>
                <div class="middle-block text-right align-top"><i class="fa fa-bar-chart fa-2x"></i></div>
                <div class="text-center">
                  <p><span class="font-md">Количество просмотров: </span><span class="font-lg" style="font-size: 30px;"><?=$metrikaData['views']?></span></p>
                  <p><span class="font-md">Количество визитов: </span><span class="font-lg" style="font-size: 30px;"><?=$metrikaData['visits']?></span></p>
                  <p><span class="font-md">Уникальных посетителей: </span><span class="font-lg" style="font-size: 30px;"><?=round((float)$metrikaData['users']*100/(float)$metrikaData['visits'], 2)?>%</span></p>
                </div>
            </div>
        </a>
    </div>
    <?php }else{; ?>
    <div class="col-xs-4 margbot20">
       <a href="/private/project-settings/" style="text-decoration:none;">
            <div class="panel panel-unlink btn-default">
                <div class="middle-block">
                    <p><span class="font-xmd">Статистика</span><br></p>

                </div>
                <div class="middle-block text-right align-top"><i class="fa fa-bar-chart fa-2x"></i></div>
                <div class="text-center">
                    <p onmouseover="document.getElementById('errorMessage1121').style.display='block';" onmouseout="document.getElementById('errorMessage1121').style.display='none';">Небходимо настроить</p>
                      <p id="errorMessage1121"style="text-align:left;display:none;position:absolute;">
                      <?php if(isset($metrikaData['error']))
                      {
                          print_r($metrikaData['error']);
                          ?>
                          
                      <?php
                      }
                      ?>
                      </p>
                </div>
            </div>
        </a>
    </div>
    <?php } ?>
</div>