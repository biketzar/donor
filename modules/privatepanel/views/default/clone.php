<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
//use yii\web\View;
use yii\widgets\DetailView;
use app\modules\privatepanel\Module as Module;

$this->params['breadcrumbs'] = $privateModelAdmin->cloneBreadcrumbs;
$this->params['menu'] = $privateModelAdmin->cloneMenu;

$this->title = $privateModelAdmin->cloneHeader;
$model = $privateModelAdmin->getModel();
?>
<?php $form = ActiveForm::begin([
	'id'=>strtolower($this->context->getModelName()).'-form',
	'enableAjaxValidation' => $this->context->enableAjaxValidation,
	
	'enableClientValidation' => true,
	/*'clientOptions' => array(
		'validateOnSubmit' => false,
		'validateOnChange' => true,
	),*/
	'options' => array(
		'enctype' => 'multipart/form-data',
	)
]); ?>

<?php echo $form->errorSummary($model); ?>
<?php echo DetailView::widget([
    'model' => $model,
    'attributes' => 
        $privateModelAdmin->getViewAttributes()
    ,
]);
?>
<?php 
/*$class = '';
foreach ( $privateModelAdmin->filterFormFields() as $field) : 
    $class = ($class != 'dark') ? 'dark' : 'light';
    //echo $privateModelAdmin->getFieldHtml($form, $field, []);
    echo $privateModelAdmin->getField($field)->asActiveForm($form, array(
			'template' => '<div class="row">{label}{help}{input}{error}</div>',
			'htmlOptions' => array()));
endforeach;*/?>
<table class="table table-striped table-bordered detail-view">
    <tbody>
        <tr>
            <td class="submit-row">
                <?= Html::submitButton('<i class="fa fa-plus"></i> '.Module::t('app', 'Clone'), ['class' => 'btn btn-success', 'name'=>'save']) ?>&nbsp;
<?php echo Html::a('<i class="fa fa-times-circle-o"></i> '.Module::t('app', 'Cancel'), array_merge(['admin'], $privateModelAdmin->additionalUrlParams), ['class' => 'btn btn-gray']); ?>
            </td>
        </tr>
    </tbody>
</table>
<?php ActiveForm::end() ?>

