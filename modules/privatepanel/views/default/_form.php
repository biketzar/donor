<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Json;
use app\modules\privatepanel\Module as Module;

mihaildev\elfinder\AssetsCallBack::register($this);
$this->registerJs("mihaildev.elFinder.register(".Json::encode("tinymce_callback").",".Json::encode(new \yii\web\JsExpression('function(file){top.tinymce.activeEditor.windowManager.getParams().oninsert(file); top.tinymce.activeEditor.windowManager.close();}')).");");


$model = $privateModelAdmin->getModel();
?>
<?php $form = ActiveForm::begin([
	'id'=>strtolower($model->formName()).'-form',
	'enableAjaxValidation' => $this->context->enableAjaxValidation,
	
	'enableClientValidation' => false,
	/*'clientOptions' => array(
		'validateOnSubmit' => false,
		'validateOnChange' => true,
	),*/
	'options' => array(
		'enctype' => 'multipart/form-data',
	)
]); ?>

<?php echo $form->errorSummary($model); ?>
    <table cellspacing="1" cellpadding="3" border="0" class="table table-striped table-editing">
        <thead>
            <tr>
                <th>&nbsp;</th>
                <th><?php echo Module::t('app', $model->isNewRecord ? 'Create' : 'Update')?></th>
            </tr>
        </thead>
    <tbody>
<?php 
$class = '';
foreach ( $privateModelAdmin->filterFormFields() as $field) : 
    /*if(!$model->getBehavior('ml') || !in_array($field, $model->getBehavior('ml')->localizedAttributes))
    {*/
        echo $privateModelAdmin->getField($field)->asActiveForm($form, array(
			'template' => '<tr><td>{label}{help}</td><td>{input}{error}</td></tr>',
			//'htmlOptions' => ['class' => ''],
            ));
    /*} else {
        foreach(\Yii::$app->params['translatedLanguages'] as $l=>$lang):
            if($l !== \Yii::$app->params['defaultLanguage']) 
                $suffix = '_' . $l;
            else 
                $suffix = '_' . $l;
            echo $privateModelAdmin->getField($field.$suffix)->asActiveForm($form, array(
			'template' => '<tr><th>{label}{help}</th><td class="'.$class.'">{input}{error}</td></tr>',
			'htmlOptions' => ['class' => ''],
			//'labelOptions'=>['label' => $model->getAttributeLabel($field).' ('.$lang.')']
                ));
        endforeach;
    }*/
endforeach;?>
    </tbody>      
    <tfoot>
        <tr>
            <td>&nbsp;</td>
            <td>
        <?= Html::submitButton('<i class="fa fa-floppy-o"></i> '.Module::t('app', $model->isNewRecord ? 'Create' : 'Save'), ['class' => 'btn btn-success', 'name'=>'save']) ?>&nbsp;
        <?= Html::submitButton('<i class="fa fa-check-circle-o"></i> '.Module::t('app', 'Save & continue edit'), ['class' => 'btn btn-primary', 'onclick'=>'jQuery(this).closest("form").find("[name=\"savetype\"]").val("apply")', 'name'=>'apply']) ?>&nbsp;
        <?php echo Html::a('<i class="fa fa-times-circle-o"></i> '.Module::t('app', 'Cancel'), array_merge(['admin'], $privateModelAdmin->additionalUrlParams), ['class' => 'btn btn-gray']); ?>
            </td>
        </tr>
    </tfoot>
</table>

<?php echo Html::hiddenInput('savetype', 'save');?>

<?php /*$this->registerJs(<<<JS
var submited = false;
$("#{$form->id}").bind('submit', function(){
if (!submited){submited = true;}else{return false;}
});
JS
,
View::POS_END, 
'create-from'
)*/?>

<?php ActiveForm::end() ?>
