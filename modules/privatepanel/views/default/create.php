<?php
$this->params['breadcrumbs'] = $privateModelAdmin->createBreadcrumbs;
$this->params['menu'] = $privateModelAdmin->createMenu;

$this->title = $privateModelAdmin->createHeader;

echo $this->context->renderPartial($this->context->formTemplate, ['privateModelAdmin'=>$privateModelAdmin]); 
?>
