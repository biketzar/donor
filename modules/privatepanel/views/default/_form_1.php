<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Json;
use app\modules\privatepanel\Module as Module;

mihaildev\elfinder\AssetsCallBack::register($this);
$this->registerJs("mihaildev.elFinder.register(".Json::encode("tinymce_callback").",".Json::encode(new \yii\web\JsExpression('function(file){top.tinymce.activeEditor.windowManager.getParams().oninsert(file); top.tinymce.activeEditor.windowManager.close();}')).");");


$model = $privateModelAdmin->getModel();
?>
<?php $form = ActiveForm::begin([
	'id'=>strtolower($model->formName()).'-form',
	'enableAjaxValidation' => $this->context->enableAjaxValidation,
	
	'enableClientValidation' => false,
	/*'clientOptions' => array(
		'validateOnSubmit' => false,
		'validateOnChange' => true,
	),*/
	'options' => array(
		'enctype' => 'multipart/form-data',
	)
]); ?>

<?php echo $form->errorSummary($model); ?>

<?php echo Module::t('app', $model->isNewRecord ? 'Create' : 'Update')?>

<?php 
$class = '';
foreach ( $privateModelAdmin->filterFormFields() as $field) : 
    $class = ($class != 'dark') ? 'dark' : 'light';
    if(!$model->getBehavior('ml') || !in_array($field, $model->getBehavior('ml')->localizedAttributes))
    {
        echo $privateModelAdmin->getField($field)->asActiveForm($form, array(
			'template' => '<div class="row">{label}{help}{input}{error}</div>',
			'htmlOptions' => array()));
    } else {
        foreach(\Yii::$app->params['translatedLanguages'] as $l=>$lang):
                if($l !== \Yii::$app->params['defaultLanguage']) 
                    $suffix = '_' . $l;
                else 
                    $suffix = '_' . $l;
            echo $privateModelAdmin->getField($field.$suffix)->asActiveForm($form, array(
			'template' => '<div class="row">{label}{help}{input}{error}</div>',
			'htmlOptions' => [], 'labelOptions'=>['label' => $model->getAttributeLabel($field).' ('.$lang.')']));
        endforeach;
    }
endforeach;?>

<?= Html::submitButton(Module::t('app', $model->isNewRecord ? 'Create' : 'Save'), ['class' => 'btn btn-primary', 'name'=>'save']) ?>
<?= Html::submitButton(Module::t('app', 'Save & continue edit'), ['class' => 'btn', 'onclick'=>'jQuery(this).closest("form").find("[name=\"savetype\"]").val("apply")', 'name'=>'apply']) ?>
<?php if(!$model->isNewRecord) echo Html::a(Module::t('app', 'Cancel'), array_merge(['admin'], $privateModelAdmin->additionalUrlParams)); ?>
<?php echo Html::hiddenInput('savetype', 'save');?>

<?php /*$this->registerJs(<<<JS
var submited = false;
$("#{$form->id}").bind('submit', function(){
if (!submited){submited = true;}else{return false;}
});
JS
,
View::POS_END, 
'create-from'
)*/?>

<?php ActiveForm::end() ?>
