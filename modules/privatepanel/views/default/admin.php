<?php

use yii\grid\GridView;

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

use app\modules\privatepanel\Module as Module;

$this->params['breadcrumbs'] = $privateModelAdmin->adminBreadcrumbs;
$this->params['menu'] = $privateModelAdmin->adminMenu;
$this->title = $privateModelAdmin->adminHeader;

/*
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('".strtolower( get_class($model))."-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
*/

?>
<?php 
    $getData = \Yii::$app->request->get();
	
    $model = $privateModelAdmin->getModel();
    $sort = $dataProvider->getSort();
    
    if($use_tree
            && $model->hasAttribute($this->context->treeParentAttribute)
            && $model->getRelation($this->context->treeChildRelation, false)
            ){
            
            /*$privateModelAdmin->treeViewEnable = $this->context->isTreeAdminView;
            $privateModelAdmin->treeViewParentField = $privateModelAdmin->treeViewParentField;
            $privateModelAdmin->treeViewChildRelation = $privateModelAdmin->treeViewChildRelation;*/
            //$dataProvider->pagination = false;
            //$dataProvider->pagination = false;

            echo GridView::widget(array(
                    'id'=>'settings-grid',
                    'dataProvider'=>$dataProvider,
                    'tableOptions' => ['class' => 'table table-striped table-hover'],
                    //'template'=>"{summary}\n{items}\n{pageSizer}\n{pager}",
                    'filterModel'=>$model,
                    'columns'=> $privateModelAdmin->getGridColumns(),
                    'summary' => '<div class="summary">'.\yii::t('modules/privatepanel/app', 'Showing parents <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}.').'</div>',
                    'layout' => "{items}\n<div class=\"middle-block\">{summary}</div>\n<div class=\"middle-block text-right\">{pager}</div>",
                    //'cssFile'=>false,
                    //'ajaxUpdate'=>false,
                    //'pager' => false
            )); 
    } else{
            echo GridView::widget(array(
                    'id'=>'settings-grid',
                    'dataProvider'=>$dataProvider,
                    'filterModel'=>$privateModelAdmin->getModel(),
                    'columns'=> $privateModelAdmin->getGridColumns(),
                    'tableOptions' => ['class' => 'table table-striped table-hover'],
                    'layout' => "{items}\n<div class=\"middle-block\">{summary}</div>\n<div class=\"middle-block text-right\">{pager}</div>",
                    //'cssFile'=>false,
                    //'ajaxUpdate'=>false,
                    /*'pager' => array(
                            'class' => 'CLinkPager',
                            'cssFile' => null,
                    ),*/
            )); 
    }
$pagiation = $dataProvider->getPagination();

if($pagiation && $dataProvider->getTotalCount() > 0)
{
    $form = ActiveForm::begin([
        'action' => Url::current([$pagiation->pageSizeParam => null]), 'method' =>'get'
    ]);
    
    echo Html::label(Module::t('app', 'Show by').': ', $pagiation->pageSizeParam).' ';
    echo Html::textInput($pagiation->pageSizeParam, $pagiation->getPageSize(), ['size' => 4, 'class' => '']).' ';
    echo Html::submitButton(Module::t('app', 'Set param'), ['class' => 'btn']);
    ActiveForm::end();
}
?>
	
 