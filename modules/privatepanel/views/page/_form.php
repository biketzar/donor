<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\Json;
use app\modules\privatepanel\Module as Module;

mihaildev\elfinder\AssetsCallBack::register($this);
$this->registerJs("mihaildev.elFinder.register(".Json::encode("tinymce_callback").",".Json::encode(new \yii\web\JsExpression('function(file){top.tinymce.activeEditor.windowManager.getParams().oninsert(file); top.tinymce.activeEditor.windowManager.close();}')).");");


$model = $privateModelAdmin->getModel();

$headers = \Yii::$app->getRequest()->getHeaders();
$is_pjax = $headers->get('X-Pjax') && $headers->get('X-Pjax-Content') === strtolower($model->formName()).'-form-container';

?>
<?php echo '<div id="'.strtolower($model->formName()).'-form-container'.'">';//\yii\widgets\Pjax::begin(['id'=>strtolower($model->formName()).'-form-container']); 
if($is_pjax)
{
    ob_start();
    ob_implicit_flush(false);
    $this->clear();
}
?>
<?php $form = ActiveForm::begin([
	'id'=>strtolower($model->formName()).'-form',
	//'enableAjaxValidation' => $this->context->enableAjaxValidation,
	'enableAjaxValidation' => false,
	'enableClientValidation' => true,
	'options' => array(
		'enctype' => 'multipart/form-data',
                //'data-pjax' => true
	),
        
]); ?>

<?php echo $form->errorSummary($model); ?>
    <table cellspacing="1" cellpadding="3" border="0" class="table table-striped table-editing">
        <thead>
            <tr>
                <th>&nbsp;</th>
                <th><?php echo Module::t('app', $model->isNewRecord ? 'Create' : 'Update')?></th>
            </tr>
        </thead>
    <tbody>
<?php 
$class = '';
foreach ( $privateModelAdmin->filterFormFields() as $field) : 
    /*if(!$model->getBehavior('ml') || !in_array($field, $model->getBehavior('ml')->localizedAttributes))
    {*/
        echo $privateModelAdmin->getField($field)->asActiveForm($form, array(
			'template' => '<tr><td>{label}{help}</td><td>{input}{error}</td></tr>',
			//'htmlOptions' => ['class' => ''],
            ));
    /*} else {
        foreach(\Yii::$app->params['translatedLanguages'] as $l=>$lang):
            if($l !== \Yii::$app->params['defaultLanguage']) 
                $suffix = '_' . $l;
            else 
                $suffix = '_' . $l;
            echo $privateModelAdmin->getField($field.$suffix)->asActiveForm($form, array(
			'template' => '<tr><th>{label}{help}</th><td class="'.$class.'">{input}{error}</td></tr>',
			'htmlOptions' => ['class' => ''],
			//'labelOptions'=>['label' => $model->getAttributeLabel($field).' ('.$lang.')']
                ));
        endforeach;
    }*/
endforeach;?>
    </tbody>      
    <tfoot>
        <tr>
            <td>&nbsp;</td>
            <td>
        <?= Html::submitButton('<i class="fa fa-floppy-o"></i> '.Module::t('app', $model->isNewRecord ? 'Create' : 'Save'), ['class' => 'btn btn-success', 'name'=>'save']) ?>&nbsp;
        <?= Html::submitButton('<i class="fa fa-check-circle-o"></i> '.Module::t('app', 'Save & continue edit'), ['class' => 'btn btn-primary', 'onclick'=>'jQuery(this).closest("form").find("[name=\"savetype\"]").val("apply")', 'name'=>'apply']) ?>&nbsp;
        <?php echo Html::a('<i class="fa fa-times-circle-o"></i> '.Module::t('app', 'Cancel'), array_merge(['admin'], $privateModelAdmin->additionalUrlParams), ['class' => 'btn btn-gray']); ?>
            </td>
        </tr>
    </tfoot>
</table>

<?php echo Html::hiddenInput('savetype', 'save');?>

<?php ActiveForm::end() ?>
<?php
//\yii\widgets\Pjax::end();  
if ($is_pjax) {
    $this->endBody();

    // Do not re-send css files as it may override the css files that were loaded after them.
    // This is a temporary fix for https://github.com/yiisoft/yii2/issues/2310
    // It should be removed once pjax supports loading only missing css files
    $this->cssFiles = null;

    $this->endPage(true);

    $content = ob_get_clean();

    // only need the content enclosed within this widget
    $response = \Yii::$app->getResponse();
    $response->clearOutputBuffers();
    $response->setStatusCode(200);
    $response->format = \yii\web\Response::FORMAT_HTML;
    $response->content = $content;
    $response->send();

    \Yii::$app->end();
}
echo '</div>';
$url = Url::to("getHandlerData");

$js = <<<JS
      $('body').on('change', '[name="Page[handler]"]', function() {
        //$(document, "Page['handler']").on('change', function(){
            var select_values = {};
            $('#{$form->getID()} select').not('[name="Page[handler]"]').each(function(){
                select_values[$(this).attr('name')] = $(this).val();
            });
            //$.pjax.reload({container:'#{$form->getID()}'}); 
            //$.pjax.reload({container:'#{$form->getID()}'}); 
            $.ajax({
                type: "get",
                //url: "{$url}",
                url: $('#{$form->getID()}').attr('action'),
                data: $('#{$form->getID()}').serialize(),
                beforeSend: function( xhr ) {
                    xhr.setRequestHeader('X-PJAX', 'true');
                    xhr.setRequestHeader('X-PJAX-Content', '{$form->getID()}-container');
                  },
                success: function (result) {
                    $("#{$form->getID()}-container").replaceWith(result);
                    
                    for(name in select_values)
                    {
                        $("#{$form->getID()} select[name=\""+name+"\"]").val(select_values[name]);
                    }
                        
                    if(typeof(window.Select2) != 'undefined')
                        $('#{$form->getID()}-container select').select2({});
                    //$.pjax.reload({container:'#{$form->getID()}'});
                }
            });
            
        });
JS;
$this->registerJs($js);
?>