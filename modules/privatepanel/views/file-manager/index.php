<?php
use mihaildev\elfinder\ElFinder;
use yii\web\JsExpression;

echo ElFinder::widget([
    'language'         => 'ru',
    'controller'       => 'elfinder', // вставляем название контроллера, по умолчанию равен elfinder
    'filter'           => ['image', 'application/plain-text', 'application/pdf', 
            'text/plain', 'application', /*'application/zip', 'application/msword',
            'application/vnd.ms-excel', 'application/vnd.oasis.opendocument.text', 'application/vnd.oasis.opendocument.spreadsheet',
            'application/vnd.oasis.opendocument.presentation', 'application/vnd.oasis.opendocument.graphics', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'application/vnd.ms-powerpoint', 'application/vnd.openxmlformats-officedocument.presentationml.presentation', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'application/x-rar-compressed', */ 'video', /*'video/mpeg', 'video/mp4',
            'video/x-ms-wmv', 'video/x-flv',*/ 'audio', 'text/csv', 'text/xml',
        ],    // фильтр файлов, можно задать массив фильтров https://github.com/Studio-42/elFinder/wiki/Client-configuration-options#wiki-onlyMimes
    'frameOptions' => ['style' => "width: 100%; height: 600px; border: 0;"],
    'callbackFunction' => new JsExpression('function(file, id){}') // id - id виджета
]);
?>
