
<p style="font-family: 'FregatRegular', Helvetica, Arial, sans-serif">Здравствуйте, <?php echo $params['NAME'];?>.</p>
<p style="font-family: 'FregatRegular', Helvetica, Arial, sans-serif">Вы запросили восстановление пароля на сайте:&nbsp;
    <a href="[WWW]">[WWW]</a>.</p>
<p style="font-family: 'FregatRegular', Helvetica, Arial, sans-serif">
    Для восстановления перейдите по:&nbsp;<a href="<?php echo $params['RECOVERY_URL'];?>"><?php echo $params['RECOVERY_URL'];?></a>.</p>
<hr/>
<p style="font-family: 'FregatRegular', Helvetica, Arial, sans-serif">Это письмо было создано автоматически, не отвечайте на него.</p>
