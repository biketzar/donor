
<p style="font-family: 'FregatRegular', Helvetica, Arial, sans-serif">Здравствуйте, <?php echo $params['NAME'];?>.</p>
<p style="font-family: 'FregatRegular', Helvetica, Arial, sans-serif">На сайте [WWW] был восстановлен пароль от личного кабинета.</p>
<p style="font-family: 'FregatRegular', Helvetica, Arial, sans-serif">Новые данные:</p>
<p style="font-family: 'FregatRegular', Helvetica, Arial, sans-serif">Логин: <?php echo $params['USERNAME'];?>.</p>
<p style="font-family: 'FregatRegular', Helvetica, Arial, sans-serif">Пароль: <?php echo $params['PASSWORD'];?>.</p>
<p style="font-family: 'FregatRegular', Helvetica, Arial, sans-serif">Это письмо было создано автоматически, не отвечайте на него.</p>
