<?php
$this->title = 'Отправка сообщений';
?>
<div class="col-md-8 offset2" style="padding-bottom: 10px;">
  <button class="btn btn-md btn-primary btn-start">Старт</button>
  <button class="btn btn-md btn-danger btn-stop" style="display: none;">Стоп</button>
</div>
<br/>
<div class="col-md-8 offset2">
    <div class="info-text">В очереди сообщений: <?=$total?></div>
    <progress value="0" max="<?=$total?>" style="width:100%"></progress>
    <ul class="info-list"></ul>
</div>
<script>
  window.addEventListener('load', function(){
    var stopFlag = true;
    function request()
    {
      if(stopFlag === false)
      {
        $.getJSON( location.href, {next: true, rand: Math.random()}, function( data ) {
          if(data.stop == 1)
          {
            $('button.btn-stop').click();
            $('.info-list').append($('<li></li>').html('END'));
            //$('.info-text').html($('progress').prop('value') + '/' + $('progress').prop('max'));
          }
          for(let i=0; i<data.items.length; i++)
          {
            $('.info-list').append($('<li></li>').html(data.items[i]));
          }
          $('progress').prop('value', $('progress').prop('max') - data.total);
          //$('.info-text').html($('progress').prop('value') + '/' + $('progress').prop('max'));
          request();
        });
      }
    }
    
    $('button.btn-start').click(function(){
      $('button.btn-stop').show();
      $('button.btn-start').hide();
      stopFlag = false;
      request();      
    });
    
    $('button.btn-stop').click(function(){
      $('button.btn-stop').hide();
      $('button.btn-start').show();
      stopFlag = true;
    })
  })
</script>