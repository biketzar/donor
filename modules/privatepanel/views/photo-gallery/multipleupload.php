<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \app\modules\privatepanel\Module;

$this->params['breadcrumbs'] = $privateModelAdmin->createBreadcrumbs;
$this->params['menu'] = $privateModelAdmin->createMenu;

$this->title = Module::t('app', 'Multiple upload');

app\components\PLUploadAssets::register($this);

$pluploadassets = new \app\components\PLUploadAssets;
?>
<?php echo Module::t('app', 'Multiple upload')?>
<div id="uploader">
    <p>Your browser doesn't have Flash, Silverlight or HTML5 support.</p>
</div>
 
<?php
$url = Url::to(['processupload'], true);
$js = <<<JS
        //$.fn.bootstrapBtn = $.fn.button.noConflict();
// Initialize the widget when the DOM is ready

    $("#uploader").plupload({
        // General settings
        runtimes : 'html5,flash,silverlight,html4',
        url : "{$url}",
 
        // Maximum file size
        max_file_size : '10mb',
 
        chunk_size: '1mb',
        
        // Resize images on clientside if we can
        //resize : {
        //    width : 200,
        //    height : 200,
        //    quality : 90,
        //    crop: true // crop to exact dimensions
        //},
 
        // Specify what files to browse for
        filters : [
            
            {title : "Image files", extensions : "jpg,gif,png"},
        ],
 
        // Rename files by clicking on their titles
        rename: true,
         
        // Sort files
        sortable: true,
 
        // Enable ability to drag'n'drop files onto the widget (currently only HTML5 supports that)
        dragdrop: true,
 
        // Views to activate
        views: {
            list: true,
            thumbs: false, // Show thumbs
            active: 'list'
        },
 
        // Flash settings
        flash_swf_url : '{$pluploadassets->getFlashUrl()}',
 
        // Silverlight settings
        silverlight_xap_url : '{$pluploadassets->getSilverLightUrl()}',
                
        init : {
            
            FilesRemoved: function(up, files) {
                // Called when files where removed from queue
                //console.log('[FilesRemoved]');
                
                plupload.each(files, function(file) {
                    //console.log('  File:', file);
                    $('#image-form').find('#image_'+file.id).remove();
                });
            },
 
            FileUploaded: function(up, file, info) {
                // Called when a file has finished uploading
                //console.log('[FileUploaded] File:', file, "Info:", info);
                $('#image-form').append('<input type="hidden" name="{$model->formName()}[image][]" id="image_'+file.id+'" value="'+file.name+'" />');
            },
            
        }
    });

JS;
$this->registerJs($js);
?>

<?php $form = ActiveForm::begin([
	'id'=>strtolower('image-form'),
	'enableAjaxValidation' => $this->context->enableAjaxValidation,
	
	'enableClientValidation' => true,
	/*'clientOptions' => array(
		'validateOnSubmit' => false,
		'validateOnChange' => true,
	),*/
	'options' => array(
	'enctype' => 'multipart/form-data',
	)
]); ?>

<?php echo $form->errorSummary($model); ?>

<br/>
<?= Html::submitButton('<i class="fa fa-floppy-o"></i> '.Module::t('app', 'Save'), ['class' => 'btn btn-primary', 'name'=>'save']) ?>&nbsp;
<?php 
$back = $privateModelAdmin->getParentBreadcrumbs();
if(count($back))
    $back = $back[count($back) - 1]['url'];
else
    $back = array_merge(['admin'], $privateModelAdmin->additionalUrlParams);
echo Html::a('<i class="fa fa-times-circle-o"></i> '.Module::t('app', 'Cancel'), $back, ['class' => 'btn btn-gray', 'name'=>'save']); ?>

<?php ActiveForm::end() ?>
 