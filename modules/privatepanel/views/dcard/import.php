<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$form = ActiveForm::begin([
    'method'=>"post",
    'options'=>['enctype'=>"multipart/form-data"],
]);
echo Html::label('Файл:');
echo Html::fileInput('file');
echo Html::submitButton('Импортировать', ['class' => 'btn']);
ActiveForm::end();
echo $out;
