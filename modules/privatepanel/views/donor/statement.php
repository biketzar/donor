<html>

<head>
    <title><?=$model->surname.' '.$model->name.' '.$model->pname?></title>
    <meta http-equiv=Content-Type content="text/html; charset=windows-1251">
    <meta name=Generator content="Microsoft Word 14 (filtered)">
    <style>
    < !--

    /* Font Definitions */
    @font-face {
        font-family: Calibri;
        panose-1: 2 15 5 2 2 2 4 3 2 4;
    }

    @font-face {
        font-family: Tahoma;
        panose-1: 2 11 6 4 3 5 4 4 2 4;
    }

    /* Style Definitions */
    p.MsoNormal,
    li.MsoNormal,
    div.MsoNormal {
        margin-top: 0cm;
        margin-right: 0cm;
        margin-bottom: 10.0pt;
        margin-left: 0cm;
        line-height: 115%;
        font-size: 11.0pt;
        font-family: "Calibri", "sans-serif";
    }

    p.MsoAcetate,
    li.MsoAcetate,
    div.MsoAcetate {
        mso-style-link: "Текст выноски Знак";
        margin: 0cm;
        margin-bottom: .0001pt;
        font-size: 8.0pt;
        font-family: "Tahoma", "sans-serif";
    }

    span.MsoPlaceholderText {
        color: gray;
    }

    span.a {
        mso-style-name: "Текст выноски Знак";
        mso-style-link: "Текст выноски";
        font-family: "Tahoma", "sans-serif";
    }

    .MsoChpDefault {
        font-size: 10.0pt;
        font-family: "Calibri", "sans-serif";
    }

    @page WordSection1 {
        size: 595.3pt 841.9pt;
        margin: 2.0cm 42.5pt 2.0cm 3.0cm;
    }

    div.WordSection1 {
        page: WordSection1;
    }

    -->
    </style>

</head>

<body lang=RU>

    <div class=WordSection1>

        <table class=MsoTableGrid border=0 cellspacing=0 cellpadding=0 width=321 style='width:240.95pt;margin-left:232.2pt;border-collapse:collapse;border:
 none'>
            <tr style='height:98.75pt'>
                <td width=321 valign=top style='width:240.95pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:98.75pt'>
                    <p class=MsoNormal style='text-align: center;margin-bottom:0cm;margin-bottom:.0001pt;line-height:
         150%'><span style='font-size:12.0pt;line-height:150%;font-family:"Times New Roman","serif"'><b>В ПРИКАЗ
                            </b><br /> оказать материальную помощь</span></p>
                    <br />
                    <p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  150%"><span style='font-size:12.0pt;line-height:150%;font-family:"Times New Roman","serif"'>Проректор<br> по
                            делам молодежи</span>
                        <span style='font-size:12.0pt;line-height:150%;font-family:"Times New Roman","serif"'> _________
                            <sup
                                style='position: absolute;margin-top: 15px;margin-left: -55px;font-family:"Times New Roman","serif"'>подпись</sup></span>
                    </p><br />
                    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  150%'><span style='font-size:14.0pt;line-height:150%;font-family:"Times New Roman","serif"'>Проректору</span></p>
                    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:14.0pt;font-family:"Times New Roman","serif"'>по делам молодежи</span></p>
                    <p class=MsoNormal style='margin-top:6.0pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:0cm;margin-bottom:.0001pt;line-height:150%'><span
                            style='font-size:14.0pt;line-height:150%;font-family:"Times New Roman","serif"'>М.А.
                            Пашоликову </span></p>
                    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  150%'><span style='font-size:14.0pt;line-height:150%;font-family:"Times New Roman","serif"'>от
                            обучающегося</span></p>
                    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  150%'><span style='font-size:14.0pt;line-height:150%;font-family:"Times New Roman","serif"'><?=$model->surname.' '.$model->name.' '.$model->pname?></span>
                    </p>
                </td>
            </tr>
        </table>

        <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-indent:
297.7pt;line-height:150%'><span style='font-size:14.0pt;line-height:150%;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

        <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-indent:
35.45pt;line-height:150%'><span style='font-size:14.0pt;line-height:150%;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

        <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;text-indent:35.45pt;line-height:normal'><b><span
                    style='font-size:14.0pt;font-family:"Times New Roman","serif"'>ОБРАЩЕНИЕ</span></b></p>

        <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;text-indent:35.45pt;line-height:150%'><span style='font-size:
14.0pt;line-height:150%;font-family:"Times New Roman","serif"'>о материальной
                помощи</span></p>

        <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:35.45pt;line-height:150%'><span style='font-size:14.0pt;
line-height:150%;font-family:"Times New Roman","serif"'>Прошу оказать мне материальную помощь из Фонда социальной
                защиты обучающихся в
                размере <?=  \Yii::$app->request->get('money', $model->money);?> руб. в связи </span><span style='font-size:14.0pt;line-height:
150%;font-family:"Times New Roman","serif"'>с тем, что я отношусь к категории
                обучающихся-доноров.</span></p>

        <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-indent:
35.45pt;line-height:150%'><span style='font-size:14.0pt;line-height:150%;
font-family:"Times New Roman","serif"'>Институт: <?=$model->department_id?></span><span style='font-size:
14.0pt;line-height:150%;font-family:"Times New Roman","serif"'> </span></p>

        <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-indent:
35.45pt;line-height:150%'><span style='font-size:14.0pt;line-height:150%;
font-family:"Times New Roman","serif"'>Паспортные данные:
                <?=$model->pass_series?> <?=$model->pass_id?>, выдан <?=$model->issued?>, дата выдачи
                <?=$model->issued_date?>, код подразделения <?=$model->subdiv_code?></span><span
                style='font-size:14.0pt;line-height:150%;font-family:"Times New Roman","serif"'>
            </span></p>

        <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-indent:
35.45pt;line-height:150%'><span style='font-size:14.0pt;line-height:150%;
font-family:"Times New Roman","serif"'>Место регистрации:
                <?=$model->reg_addr?></span><span
                style='font-size:14.0pt;line-height:150%;font-family:"Times New Roman","serif"'>
            </span></p>

        <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-indent:
35.45pt;line-height:150%'><span style='font-size:14.0pt;line-height:150%;
font-family:"Times New Roman","serif"'>ИНН: <?=$model->inn?></span></p>

        <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-indent:
35.45pt;line-height:150%'><span style='font-size:14.0pt;line-height:150%;
font-family:"Times New Roman","serif"'>Дата рождения: <?=$model->birth_date?></span><span
                style='font-size:14.0pt;line-height:150%;font-family:"Times New Roman","serif"'>
            </span></p>

        <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-indent:
35.45pt;line-height:150%'><span style='font-size:14.0pt;line-height:150%;
font-family:"Times New Roman","serif"'>Тел.: <?=$model->phone?></span><span style='font-size:14.0pt;
line-height:150%;font-family:"Times New Roman","serif"'> </span></p>

        <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-indent:
35.45pt;line-height:150%'><span style='font-size:14.0pt;line-height:150%;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

        <table class=MsoTableGrid border=0 cellspacing=0 cellpadding=0 width=640
            style='width:480.3pt;border-collapse:collapse;border:none'>
            <tr>
                <td width=177 valign=top style='width:200.0pt;padding:0cm 10.4pt 0cm 5.4pt'>
                    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  150%'><i><span style='font-size:14.0pt;line-height:150%;font-family:"Times New Roman","serif"'></span></i>
                    </p>
                    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  150%'><span style='font-size:14.0pt;line-height:150%;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
                </td>
                <td width=227 valign=top style='width:6.0cm;padding:0cm 5.4pt 0cm 5.4pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:14.0pt;
  font-family:"Times New Roman","serif"'>_____________</span></p>
                    <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><sup><span style='font-size:14.0pt;
  font-family:"Times New Roman","serif"'>подпись</span></sup></p>
                </td>
                <td width=236 valign=top style='width:177.2pt;padding:0cm 5.4pt 0cm 5.4pt'><span
                        style='font-size:14.0pt;line-height:150%;
font-family:"Times New Roman","serif"'><i><?=substr($model->name, 0, 2).'.'.substr($model->pname, 0, 2).'. '.$model->surname?></i></span></td>
            </tr>
        </table>

        <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-indent:
35.45pt;line-height:150%'><span style='font-size:14.0pt;line-height:150%;
font-family:"Times New Roman","serif"'></span></p>

        <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-indent:
35.45pt;line-height:150%'><span style='font-size:14.0pt;line-height:150%;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

        <table class=MsoTableGrid border=0 cellspacing=0 cellpadding=0 width=359 style='width:269.3pt;margin-left:218.05pt;border-collapse:collapse;border:
 none'>
            <tr style='height:115.9pt'>
                <td width=359 valign=top style='width:269.3pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:115.9pt'>
                    <p class=MsoNormal align=center style='margin-top:0cm;margin-right:-.05pt;
  margin-bottom:0cm;margin-left:0cm;margin-bottom:.0001pt;text-align:center;
  line-height:normal'><b><span style='font-family:"Times New Roman","serif";
  layout-grid-mode:line'>ВЫПИСКА</span></b></p>
                    <p class=MsoNormal align=center style='margin-top:0cm;margin-right:-.05pt;
  margin-bottom:0cm;margin-left:0cm;margin-bottom:.0001pt;text-align:center;
  line-height:normal'><span style='font-family:"Times New Roman","serif";
  layout-grid-mode:line'>из постановления профбюро, протокол</span></p>
                    <p class=MsoNormal align=center style='margin-top:0cm;margin-right:-.05pt;
  margin-bottom:0cm;margin-left:0cm;margin-bottom:.0001pt;text-align:center;
  line-height:normal'><span style='font-family:"Times New Roman","serif";
  layout-grid-mode:line'> №___ от «__»________<?=date('Y')?>г.</span></p>
                    <p class=MsoNormal align=center style='margin-top:0cm;margin-right:-.05pt;
  margin-bottom:0cm;margin-left:0cm;margin-bottom:.0001pt;text-align:center;
  line-height:normal'><span style='font-family:"Times New Roman","serif";
  layout-grid-mode:line'>Постановили: считать представленные документы достаточными
  и достоверными для оказания материальной помощи</span></p>
                    <p class=MsoNormal align=right style='margin-top:0cm;margin-right:-.05pt;
  margin-bottom:0cm;margin-left:0cm;margin-bottom:.0001pt;text-align:right;
  line-height:normal'><i><span style='font-family:"Times New Roman","serif";
  color:#0070C0;layout-grid-mode:line'>&nbsp;</span></i></p>
                    <p class=MsoNormal style='margin-top:0cm;margin-right:-.05pt;margin-bottom:
  0cm;margin-left:0cm;margin-bottom:.0001pt;line-height:normal'><span
                            style='font-family:"Times New Roman","serif";layout-grid-mode:line'>Председатель
                            профбюро     </span></p>
                    <p class=MsoNormal style='margin-top:0cm;margin-right:-.05pt;margin-bottom:
  0cm;margin-left:0cm;margin-bottom:.0001pt;line-height:normal'><i><span
                                style='font-family:"Times New Roman","serif";layout-grid-mode:line'>__________ 
                                <span
                                    style='color:#0070C0'>                                                 </span>__________</span></i>
                    </p>
                    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><i><span style='font-family:"Times New Roman","serif";color:#0070C0;
  layout-grid-mode:line'>                                                                           
                            </span></i><i><span style='font-family:"Times New Roman","serif";color:black;
  layout-grid-mode:line'> <sup>Подпись</sup></span></i></p>
                </td>
            </tr>
        </table>

        <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
150%'><span style='font-size:14.0pt;line-height:150%;font-family:"Times New Roman","serif"'>&nbsp;</span></p>

    </div>

</body>

</html>