<?php
use yii\helpers\Html;
use dosamigos\tinymce\TinyMce;
use mihaildev\elfinder\ElFinder;
use yii\web\JsExpression;
$this->registerAssetBundle(yii\web\JqueryAsset::className(), yii\web\View::POS_HEAD);
//\dosamigos\tinymce\TinyMceAsset::register($this);
$model->setScenario('insert');
$model->status_id = 1;
?>
<div id="notification-add-modal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
        <?=Html::beginForm('/private/notification/send/')?>   
        <?=Html::activeHiddenInput($model, 'type_id');?>
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">×</button>
            <h4 class="modal-title">Отправить уведомления</h4>
        </div>
        <div class="modal-body" style="overflow: hidden;">
            <div class="form-group">
                <label class="control-label col-sm-2">Статус<sup>*</sup></label>
                <div class="col-sm-10">
                    <?=Html::activeDropDownList($model, 'status_id', app\models\Donorlist::getApproved(), ['class' => 'form-control', 'required' => TRUE])?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2">Время<sup>*</sup></label>
                <div class="col-sm-10">
                    <?=Html::activeDropDownList($model, 'date_id', app\models\Donorlist::getTimesList(), ['class' => 'form-control', 'required' => TRUE])?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2">Тема сообщения<sup>*</sup></label>
                <div class="col-sm-10">
                    <?=Html::activeTextInput($model, 'subject', ['class' => 'form-control', 'required' => TRUE])?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2">Текст<sup>*</sup></label>
                <div class="col-sm-10">
                    <?=dosamigos\tinymce\TinyMce::widget([
                            'options' => ['rows' => 8, 'required' => TRUE],
                            'language' => 'ru',
                            'model' => $model,
                            'id' => Html::getInputId($model, 'message'),
                            'options' => ['id' => Html::getInputId($model, 'message')],
                            'attribute' => 'message',
                            'clientOptions' => [
                                'image_advtab' => new JsExpression('true'),
                                'image_description'=> new JsExpression('true'),
                                'image_title'=> new JsExpression('true'),
                                'image_dimensions'=> new JsExpression('true'),
                                'plugins' => [
                                    "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                                    "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                                    "noindex save table contextmenu directionality emoticons template paste textcolor importcss"
                                ],
                                'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
                                'toolbar1' => "newdocument fullpage | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect",
                                'toolbar2' => "table | bullist numlist | outdent indent blockquote | undo redo | link unlink anchor image media code | inserttime preview | forecolor backcolor",
                                //'toolbar3' => "table | hr removeformat | subscript superscript | charmap emoticons | print fullscreen | ltr rtl | spellchecker | visualchars visualblocks nonbreaking template pagebreak restoredraft",
                                'relative_urls' => new JsExpression('false'),
                                'convert_urls' => new JsExpression('false'),
                                'remove_script_host' => new JsExpression('true'),
                                'image_advtab' => new JsExpression('true'),
                                'custom_elements' => 'noindex',
                                'extended_valid_elements' => 'noindex',
                                'file_browser_callback' => new JsExpression('function(field_name, url, type, win) { 
                                    
                                    var filemanager_url = {
                                        image: \''.ElFinder::getManagerUrl('elfinder', ['callback' => 'tinymce_callback' ,'filter' => 'image']).'\',
                                        media: \''.ElFinder::getManagerUrl('elfinder', ['callback' => 'tinymce_callback' ,'filter' => 'video']).'\',
                                        file: \''.ElFinder::getManagerUrl('elfinder', ['callback' => 'tinymce_callback' ,'filter' => ['image', 'video', 'application', 'text/plain']]).'\',
                                        common: \''.ElFinder::getManagerUrl('elfinder', ['callback' => 'tinymce_callback' ,'filter' => ['image', 'video', 'application', 'text/plain']]).'\',
                                    }
                                    if(typeof(filemanager_url[type]) != "undefined")
                                        url = filemanager_url[type];
                                    else
                                        url = filemanager_url.common;
                                    tinymce.activeEditor.windowManager.open({
                                      file: url,// use an absolute path!
                                      title: \'file manager\',
                                      width: 900,  
                                      height: 450,
                                      resizable: \'yes\'
                                    }, {
                                      oninsert: function (file) {
                                            win.document.getElementById(field_name).value = file.url;
                                              var event = new Event("change")
                                              win.document.getElementById(field_name).dispatchEvent(event);
                                      }
                                    });
                                    return false;
                                    }'),
                                'extended_valid_elements'=> new JsExpression("'noindex,script,img[E|rel=|rel|src|alt=|title|name|longdesc|width|height|usemap|ismap|align|border|hspace|vspace|real_width|real_height|preview|preview_desc|design][]'"),
                                'setup' => new JsExpression(
                                        "function(ed) {
                                            ed.addMenuItem('example', {
                                                text: 'noindex',
                                                context: 'insert',
                                                onclick: function() {
                                                //вставка тега noindex
                                                    var node  = document.createElement('noindex');
                                                    node.innerHTML = ed.selection.getNode().outerHTML;
                                                    ed.selection.getNode().parentNode.replaceChild(node, ed.selection.getNode());
                                                }
                                             });
                                         }"
                                        ),
                            ]
                        ]);?>
                    <?php /*Html::activeTextarea($model, 'message', ['class' => 'form-control', 'required' => TRUE]);*/?>
                </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-default btn-blue">Отправить</button>
        </div>
        <?=Html::endForm();?> 
    </div>
  </div>
</div>
<style>
  .modal-open {
  overflow: hidden; }

.modal {
  display: none;
  overflow: hidden;
  position: fixed;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  z-index: 1050;
  -webkit-overflow-scrolling: touch;
  outline: 0; }
  .modal.fade .modal-dialog {
    -webkit-transform: translate(0, -25%);
    -ms-transform: translate(0, -25%);
    -o-transform: translate(0, -25%);
    -moz-transform: translate(0, -25%);
         transform: translate(0, -25%);
    -webkit-transition: -webkit-transform 0.3s ease-out;
    -moz-transition: -moz-transform 0.3s ease-out;
    -o-transition: -o-transform 0.3s ease-out;
    transition: transform 0.3s ease-out; }
  .modal.in .modal-dialog {
    -webkit-transform: translate(0, 0);
    -ms-transform: translate(0, 0);
    -o-transform: translate(0, 0);
    -moz-transform: translate(0, 0);
         transform: translate(0, 0); }

.modal-open .modal {
  overflow-x: hidden;
  overflow-y: auto; }

.modal-dialog {
  position: relative; }

.modal-content {
  position: relative;
  background-color: #fff;
  border: 1px solid transparent;
  -webkit-border-radius: 0;
     -moz-border-radius: 0;
          border-radius: 0;
  -webkit-box-shadow: 0 3px 9px rgba(0, 0, 0, 0.5) 2px 5px rgba(0, 0, 0, 0.4);
  -moz-box-shadow: 0 3px 9px rgba(0, 0, 0, 0.5) 2px 5px rgba(0, 0, 0, 0.4);
  -ms-box-shadow: 0 3px 9px rgba(0, 0, 0, 0.5) 2px 5px rgba(0, 0, 0, 0.4);
  -o-box-shadow: 0 3px 9px rgba(0, 0, 0, 0.5) 2px 5px rgba(0, 0, 0, 0.4);
  box-shadow: 0 3px 9px rgba(0, 0, 0, 0.5) 2px 5px rgba(0, 0, 0, 0.4);
  -webkit-background-clip: padding-box;
     -moz-background-clip: padding-box;
       -o-background-clip: padding-box;
          background-clip: padding-box;
  outline: 0; }
  .modal-sm .modal-content .btn {
    padding: 4px 29px;
    width: auto; }

.modal-backdrop {
  position: fixed;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  z-index: 1040;
  background-color: #000; }
  .modal-backdrop.fade {
    -webkit-opacity: 0;
    -moz-opacity: 0;
    opacity: 0; }
  .modal-backdrop.in {
    -webkit-opacity: 0.75;
    -moz-opacity: 0.75;
    opacity: 0.75; }

.modal-header {
  padding: 20px;
  background: #eaebec; }
  .modal-header:before,
  .modal-header:after {
    content: " ";
    display: table; }
  .modal-header:after {
    clear: both; }

.modal-header .close {
  margin-top: -2px; }

.modal-title {
  margin: 0;
  line-height: 1.4; }

.modal-body {
  position: relative;
  padding: 20px; }

.modal-footer {
  padding: 20px;
  text-align: right;
  border-top: 1px solid #e5e5e5; }
  .modal-footer:before,
  .modal-footer:after {
    content: " ";
    display: table; }
  .modal-footer:after {
    clear: both; }
  .modal-footer .btn + .btn {
    margin-left: 5px;
    margin-bottom: 0; }
  .modal-footer .btn-group .btn + .btn {
    margin-left: -1px; }
  .modal-footer .btn-block + .btn-block {
    margin-left: 0; }

.modal-scrollbar-measure {
  position: absolute;
  top: -9999px;
  width: 50px;
  height: 50px;
  overflow: scroll; }

.modal-dialog {
  width: 80%;
  margin: 60px auto; }

.modal-content {
  -webkit-box-shadow: 0 5px 15px rgba(0, 0, 0, 0.5) 2px 5px rgba(0, 0, 0, 0.4);
  -moz-box-shadow: 0 5px 15px rgba(0, 0, 0, 0.5) 2px 5px rgba(0, 0, 0, 0.4);
  -ms-box-shadow: 0 5px 15px rgba(0, 0, 0, 0.5) 2px 5px rgba(0, 0, 0, 0.4);
  -o-box-shadow: 0 5px 15px rgba(0, 0, 0, 0.5) 2px 5px rgba(0, 0, 0, 0.4);
  box-shadow: 0 5px 15px rgba(0, 0, 0, 0.5) 2px 5px rgba(0, 0, 0, 0.4); }
  .modal-content p {
    margin-bottom: 18px; }

.modal-sm, .modal-lg {
  width: auto; }

.modal-md {
  width: 620px; }

@media (min-width: 768px) {
  .modal-sm {
    width: 260px; } }

@media (min-width: 992px) {
  .modal-lg {
    width: 800px; } }
.form-control.note-block
{
  width:100%;
  height:350px;
}
.form-group{
    float: left;
    width: 100%;
}
.form-control{
    width:100%;
}
</style>
<script>
window.addEventListener('load', function(){
    var lockSubmit = false;
    $('#notification-add-modal').find('form').submit(function(event){
        if(lockSubmit)
        {
            alert('Дождитесь результата отправки уведомлений');
            return false;
        }
        lockSubmit = true;
        event.preventDefault();
        var form = $(this);
        $.ajax({
            url: form.attr('action'),
            type: "POST",
            data: form.serialize(),
            dataType: 'json',
            success: function(data){
                if(data.result == true)
                {
                    alert(data.message);
                }
                else
                {
                    alert(data.message);
                }
            }
        });
        return false;
    });
    
//    tinymce.init({
//        selector: '#<?=Html::getInputId($model, 'message')?>',
//        height: 500,
//        menubar: false,
//        plugins: [
//          'advlist autolink lists link image charmap print preview anchor',
//          'searchreplace visualblocks code fullscreen',
//          'insertdatetime media table contextmenu paste code'
//        ],
//        toolbar: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
//        content_css: '//www.tinymce.com/css/codepen.min.css'
//      });
});
</script>
