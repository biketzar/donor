<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="ru-RU">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="ru" />
        <title><?=$title;?></title>
    </head>
<body>
    <table>
        <tbody>
            <?php 
            foreach ($timeList as $timeId => $time)
            {
                ?>
            <tr><td colspan="8" style="text-align: center; font-weight: bold"><?=$time;?></td></tr>
            <tr>
                        <td></td>
                        <td>ФИО</td>
                        <td>Группа</td>
                        <td>ИНН</td>
                        <td>Серия 
                       Номер 
                        Выдан
                        Дата выд.
                        Код подр.</td>
                        <td>Регистрация</td>
                        <td>Телефон</td>
                        <td>Бюджет</td>
                    </tr>
                <?php
                    if(!isset($donorlist[$timeId]))
                    {
                        continue;
                    }
                    $counter = 0;
                    foreach ($donorlist[$timeId] as $key => $value)
                    {
                        $donorId = $value['id_donor'];
                        $counter++;
                        ?>
                    <tr>
                        <td><?=$counter?></td>
                        <td><?="{$donorArr[$donorId]['surname']} {$donorArr[$donorId]['name']} {$donorArr[$donorId]['pname']}"?></td>
                        <td>гр. <?=$donorArr[$donorId]['group_num']?>;</td>
                        <td><?=$donorArr[$donorId]['inn']?></td>
                        <td><?=$donorArr[$donorId]['pass_series']?>
                        <?=$donorArr[$donorId]['pass_id']?><br/>
                       <?=$donorArr[$donorId]['issued']?>
                        <?=$donorArr[$donorId]['issued_date']?>
                        <?=$donorArr[$donorId]['subdiv_code']?></td>
                        <td><?=$donorArr[$donorId]['reg_addr']?></td>
                        <td style="text-align: right"><?=$donorArr[$donorId]['phone']?></td>
                        <td style="text-align: right"><?=$donorArr[$donorId]['is_budget'] === '1' ? 'Да' : 'Нет' ?></td>
                    </tr>
                        <?php
                    }
            }
            ?>
        </tbody>
    </table>
    <style>
        table {
            border-collapse: collapse;
        }

        table, th, td {
            border: 1px solid black;
        }
        tr:nth-child(odd)
        {
            background-color: #F2F2F2;
        }
        td{
            line-height: 20px;
        }
    </style>
</body>
</html>
