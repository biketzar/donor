<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="ru-RU">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="ru" />
        <title><?=$title;?></title>
    </head>
<body>
    <table>
        <tbody>
            <?php 
            foreach ($timeList as $timeId => $time)
            {
                ?>
            <tr><td colspan="5" style="text-align: center; font-weight: bold"><?=$time;?></td></tr>
                <?php
                    if(!isset($donorlist[$timeId]))
                    {
                        continue;
                    }
                    $counter = 0;
                    foreach ($donorlist[$timeId] as $key => $value)
                    {
                        $donorId = $value['id_donor'];
                        $counter++;
                        ?>
                    <tr>
                        <td><?=$counter?></td>
                        <td><?="{$donorArr[$donorId]['surname']} {$donorArr[$donorId]['name']} {$donorArr[$donorId]['pname']}"?>&nbsp;</td>
                        <td>&nbsp;<?=  in_array($donorId, $donorHistory) ? 'В' : 'П'?>&nbsp;</td>
						<td>&nbsp;<?=$donorArr[$donorId]['typing'] ? ' Т ' : ' '?>&nbsp;</td>
                        <td style="text-align: right"> &nbsp;<?=$donorArr[$donorId]['phone']?> &nbsp;</td>
                    </tr>
                        <?php
                    }
            }
            ?>
        </tbody>
    </table>
    <style>
        table {
            border-collapse: collapse;
        }

        table, th, td {
            border: 1px solid black;
        }
        tr:nth-child(odd)
        {
            background-color: #F2F2F2;
        }
        td{
            line-height: 20px;
        }
    </style>
</body>
</html>
