<?php
use \yii\helpers\Html;
$this->title = 'Список доноров';
?>
<div class="row choose-time-block full-list-row">
    <div class="col-xs-3 col-lg-2">Выберите время: </div>
    <div class="col-xs-7 col-lg-8"><?= Html::dropDownList('time', NULL, $timeList, ['id' => 'time-list-select'])?></div>
</div>
<div class="row  full-list-row">
    <div class="col-xs-3 col-lg-2">Выберите координатора: </div>
    <div class="col-xs-7 col-lg-8"><?= Html::dropDownList('coordinator', NULL, $userList, ['id' => 'coordinator-list-select'])?></div>
</div>
<div class="row  full-list-row">
    <div class="col-xs-3 col-lg-2">Выберите фотографа: </div>
    <div class="col-xs-7 col-lg-8"><?= Html::dropDownList('photographer', NULL, $userList, ['id' => 'photographer-list-select'])?></div>
</div>
<div class="row  full-list-row">
    <div class="col-xs-3 col-lg-2">Выберите ответсвенного за справки: </div>
    <div class="col-xs-7 col-lg-8"><?= Html::dropDownList('responsible', NULL, $userList, ['id' => 'responsible-list-select'])?></div>
    <div class="col-xs-2"><a href="#" class="btn btn-success" id="print-table">Печать таблицы</a></div>
</div>
<table class="event-people show-print">
    <tr><td>Координатор:</td><td id="coordinator-info"></td></tr>
    <tr><td>Фотограф:</td><td id="photographer-info"></td></tr>
    <tr><td>Ответственный за справки:</td><td id="responsible-info"></td></tr>
</table>
<table class="table table-striped table-hover" id="donor-table">
<thead>
<tr>
    <td></td>
    <td>ФИО</td>
    <td></td>
    <td></td>
    <td>Тел.</td>
    <td>Гр.крови</td>
    <td>Сдал/не сдал</td>
    <td>Необх.донести</td>
    <td>Явка</td>
</tr>
</thead>
<tbody>
</tbody>
<tfoot>

</tfoot>
</table>
<script type="text/javascript">
    window.addEventListener('load', function(){
        $('#print-table').click(function(e){
            e.preventDefault();
            window.print();
            return false;
        });
        
        var tbody = $('#donor-table tbody');
        var timeSelect = $('#time-list-select');
        
        timeSelect.change(function(e){
            e.preventDefault();
            $.ajax({
                url: location.href,
                data: {action: 'get-donors', time: $(this).val(), rand: Math.random()},
                success: function(data){
                    if(data.error)
                    {
                        alert(data.error);
                    }
                    else
                    {
                        buildTable(data);
                    }
                },
                error: function(){alert('Произошла ошибка. Обновите страницу');}
            });
            return false;
        });
        timeSelect.change();
        
        function buildTable(data)
        {
            tbody.html('');
            tbody.append($('<tr class="time-info-row"></tr>').append($('<td colspan="10"></td>').html(data.time)))
            data.rows.forEach(function(cols, i, rows) {
                var tr = $('<tr></tr>');
                cols.forEach(function(col, j, row) {
                    var td = $('<td></td>');
                    td.html(col);
                    tr.append(td);
                });
                initRowActions(data, data.donorList[i], tr);
                tbody.append(tr);
            });
        }
        
        function initRowActions(data, donor, tr)
        {
            initSelect2(tr);
            actionChangeBloodType(donor, tr);
            actionChangeNeedDocs(donor, data.timeId, tr);
            actionChangeDonated(donor, data.timeId, tr);
        }
        
        function actionChangeBloodType(donor, tr)
        {
            tr.find('select[name=blood_type]').change(function(e){
                e.preventDefault();
                $.ajax({
                    url: location.href,
                    data: {action: 'set-bloodtype', bloodType: $(this).val(), donorId: donor.id, rand: Math.random()},
                    success: function(data){
                        if(data.error)
                        {
                            alert(data.error);
                        }
                        else
                        {
                            timeSelect.change();
                        }
                    },
                    error: function(){alert('Произошла ошибка. Обновите страницу');}
                });
                return false;
            });
        }
        
        function actionChangeNeedDocs(donor, timeId, tr)
        {
            tr.find('select[name*=docs]').change(function(e){
                e.preventDefault();
                $.ajax({
                    url: location.href,
                    data: {action: 'set-need-docs', needDocs: $(this).val(), donorId: donor.id, timeId: timeId, rand: Math.random()},
                    success: function(data){
                        if(data.error)
                        {
                            alert(data.error);
                        }
                        else
                        {
                            timeSelect.change();
                        }
                    },
                    error: function(){alert('Произошла ошибка. Обновите страницу');}
                });
                return false;
            });
        }
        
        function actionChangeDonated(donor, timeId, tr)
        {
            tr.find('select[name=donated]').change(function(e){
                e.preventDefault();
                $.ajax({
                    url: location.href,
                    data: {action: 'set-donated', donated: $(this).val(), donorId: donor.id, timeId: timeId, rand: Math.random()},
                    success: function(data){
                        if(data.error)
                        {
                            alert(data.error);
                        }
                        else
                        {
                            timeSelect.change();
                        }
                    },
                    error: function(){alert('Произошла ошибка. Обновите страницу');}
                });
                return false;
            });
        }
        
        function initSelect2(tr)
        {
            tr.find('select').select2({
                formatNoMatches: function () {return "Нет результатов";},
                formatMatches: function(a){return 1===a?"One result is available, press enter to select it.":a+" results are available, use up and down arrow keys to navigate."},
                formatAjaxError:function(){return"Ошибка загрузки"},
                formatInputTooShort:function(a,b){var c=b-a.length;return"Введите "+c+"  или больше символов";},
                formatInputTooLong:function(a,b){var c=a.length-b;return"Удалите "+c+" символов";},
                formatSelectionTooBig:function(a){return"Вы можете выбрать "+a+" элемент(ов)";},
                formatLoadMore:function(){return"Слишком много результатов\u2026"},
                formatSearching:function(){return"Поиск\u2026"},
            });
        }
        
    });
    
    window.addEventListener('load', function(){
        $('#coordinator-list-select').change(function(){
           $('#coordinator-info').text($(this).val()) ;
        });
        $('#responsible-list-select').change(function(){
           $('#responsible-info').text($(this).val()) ;
        });
        $('#photographer-list-select').change(function(){
           $('#photographer-info').text($(this).val()) ;
        });
    });
</script>
<style>
    .show-print{
        display:none;
    }
    .time-info-row td{
        text-align: center;        
    }
    .full-list-row{
        padding: 3px 0px;
    }
    @media print {
        @page {size:landscape;}
        .show-print{
            display:inline-block;
        }
        select, nav, .breadcrumbs, .padtop20, 
        .copy, .choose-time-block, #yii-debug-toolbar, #yii-debug-toolbar-min,
        .select2-container, #select2-drop-mask, .select2-drop{
            display:none!important;
        }
        .page-content-wrapper .page-content{
            margin-left: 0px;
        }
        table#donor-table td {
            border: 1px solid black;
            color: black;
            padding: 1px 8px;
            line-height: 20px;
        }
        table#donor-table{
            border: 1px solid black;
        }
        table#donor-table tr:nth-child(odd) {
            background-color: #F2F2F2;
        }
        table#donor-table tr:nth-child(event) {
            background-color: #FFF;
        }
        div.select2-container {
            display: none;
        }
        .full-list-row{
            display: none;
        }
        table.event-people {
            border: 1px solid black;
            width: 100%;
            display: table;
            margin-bottom: 5px;
        }
        table.event-people td {
            border: 1px solid black;
            padding: 2px 10px;
            color: black;
        }
    }
</style>