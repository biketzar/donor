<p>Здравствуйте, [NAME]!</p>
<p>
    Ваш трансфер до Первого медицинского университета отходит от <a href="https://vk.com/photo-106328376_434875437" title="План прохода">Экспоцентра Главного здания</a> <b style="white-space: nowrap;">[TIME]</b>, будьте на месте за 15 минут до отправления.
</p>
<p>
Рекомендуем ознакомиться еще раз с общей <a href="http://donor.spb.ru/day" title="Об акции">информацией</a>, а также обратить внимание на то, что материальную помощь университет не оформляет студентам контрактной формы обучения.
</p>
Вам необходимо взять с собой:   
<ol>
<li>Паспорт с регистрацией в России от 1 года</li>
<li>Бутылку воды.</li>
</ol>
<p>Спасибо Вам за желание стать донором. <span style="color:#c52b31;font-weight: bold;">❤</span></p>
<p>
    Все вопросы Вы можете задать в <a href="https://vk.com/polydonor" title="День донора СПбПУ">сообщениях группы</a>, а также на <a href="mailto:polydonor@gmail.com">polydonor@gmail.com</a>.<br/>
</p>
<p>С уважением, команда Дня донора.
</p>