<?php
$this->title = 'Доноры - Политех';
?>
<div>
   Всего в списках - <?=$allCount;?>;<br/>
   Политех - <?=$inCount;?>; Утверждено - <?=\app\models\Donorlist::find()->where('type=0 and approved=1')->count();?>; Не утверждено - <?=\app\models\Donorlist::find()->where('type=0 and approved=0')->count();?><br/>
   Выезд - <?=$outCount;?>; Утверждено - <?=\app\models\Donorlist::find()->where('type=1 and approved=1')->count();?>; Не утверждено - <?=\app\models\Donorlist::find()->where('type=1 and approved=0')->count();?><br/>
   Отказано - <?=$noCount;?>;<br/>
   Не удалось рапределить - <?=count($nocorrectDonors);?>
</div>
<div class="col-xs-12 text-right">
    <ul class="list-inline">
        <li class=""><a href="/private/donorlist/list2" class="btn btn-primary"><i class="fa"></i> Доноры - выезд</a></li>
        <li class=""><a href="/private/donorlist/list3" class="btn btn-primary"><i class="fa"></i> Печать</a></li>
         <li class=""><a href="/private/donorlist/list5" class="btn btn-primary"><i class="fa"></i> Список с данными</a></li>
		<?php if(\Yii::$app->user->can('/private/donorlist/add')) { ?>
        <li class=""><a href="/private/donorlist/clear" class="btn btn-orange" onclick="return clearAction()"><i class="fa"></i> Очистить весь список</a></li>
        <li class=""><a href="/private/donorlist/add" class="btn btn-lilac"><i class="fa"></i> Донабрать</a></li>
		<?php } ?>
    </ul>
</div>
<div id="settings-grid" class="grid-view">
    <table class="table table-striped table-hover">
        <thead>
            <tr>
                <td></td>
                <td>Донор</td>
                <td>Телефон</td>
                <td>Желаемое время</td>
                <td>Изменить время</td>
                <td>Статус</td>
                <td></td>
            </tr>
        </thead>
        <tbody>
            <?php
            foreach($timeList as $timeKey => $time)
            {
                ?>
            <tr>
                <td colspan="6" style="background-color: bisque">
                    <?=$time['dtime']?>; 
                Утверждено - <?=\app\models\Donorlist::find()->where('date_id=:date_id and approved=1', [':date_id' => $timeKey])->count();?>;
                Регистрация <?=$time['hide'] ? '' : 'не'?> остановлена;
                <?=$time['max_donor_count'] > 0 ? 'Максимальное кол-во доноров: '.$time['max_donor_count'] : '';?>
                </td>
                <td style="background-color: bisque">
                    <?php if(\Yii::$app->user->can('/private/notification/send')){?>
                    <a href="#" class="btn btn-primary" onclick="openNotificationBlock(<?=$timeKey?>);return false;">Уведомления</a>
                    <?php } ?>
                </td>
            </tr>
            <?php
            $counter = 0;
            if(!array_key_exists($timeKey, $donors)){continue;}
            foreach($donors[$timeKey] as $donor)
            {
                $counter++;
                ?>
            <tr>
                <td>
                    <?=$counter;?>
                    <?php if(app\models\Notification::inSendedNotification($donor['id_donor'], $timeKey)) { ?>
                        <br/><i class="fa fa-envelope" aria-hidden="true" title="уведомление отправлено"></i>
                    <?php } ?>
                    
                </td>
                <td style="line-height: 14px">
                    <a href="/private/donor/view/?id=<?=$donor['id_donor']?>" target="_blank" style="<?=$donorModels[$donor['id_donor']]->is_vip ? '  color:green;' : ''?>">
                    <?=$donorModels[$donor['id_donor']]->getAdminReprString()?>
                    <?php if($donorModels[$donor['id_donor']]->is_vip) { ?> (VIP)<?php } ?>
                    <?php if($donorModels[$donor['id_donor']]->is_vip) { ?> <br/>(VIP)<?php } ?>
                    <?php if($donorModels[$donor['id_donor']]->weight == 0) { ?> <br/>(Вес от 50 до 58)<?php } ?>
                    <?php if($donorModels[$donor['id_donor']]->typing == 1) { ?> <br/>(Типирование)<?php } ?>
                    </a>
                </td>
                <!--<td>
                    <?=$donorModels[$donor['id_donor']]->departament->name ?>
                </td>-->
                <td>
                    <?=$donorModels[$donor['id_donor']]->phone ?>
                </td>
				<td style="line-height: 14px">
                        <?php
                        $dtimes = \app\models\Eventday::find()
                                ->select('id, date_id')
                                ->where('visible=1 and date_id>0 and id_donor=:id_donor', [':id_donor' => $donor['id_donor']])
                                ->orderBy('priority ASC')
                                ->asArray()->all();
                        $outStr = '';
                        foreach ($dtimes as $dtime)
                        {
                            ?>
                            <a href="/private/eventday/update/?id=<?=$dtime['id']?>" target="_blank">
                                   <?="{$fullTimeList[$dtime['date_id']]['time']} {$fullTimeList[$dtime['date_id']]['typing']}";?>
                                   <br/>
                            </a>
                            <?php 
                        }
                        ?>
                    </td>
                <td>
                    <?php 
                    if($donor['approved'] != 1)
                    { ?>
                    <?php 
                        $donorTimeItems = isset($accessTimes[$timeKey]) ? $accessTimes : [$timeKey => $time['dtime']] + $accessTimes;?>
                    <?=\yii\helpers\Html::dropDownList(
                            'change-time', 
                            $donor['date_id'], 
                            $donorTimeItems, 
                            [
                                'onchange' => 'changedate(this)',
                                'donor-id' => $donor['id_donor'],
                                'time-id' => $donor['date_id'],
                                'item-id' => $donor['id']
                            ])?>
                    <?php }else{ ?>
                    <?=$time['dtime'];?>
                    <?php } ?>
                </td>
                <td>
                    <?php 
                    if($donor['approved'] != 1)
                    { ?>
                    <?=\yii\helpers\Html::dropDownList(
                            'change-status', 
                            $donor['approved'], 
                            app\models\Donorlist::getApproved(),
                            [
                                'onchange' => 'changestatus(this)',
                                'approved-id' => $donor['approved'],
                                'item-id' => $donor['id']
                            ])?>
                    <?php }else{ ?>
                    Утверждён
                    <?php } ?>
                </td>
                <td>
                    <a class="btn btn-success btn-sm" href="/private/donorlist/update?id=<?=$donor['id']?>" title="Редактировать" aria-label="Редактировать" data-pjax="0"><i class="fa fa-edit"></i></a>
                    <a class="btn btn-blue btn-sm" href="/private/donor/print?id=<?=$donor['id_donor']?>" title="Печать" aria-label="Печать" data-pjax="<?=$donor['id_donor']?>"><i class="fa fa-print"></i></a>
                </td>
            </tr>
            <?php
            //unset($donors[$timeKey]);
            }
            ?>
            <?php
            }
            ?>
            <tr>
                <td colspan="7" style="background-color: bisque">Доноры с неправильным временем</td>
            </tr>
            <?php
            $counter = 0;
            foreach($donors['other'] as $donor)
            {
                $counter++;
                ?>
            <tr>
                <td><?=$counter;?></td>
                <td style="line-height: 14px">
                    <a href="/private/donor/view/?id=<?=$donor['id_donor']?>" target="_blank" style="<?=$donorModels[$donor['id_donor']]->is_vip ? '  color:green;' : ''?>">
                    <?=$donorModels[$donor['id_donor']]->getAdminReprString()?>
                    <?php if($donorModels[$donor['id_donor']]->is_vip) { ?> (VIP)<?php } ?>
                    <?php if($donorModels[$donor['id_donor']]->is_vip) { ?> <br/>(VIP)<?php } ?>
                    <?php if($donorModels[$donor['id_donor']]->weight == 0) { ?> <br/>(Вес от 50 до 58)<?php } ?>
                    <?php if($donorModels[$donor['id_donor']]->typing == 1) { ?> <br/>(Типирование)<?php } ?>
                    </a>
                </td>
                <!--<td>
                    <?=$donorModels[$donor['id_donor']]->departament->name ?>
                </td>-->
                <td>
                    <?=$donorModels[$donor['id_donor']]->phone ?>
                </td>
                <td style="line-height: 14px">
                        <?php
                        $dtimes = \app\models\Eventday::find()
                                ->select('id, date_id')
                                ->where('visible=1 and date_id>0 and id_donor=:id_donor', [':id_donor' => $donor['id_donor']])
                                ->orderBy('priority ASC')
                                ->asArray()->all();
                        $outStr = '';
                        foreach ($dtimes as $dtime)
                        {
                            ?>
                            <a href="/private/eventday/update/?id=<?=$dtime['id']?>" target="_blank">
                                   <?="{$fullTimeList[$dtime['date_id']]['time']} {$fullTimeList[$dtime['date_id']]['typing']}";?>
                                   <br/>
                            </a>
                            <?php 
                        }
                        ?>
                    </td>
                <td>
                    <?php 
                    if($donor['approved'] != 1)
                    { ?>
                    <?php 
                        $donorTimeItems = isset($accessTimes[$timeKey]) ? $accessTimes : [$timeKey => $time['dtime']] + $accessTimes;?>
                    <?=\yii\helpers\Html::dropDownList(
                            'change-time', 
                            $donor['date_id'], 
                            $donorTimeItems, 
                            [
                                'onchange' => 'changedate(this)',
                                'donor-id' => $donor['id_donor'],
                                'time-id' => $donor['date_id'],
                                'item-id' => $donor['id']
                            ])?>
                    <?php }else{ ?>
                    <?=$time['dtime'];?>
                    <?php } ?>
                </td>
                <td>
                    <?php 
                    if($donor['approved'] != 1)
                    { ?>
                    <?=\yii\helpers\Html::dropDownList(
                            'change-status', 
                            $donor['approved'], 
                            app\models\Donorlist::getApproved(),
                            [
                                'onchange' => 'changestatus(this)',
                                'approved-id' => $donor['approved'],
                                'item-id' => $donor['id']
                            ])?>
                    <?php }else{ ?>
                    Утверждён
                    <?php } ?>
                </td>
                <td>
                    <a class="btn btn-success btn-sm" href="/private/donorlist/update?id=<?=$donor['id']?>" title="Редактировать" aria-label="Редактировать" data-pjax="0"><i class="fa fa-edit"></i></a>
                    <a class="btn btn-blue btn-sm" href="/private/donor/print?id=<?=$donor['id_donor']?>" title="Печать" aria-label="Печать" data-pjax="<?=$donor['id_donor']?>"><i class="fa fa-print"></i></a>
                </td>
            </tr>
            <?php
            }
            ?>
            <tr>
                <td colspan="7" style="background-color: bisque">Доноры с ошибками</td>
                <?php
                $counter = 0;
                foreach($nocorrectDonors as $donor)
                {
                    $counter++;
                    ?>
                <tr>
                    <td><?=$counter;?></td>
                    <td>
                        <a href="/private/donor/view/?id=<?=$donor->id?>" target="_blank" style="<?=$donor->is_vip ? '  color:green;' : ''?>">
                        <?=$donor->getAdminReprString()?>
                        <?php if($donor->is_vip) { ?> <br/>(VIP)<?php } ?>
                        <?php if($donor->weight == 0) { ?> <br/>(Вес от 50 до 58)<?php } ?>
                        <?php if($donor->typing == 1) { ?> <br/>(Типирование)<?php } ?>
                        </a>
                    </td>
                    <td>
                        <?php
                        $dtimes = \app\models\Eventday::find()
                                ->select('id, date_id')
                                ->where('visible=1 and date_id>0 and id_donor=:id_donor', [':id_donor' => $donor->id])
                                ->orderBy('priority ASC')
                                ->asArray()->all();
                        $outStr = '';
                        foreach ($dtimes as $dtime)
                        {
                            ?>
                            <a href="/private/eventday/update/?id=<?=$dtime['id']?>" target="_blank">
                                   <?="{$fullTimeList[$dtime['date_id']]['time']} {$fullTimeList[$dtime['date_id']]['typing']}";?>
                                   <br/>
                            </a>
                            <?php 
                        }
                        ?>
                    </td>
                    <td>
                        <a href="/private/donor/update/?id=<?=$donor->id?>" target="_blank">Ред. донора</a>
                    </td>
                </tr>
                <?php
                }
                ?>
            </tr>
        </tbody>
    </table>
</div>
<?php 
$model = new app\models\Notification;
$model->type_id = 0;
echo $this->render('notification-form', ['model' => $model]);
?>
<script>
function changedate(elem)
{
    if($(elem).val() != $(elem).attr('time-id'))
    {
        $.get(
            '/private/donorlist/changedate/', 
            {
                new_date_id: $(elem).val(),
                id : $(elem).attr('item-id')
            }, 
            function(){
                location.reload()
            }
            )
    }
}

function changestatus(elem)
{
    if($(elem).val() != $(elem).attr('approved-id'))
    {
        $.get(
            '/private/donorlist/changestatus/', 
            {
                new_approved_id: $(elem).val(),
                id : $(elem).attr('item-id')
            }, 
            function(){
                location.reload()
            }
            )
    }
}

function clearAction()
{
    return (confirm('Вы уверены, что хотите очистить список полностью?'))
}

function openNotificationBlock(timeId)
{
    $('#<?=\yii\helpers\Html::getInputId($model, 'date_id')?>').val(timeId).change();
    $('#notification-add-modal').modal('show');
}
</script>