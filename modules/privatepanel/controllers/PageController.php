<?php

namespace app\modules\privatepanel\controllers;

//use yii\web\Controller;
use app\modules\privatepanel\components\PrivateController;

class PageController extends PrivateController
{
    protected $_modelName = 'app\models\Page';
    
    public $formTemplate = '_form';
    
    public $isTreeAdminView = true;
    
    public $treeParentAttribute = 'parent_id';
    public $treeChildRelation = 'children';
}
?>
