<?php

namespace app\modules\privatepanel\controllers;

use app\modules\privatepanel\components\PrivateController;

class MailTemplateController extends PrivateController
{
    protected $_modelName = 'app\models\MailTemplate';
    
    public function actionSendfail()
    {
		die;
        $donorsFail = \app\models\Donorlite::find()->where('donorstatus != 6')->all();
		//$donorsFail = \app\models\Donorlite::find()->where('id = 42')->all();
		$counter = 0;
        foreach($donorsFail as $donor)
        {
				echo ++$counter;
				echo " - ";
				echo $donor->id;
				echo " - ";
                //$this->sendFailEmail($donor);
				echo "\n<br/>";
        }
    }
	
	public function actionSendsuccess()
    {
        $donorsSucc= \app\models\Donorlite::find()->where('donorstatus = 6')->all();
		//$donorsSucc = \app\models\Donorlite::find()->where('id = 42')->all();
		$counter = 0;
        foreach($donorsSucc as $donor)
        {
				echo ++$counter;
				echo " - ";
				echo $donor->id;
				echo " - ";
                //$this->sendSuccEmail($donor);
				echo "\n<br/>";
        }
    }
	
	private function sendFailEmail($donor)
    {
            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
            $headers .= 'From: donor.spb.ru' . "\r\n" .'Reply-To: borovov.alexandr@gmail.com' . "\r\n";
            $template = "
            <html>
            <head>
              <title>Информационное письмо - День Донора</title>
            </head>
            <body>
            <p>Здравствуйте, {$donor->name}!</p>";
            $template .= "<p>К сожалению, станция переливания может принять лишь ограниченное количество доноров, и Вы не попали в их число.</p>
				<p>Вам не стоит расстраиваться, потому что:<br />
				1. На следующий День Донора, который состоится осенью, мы планируем договориться со станцией об увеличении количества доноров.<br />
				2. В следующий раз Вы будете иметь более высокий приоритет, чем люди, которые сдали кровь в этот раз.<br />
				3. Мы планируем создать систему, которая позволит сдавать кровь вне ВУЗа с получением материальной компенсации. 
				Информацию о запуске системы Вы можете узнать на сайте и через email-рассылку.
				Если Вы будете сдавать кровь, пожалуйста, сохраните справку об этом, она может пригодиться.
				</p>
				<p>Спасибо Вам за стремление помогать.</p>
				<p>С уважением, команда Дня Донора.</p>
            </body>
            </html>";
            echo ($donor->email);
			echo " - ";
            var_dump(mail($donor->email, 'Информационное письмо - День Донора', $template, $headers));
			
    }
	
	
	private function sendSuccEmail($donor)
    {
		die;
            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
            $headers .= 'From: donor.spb.ru' . "\r\n" .'Reply-To: borovov.alexandr@gmail.com' . "\r\n";
            $template = "
            <html>
            <head>
              <title>Информационное письмо - День Донора</title>
            </head>
            <body>
            <p>Здравствуйте, {$donor->name}!</p>";
			$dateTime = date('d.m.Y в H:i', strtotime($donor->donate_day));
            $template .= "<p>Вы сдаете кровь <b>{$dateTime}</b> в Экспоцентре ГЗ. <br/>
			Пожалуйста, не опаздывайте.<br /><br />
			С собой возьмите:<br />
			1. Паспорт, местную регистрацию (для иногородних).<br />
			2. Мелочь: 11 рублей на сдачу.</p>
			<p>Имейте ввиду:<br />
			1. На мероприятии будут присутствовать фотографы.<br />
			2. Станция может сделать отвод по здоровью.</p>
			<p>С уважением, команда Дня Донора.</p>
            </body>
            </html>";
            echo ($donor->email);
			echo " - ";
            var_dump(mail($donor->email, 'Информационное письмо - День Донора', $template, $headers));
			mail('w1121@yandex.ru', 'Информационное письмо - День Донора', $template, $headers);  
			
    }
	
	
	
}
?>
