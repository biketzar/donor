<?php

namespace app\modules\privatepanel\controllers;

//use yii\web\Controller;
use app\modules\privatepanel\components\PrivateController;
use app\components\SiteHelper;
use app\modules\privatepanel\components\PrivateModelAdmin;
use \app\modules\privatepanel\Module;

class PhotoGalleryController extends PrivateController
{
    protected $_modelName = 'app\models\Photogallery';
    
    protected $targetDir = "plupload";
    
    protected $createIfEmptyList = false;
    
    public $enableCsrfValidation = false;
    
    public function init()
    {
        parent::init();
        $this->targetDir = \Yii::getAlias('@runtime'). "/".$this->targetDir;
    }
    
    public function actionMultipleupload()
    {
        $modelName = $this->_modelName;

        $model = \Yii::createObject(['class' => $modelName, 'scenario' => 'insert']);
        
        $getParams = \yii::$app->request->getQueryParams();
           
        if(isset($getParams))
        {
            foreach($getParams as $param=>$value)
            {
                if(isset($value))
                {
                    if($model->hasAttribute($param))
                        $model->$param = $value;
                }
            }
            foreach($getParams as $param=>$value)
            {
                if(isset($value))
                {
                    if($relation = $model->getRelation($param, false))
                    {
                        if(!$relation->multiple)
                        {
                            foreach($relation->link as $a=>$b)
                                $model->$b = $value;
                        } 
                    }
                }
            }
        }
        
        $privateModelAdmin = \Yii::createObject(['class' => 'app\modules\privatepanel\components\PrivateModelAdmin'],[$model]);
        
        if(\Yii::$app->request->isPost && !\Yii::$app->request->isAjax)
        {
            $data = \Yii::$app->request->post($model->formName());
            $errors = array();
            $success = true;
            
            if(is_array($data['image']))
            {
            foreach($data['image'] as $file)
            {
                $newmodel = \Yii::createObject(['class' => $modelName, 'scenario' => 'insert']);
        
                $newmodel->load(\Yii::$app->request->get());
        
                $getParams = \Yii::$app->request->getQueryParams(); 

                if(isset($getParams))
                {
                    foreach($getParams as $param=>$value)
                    {
                        if(isset($value))
                        {
                            if($newmodel->hasAttribute($param))
                                $newmodel->$param = $value;
                        }
                    }
                    foreach($getParams as $param=>$value)
                    {
                        if(isset($value))
                        {
                            if($relation = $newmodel->getRelation($param, false))
                            {
                                if(!$relation->multiple)
                                {
                                    foreach($relation->link as $a=>$b)
                                        $newmodel->$b = $value;
                                } 
                            } 
                        }
                    }
                }
                
                $filename = $file;
                $pi = pathinfo($filename);
                
                $filename = SiteHelper::translit(trim($pi['filename'])).'.'.$pi['extension'];
                
                $path = trim($newmodel->getImageDirectory(), '/');
                
                $path = \Yii::getAlias('@webroot').'/'.trim($path);
                
                SiteHelper::checkFolderPath($path);
                /*if(!is_dir($path))
                {
                    $path_parts = str_replace(\Yii::getAlias('@webroot').'/', '', $path);
                    $path_parts = explode('/',$path_parts);

                    $path_temp = '';
                    for($i = 0; $i<count($path_parts); $i++)
                    {
                        $path_temp.= $path_parts[$i].'/';
                        if(!file_exists(\Yii::getAlias('@webroot').'/'.$path_temp))
                        {
                            mkdir(\Yii::getAlias('@webroot').'/'.$path_temp, 0755);
                        }
                    }
                }*/
                
                $newmodel->image = $filename;
                
                $path = \Yii::getAlias('@webroot').'/'.trim($newmodel->getImagePath(), '/');
                
                if(file_exists($this->targetDir.'/'.$file) && copy($this->targetDir.'/'.$file, $path))
                {
                    @unlink($this->targetDir.'/'.$file);
                    
                    if($newmodel->save()){
                        //$this->createSuccessMessage && $this->setAdminMessage($this->createSuccessMessage, 'success');
                    } else {
                        $errors = array_merge($errors, $newmodel->getErrors());
                        $success = false;
                    }
                } else {
                    if(!isset($errors['image']))
                        $errors['image'] = array();
                    $errors['image'][] = Module::t('app', 'Cannot move file').' '.$file;
                    $success = false;
                }

            }
            }
            
            if($success)
            {
                $this->createSuccessMessage && $this->setAdminMessage($this->createSuccessMessage, 'success');
                $params = ['admin'];
                $params = array_merge($params, $privateModelAdmin->additionalUrlParams);
                
                $this->redirect($params);
                return;
            } else {
                $model->addErrors($errors);
            }


        }

        return $this->render('multipleupload', array(
            'model'=>$model,
            'privateModelAdmin'=>$privateModelAdmin,
        ));


    }
    public function actionProcessupload()
    {
        // Make sure file is not cached (as it happens for example on iOS devices)
        header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");

        /* 
        // Support CORS
        header("Access-Control-Allow-Origin: *");
        // other CORS headers if any...
        if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
                exit; // finish preflight CORS requests here
        }
        */

        // 5 minutes execution time
        @set_time_limit(5 * 60);

        // Uncomment this one to fake upload time
        // usleep(5000);

        // Settings
        //$targetDir = ini_get("upload_tmp_dir") . DIRECTORY_SEPARATOR . "plupload";

        $targetDir = $this->targetDir; 

        //$targetDir = 'uploads';
        $cleanupTargetDir = true; // Remove old files
        $maxFileAge = 5 * 3600; // Temp file age in seconds


        // Create target dir
        if (!file_exists($targetDir)) {
                @mkdir($targetDir);
        }
        
        $fileName = '';
        
        // Get a file name
        if (isset($_REQUEST["name"])) {
                $fileName = $_REQUEST["name"];
        } elseif (!empty($_FILES)) {
                $fileName = $_FILES["file"]["name"];
        } 
                    //else {
        //        $fileName = uniqid("file_");
       //}

        $filePath = $targetDir . DIRECTORY_SEPARATOR . $fileName;

        // Chunking might be enabled
        $chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
        $chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;


        // Remove old temp files	
        if ($cleanupTargetDir) {
                if (!is_dir($targetDir) || !$dir = opendir($targetDir)) {
                        die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "Failed to open temp directory."}, "id" : "id"}');
                }

                while (($file = readdir($dir)) !== false) {
                        $tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;

                        // If temp file is current file proceed to the next
                        if ($tmpfilePath == "{$filePath}.part") {
                                continue;
                        }

                        // Remove temp file if it is older than the max age and is not the current file
                        if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge)) {
                                @unlink($tmpfilePath);
                        }
                }
                closedir($dir);
        }	


        // Open temp file
        if (!$out = @fopen("{$filePath}.part", $chunks ? "ab" : "wb")) {
                die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
        }

        if (!empty($_FILES)) {
                if ($_FILES["file"]["error"] || !is_uploaded_file($_FILES["file"]["tmp_name"])) {
                        die('{"jsonrpc" : "2.0", "error" : {"code": 103, "message": "Failed to move uploaded file."}, "id" : "id"}');
                }

                // Read binary input stream and append it to temp file
                if (!$in = @fopen($_FILES["file"]["tmp_name"], "rb")) {
                        die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
                }
        } else {	
                if (!$in = @fopen("php://input", "rb")) {
                        die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
                }
        }

        while ($buff = fread($in, 4096)) {
                fwrite($out, $buff);
        }

        @fclose($out);
        @fclose($in);

        // Check if file has been uploaded
        if (!$chunks || $chunk == $chunks - 1) {
                // Strip the temp .part suffix off 
                rename("{$filePath}.part", $filePath);
        }

        // Return Success JSON-RPC response
        die('{"jsonrpc" : "2.0", "result" : "'.$fileName.'", "id" : "id"}');
    }
    
}
?>
