<?php

namespace app\modules\privatepanel\controllers;

use yii\web\Controller;

use app\components\AccessControl;

class UserController extends Controller
{
    public $_logAction = true;
    
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    ['class' => 'app\components\ActionRule']
                ]
            ],
        ];
    }
    
    /**
     * Установка сообщения для пользователя
     * @param string $msg
     * @param boolean $ctrl 
     */
    public function setAdminMessage($msg, $type = 'info')
    {
        \Yii::$app->session->setFlash('admin_message_'.$type, $msg);
    }

    /**
     * Получение сообщения для пользователя
     * @param boolean $ctrl 
     * @return string
     */
    public function getAdminMessage($type = 'info')
    {
        if ( $this->hasAdminMessage($type) )
            return \Yii::$app->session->getFlash('admin_message_'.$type);
        else
            return '';
    }
	
	/**
     * Проверка наличия сообщения для польщователя
     * @param boolean $ctrl
     * @return boolean 
     */
    public function hasAdminMessage($type = 'info')
    {
        return \Yii::$app->session->hasFlash('admin_message_'.$type);
    }
    
    public function actionIndex()
    {
        return $this->actionAdmin();
    }
    
    public function actionAdmin()
    {
        $model = \Yii::createObject(['class' => 'app\models\User', 'scenario' => 'search']);
	
        $session_key = 'navigation_'.$this->module->id.'_'.$this->id.'_'.$this->action->id;
        $session = \Yii::$app->session->get($session_key, array());
	$data = \Yii::$app->request->get();
	//$search_params = \Yii::$app->request->get();
        
        if(isset($data)) {
                $model->load($data);
                $session['filter'] = $data;
        } elseif(isset($session['filter'])) {
                $model->load($session['filter']);
        }
        
        $countOnPage = $this->getCountOnPage($session);
        
        $query = $model->searchQuery($data);
        
        //$query = $model::find();
        
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $countOnPage,
            ],
        ]);
	
        \Yii::$app->session->set($session_key, $session);
        
        return $this->render('admin',array(
            'dataProvider'=>$dataProvider,
            'model'=>$model,
            
        ));
    }
    
    public function actionView()
    {
        $model = \Yii::createObject(['class' => 'app\models\User']);
	
        $model = \app\models\User::findOne(\Yii::$app->request->get('id', ''));
        
        if($model)
        {
            return $this->render('view',
                [
                    'model'=>$model,
                ]
            );
        } else {
            $this->setAdminMessage(\Yii::t('app', 'There is no record with this primary key', 'error'));
            $params = ['admin'];
            $this->redirect($params);
        }
    }
    
    public function actionCreate()
    {
	$modelName = 'app\\models\\'.$this->_modelName;

        $model = \Yii::createObject(['class' => $modelName]);
	//$model->getAdmin();
        $model->load(\Yii::$app->request->get());
        
        $getParams = \Yii::$app->request->getQueryParams(); 
        
        if(isset($getParams))
        {
            foreach($getParams as $param=>$value)
            {
                if(isset($value))
                {
                    if($relation = $model->getRelation($param, false))
                    {
                        if(!$relation->multiple)
                        {
                            foreach($relation->link as $a=>$b)
                                $model->$b = $value;
                        } 
                    }
                }
            }
        }
	
        if($this->enableAjaxValidation && \Yii::$app->request->isAjax && \Yii::$app->request->post('ajax', null) === strtolower($this->_modelName).'-form')
        {
            return $this->performAjaxValidation($model);
        }

        if(\Yii::$app->request->isPost)
        {
            $privateModelAdmin = new PrivateModelAdmin($model);
            
            $data = $privateModelAdmin->prepareForSave(\Yii::$app->request->post(), false);
            
            $model->load($data);
            
            if($model->save())
            {
                $privateModelAdmin->setModel($model);
                $privateModelAdmin->afterSave($data);
                
                $this->createSuccessMessage && $this->setAdminMessage($this->createSuccessMessage, 'success');
        	$id = $model->getPrimaryKey();
                                
                if(\Yii::$app->request->post('savetype', '')  == 'apply')
                {
                    $params = ['update', 'id'=>$id];
                    $params = array_merge($params, $privateModelAdmin->additionalUrlParams);
                        
                    $this->redirect($params);
                    return;
                } else {
                    $params = ['admin'];
                    $params = array_merge($params, $privateModelAdmin->additionalUrlParams);
                    $this->redirect($params);
                    return;
                }
            }
        }
        
        $privateModelAdmin = new PrivateModelAdmin($model);
        
        return $this->render($this->actionCreateTemplate,
            [
                'privateModelAdmin'=>$privateModelAdmin,
            ]
        );
    }
    
    
    public function actionUpdate()
    {
	$model = \app\models\User::findOne(\Yii::$app->request->get('id', ''));
        
	if($model)
        {	
            if($this->enableAjaxValidation && \Yii::$app->request->isAjax && \Yii::$app->request->post('ajax', null) === strtolower($model->formName()).'-form')
            {
                return $this->performAjaxValidation($model);
            }

            if(\Yii::$app->request->isPost)
            {
                $model->load(\Yii::$app->request->post());

                if($model->save())
                {
                    $this->updateSuccessMessage && $this->setAdminMessage(\Yii::t('app', 'Success update'), 'success');
                    $id = $model->getPrimaryKey();
                    
                    if(\Yii::$app->request->post('savetype', '')  == 'apply')
                    {
                        $params = ['update', 'id'=>$id];
                        $this->redirect($params);
                        return;
                    } else {
                        $params = ['admin'];
                        $this->redirect($params);
                        return;
                    }
                }
            }

            return $this->render('update',
                [
                    'model'=>$model,
                ]
            );
        } else {
            $this->setAdminMessage(\Yii::t('app', 'There is no record with this primary key', 'error'));
            $params = ['admin'];
            $this->redirect($params);
        }
    }
    
    public function actionDelete()
    {
        if(\Yii::$app->request->isPost || \Yii::$app->request->isGet && \Yii::$app->request->validateCsrfToken(\Yii::$app->request->get('token', '')))
        {
            // we only allow deletion via POST request
            $modelName = 'app\\models\\'.$this->_modelName;

            $model = $modelName::findOne(\Yii::$app->request->get('id', ''));
            
            $model->delete();
		
            if(\Yii::$app->request->get('ajax', ''))
            {
            } else {
                $params = ['admin'];
                $params = array_merge($params, $privateModelAdmin->additionalUrlParams);
                $this->redirect($params);
            }
            
        }
        else
            throw new \yii\web\NotFoundHttpException(\Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }
    public function actionSuggest()
    {
        if(\Yii::$app->request->get('field') || \Yii::$app->request->get('term'))
        {
            $field = \Yii::$app->request->get('field');
            $term = \Yii::$app->request->get('term');
            $modelName = 'app\\models\\'.$this->_modelName;
            
            $model = \Yii::createObject(['class' => $modelName, 'scenario' => 'search']);
	
            $query = $modelName::find();
        
            $query->andFilterWhere(['like', $field, $term]);
            
            $dataProvider = new ActiveDataProvider([
                'query' => $query,
                'pagination' => [
                    'pageSize' => 100,
                ],
            ]);
            $result = [];
            foreach($dataProvider->getModels() as $object)
            {
                $result[] = array(
                        'id' => $object->primaryKey,
                        'label' => $object->$field,
                        'value' => $object->$field,
                );
            }
            
            \Yii::$app->response->format = \Yii\web\Response::FORMAT_JSON;
            return json_encode($result);
            
        } else {
            throw new \yii\web\NotFoundHttpException(\Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
        }
    }
    
    public function afterAction($action, $result)
    {
        $result = parent::afterAction($action, $result);

        if($this->_logAction){
                $this->whiteLog();
        }
        return $result;
    }
    
    protected function performAjaxValidation($model)
    {
        
        $model->load(\Yii::$app->request->post());

        \Yii::$app->response->format = \Yii\web\Response::FORMAT_JSON;
        return ActiveForm::validate($model);
            
        
    }
    
    protected function whiteLog()
    {
        $id = \Yii::$app->request->get('id', 0);
        \Yii::$app->db->createCommand()->insert('{{%private_log}}',[
            'user_id' => \Yii::$app->user->getId(),
            'controller' => $this->id,
            'action' => $this->action->id,
            'model' => 'app\\models\\User',
            'key' => $id,
            'is_post' => \Yii::$app->request->isPost?1:0,
            'datetime' => date('Y-m-d H:i:s'),
        ])->execute();
    }
    
    protected function getCountOnPage($session)
    {
        $count = \Yii::$app->request->get('countOnPage', null);
        if(isset($count) && ($count > 0 || $count == -1)){
            $countOnPage = min((int)$count, 999);
            $session['countOnPage'] = $countOnPage;
        }else{
            if(isset($session['countOnPage'])){
                $countOnPage = $session['countOnPage'];
            } else {
                $countOnPage = 20;
                $session['countOnPage'] = $countOnPage;
            }
        }
        return $countOnPage;
    }
}
?>
