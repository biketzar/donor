<?php

namespace app\modules\privatepanel\controllers;

use \app\modules\privatepanel\Module;
//use yii\web\Controller;
use app\modules\privatepanel\components\PrivateController;
use app\modules\privatepanel\components\PrivateModelAdmin;

class MetadataController extends PrivateController
{
    protected $_modelName = 'app\models\Metadata';
    
    public $formTemplate = '_form';
    
    public function actionAdmin() {
        $modelName = $this->_modelName;

        $model = \Yii::createObject(['class' => $modelName, 'scenario' => 'insert']);
        
        //$model->getAdmin();
        $model->load(\Yii::$app->request->get());
        
        $privateModelAdmin = \Yii::createObject(['class' => 'app\modules\privatepanel\components\PrivateModelAdmin'],[$model]);
        
        $getParams = \yii::$app->request->getQueryParams();
                
        if(isset($getParams))
        {
            foreach($getParams as $param=>$value)
            {
                if(isset($value))
                {
                    if($model->hasAttribute($param))
                        $model->$param = $value;
                }
            }
            foreach($getParams as $param=>$value)
            {
                if(isset($value))
                {
                    if($relation = $model->getRelation($param, false))
                    {
                        if(!$relation->multiple)
                        {
                            foreach($relation->link as $a=>$b)
                                $model->$b = $value;
                        } 
                    }
                }
            }
        }
        if($model->model && $model->item_id)
        {
            $newmodel = $modelName::find()->andWhere('model=:model', [':model' => $model->model])->andWhere('item_id=:item_id', [':item_id' => $model->item_id])
                ->limit(1)->one();
            if(!$newmodel)
            {
                $params = ['create'];
                $params = array_merge($params, $privateModelAdmin->additionalUrlParams);
                $this->redirect($params);
                return;
            } else {
                $params = ['update', 'id' => $newmodel->primaryKey];
                $params = array_merge($params, $privateModelAdmin->additionalUrlParams);
                $this->redirect($params);
                return;
            }
        } else {
            $this->setAdminMessage(Module::t('app', 'Incorrect request', 'error'));
            $this->redirect(\yii::$app->request->getReferrer());
        }
        
    }
    
}
?>
