<?php

namespace app\modules\privatepanel\controllers;

use yii\web\Controller;
use \app\modules\privatepanel\Module;

use app\components\AccessControl;
use yii\data\ActiveDataProvider;
//use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\modules\privatepanel\components\PrivateModelAdmin;
use app\modules\privatepanel\components\TreeActiveDataProvider;
use app\modules\privatepanel\components\fields;
use app\modules\privatepanel\components\PrivateController;

class DonorController extends PrivateController
{
    protected $_modelName = 'app\models\Donorlite';
    
    public function actionPrint()
    {
        if(\Yii::$app->request->isPost)
        {
            $model = new $this->_modelName;
            $model->setScenario('print');
            $privateModelAdmin = new PrivateModelAdmin($model);
            if(\Yii::$app->request->get('id'))
            {
                $id = (int)\Yii::$app->request->get('id');
                $model = \app\models\Donorlite::find()->where("id={$id}")->one();
                if(!$model)
                {
                    $model = new $this->_modelName;
                }
            }
            else
            {
                $model = new $this->_modelName;
            }
            $model->setScenario('print');
            $privateModelAdmin = new PrivateModelAdmin($model);
            if(\Yii::$app->request->get('id'))
            {
                $data = $privateModelAdmin->prepareForSave(\Yii::$app->request->post(), false);
                $model->load($data);
                //если выставлен сдал официально, то обновляем статус на статус сезона
                if($model->inPoliteh)
                {
                    $activestatus = \app\models\Donorstatus::find()->where('active=1')->one();
                    if($activestatus)
                    {
                        //$model->donorstatus = $activestatus->id;
                    }
                    //добавляем в историю донора
//                    $donorhistory = \app\models\Donorhistory::find()
//                            ->where("id_donor=:id_donor AND month(date)=:month AND year(date)=:year", 
//                                    [
//                                        ':id_donor' => $model->id,
//                                        ':month' => date('n'),
//                                        ':year' => date('Y')
//                                        ])->one();
//                    if(!$donorhistory)//на случай повтора печати
//                    {
//                        $donorhistory = new \app\models\Donorhistory;
//                        $donorhistory->id_donor = $model->id;
//                        $donorhistory->is_politeh = 1;
//                        $donorhistory->save();
//                    }
                }
                $model->save();
                //var_dump( $model->getErrors());
            }
            else
            {
                $data = $privateModelAdmin->prepareForSave(\Yii::$app->request->post(), false);
                $model->load($data);
            }

            if(\Yii::$app->request->post('new-tab', 0) == 1)
            {
                return $this->render('statementedit',
                    [
                        'privateModelAdmin'=>$privateModelAdmin,
                    ]
                );
            }
            else {
                $model->department_id = \app\models\Departments::find()->where('id=:id', [':id'=>$model->department_id])->one()->name;
                return $this->renderPartial('statement',
                    [
                        'model'=>$model,
                    ]
                );
            }
        }
        
        $this->logOn();
        
	$modelName = $this->_modelName;
        
        $keys = [];
        foreach($modelName::primaryKey() as $key)
        {
            if(\Yii::$app->request->get($key))
                $keys[$key] = \Yii::$app->request->get($key);
        }
        if(!count($keys))
            $keys['id'] = -1;
        $model = $modelName::find()->andWhere($keys)->one();
        if($model)
        {
            //include_once $_SERVER['DOCUMENT_ROOT'].'/vendor/Library/NCL.NameCase.ru.php';
			$model->inPoliteh = 1;
            $model->setScenario('print');
            //$ncl = new \NCLNameCaseRu();
            //$model->roditName = $ncl->qFullName($model->surname, $model->name, $model->pname, ($model->sex == 0 ? \NCL::$MAN : \NCL::$WOMAN), \NCL::$RODITLN, "S N F");
        }
        else
        {
            return 'Donor not found';
        }
        $privateModelAdmin = new PrivateModelAdmin($model);
        
        if($model)
        {
            $model->printDate = date('d.m.Y');
            return $this->render('statementedit',
                [
                    'privateModelAdmin'=>$privateModelAdmin,
                ]
            );
        } else {
            $this->setAdminMessage(Module::t('app', 'There is no record with this primary key', 'error'));
            $params = ['admin'];
            $params = array_merge($params, $privateModelAdmin->additionalUrlParams);
            $this->redirect($params);
        }
    }
    
    public function actionBlank()
    {
        if(\Yii::$app->request->get('id'))
        {
            $model = \app\models\Donorlite::find()->where('id=:id', [':id' => \Yii::$app->request->get('id')])->one();
            if($model)
            {
                $model->department_id = \app\models\Departments::find()->where('id=:id', [':id'=>$model->department_id])->one()->name;
                return $this->renderPartial('statement',
                    [
                        'model'=>$model,
                    ]
                );
            }
        }
        $model = new \app\models\Donorlite;
        $privateModelAdmin = new PrivateModelAdmin($model);
        $params = ['admin'];
        $params = array_merge($params, $privateModelAdmin->additionalUrlParams);
        return $this->redirect($params);
        
    }
    
    
    public function actionChange()
    {
        $this->logOff();

        $modelName = $this->_modelName;
        
        $keys = [];
        foreach($modelName::primaryKey() as $key)
        {
            if(\Yii::$app->request->get($key))
                $keys[$key] = \Yii::$app->request->get($key);
        }
        if(!count($keys))
            $keys['id'] = -1;
        
        $model = $modelName::find()->andWhere($keys)->one();
        
        if($model)
        {
                $model->setScenario('setdate');
	
            $privateModelAdmin = new PrivateModelAdmin($model);

            if(\Yii::$app->request->get($model->formName()))
            {
                $data = $privateModelAdmin->prepareForSave(\Yii::$app->request->get(), true);
                $model->load($data);
            }
            
            if($this->enableAjaxValidation && \Yii::$app->request->isAjax && \Yii::$app->request->post('ajax', null) === strtolower($model->formName()).'-form')
            {
                return $this->performAjaxValidation($model);
            }

            if(\Yii::$app->request->isPost)
            {
                //$privateModelAdmin = new PrivateModelAdmin($model);
                $data = $privateModelAdmin->prepareForSave(\Yii::$app->request->post(), false);

                $model->load($data);
                if($model->save())
                {
                    $privateModelAdmin->setModel($model);
                    $privateModelAdmin->afterSave($data);
                    
                    $this->updateSuccessMessage && $this->setAdminMessage($this->updateSuccessMessage, 'success');
                    
                    $this->_modelPrimaryKeys = $model->getPrimaryKey(true);
                    
                    $this->logOn();
                    
                    if(\Yii::$app->request->post('savetype', '')  == 'apply')
                    {
                        $params = ArrayHelper::merge(['change'], $model->getPrimaryKey(true));
                        $params = array_merge($params, $privateModelAdmin->additionalUrlParams);
                        $this->redirect($params);
                        return;
                    } else {
                        $params = ['admin'];
                        $params = array_merge($params, $privateModelAdmin->additionalUrlParams);
                        $this->redirect($params);
                        return;
                    }
                } else {
                    $privateModelAdmin->setModel($model);
                }
            }

            if(\Yii::$app->request->isAjax)
            {
                return $this->renderPartial($this->actionCreateTemplate,
                    [
                        'privateModelAdmin'=>$privateModelAdmin,
                    ]
                );
            }
            return $this->render($this->actionUpdateTemplate,
                [
                    'privateModelAdmin'=>$privateModelAdmin,
                ]
            );
        } else {
            $this->setAdminMessage(Module::t('app', 'There is no record with this primary key', 'error'));
            $params = ['admin'];
            $params = array_merge($params, $privateModelAdmin->additionalUrlParams);
            $this->redirect($params);
        }
    } 
    
    public function actionShowtime()
    {
        $dates = ['2016-04-13', '2016-04-14', '2016-04-15'];
        $times = ['09:00', '09:30', 
                  '10:00', '10:30', 
                  '11:00', '11:30',
            ];
        $timeStamps = [];
        foreach($dates as $date)
        {
            foreach($times as $time)
            {
                $timeStamps[] = strtotime("{$date} {$time}");
            }
        }
        $eventTimes = $this->getEventTime();
        $donors = \app\models\Donorlite::find()->where('donorstatus=3 or donorstatus=6')->orderBy('id ASC')->all();
        $eventDonorList = [];
        foreach($timeStamps as $timeStamp)
        {
            foreach($donors as $donor)
            {
                if($timeStamp == strtotime($donor->donate_day))
                {
                    if($donor->dcard_id > 0)
                    {
                        $eventDonorList[$timeStamp][1][] = $donor;
                    }
                    else 
                    {
                        $eventDonorList[$timeStamp][0][] = $donor;
                    }
                }
            }
        }
		$depModels = \app\models\Departments::find()->all();
		$dertaments = [];
		foreach($depModels  as $depModel)
		{
			$dertaments[$depModel->id] = $depModel->name;
		}
		
        return $this->render('showtime', [
            'timeStamps'=>$timeStamps,
            'eventDonorList'=>$eventDonorList,
			'dertaments' => $dertaments
                ]);
    }
    
    private function getSetTimes()
    {
        $result = [];
        $donors = \app\models\Donorlite::find()->where('donorstatus=3 or donorstatus=6')->orderBy('id ASC')->all();
        foreach($donors as $donor)
        {
            
            if($donor->dcard_id > 0)
            {
                if(!isset($result[strtotime($donor->donate_day)][1]))
                {
                    $result[strtotime($donor->donate_day)][1] = 0;
                }
                $result[strtotime($donor->donate_day)][1]++;
            }
            else 
            {
                if(!isset($result[strtotime($donor->donate_day)][0]))
                {
                    $result[strtotime($donor->donate_day)][0] = 0;
                }
                $result[strtotime($donor->donate_day)][0]++;
            }
        }
        return $result;
    }

    public function actionChooseOld()
    {
        $dates = ['2016-04-13', '2016-04-14', '2016-04-15'];
        $times = ['09:00', '09:30',
                  '10:00', '10:30',
                  '11:00', '11:30'
            ];
        $donorsNoRepeat = \app\models\Donorlite::find()->where('donorstatus!=7 and donorstatus!=3 and donorstatus!=6 and dcard_id=0')->orderBy('id ASC')->all();
        $donorRepeat = \app\models\Donorlite::find()->where('donorstatus!=7 and donorstatus!=3 and donorstatus!=6 and dcard_id>0')->orderBy('id ASC')->all();
        $eventTimes = $this->getEventTime();
        $setEventTimes = $this->getSetTimes();
        foreach($dates as $date)
        {
            foreach($times as $time)
            {
                $timeStamp = strtotime("{$date} {$time}");
                $condition = 0;
                if(isset($setEventTimes[$timeStamp][1]))
                {
                    $condition = $setEventTimes[$timeStamp][1];
                }
                if(isset($setEventTimes[$timeStamp][0]))
                {
                    $condition = $condition + $setEventTimes[$timeStamp][0];
                }
                //повторно
                if(isset($setEventTimes[$timeStamp][1]))
                {
                    $counter = $setEventTimes[$timeStamp][1];
                }
                else 
                {
                    $counter = 0;
                }
                //echo "2 - {$date} {$time} - $counter - $condition <br/>";
                
                foreach($donorRepeat as $key=>$donor)
                {
                    if($counter >= 5 || $condition>=10)
                    {
                        break;
                    }
                    $priotity=1;
                    if(isset($eventTimes[$donor->id][$priotity]) && $eventTimes[$donor->id][$priotity] == $timeStamp)
                    {
                        
                        $donor->donate_day = "{$date} {$time}";
                        $donor->donorstatus = 6;
                        $donor->save();
                        unset($donorRepeat[$key]);
                        $counter++;
                        $condition++;
                        continue;
                    }
                    if($counter >= 5 || $condition>=10)
                    {
                        break;
                    }
                    $priotity=2;
                    if(isset($eventTimes[$donor->id][$priotity]) && $eventTimes[$donor->id][$priotity] == $timeStamp)
                    {
                        
                        $donor->donate_day = "{$date} {$time}";
                        $donor->donorstatus = 6;
                        $donor->save();
                        unset($donorRepeat[$key]);
                        $counter++;
                        $condition++;
                        continue;
                    }
                    if($counter >= 5 || $condition>=10)
                    {
                        break;
                    }
                    $priotity=3;
                    if(isset($eventTimes[$donor->id][$priotity]) && $eventTimes[$donor->id][$priotity] == $timeStamp)
                    {
                        
                        $donor->donate_day = "{$date} {$time}";
                        $donor->donorstatus = 6;
                        $donor->save();
                        unset($donorRepeat[$key]);
                        $counter++;
                        $condition++;
                        continue;
                    }
                }
                
                
                // в первый раз
                if(isset($setEventTimes[$timeStamp][0]))
                {
                    //$counter = $setEventTimes[$timeStamp][0];
                    $counter = $setEventTimes[$timeStamp][0] - (5 - $counter);
                }
                else
                {
                    //$counter = 0;
                    $counter = 0 - (5 - $counter);
                }
                //echo "1 - {$date} {$time} - $counter - $condition <br/>";
                
                foreach($donorsNoRepeat as $key=>$donor)
                {
                    if($counter >= 5 || $condition>=10)
                    {
                        break;
                    }
                    $priotity=1;
                    if(isset($eventTimes[$donor->id][$priotity]) && $eventTimes[$donor->id][$priotity] == $timeStamp)
                    {
                        $donor->donate_day = "{$date} {$time}";
                        $donor->donorstatus = 6;
                        $donor->save();
                        unset($donorsNoRepeat[$key]);
                        $counter++;
                        $condition++;
                        continue;
                    }
                    if($counter >= 5 || $condition>=10)
                    {
                        break;
                    }
                    $priotity=2;
                    if(isset($eventTimes[$donor->id][$priotity]) && $eventTimes[$donor->id][$priotity] == $timeStamp)
                    {
                        $donor->donate_day = "{$date} {$time}";
                        $donor->donorstatus = 6;
                        $donor->save();
                        unset($donorsNoRepeat[$key]);
                        $counter++;
                        $condition++;
                        continue;
                    }
                    if($counter >= 5 || $condition>=10)
                    {
                        break;
                    }
                    $priotity=3;
                    if(isset($eventTimes[$donor->id][$priotity]) && $eventTimes[$donor->id][$priotity] == $timeStamp)
                    {
                        $donor->donate_day = "{$date} {$time}";
                        $donor->donorstatus = 6;
                        $donor->save();
                        unset($donorsNoRepeat[$key]);
                        $counter++;
                        $condition++;
                        continue;
                    }
                }
            }
        }
        $this->redirect('/private/donor/showtime/');
    }
    
    public function actionChoose()
    {
        if(\Yii::$app->user->getId() != 1)
        {
            $this->redirect('/private/donor/showtime/');
            die;
        }
        $dates = ['2016-04-13', '2016-04-14', '2016-04-15'];
        $times = ['09:00', '09:30',
                  '10:00', '10:30',
                  '11:00', '11:30'
            ];
        $timeStamps = [];
        $datesArr = [];
        foreach($dates as $date)
        {
            foreach($times as $time)
            {
                $timeStamps[strtotime("{$date} {$time}")] = \app\models\Donorlite::find()->where('donorstatus=6 and donate_day=:donate_day', [':donate_day'=>"{$date} {$time}"])->count();
                $datesArr[strtotime("{$date} {$time}")] = "{$date} {$time}";
            }
        }
        //var_dump($timeStamps);die;
        $donorsNoRepeat = \app\models\Donorlite::find()->where('donorstatus!=7 and donorstatus!=3 and donorstatus!=6 and dcard_id=0')->orderBy('id ASC')->all();
        $donorRepeat = \app\models\Donorlite::find()->where('donorstatus!=7 and donorstatus!=3 and donorstatus!=6 and dcard_id>0')->orderBy('id ASC')->all();
        $eventTimes = $this->getEventTime();
        $setEventTimes = $this->getSetTimes();
        $counter = \app\models\Donorlite::find()->where('donorstatus=6 and dcard_id>0')->count();
        foreach($donorRepeat as $key=>$donor)
        {
            if($counter >= 90)
            {
                break;
            }
            $priotity=1;
            if(isset($eventTimes[$donor->id][$priotity]) && isset($timeStamps[$eventTimes[$donor->id][$priotity]]) && $timeStamps[$eventTimes[$donor->id][$priotity]] < 10)
            {
                $timeStamps[$eventTimes[$donor->id][$priotity]]++;
                $donor->donate_day = $datesArr[$eventTimes[$donor->id][$priotity]];
                $donor->donorstatus = 6;
                $donor->save();
                unset($donorRepeat[$key]);
                $counter++;
                continue;
            }
            if($counter >= 90)
            {
                break;
            }
            $priotity=2;
            if(isset($eventTimes[$donor->id][$priotity]) && isset($timeStamps[$eventTimes[$donor->id][$priotity]]) && $timeStamps[$eventTimes[$donor->id][$priotity]] < 10)
            {
                $timeStamps[$eventTimes[$donor->id][$priotity]]++;
                $donor->donate_day = $datesArr[$eventTimes[$donor->id][$priotity]];
                $donor->donorstatus = 6;
                $donor->save();
                unset($donorRepeat[$key]);
                $counter++;
                continue;
            }
            if($counter >= 90)
            {
                break;
            }
            $priotity=3;
            if(isset($eventTimes[$donor->id][$priotity]) && isset($timeStamps[$eventTimes[$donor->id][$priotity]]) && $timeStamps[$eventTimes[$donor->id][$priotity]] < 10)
            {
                $timeStamps[$eventTimes[$donor->id][$priotity]]++;
                $donor->donate_day = $datesArr[$eventTimes[$donor->id][$priotity]];
                $donor->donorstatus = 6;
                $donor->save();
                unset($donorRepeat[$key]);
                $counter++;
                continue;
            }
        }
        $counter = \app\models\Donorlite::find()->where('donorstatus=6 and dcard_id=0')->count();
        foreach($donorsNoRepeat as $key=>$donor)
        {
            if($counter >= 90)
            {
                break;
            }
            $priotity=1;
            if(isset($eventTimes[$donor->id][$priotity]) && isset($timeStamps[$eventTimes[$donor->id][$priotity]]) && $timeStamps[$eventTimes[$donor->id][$priotity]] < 10)
            {
                $timeStamps[$eventTimes[$donor->id][$priotity]]++;
                $donor->donate_day = $datesArr[$eventTimes[$donor->id][$priotity]];
                $donor->donorstatus = 6;
                $donor->save();
                unset($donorsNoRepeat[$key]);
                $counter++;
                continue;
            }
            if($counter >= 90)
            {
                break;
            }
            $priotity=2;
            if(isset($eventTimes[$donor->id][$priotity]) && isset($timeStamps[$eventTimes[$donor->id][$priotity]]) && $timeStamps[$eventTimes[$donor->id][$priotity]] < 10)
            {
                $timeStamps[$eventTimes[$donor->id][$priotity]]++;
                $donor->donate_day = $datesArr[$eventTimes[$donor->id][$priotity]];
                $donor->donorstatus = 6;
                $donor->save();
                unset($donorsNoRepeat[$key]);
                $counter++;
                continue;
            }
            if($counter >= 90)
            {
                break;
            }
            $priotity=3;
            if(isset($eventTimes[$donor->id][$priotity]) && isset($timeStamps[$eventTimes[$donor->id][$priotity]]) && $timeStamps[$eventTimes[$donor->id][$priotity]] < 10)
            {
                $timeStamps[$eventTimes[$donor->id][$priotity]]++;
                $donor->donate_day = $datesArr[$eventTimes[$donor->id][$priotity]];
                $donor->donorstatus = 6;
                $donor->save();
                unset($donorsNoRepeat[$key]);
                $counter++;
                continue;
            }
        }
        //var_dump($timeStamps);die;
        $this->redirect('/private/donor/showtime/');
    }
    
    private function getEventTime()
    {
        $resilt = [];
        $times = \app\models\Eventday::find()->all();
        foreach($times as $time)
        {
            $resilt[$time->id_donor][$time->priority] = strtotime($time->date_);
        }
        return $resilt;
    }
}
?>
