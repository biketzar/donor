<?php

namespace app\modules\privatepanel\controllers;

//use yii\web\Controller;
use app\modules\privatepanel\components\PrivateController;

class MailController extends PrivateController
{
    protected $_modelName = 'app\models\Mail';
    
    public function actionSend()
    {
      $query = \app\models\MailQueue::find()->where(['visible' => 1]);
      if(\Yii::$app->request->isAjax)
      {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $totalCount = $query->count();
        $models = $query->limit(10)->all();
        if(!$models)
        {
          return ['stop' => 1, 'total' => $totalCount, 'items' => []];
        }
        $items = [];
        foreach($models as $model)
        {
          $result = $model->send();
          $items[] = "{$model->donor->surname} {$model->donor->name} {$model->donor->pname} - "
          . "{$model->mail->subject}".( $result === TRUE ? ' - ok' : ' - '.$result);
          $model->delete();
          $totalCount--;
        }
        return ['stop' => 0, 'total' => $totalCount, 'items' => $items];
      }
      else
      {
        return $this->render('send', ['total' => $query->count()]);
      }
    }

}
