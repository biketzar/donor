<?php

namespace app\modules\privatepanel\controllers;

//use yii\web\Controller;
use app\modules\privatepanel\components\PrivateController;

class DcardController extends PrivateController
{
    protected $_modelName = 'app\models\Dcard';
    
    public function actionImport()
    {
        $out = '';
        if(isset($_FILES['file']))
        {
            //print_r($_FILES['file']);die();
            $fp = fopen($_FILES['file']['tmp_name'], "r+");
            if (flock($fp, LOCK_EX)) 
            {
                ob_start();
                while(($data = fgetcsv($fp, 0, ";")) !== FALSE)
                {
                    $arr = $this->parseName($data[1]);
                    $arr['department_id'] = $this->getDepartment($data[2]);
                    $arr['group_num'] = trim($data[3]);
                    $arr['is_budget'] = $this->getBudget($data[4]);
                    $this->addDcard($arr);
                    
                }
                $out = ob_get_clean();
                flock($fp, LOCK_UN);
            }
            fclose($fp);
        }
        return $this->render('import', ['out'=>$out]);
    }
    
    private function getBudget($data)
    {
        $data = trim($data);
        return (($data == 'К' || $data == 'K' || $data == 'k' || $data == 'к') ? 0 : 1);
    }
    
    private function getDepartment($data)
    {
        $departments = [];
        $departments['ИСИ'] = 12;
        $departments['ИЭИТС'] = 1;
        $departments['ИЭиТС'] = 1;
        $departments['ИММИТ'] = 5;
        $departments['ИММиТ'] = 5;
        $departments['ИФНИТ'] = 10;
        $departments['ИФНиТ'] = 10;
        $departments['ИКНТ'] = 7;
        $departments['ИИТУ'] = 7;
        $departments['ИПММ'] = 3;
        $departments['ИЭИ'] = 11;
        $departments['ИВТОБ'] = 8;
        $departments['ИМОП'] = 6;
        $departments['ГИ'] = 13;
        $departments['ИПЛ'] = 13;
        $departments['ИМАШ'] = 15;
        $departments['ИМаш'] = 15;
        $departments['УПК РП'] = 14;
        
        echo $data = mb_strtoupper(trim($data));
        if(isset($departments[$data]))
        {
            return $departments[$data];
        }
        return 0;
    }
    
    private function parseName($data)
    {
        $result = [];
        $dataArr = explode(" ", trim($data));
        if(count($dataArr) >= 3)
        {
            $result['surname'] = $dataArr[0];
            $result['name'] = $dataArr[1];
            $result['pname'] = $dataArr[2];
            return $result;
        }
        if(count($dataArr) == 2)
        {
            $result['surname'] = $dataArr[0];
            $result['name'] = $dataArr[1];
            $result['pname'] = ' ';
        }
        $result['surname'] = '0';
        $result['name'] = $data;
        $result['pname'] = ' ';
        
        return $result;
    }
    
    private function addDcard($data)
    {
        $model = \app\models\Dcard::find()->where('name=:name AND surname=:surname AND pname=:pname', 
                [
                    ':name'=>$data['name'],
                    ':surname'=>$data['surname'],
                    ':pname'=>$data['pname'],
                    ])->one();
        if($model)
        {
            echo '<br/>Добавлен ранее<br/>';
            echo $model->id.'<br/>';
            var_dump($data);
            echo '<hr/>';
        }
        else {
            $model = new \app\models\Dcard();
            $model->name = $data['name'];
            $model->surname = $data['surname'];
            $model->pname = $data['pname'];
            $model->group_num = $data['group_num'];
            $model->department_id = $data['department_id'];
            $model->is_budget = $data['is_budget'];
            if($model->save())
            {
                echo '<br/>Успешно добавлен<br/>';
                var_dump($data);
                echo '<hr/>';
            }
            else {
                echo '<br/>Ошибка<br/>';
                var_dump($model->getErrors());
                var_dump($data);
                echo '<hr/>';
            }
        }
    }
}
?>