<?php
namespace app\modules\privatepanel\controllers;
use app\modules\privatepanel\components\PrivateController;

class ProjectSettingsController extends PrivateController
{
    /*
     * редактирование настроек из json файла
     */
    public function actionAdmin() {
       $config = \Yii::$app->params['projectSettings'];
       if(isset($_POST['ProjectSettings']))
       {
           $insertArray = [];
           foreach ($config as $key=>$data)
           {
               $config[$key]['value'] = isset($_POST['ProjectSettings'][$key]) ? $_POST['ProjectSettings'][$key] : 0;
               unset($_POST['ProjectSettings'][$key]);
           }
           file_put_contents(CONFIG_FILE, json_encode($config));
       }
       return $this->render('settings', ['config'=>$config]);
    }
    
    /*
     * добавление настроек в json файл
     */
    public function actionCreate()
    {
        if(isset($_POST['ProjectSettings']))
        {
            $config = \Yii::$app->params['projectSettings'];
            $config[$_POST['ProjectSettings']['key']] = ['name'=>$_POST['ProjectSettings']['name'],
                'value'=>$_POST['ProjectSettings']['value'],
                'type'=>$_POST['ProjectSettings']['type'],];
            file_put_contents(CONFIG_FILE, json_encode($config));
            $this->redirect(['admin']);
                return;
        }
        return $this->render('addSetting');
    }
    
    /*
     * удаление настроек из json файла
     */
    public function actionDelete() {
        if(isset($_GET['key']))
        {
            $config = \Yii::$app->params['projectSettings'];
            unset($config[$_GET['key']]);
            file_put_contents(CONFIG_FILE, json_encode($config));
        }
        $this->redirect(['admin']);
        return;
    }
}
