<?php

namespace app\modules\privatepanel\controllers;

//use yii\web\Controller;
use app\modules\privatepanel\components\PrivateController;
use yii\web\Controller;
use \app\modules\privatepanel\Module;

use app\components\AccessControl;
use yii\data\ActiveDataProvider;
//use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\modules\privatepanel\components\PrivateModelAdmin;
use app\modules\privatepanel\components\TreeActiveDataProvider;
use app\modules\privatepanel\components\fields;

class DonorlistController extends PrivateController
{
    public $enableCsrfValidation = false;
    protected $_modelName = 'app\models\Donorlist';
    
    public function actionList3()
    {
        $this->logOff();
        //header('Content-Type: text/csv; charset=utf-8');
        //header('Content-Disposition: attachment; filename=politech.csv');
        //$output = fopen('php://output', 'w');
        $donorlist  = [];
        $donorRows = \app\models\Donorlist::find()->where('approved=1 and type=0')->orderBy('id ASC')->asArray()->all();
        foreach($donorRows as $row)
        {
            $donorlist[$row['date_id']][] = $row;
        }
        $timesRows = \app\models\Times::find()->where('typing=0')->orderBy('dtime ASC')->asArray()->all();
        $timeList = []; 
        foreach ($timesRows as $time)
        {
            $timeList[$time['id']] = date('d.m H:i', strtotime($time['dtime']));
        }
        $donorArr = [];
        $donorRows = \app\models\Donorlite::find()->asArray()->all();
        foreach($donorRows as $row)
        {
            $donorArr[$row['id']] = $row;
        }
        //$departmentsList = \yii\helpers\ArrayHelper::map(\app\models\Departments::find()->select('id, name')->asArray()->all(), 'id', 'name');
        $donorHistory = \app\models\Donorhistory::find()->select('id_donor')->column();
        return $this->renderPartial('table', [
            'donorArr' => $donorArr, 
            'donorlist' => $donorlist,
            'timeList' => $timeList,
            'donorHistory' => $donorHistory,
            'title' => 'Список утверждённых доноров - ПОЛИТЕХ'
                ]);
    }
    
    public function actionList4()
    {
        $this->logOff();
        //header('Content-Type: text/csv; charset=utf-8');
        //header('Content-Disposition: attachment; filename=politech.csv');
        //$output = fopen('php://output', 'w');
        $donorlist  = [];
        $donorRows = \app\models\Donorlist::find()->where('approved=1 and type=1')->orderBy('id ASC')->asArray()->all();
        foreach($donorRows as $row)
        {
            $donorlist[$row['date_id']][] = $row;
        }
        $timesRows = \app\models\Times::find()->where('typing=1')->orderBy('dtime ASC')->asArray()->all();
        $timeList = []; 
        foreach ($timesRows as $time)
        {
            $timeList[$time['id']] = date('d.m H:i', strtotime($time['dtime']));
        }
        $donorArr = [];
        $donorRows = \app\models\Donorlite::find()->asArray()->all();
        foreach($donorRows as $row)
        {
            $donorArr[$row['id']] = $row;
        }
        //$departmentsList = \yii\helpers\ArrayHelper::map(\app\models\Departments::find()->select('id, name')->asArray()->all(), 'id', 'name');
        $donorHistory = \app\models\Donorhistory::find()->select('id_donor')->column();
        return $this->renderPartial('table', [
            'donorArr' => $donorArr, 
            'donorlist' => $donorlist,
            'timeList' => $timeList,
            'donorHistory' => $donorHistory,
            'title' => 'Список утверждённых доноров - ВЫЕЗД'
                ]);
    }
    
        public function actionList5()
    {
            $this->logOff();
        //header('Content-Type: text/csv; charset=utf-8');
        //header('Content-Disposition: attachment; filename=politech.csv');
        //$output = fopen('php://output', 'w');
        $donorlist  = [];
        $donorRows = \app\models\Donorlist::find()->where('approved=1 and type=0')->orderBy('id ASC')->asArray()->all();
        foreach($donorRows as $row)
        {
            $donorlist[$row['date_id']][] = $row;
        }
        $timesRows = \app\models\Times::find()->where('typing=0')->orderBy('dtime ASC')->asArray()->all();
        $timeList = []; 
        foreach ($timesRows as $time)
        {
            $timeList[$time['id']] = date('d.m H:i', strtotime($time['dtime']));
        }
        $donorArr = [];
        $donorRows = \app\models\Donorlite::find()->asArray()->all();
        foreach($donorRows as $row)
        {
            $donorArr[$row['id']] = $row;
        }
        //$departmentsList = \yii\helpers\ArrayHelper::map(\app\models\Departments::find()->select('id, name')->asArray()->all(), 'id', 'name');
        $donorHistory = \app\models\Donorhistory::find()->select('id_donor')->column();
        return $this->renderPartial('table2', [
            'donorArr' => $donorArr, 
            'donorlist' => $donorlist,
            'timeList' => $timeList,
            'donorHistory' => $donorHistory,
            'title' => 'Список утверждённых доноров - ПОЛИТЕХ'
                ]);
    }
    
    public function actionList6()
    {
        $this->logOff();
        //header('Content-Type: text/csv; charset=utf-8');
        //header('Content-Disposition: attachment; filename=politech.csv');
        //$output = fopen('php://output', 'w');
        $donorlist  = [];
        $donorRows = \app\models\Donorlist::find()->where('approved=1 and type=1')->orderBy('id ASC')->asArray()->all();
        foreach($donorRows as $row)
        {
            $donorlist[$row['date_id']][] = $row;
        }
        $timesRows = \app\models\Times::find()->where('typing=1')->orderBy('dtime ASC')->asArray()->all();
        $timeList = []; 
        foreach ($timesRows as $time)
        {
            $timeList[$time['id']] = date('d.m H:i', strtotime($time['dtime']));
        }
        $donorArr = [];
        $donorRows = \app\models\Donorlite::find()->asArray()->all();
        foreach($donorRows as $row)
        {
            $donorArr[$row['id']] = $row;
        }
        //$departmentsList = \yii\helpers\ArrayHelper::map(\app\models\Departments::find()->select('id, name')->asArray()->all(), 'id', 'name');
        $donorHistory = \app\models\Donorhistory::find()->select('id_donor')->column();
        return $this->renderPartial('table2', [
            'donorArr' => $donorArr, 
            'donorlist' => $donorlist,
            'timeList' => $timeList,
            'donorHistory' => $donorHistory,
            'title' => 'Список утверждённых доноров - ВЫЕЗД'
                ]);
    }
    
    //список сдачи в политехе
    public function actionList1()
    {
        $this->logOff();
        $donors = array();
        $timeList = [];
        $donorlist = \app\models\Donorlist::find()->where('type=0')->orderBy('id ASC')->asArray()->all();
        $timesRows = \app\models\Times::find()->where('typing=0')->orderBy('dtime ASC')->asArray()->all();
        foreach ($timesRows as $time)
        {
            $time['dtime'] = date('d.m H:i', strtotime($time['dtime']));
            $timeList[$time['id']] = $time;
            $donors[$time['id']] = [];
        }
        $fullTimesRows = \app\models\Times::find()->orderBy('dtime ASC')->asArray()->all();
        foreach ($fullTimesRows as $time)
        {
            $fullTimeList[$time['id']] = [ 
                'time' => date('d.m H:i', strtotime($time['dtime'])), 
                'typing' => $time['typing'] ? ' T ' : '' ];
        }
        $donorModels = [];
        $donors['other'] = [];
        foreach($donorlist as $donor)
        {
            $donorModels[$donor['id_donor']] = \app\models\Donorlite::findOne(['id' => $donor['id_donor']]);
            if(array_key_exists($donor['date_id'], $timeList))
            {
                
                $donors[$donor['date_id']][$donor['id_donor']] = $donor;
            }
            else 
            {
                $donors['other'][$donor['id_donor']] = $donor;
            }
        }
        $accessTimes = [];
        foreach($timeList as $timeKey => $time)
        {
            if(count($donors[$timeKey]) < 15)
            {
                $accessTimes[$timeKey] = $time['dtime'];
            }
        }
        $noCorrectList = \app\models\Eventday::find()
                ->select('id_donor')
                ->where(['NOT IN', 'id_donor', \app\models\Donorlist::find()->select('id_donor')->column()])
                ->andWhere(['IN', 'id_donor', \app\models\Donorlite::find()->select('id')->where('donorstatus!=7')->column()])
                ->andWhere(['IN', 'date_id', \app\models\Times::find()->select('id')->column()])
                ->groupBy('id_donor')
                ->column();
        $nocorrectDonors = \app\models\Donorlite::find()->where(['IN', 'id', $noCorrectList])->all();
        return $this->render('list1', [
            'timeList' => $timeList,
            'donors' => $donors,
            'donorModels' => $donorModels,
            'accessTimes' => $accessTimes,
            'inCount' => \app\models\Donorlist::find()->where('approved!=2 and type=0')->count(),
            'outCount' => \app\models\Donorlist::find()->where('approved!=2 and type=1')->count(),
            'noCount' => \app\models\Donorlist::find()->where('approved=2')->count(),
            'allCount' =>\app\models\Donorlist::find()->count(),
            'nocorrectDonors' => $nocorrectDonors,
            'fullTimeList' => $fullTimeList
        ]);
    }
    
    //список сдачи в политехе
    public function actionList2()
    {
        $this->logOff();
        $donors = array();
        $timeList = [];
        $donorlist = \app\models\Donorlist::find()->where('type=1')->orderBy('id ASC')->asArray()->all();
        $timesRows = \app\models\Times::find()->where('typing=1')->orderBy('dtime ASC')->asArray()->all();
        foreach ($timesRows as $time)
        {
            $time['dtime'] = date('d.m H:i', strtotime($time['dtime']));
            $timeList[$time['id']] = $time;
        }
        $donors['other'] = [];
        $donorModels = [];
        foreach($donorlist as $donor)
        {
            $donorModels[$donor['id_donor']] = \app\models\Donorlite::findOne(['id' => $donor['id_donor']]);
            if(array_key_exists($donor['date_id'], $timeList))
            {
                
                $donors[$donor['date_id']][$donor['id_donor']] = $donor;
            }
            else 
            {
                $donors['other'][$donor['id_donor']] = $donor;
            }
        }
        $accessTimes = [];
        foreach($timeList as $timeKey => $time)
        {
            //if(count($donors[$timeKey]) < 15)
            {
                $accessTimes[$timeKey] = $time['dtime'];
            }
        }
        $fullTimesRows = \app\models\Times::find()->orderBy('dtime ASC')->asArray()->all();
        foreach ($fullTimesRows as $time)
        {
            $fullTimeList[$time['id']] = [ 
                'time' => date('d.m H:i', strtotime($time['dtime'])), 
                'typing' => $time['typing'] ? ' T ' : '' ];
        }
        return $this->render('list2', [
            'timeList' => $timeList,
            'donors' => $donors,
            'donorModels' => $donorModels,
            'accessTimes' => $accessTimes,
            'fullTimeList' => $fullTimeList
        ]);
    }
    
    
    
    //изменение даты
    public function actionChangedate()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $model = \app\models\Donorlist::findOne(['id' => \Yii::$app->request->get('id')]);
        if($model && \Yii::$app->request->get('new_date_id', FALSE))
        {
            //$count = \app\models\Donorlist::find()->where('approved!=2 and type=0 and date_id=:date_id', [':date_id' => \Yii::$app->request->get('new_date_id')])->count();
            $timeModel = \app\models\Times::findOne(['id' => \Yii::$app->request->get('new_date_id')]);
            if(/*$count < 15 && */$timeModel)
            {
                $model->date_id = \Yii::$app->request->get('new_date_id');
                $model->save();
                return ['error' => !$model->save()];
            }
        }
        return ['error' => true];
    }
    
    //изменение статуса
    public function actionChangestatus()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $model = \app\models\Donorlist::findOne(['id' => \Yii::$app->request->get('id')]);
        if($model && \Yii::$app->request->get('new_approved_id', FALSE))
        {
            if(array_key_exists(\Yii::$app->request->get('new_approved_id'), \app\models\Donorlist::getApproved()))
            {
                $model->approved = \Yii::$app->request->get('new_approved_id');
                return ['error' => !$model->save()];
            }
        }
        return ['error' => true];
    }
    
    //очитска списка
    public function actionClear()
    {
        if(\Yii::$app->user->id != 1)
        {
            return $this->redirect('/private/donorlist/list1');
        }
        \app\models\Donorlist::deleteAll();
        $this->redirect('/private/donorlist/list1');
    }
    
    public function actionAdd()
    {
        $helper = new \app\components\DonorList();
        $helper->add();
        $this->redirect('/private/donorlist/list1');
    }
    
    public function actionUpdate()
    {
	$this->logOff();

        $modelName = $this->_modelName;
        
        $keys = [];
        foreach($modelName::primaryKey() as $key)
        {
            if(\Yii::$app->request->get($key))
                $keys[$key] = \Yii::$app->request->get($key);
        }
        if(!count($keys))
            $keys['id'] = -1;
        
        $model = $modelName::find()->andWhere($keys)->one();
        
        if($model)
        {
            if(array_key_exists('update', $model->scenarios()))
                $model->setScenario('update');
	
            $privateModelAdmin = new PrivateModelAdmin($model);

            if(\Yii::$app->request->get($model->formName()))
            {
                $data = $privateModelAdmin->prepareForSave(\Yii::$app->request->get(), true);
                $model->load($data);
            }
            
            if($this->enableAjaxValidation && \Yii::$app->request->isAjax && \Yii::$app->request->post('ajax', null) === strtolower($model->formName()).'-form')
            {
                return $this->performAjaxValidation($model);
            }

            if(\Yii::$app->request->isPost)
            {
                //$privateModelAdmin = new PrivateModelAdmin($model);
                $data = $privateModelAdmin->prepareForSave(\Yii::$app->request->post(), false);

                $model->load($data);
                
                if($model->save())
                {
                    $privateModelAdmin->setModel($model);
                    $privateModelAdmin->afterSave($data);
                    
                    $this->updateSuccessMessage && $this->setAdminMessage($this->updateSuccessMessage, 'success');
                    
                    $this->_modelPrimaryKeys = $model->getPrimaryKey(true);
                    
                    $this->logOn();
                    
                    if(\Yii::$app->request->post('savetype', '')  == 'apply')
                    {
                        $params = ArrayHelper::merge(['update'], $model->getPrimaryKey(true));
                        $params = array_merge($params, $privateModelAdmin->additionalUrlParams);
                        $this->redirect($params);
                        return;
                    } else {
                        if($model->type == 0)
                        {
                            $params = ['list1'];
                        }
                        else
                        {
                            $params = ['list2'];
                        }
                        $params = array_merge($params, $privateModelAdmin->additionalUrlParams);
                        $this->redirect($params);
                        return;
                    }
                } else {
                    $privateModelAdmin->setModel($model);
                }
            }

            if(\Yii::$app->request->isAjax)
            {
                return $this->renderPartial($this->actionCreateTemplate,
                    [
                        'privateModelAdmin'=>$privateModelAdmin,
                    ]
                );
            }
            return $this->render($this->actionUpdateTemplate,
                [
                    'privateModelAdmin'=>$privateModelAdmin,
                ]
            );
        } else {
            $this->setAdminMessage(Module::t('app', 'There is no record with this primary key', 'error'));
            $params = ['admin'];
            $params = array_merge($params, $privateModelAdmin->additionalUrlParams);
            $this->redirect($params);
        }
    }
    
    public function actionMail1()
    {
        set_time_limit(10000);
        $condition = "approved=1 AND notification=0 AND type=0";
        $donorlist = \app\models\Donorlist::find()->select('id_donor')->where($condition)->column();
        $donorTimes = ArrayHelper::map(\app\models\Donorlist::find()->select('id_donor, date_id')->asArray()->all(), 'id_donor', 'date_id');
        $fullTimesRows = \app\models\Times::find()->orderBy('dtime ASC')->asArray()->all();
        foreach ($fullTimesRows as $time)
        {
            $fullTimeList[$time['id']] = [ 
                'time' => date('j октября в H:i', strtotime($time['dtime'])), 
                'typing' => $time['typing'] ? ' T ' : '' ];
        }
        $donors = \app\models\Donorlite::find()->where(['IN', 'id', $donorlist])->all();
        $body = $this->renderPartial('mail1');
        foreach($donors as $donor)
        {
            $mailer = new \app\components\MailHelper;
            $mailer->to = $donor->email;
            $mailer->subject = 'День донора в СПбПУ - дата сдачи';
            $mailer->content = $body;
            $mailer->from = 'no-reply@donor.spb.ru';
            $mailer->params = [
                'FULLNAME' => "{$donor->surname} {$donor->name} {$donor->pname}",
                'NAME' => "{$donor->name}",
                'TIME' => $fullTimeList[$donorTimes[$donor->id]]['time']
                        ];
            $result = $mailer->sendOne();
            if($result)
            {
                $donorlistModel = \app\models\Donorlist::findOne(['id_donor' => $donor->id]);
                $donorlistModel->notification = 1;
                $donorlistModel->save();
            }
            $log = new \app\models\Maillog;
            $log->id_mail = '201610091';
            $log->id_donor = $donor->id;
            $log->status = (int)$result;
            $log->save();
        }
    }
    
    public function actionMail2()
    {
        set_time_limit(10000);
        $condition = "approved=1 AND notification=0 AND type=1";
        //$condition = "approved=1 AND type=1 AND (date_id=43 OR date_id=44)";
        $donorlist = \app\models\Donorlist::find()->select('id_donor')->where($condition)->column();
        
        $donorTimes = ArrayHelper::map(\app\models\Donorlist::find()->select('id_donor, date_id')->asArray()->all(), 'id_donor', 'date_id');
        $fullTimesRows = \app\models\Times::find()->orderBy('dtime ASC')->asArray()->all();
        foreach ($fullTimesRows as $time)
        {
            $fullTimeList[$time['id']] = [ 
                'time' => date('j октября в H:i', strtotime($time['dtime'])), 
                'typing' => $time['typing'] ? ' T ' : '' ];
        }
        $donors = \app\models\Donorlite::find()->where(['IN', 'id', $donorlist])->all();
        $body = $this->renderPartial('mail2');
        foreach($donors as $donor)
        {
            $mailer = new \app\components\MailHelper;
            $mailer->to = $donor->email;
            $mailer->subject = 'День донора в СПбПУ';
            $mailer->content = $body;
            $mailer->from = 'no-reply@donor.spb.ru';
            $mailer->params = [
                'FULLNAME' => "{$donor->surname} {$donor->name} {$donor->pname}",
                'NAME' => "{$donor->name}",
                'TIME' => $fullTimeList[$donorTimes[$donor->id]]['time']
                        ];
            $result = $mailer->sendOne();
            if($result)
            {
                $donorlistModel = \app\models\Donorlist::findOne(['id_donor' => $donor->id]);
                $donorlistModel->notification = 1;
                $donorlistModel->save();
            }
            $log = new \app\models\Maillog;
            $log->id_mail = '201610092';
            $log->id_donor = $donor->id;
            $log->status = (int)$result;
            $log->save();
        }
    }
    
    public function actionMail3()
    {
        die;
        //$connection = \Yii::$app->getDb();
        //$command = $connection->createCommand('SELECT id, email, name, surname FROM `yii2_donors` where id in (SELECT id_donor FROM `yii2_donorlist` where approved=1 and type=0 and date_id in (SELECT id FROM `yii2_times` where year(dtime)=2016 AND month(dtime)=10 and day(dtime)=11 and typing=0)) and donorstatus not in (285, 286, 7)');
        //$rows = $command->queryAll();
        $body = $this->renderPartial('mail3');
        $counter = 0;
        foreach($rows as $row)
        {
            $mailer = new \app\components\MailHelper;
            $mailer->to = $row['email'];////////////
            $mailer->subject = 'День донора в СПбПУ';
            $mailer->content = $body;
            $mailer->from = 'no-reply@donor.spb.ru';
            $mailer->params = [
                'NAME' => $row['name'],
                        ];
            $result = $mailer->sendOne();
            echo "{$counter} - ";
            var_dump($result);
            var_dump($row);
            //var_dump($mailer->params);
            echo '<br/>';
            $counter++;
        }
    }
    
    public function actionFullList()
    {
        if(\Yii::$app->request->isAjax || \Yii::$app->request->get('action'))
        {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return \app\components\DonorHelper::getResult(\Yii::$app->request->get('action'), \Yii::$app->request->get());
        }
        $times = ArrayHelper::index(\app\models\Times::find()->orderBy('dtime ASC')->all(), 'id');
        $timeList = [];
        foreach($times as $time)
        {
            $timeList[$time->id] = date('d.m H:i', strtotime($time['dtime'])).($time->typing ? ' Выезд' : '');
        }
        $userList = ['' => '---'];
        foreach(\app\models\Donorlite::find()->select(['id', 'surname', 'name', 'pname', 'phone'])->asArray()->all() as $row)
        {
            $name = ($row['surname'] ? $row['surname'].' ' : '')
                .($row['name'] ? $row['name'].' ' : '')
                .($row['pname'] ? $row['pname'].' ' : '')
                .($row['phone'] ? $row['phone'].' ' : '');
            $userList[$name] = $name;
        }
        return $this->render('full-list', [
            'times' => $times,
            'timeList' => $timeList,
            'userList' => $userList
        ]);
    }
    
}
