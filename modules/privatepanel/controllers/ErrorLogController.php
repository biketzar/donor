<?php

namespace app\modules\privatepanel\controllers;

use Yii;
use yii\base\InvalidConfigException;
use yii\helpers\FileHelper;
use app\modules\privatepanel\components\PrivateController;

class ErrorLogController extends PrivateController
{
    public function actionShow() {
        if(file_exists(Yii::$app->getRuntimePath() . '/logs/app.log'))
        {
            $data = file_get_contents(Yii::$app->getRuntimePath() . '/logs/app.log');
        }
        else
        {
            $data = 'Log file(app.log) not found';
        }
        $data = strip_tags($data);
        $data = str_replace("\n", "<br>", $data);
        $data = str_replace("Next exception", "<delimiter/><hr style=\"border-top: 1px solid #a94442\">Next exception", $data);
        $dataArray = explode('<delimiter/>', $data);
        $showErrors = 20;
        $outData = '';
        $cursor = count($dataArray)-1;
        while($showErrors>=0 && $cursor>=0)
        {
            $outData .=  $dataArray[$cursor];
            $showErrors--;
            $cursor--;
        }
        if($showErrors>0 && file_exists(Yii::$app->getRuntimePath() . '/logs/app.log.1'))
        {
            $data = file_get_contents(Yii::$app->getRuntimePath() . '/logs/app.log.1');
            $data = strip_tags($data);
            $data = str_replace("\n", "<br>", $data);
            $data = str_replace("Next exception", "<delimiter/><hr style=\"border-top: 1px solid #a94442\">Next exception", $data);
            $dataArray = explode('<delimiter/>', $data);
            $cursor = count($dataArray)-1;
            while($showErrors>=0 && $cursor>=0)
            {
                $outData .=  $dataArray[$cursor];
                $showErrors--;
                $cursor--;
            }
        }
        return $this->render('@app/modules/privatepanel/views/errorLog/view',array( 
            'data' => $outData,
        ));
    }

}
?>