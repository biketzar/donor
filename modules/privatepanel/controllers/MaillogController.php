<?php

namespace app\modules\privatepanel\controllers;

use app\modules\privatepanel\components\PrivateController;

class MaillogController extends PrivateController
{
    protected $_modelName = 'app\models\Maillog';

}
