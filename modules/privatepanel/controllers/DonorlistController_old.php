<?php

namespace app\modules\privatepanel\controllers;

//use yii\web\Controller;
use app\modules\privatepanel\components\PrivateController;

class DonorlistController extends PrivateController
{
    protected $_modelName = 'app\models\Donorlist';
    
    public function actionList3()
    {
        //header('Content-Type: text/csv; charset=utf-8');
        //header('Content-Disposition: attachment; filename=politech.csv');
        //$output = fopen('php://output', 'w');
        $donorlist  = [];
        $donorRows = \app\models\Donorlist::find()->where('approved=1 and type=0')->orderBy('id ASC')->asArray()->all();
        foreach($donorRows as $row)
        {
            $donorlist[$row['date_id']][] = $row;
        }
        $timesRows = \app\models\Times::find()->where('typing=0')->orderBy('dtime ASC')->asArray()->all();
        $timeList = []; 
        foreach ($timesRows as $time)
        {
            $timeList[$time['id']] = date('d.m H:i', strtotime($time['dtime']));
        }
        $donorArr = [];
        $donorRows = \app\models\Donorlite::find()->asArray()->all();
        foreach($donorRows as $row)
        {
            $donorArr[$row['id']] = $row;
        }
        //$departmentsList = \yii\helpers\ArrayHelper::map(\app\models\Departments::find()->select('id, name')->asArray()->all(), 'id', 'name');
        $donorHistory = \app\models\Donorhistory::find()->select('id_donor')->column();
        return $this->renderPartial('table', [
            'donorArr' => $donorArr, 
            'donorlist' => $donorlist,
            'timeList' => $timeList,
            'donorHistory' => $donorHistory,
            'title' => 'Список утверждённых доноров - ПОЛИТЕХ'
                ]);
    }
    
    public function actionList4()
    {
        //header('Content-Type: text/csv; charset=utf-8');
        //header('Content-Disposition: attachment; filename=politech.csv');
        //$output = fopen('php://output', 'w');
        $donorlist  = [];
        $donorRows = \app\models\Donorlist::find()->where('approved=1 and type=1')->orderBy('id ASC')->asArray()->all();
        foreach($donorRows as $row)
        {
            $donorlist[$row['date_id']][] = $row;
        }
        $timesRows = \app\models\Times::find()->where('typing=1')->orderBy('dtime ASC')->asArray()->all();
        $timeList = []; 
        foreach ($timesRows as $time)
        {
            $timeList[$time['id']] = date('d.m H:i', strtotime($time['dtime']));
        }
        $donorArr = [];
        $donorRows = \app\models\Donorlite::find()->asArray()->all();
        foreach($donorRows as $row)
        {
            $donorArr[$row['id']] = $row;
        }
        //$departmentsList = \yii\helpers\ArrayHelper::map(\app\models\Departments::find()->select('id, name')->asArray()->all(), 'id', 'name');
        $donorHistory = \app\models\Donorhistory::find()->select('id_donor')->column();
        return $this->renderPartial('table', [
            'donorArr' => $donorArr, 
            'donorlist' => $donorlist,
            'timeList' => $timeList,
            'donorHistory' => $donorHistory,
            'title' => 'Список утверждённых доноров - ВЫЕЗД'
                ]);
    }
    
    //список сдачи в политехе
    public function actionList1()
    {
        $donors = array();
        $timeList = [];
        $donorlist = \app\models\Donorlist::find()->where('approved!=2 and type=0')->orderBy('id ASC')->asArray()->all();
        $timesRows = \app\models\Times::find()->where('typing=0')->orderBy('dtime ASC')->asArray()->all();
        foreach ($timesRows as $time)
        {
            $timeList[$time['id']] = date('d.m H:i', strtotime($time['dtime']));
            $donors[$time['id']] = [];
        }
        $fullTimesRows = \app\models\Times::find()->orderBy('dtime ASC')->asArray()->all();
        foreach ($fullTimesRows as $time)
        {
            $fullTimeList[$time['id']] = [ 
                'time' => date('d.m H:i', strtotime($time['dtime'])), 
                'typing' => $time['typing'] ? ' T ' : '' ];
        }
        $donorModels = [];
        $donors['other'] = [];
        foreach($donorlist as $donor)
        {
            $donorModels[$donor['id_donor']] = \app\models\Donorlite::findOne(['id' => $donor['id_donor']]);
            if(TRUE || array_key_exists($donor['date_id'], $timeList))
            {
                
                $donors[$donor['date_id']][$donor['id_donor']] = $donor;
            }
            else 
            {
                $donors['other'][$donor['id_donor']] = $donor;
            }
        }
        $accessTimes = [];
        foreach($timeList as $timeKey => $time)
        {
            if(count($donors[$timeKey]) < 15)
            {
                $accessTimes[$timeKey] = $time;
            }
        }
        $noCorrectList = \app\models\Eventday::find()
                ->select('id_donor')
                ->where(['NOT IN', 'id_donor', \app\models\Donorlist::find()->select('id_donor')->column()])
                ->andWhere(['IN', 'id_donor', \app\models\Donorlite::find()->select('id')->where('donorstatus!=7')->column()])
                ->andWhere(['IN', 'date_id', \app\models\Times::find()->select('id')->column()])
                ->groupBy('id_donor')
                ->column();
        $nocorrectDonors = \app\models\Donorlite::find()->where(['IN', 'id', $noCorrectList])->all();
        return $this->render('list1', [
            'timeList' => $timeList,
            'donors' => $donors,
            'donorModels' => $donorModels,
            'accessTimes' => $accessTimes,
            'inCount' => \app\models\Donorlist::find()->where('approved!=2 and type=0')->count(),
            'outCount' => \app\models\Donorlist::find()->where('approved!=2 and type=1')->count(),
            'noCount' => \app\models\Donorlist::find()->where('approved=2')->count(),
            'allCount' =>\app\models\Donorlist::find()->count(),
            'nocorrectDonors' => $nocorrectDonors,
            'fullTimeList' => $fullTimeList
        ]);
    }
    
    //список сдачи в политехе
    public function actionList2()
    {
        $donors = array();
        $timeList = [];
        $donorlist = \app\models\Donorlist::find()->where('approved!=2 and type=1')->orderBy('id ASC')->asArray()->all();
        $timesRows = \app\models\Times::find()->where('typing=1')->orderBy('dtime ASC')->asArray()->all();
        foreach ($timesRows as $time)
        {
            $timeList[$time['id']] = date('d.m H:i', strtotime($time['dtime']));
            $donors[$time['id']] = [];
        }
        $donors['other'] = [];
        $donorModels = [];
        foreach($donorlist as $donor)
        {
            $donorModels[$donor['id_donor']] = \app\models\Donorlite::findOne(['id' => $donor['id_donor']]);
            if(TRUE || array_key_exists($donor['date_id'], $timeList))
            {
                
                $donors[$donor['date_id']][$donor['id_donor']] = $donor;
            }
            else 
            {
                $donors['other'][$donor['id_donor']] = $donor;
            }
        }
        $accessTimes = [];
        foreach($timeList as $timeKey => $time)
        {
            //if(count($donors[$timeKey]) < 15)
            {
                $accessTimes[$timeKey] = $time;
            }
        }
        return $this->render('list2', [
            'timeList' => $timeList,
            'donors' => $donors,
            'donorModels' => $donorModels,
            'accessTimes' => $accessTimes
        ]);
    }
    
    
    
    //изменение даты
    public function actionChangedate()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $model = \app\models\Donorlist::findOne(['id' => \Yii::$app->request->get('id')]);
        if($model && \Yii::$app->request->get('new_date_id', FALSE))
        {
            //$count = \app\models\Donorlist::find()->where('approved!=2 and type=0 and date_id=:date_id', [':date_id' => \Yii::$app->request->get('new_date_id')])->count();
            $timeModel = \app\models\Times::findOne(['id' => \Yii::$app->request->get('new_date_id')]);
            if(/*$count < 15 && */$timeModel)
            {
                $model->date_id = \Yii::$app->request->get('new_date_id');
                $model->save();
                return ['error' => !$model->save()];
            }
        }
        return ['error' => true];
    }
    
    //изменение статуса
    public function actionChangestatus()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $model = \app\models\Donorlist::findOne(['id' => \Yii::$app->request->get('id')]);
        if($model && \Yii::$app->request->get('new_approved_id', FALSE))
        {
            if(array_key_exists(\Yii::$app->request->get('new_approved_id'), \app\models\Donorlist::getApproved()))
            {
                $model->approved = \Yii::$app->request->get('new_approved_id');
                return ['error' => !$model->save()];
            }
        }
        return ['error' => true];
    }
    
    //очитска списка
    public function actionClear()
    {
        if(\Yii::$app->user->id != 1)
        {
            return $this->redirect('/private/donorlist/list1');
        }
        \app\models\Donorlist::deleteAll();
        $this->redirect('/private/donorlist/list1');
    }
    
    private function getDonorList($timeId)
    {
        $donorList = [];
        $rows = \app\models\Donorlist::find()
                ->select('id_donor')
                ->where('date_id=:date_id and approved!=2 and type=0', [':date_id'=>$timeId])
                ->asArray()
                ->all();
        foreach($rows as $row)
        {
            $donorList[] = (int)$row['id_donor'];
        }
        return $donorList;
    }
    
    private function getOutDonorList($timeId)
    {
        $outDonorList = [];
        $rows = \app\models\Donorlist::find()
                ->select('id_donor')
                ->where('date_id=:date_id and approved!=2 and type=1', [':date_id'=>$timeId])
                ->asArray()
                ->all();
        foreach($rows as $row)
        {
            $outDonorList[] = (int)$row['id_donor'];
        }
        return $outDonorList;
    }
    
    public function actionAdd()
    {
        set_time_limit(10000);
        ini_set('memory_limit', '256M');
        $idTimeList = \app\models\Times::find()
                //->where('typing=1')
                ->orderBy('dtime ASC')->asArray()->all();
        $timeIds = ['in' => [], 'out' => []];
        $donorList = [];
        $outDonorList = [];
        foreach($idTimeList as $time)
        {
            if($time['typing'] == 0)
            {
                $donorList[$time['id']] = $this->getDonorList($time['id']);
                $timeIds['in'][] = $time['id'];
            }
            else
            {
                $timeIds['out'][] = $time['id'];
            }
            $outDonorList[$time['id']] = $this->getOutDonorList($time['id']);
        }
        $dbDonorVipList = \app\models\Donorlite::find()->where('is_vip=1')->orderBy('create_time ASC')->asArray()->all();
        $returnData = $this->addToDonorList($donorList, $outDonorList, $dbDonorVipList, $idTimeList, $timeIds);
        $donorList = $returnData[0];
        $outDonorList = $returnData[1];
        $donorFromHistory = \app\models\Donorhistory::find()->select('id_donor')->column();
        $dbDonorList = \app\models\Donorlite::find()->where('is_vip!=1 and donorstatus!=7')->andWhere(['NOT IN', 'id', $donorFromHistory])->orderBy('create_time ASC')->asArray()->all();
        $returnData = $this->addToDonorList($donorList, $outDonorList, $dbDonorList, $idTimeList, $timeIds);
        $donorList = $returnData[0];
        $outDonorList = $returnData[1];
        $dbDonorList = \app\models\Donorlite::find()->where('is_vip!=1 and donorstatus!=7')->andWhere(['IN', 'id', $donorFromHistory])->orderBy('create_time ASC')->asArray()->all();
        $returnData = $this->addToDonorList($donorList, $outDonorList, $dbDonorList, $idTimeList, $timeIds);
        $donorList = $returnData[0];
        $outDonorList = $returnData[1];
        foreach($donorList as $timeKey => $childList)
        {
            foreach($childList as $donorId)
            {
                $model = \app\models\Donorlist::findOne(['id_donor' => $donorId]);
                if(!$model)
                {
                    $model = new \app\models\Donorlist;
                    $model->id_donor = $donorId;
                    $model->approved = 0;
                }
                if($model->approved != 1)//если не утверждено время
                {
                    $model->type = 0;
                    $model->date_id = $timeKey;
                }
                $model->save();
            }
        }
        foreach($outDonorList as $timeKey => $childList)
        {
            foreach($childList as $donorId)
            {
                $model = \app\models\Donorlist::findOne(['id_donor' => $donorId]);
                if(!$model)
                {
                    $model = new \app\models\Donorlist;
                    $model->id_donor = $donorId;
                    $model->approved = 0;
                }
                if($model->approved != 1)//если не утверждено время
                {
                    $model->type = 1;
                    $model->date_id = $timeKey;
                }
                $model->save();
            }
        }//die;
        $this->redirect('/private/donorlist/list1');
    }
    
    /**
     * добавление доноров в список
     * @param array $donorList списко основных доноров
     * @param array $outDonorList список выездных доноров
     * @param array $dbDonorList список доноров, которых надо распределить
     * @param array $idTimeList массив со временем
     * @return array ($donorList, $outDonorList)
     */
    private function addToDonorList($donorList, $outDonorList, $dbDonorList, $idTimeList, $timeIds)
    {
        $priorityList = [1,2,3];
        //foreach($priorityList as $priority)
        {
            foreach($dbDonorList as $key => $donor)
            {
                if($this->inList($donor['id'], $donorList, $outDonorList))
                {
                    unset($dbDonorList[$key]);
                    continue;
                }
                $timeForDonor = $this->getTimeForDonor(
                        (int)$donor['id'], 
                        $idTimeList, 
                        $donor['typing'] == 0 && $donor['weight'] == 1 ? $donorList : $outDonorList, 
                        $donor['typing'] == 0 && $donor['weight'] == 1 ? $timeIds['in'] : $timeIds['out']
                        );
                if($timeForDonor !== FALSE)
                {
                    //var_dump($this->checkCount($donorList), $donor['weight'], $donor['typing']);
                    if($this->checkCount($donorList) !== FALSE 
                            && $donor['weight'] == 1 
                            && $donor['typing'] == 0
                            )
                    {
                        $donorList[$timeForDonor][] = (int)$donor['id'];
                    }
                    else
                    {
                        $outDonorList[$timeForDonor][] = (int)$donor['id'];
                    }
                    unset($dbDonorList[$key]);
                }
            }
        }
        return [$donorList, $outDonorList];
    }
    
    /**
     * 
     * @param int $donorId id донора
     * @param int $priority приоритет
     * @param array $idTimeList - список с доступным временем
     * @return boolean|int id выбранного времени или false
     */
    private function getTimeForDonor($donorId, $idTimeList, $donorList, $timeIds)
    {
        $models = \app\models\Eventday::find()
                ->select('date_id')
                ->where('id_donor=:id_donor', [':id_donor' => $donorId])
                ->andWhere(['in', 'date_id', $timeIds])
                ->orderBy('priority ASC')
                ->asArray()
                ->all();
        if($models)
        {
            $first = reset($models);
            $min = count($donorList[$first['date_id']]);
            $dateId = $first['date_id'];
//            if($min < 15)
//            {
//                return $dateId;
//            }
            foreach($models as $model)
            {
                if($min > count($donorList[$model['date_id']]))
                {
                    $min = count($donorList[$model['date_id']]);
                    $dateId = $model['date_id'];
                }
//                if($min < 15)
//                {
//                    return $dateId;
//                }
            }
            return $dateId;
        }
        return FALSE;
    }
    
    /**
     * проверка - можно ди добавить донора в основной список
     * @param type $donorList
     * @return type
     */
    private function checkCount($donorList)
    {
        $max = (int)\app\components\ConstantHelper::getValue('max-donor-count', 270);
        $counter = 0;
        foreach($donorList as $childList)
        {
            $counter += count($childList);
        }
        return $max > $counter;
    }
    
    /**
     * есть ли $donorId в списках
     * @param int $donorId
     * @param type $donorList
     * @param type $outDonorList
     * @return boolean
     */
    private function inList($donorId, $donorList, $outDonorList)
    {
        $donorId = (int)$donorId;
        foreach($donorList as $childList)
        {
            if(in_array($donorId, $childList))
            {
                return TRUE;
            }
        }
        foreach($outDonorList as $childList)
        {
            if(in_array($donorId, $childList))
            {
                return TRUE;
            }
        }
        return FALSE;
    }
}
