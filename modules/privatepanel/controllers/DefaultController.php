<?php

namespace app\modules\privatepanel\controllers;

use yii\web\Controller;
use app\modules\privatepanel\models\LoginForm;

use app\modules\privatepanel\models\ForgetPasswordForm;
use app\modules\privatepanel\models\RecoveryPasswordForm;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

class DefaultController extends Controller
{
    
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['access']= 
                    [
                        'class' => 'app\components\AccessControl',
                        'rules' => [
                            [
                                'actions' => ['login', 'recovery-password', 'logout'],
                                'allow' => true,
                                'roles' => ['@', '?'],
                            ],
                            ['class' => 'app\components\ActionRule']
                            //['class' => 'app\components\ManagerControl'],
                        ]
                    ];
        
        return $behaviors;
    
        /*return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];*/
    }
    
    protected function menuSort($a, $b)
    {
        if(!isset($a['sort']))
            $a['sort'] = 500;
        if(!isset($b['sort']))
            $b['sort'] = 500;
        if($a['sort'] == $b['sort'])
            return 0;
        return ($a['sort'] > $b['sort']);
    }
        
        
    public function actionIndex()
    {
        /*$quickMenus = \yii::$app->getModule('private')->quickMenus;
        
        foreach($quickMenus as $key => $menu)
        {
            if(isset($menu['icon']) && $menu['icon'])
            {
                $quickMenus[$key]['label'] = '<i class="fa fa-'.$menu['icon'].' fa-3x margbot20"></i><br/>'.$menu['label'];
            }
            $class = 'success';
            if(isset($menu['class']) && $menu['class'])
            {
                $class = $menu['class'];
            }
            $quickMenus[$key]['template'] = '<a href="{url}" class="panel btn-'.$class.' text-center">
                <p>{label}</p></a>';
        }
        usort($quickMenus, [$this, 'menuSort']);*/
        $metrika = new \app\modules\privatepanel\components\Metrika();
        $metrikaData = $metrika->getTodayInfo();
        if(isset($metrikaData['error']))
        {
            $metrikaData['error'] = $this->processMetrikaError($metrikaData['error']);
        }
        return $this->render('index',
        [
            'metrikaData' => $metrikaData,
        ]);
    }
    
    private function processMetrikaError($errorArray, $level=0)
    {
        $string = '';
        if(is_array($errorArray))
        {
            foreach($errorArray as $key=>$error)
            {
                $counter = 0;
                while($counter<$level)
                {
                    $string .= '&nbsp;';
                    $counter++;
                }
                $string .= $key.'=>'.(is_array($error) ? '<br/>' : '');
                $string .= $this->processMetrikaError($error, $level+1);
            }
        }
        else
        {
            $string .= $errorArray.'<br/>';
        }
        return $string;
    }
    
    public function actionLogin()
    {
        $this->layout = 'login';
        if (!\Yii::$app->user->isGuest) {
            return \Yii::$app->getResponse()->redirect(['/private/default/index']);
        }

        $model = new LoginForm();
        
        if ($model->load(\Yii::$app->request->post()) && $model->login()) {
            return $this->goBack(['/private/default/index']);
        } else {
            $forgetmodel = new ForgetPasswordForm();
            if(\Yii::$app->request->isAjax && $forgetmodel->load(\Yii::$app->request->post()))
            {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ActiveForm::validate($forgetmodel);
            }
        
            if ($forgetmodel->load(\Yii::$app->request->post()) && $forgetmodel->validate()) {
                $user = \app\models\UserIndentity::find()
                        ->where('username=:username OR email=:email', [':username' => $forgetmodel->username, ':email' => $forgetmodel->email])->one();
                if($user)
                {
                    $user->generatePasswordResetToken();
                    $user->save(true, ['access_token']);
                    $mailParams = [];
                    
                    $mailParams['NAME'] = $user->name;
                    $mailParams['ACCESS_TOKEN'] = $user->access_token;
                    $mailParams['RECOVERY_URL'] = Url::to(['/private/default/recovery-password', 'token' => base64_encode($user->access_token.'---'.time())], true);
                    $mailer = new \app\components\MailHelper;
                    $mailer->to = $user->email;
                    $mailer->subject = \yii::t('modules/privatepanel/app', 'Recovery of password');
                    /*$mailer->mailTemplate = 'forget-password';
                    $mailer->params = $mailParams;*/
                    $mailer->content = \yii::$app->controller->renderFile(\yii::$app->getModule('private')->getViewPath(). '/mail/forget_password.php', [
                        'params' => $mailParams,
                    ]);
                    $mailer->send();
                    
                    \Yii::$app->session->setFlash('login_message_success', \yii::t('modules/privatepanel/app', 'E-mail with information about recovery password was sent'));
                } else {
                    \Yii::$app->session->setFlash('login_message_error', \yii::t('modules/privatepanel/app', 'Email is not found'));
                }
                //return $this->goBack();
            }
            return $this->render('login', [
                'model' => $model,
                'forgetmodel' => $forgetmodel,
            ]);
        }
    }
    
    
    public function actionLogout()
    {
        \Yii::$app->user->logout();

        //return $this->goHome();
        return \Yii::$app->getResponse()->redirect(['/private/default/login']);
    }
    
    public function actionRecoveryPassword()
    {
        $this->layout = 'login';
        if (!\Yii::$app->user->isGuest) {
            return \Yii::$app->getResponse()->redirect(['/private/default/index']);
        }
        
        $model = null;
        
        if(\yii::$app->request->get('token'))
        {
            $token = base64_decode(\yii::$app->request->get('token'));
            
            if(count(explode('---', $token)) == 2)
            {
                list($token, $time) = explode('---', $token);

                if(time() - $time <= 24*60*60)
                {
                    $user = \app\models\UserIndentity::find()
                        ->where('access_token=:token', [':token' => $token])->one();

                    if($user)
                    {
                        $model = new RecoveryPasswordForm();
                        if ($model->load(\Yii::$app->request->post()) && $model->validate()) {
                            $user->password = $model->password;
                            $user->access_token = '';
                            $user->save(true, ['password', 'access_token', 'update_time']);
                            $mailParams = [
                                'NAME' => $user->name,
                                'USERNAME' => $user->username,
                                'EMAIL' => $user->email,
                                'PASSWORD' => $model->password,
                            ];
                            
                            $mailer = new \app\components\MailHelper;
                            $mailer->to = $user->email;
                            $mailer->subject = \yii::t('app', 'Recovery of password');
                            //$mailer->mailTemplate = 'recovery-password';
                            //$mailer->params = $mailParams;
                            $mailer->content = \yii::$app->controller->renderFile(\yii::$app->getModule('private')->getViewPath(). '/mail/recovery_password.php', [
                                'params' => $mailParams,
                            ]);
                            $mailer->Send();
                            \Yii::$app->session->setFlash('login_message_success', \yii::t('modules/privatepanel/app', 'Set new password'));
                            return $this->redirect(['/private/default/login']);
                        }

                    } else {
                        \Yii::$app->session->setFlash('login_message_error', \yii::t('modules/privatepanel/app', 'User is not found'));
                    }
                } else {
                    \Yii::$app->session->setFlash('login_message_error', \yii::t('modules/privatepanel/app', 'Validity of links to the password recovery has expired. Please repeat the password recovery procedure.'));
                }
            } else {
                \Yii::$app->session->setFlash('login_message_error', \yii::t('modules/privatepanel/app', 'User is not found'));
            }
        } else {
            \Yii::$app->session->setFlash('login_message_error', \yii::t('modules/privatepanel/app', 'User is not found'));
        }
        
        return $this->render('recovery-password', [
            'model' => $model,
        ]);
        
    }
}
