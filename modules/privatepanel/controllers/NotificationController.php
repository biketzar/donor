<?php

namespace app\modules\privatepanel\controllers;

use app\modules\privatepanel\components\PrivateController;

class NotificationController extends PrivateController
{
    protected $_modelName = 'app\models\Notification';
    
    public function actionSend()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if(\Yii::$app->request->isPost)
        {
            $model = new \app\models\Notification();
            $model->setScenario('insert');
            $model->load(\Yii::$app->request->post());
            if($model->save())
            {
                return ['result' => TRUE, 'message' => 'Уведомления отправлены'];
            }
            else
            {
                return ['result' => TRUE, 'message' => strip_tags(\yii\helpers\Html::errorSummary($model))];
            }
        }
    }
}