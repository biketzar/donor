<?php

namespace app\modules\privatepanel\controllers;

use app\modules\privatepanel\components\PrivateController;
class MetrikaController extends PrivateController
{
    public function actionSettings() {
        if(isset($_GET['metrikaKey']) && isset($_GET['metrikaID']))
        {
            $config = \Yii::$app->params['projectSettings'];
            $config['metrikaKey']['value'] = $_GET['metrikaKey'];
            $config['metrikaID']['value'] = $_GET['metrikaID'];
            file_put_contents(CONFIG_FILE, json_encode($config));
            
        }
        $this->redirect(array('/private'));
    }

}
