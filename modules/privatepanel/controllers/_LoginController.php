<?php
namespace app\modules\privatepanel\controllers;

use yii\web\Controller;
use yii\helpers\Url;

class LoginController extends Controller
{
    public function actionIndex()
    {
        if(!\Yii::$app->user->isGuest)
        {
            return $this->goHome();
                //$this->redirect(Yii::app()->createUrl($this->getModule()->mainPageUrl));
        }
        return $this->render('index');
    }
    
    public function goHome()
    {
        return \Yii::$app->getResponse()->redirect(Url::to(['default/index']));
    }
}
?>
