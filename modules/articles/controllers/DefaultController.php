<?php

namespace app\modules\articles\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\modules\articles\models;

use yii\data\ActiveDataProvider;

class DefaultController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        //'actions' => ['index', 'view'],
                        'actions' => ['index', 'view'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /*public function actions()
    {
        return [
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
    */
    public function actionIndex()
    {
        $query = models\Article::find();
        
        $section_id = Yii::$app->request->get('section_id', 0);
        
        $section = null;
        
        if($section_id)
        {
            $section = models\ArticleSection::find()->where('id=:id', [':id' => $section_id])->published()->one();
            if(!$section)
            {
                throw new \yii\web\NotFoundHttpException(\yii::t('app', 'Section is not found'));
            }
            $query = $query->section($section_id);
        }
        $query = $query->withTranslation()->published();
        
        $provider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'date' => SORT_DESC,
                    'id' => SORT_DESC, 
                ]
            ],
        ]);
        
        $models = models\Article::find()->all();

        //$items = $provider->getModels();
        return $this->render('index',
                [
                    'dataProvider' => $provider,
                    'section'   => $section,
                    'models' => $models,
                ]);
    }
    
    public function actionView($id)
    {
        $item = models\Article::find()->where('id=:id', [':id' => $id])->published()->one();
        
        if(!$item)
        {
            throw new \yii\web\NotFoundHttpException(\yii::t('app', 'Element is not found'));
        }
        
        return $this->render('view',
                [
                    'model' => $item,
                    'section'   => $item->section,
                ]);
    }
    
    public function actionArchive()
    {
        $query = models\Article::find()->published();
        
        $section_id = Yii::$app->request->get('section_id', 0);
        
        $section = null;
        
        if($section_id)
        {
            $section = models\ArticleSection::find()->where('id=:id', [':id' => $section_id])->published()->one();
            if(!$section)
            {
                throw new \yii\web\NotFoundHttpException(\yii::t('app', 'Section is not found'));
            }
            
            $query = $query->section($section_id);
        }
        
        $years = models\Article::getYears($query);
        $year = Yii::$app->request->get('year', date('Y'));
        if($year)
        {
            $query = $query->year($year);
        }
        $months = models\Article::getMonths($query);
        $month = Yii::$app->request->get('month', date('m'));
        if($month)
        {
            $query = $query->month($month);
        }
        
        $provider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 100,
            ],
            'sort' => [
                'defaultOrder' => [
                    'date' => SORT_DESC,
                    'id' => SORT_DESC, 
                ]
            ],
        ]);
        
        //$items = $provider->getModels();
        return $this->render('archive',
                [
                    'dataProvider' => $provider,
                    'currentYear'   => $year,
                    'currentMonth'   => $month,
                    'years'   => $years,
                    'months'   => $months,
                    'section'   => $section,
                ]);
    }
    
    
}
