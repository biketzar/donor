<?php

namespace app\modules\articles;

use yii\helpers\ArrayHelper;

use yii\data\ActiveDataProvider;
use yii\helpers\Url;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\articles\controllers';
    
    public function init()
    {
        parent::init();
        // custom initialization code goes here
        self::registerTranslations();
    }
    
    public static function registerTranslations()
    {
        \Yii::$app->i18n->translations['modules/articles/*'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'sourceLanguage' => 'en-US',
            'basePath' => '@app/modules/articles/messages',
            'fileMap' => [
                'modules/articles/app' => 'app.php',
                //'modules/articles/app' => 'app.php',
            ],
        ];
    }
    
    public function getHandlers()
    {
        return [
            'articles/default/index' => \yii::t('modules/articles/app', 'News list'),
        ];
    }
    
    public function getHandlerData()
    {
        $result = ['articles/default/index' => []];
        
        $sections = models\ArticleSection::find()->asArray()->all();
        $result['articles/default/index']['section_id'] = ['RDbSelect', 'data' => ArrayHelper::map($sections, 'id', 'name'), 'label' => \yii::t('modules/articles/app', 'Article section')];
        
        return $result;
    }
    public function getHandlerRules()
    {
        $result = ['articles/default/index' => [
            ['class' => 'app\\modules\\articles\\components\\UrlRule'],
        ]];
        
        return $result;
    }
    
    public function getSitemapByHandler($page_id, $handler, $data = [])
    {
        $result = [];
		return $result;
        if($handler == 'articles/default/index')
        {
            $query = models\Article::find()->published();
            
            $section_id = (isset($data['section_id'])?$data['section_id']:0);
        
            $section = null;

            if($section_id)
            {
                $section = models\ArticleSection::find()->where('id=:id', [':id' => $section_id])->published()->one();
                if($section)
                {
                    $query = $query->section($section_id);
                }
                
            }

            $provider = new ActiveDataProvider([
                'query' => $query,
                'pagination' => [
                    'pageSize' => 100,
                ],
                'sort' => [
                    'defaultOrder' => [
                        'date' => SORT_DESC,
                        'id' => SORT_DESC, 
                    ]
                ],
            ]);
            $models = $provider->getModels();
            foreach($models as $model)
            {
                $url = Url::to(['/articles/default/view', 'id' => $model->id, 'page_id'=>$page_id]);
                $result[] = [
                    'url' => $url,
                    'label' => $model->name,
                    'items' => [],
                ];
            }
        }
        
        return $result;
    }
    
    public function getSearchResultsByHandler($form, $page_id, $handler, $data = [])
    {
        $result = [];
        if($handler == 'articles/default/index')
        {
            $section_id = (isset($data['section_id'])?$data['section_id']:0);
        
            $section = null;
            
            //$modelClass = new models\ArticleSection('search');
            //$modelClass->setScenario('search');
            $query = models\ArticleSection::find();
            
            if($section_id)
            {
                $query = $query->andWhere('id=:id', [':id' => $section_id]);
            }
            
            $query = $query->andFilterWhere(['like', 'name', $form->query]);
            $query = $query->orFilterWhere(['like', 'desc', $form->query]);
            
            $data = $query->published()->all();
            
            foreach($data as $item)
            {
                $url = Url::to(['articles/default/index', 'section_id' => $section_id, 'page_id' => $page_id]);
                $result[$url] =[
                    'title' => $item->name,
                    'url' => $url,
                    'content' => '',
                    'view' => ''
                ];
            }
            
            //$modelClass = new models\Article('search');
            $query = models\Article::find();
            
            $query = $query->andFilterWhere(['like', 'name', $form->query]);
            $query = $query->orFilterWhere(['like', 'short_desc', $form->query]);
            $query = $query->orFilterWhere(['like', 'desc', $form->query]);
            
            $query->published();
            
            $section_id = (isset($data['section_id'])?$data['section_id']:0);
        
            $section = null;

            if($section_id)
            {
                $section = models\ArticleSection::find()->where('id=:id', [':id' => $section_id])->published()->one();
                if($section)
                {
                    $query = $query->section($section_id);
                }
                
            }
            

            $data = $query->all();
            
            foreach($data as $item)
            {
                $url = Url::to(['/articles/default/view', 'id' => $item->id, 'page_id'=>$page_id]);
                $result[$url] =[
                    'title' => $item->name,
                    'url' => $url,
                    'content' => '',
                    'view' => ''
                ];
            }
        }
        if($handler == 'articles/default/view')
        {
            $id = (isset($data['id'])?$data['id']:0);
        
            $modelClass = new models\ArticleSection('search');
            $query = $modelClass::find()->published();
            
            $query = $query->andFilterWhere(['like', 'name', $form->query]);
            $query = $query->andFilterWhere(['like', 'short_desc', $form->query]);
            $query = $query->andFilterWhere(['like', 'desc', $form->query]);
            
            $data = $query->all();
            
            foreach($data as $item)
            {
                $url = Url::to(['/articles/default/view', 'id' => $item->id, 'page_id'=>$page_id]);
                $result[$url] =[
                    'title' => $item->name,
                    'url' => $url,
                    'content' => '',
                    'view' => ''
                ];
            }
        }
        
        return $result;
    }
    
    public function privateMenuLinks()
    {
        return ['content' => ['items' => [['label' => \yii::t('modules/articles/app', 'Articles'), 'url'=> ['/private/article-section'], 'items' => [
            //['label' => \yii::t('modules/articles/app', 'Article sections'), 'url'=> ['/private/article-section'], 'visible' => \yii::$app->user->can('/private/article-section/admin')],
            ['label' => \yii::t('modules/articles/app', 'Articles'), 'url'=> ['/private/article'], 'visible' => \yii::$app->user->can('/private/article/admin')],
            ]]]
        ]];
    }
    public function privateControllerMap()
    {
        return [
            'article' => '\app\modules\articles\controllers\privatepanel\ArticleController',
            //'article-section' => '\app\modules\articles\controllers\privatepanel\ArticleSectionController'
        ]
        ;
    }
    
}
