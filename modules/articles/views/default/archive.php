<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ListView;
use app\components\SiteHelper;

$page = SiteHelper::getCurrentPage();

$metaData = SiteHelper::metaData($this, [$section], [
    'title' => ($page? ($page->handler == 'articles/default/archive'?$page->name:$page->name.' : '.\Yii::t('modules/articles/app', 'archive')):\Yii::t('modules/articles/app', 'News archive')),
    'meta_title' => ($page? ($page->handler == 'articles/default/archive'?$page->name:$page->name.' : '.\Yii::t('modules/articles/app', 'archive')):\Yii::t('modules/articles/app', 'News archive')),
    'meta_description' => '',
    'meta_keywords' => '',
    ]);
echo '<h1>'.$metaData['title'].'</h1>';
$this->params['breadcrumbs'] = isset($this->params['breadcrumbs'])?$this->params['breadcrumbs']:[];
if($section)
    $this->params['breadcrumbs'][] = ['label' => $section->name, 'url'=> Url::to(['index', 'section_id' => $section->id, 'page_id'=>\yii::$app->request->get('page_id')])];
$this->params['breadcrumbs'][] = ['label' => $metaData['title']];
$new_years = [];
foreach($years as $k=>$r)
{
    $new_years[] = Html::a($r, Url::to(['archive', 'year' => $k, 'page_id'=>\yii::$app->request->get('page_id')]), ['class' => ($k == $currentYear)?'active':'']);
}
echo Html::ul($new_years, ['class'=>'year', 'encode' => false]);

$new_months = [];
foreach($months as $k=>$r)
{
    $new_months[] = Html::a(\yii::t('common', date('F', strtotime($currentYear.'-'.$r.'-01'))), Url::to(['archive', 'year' => $currentYear, 'month' => $k, 'page_id'=>\yii::$app->request->get('page_id')]), ['class' => ($k == $currentMonth)?'active':'']);
}
echo Html::ul($new_months, ['class'=>'month', 'encode' => false]);
?>
<div>
    <a href="<?=Url::to(['index','page_id'=>\Yii::$app->request->get('page_id')]);?>"><?=\Yii::t('modules/articles/app', 'News')?></a>
    <a href="<?=Url::to(['archive','page_id'=>\Yii::$app->request->get('page_id')]);?>"><?=\Yii::t('modules/articles/app', 'News archive')?></a>
</div>

<?php
echo ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => '_post_archive',
    'options' => ['class' => 'news-archive'],
    'summary' => false
]);
?>
