<?php
use yii\helpers\Html;
use yii\helpers\Url;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

//\yii::t('common', date('F', strtotime($currentYear.'-'.$r.'-01')));
$url = Url::to(['view', 'id' => $model->id, 'page_id'=>\yii::$app->request->get('page_id')]);
?>
<div class="news-post">
    <span class="date"><?php echo \Yii::$app->formatter->asDate($model->date, 'php:d M Y');?></span>
    <span><?php echo Html::a(Html::encode($model->name), $url);?></span>
</div> 