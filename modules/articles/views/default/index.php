<?php

use yii\widgets\ListView;
use app\components\SiteHelper;
use yii\helpers\Url;

$this->title = 'Новости';
$page = SiteHelper::getCurrentPage();

//$this->params['breadcrumbs'] = isset($this->params['breadcrumbs'])?$this->params['breadcrumbs']:[];
//$this->params['breadcrumbs'] = array_merge($this->params['breadcrumbs'], SiteHelper::getCurrentPageBreadcrumbs(false));
//
//$metaData = SiteHelper::metaData($this, [$section], [
//    'title' => $page? $page->name:\Yii::t('app', 'News'),
//    'meta_title' => $page? $page->name:\Yii::t('app', 'News'),
//    'meta_description' => '',
//    'meta_keywords' => '',
//    ]);
//echo '<h1>'.$metaData['title'].'</h1>';
//if(!$page)
//    $this->params['breadcrumbs'][] = ['label' => $metaData['title']];

?>
<div class="container">
    <div class="row row-offcanvas row-offcanvas-right">
        <div class="col-xs-12 col-sm-9">
            <p class="pull-right visible-xs">
                <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Содержание</button>
            </p>
            <?php
            foreach($models as $model)
            {
                ?>
            <h1 id="news<?=$model->id?>"><?=$model->name?></h1>
			  <div class="news-date"><?=app\components\SiteHelper::formatDate($model->date);?></div>
            <?=$model->short_desc?>
            <hr class="margin40"/>
            <div class="clear"></div>
            <?php
            }
            ?>
            
            </div>            
            <div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar">
                <div class="list-group">
                    <?php
                    foreach($models as $model)
                    {
                        ?>
                    <a href="#news<?=$model->id?>" class="list-group-item"><?=$model->name?></a>
                    <?php
                    }
                    ?>
                </div>
            </div><!--/.sidebar-offcanvas-->
            </div><!--/row-->
        </div>
