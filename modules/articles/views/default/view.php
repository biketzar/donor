<?php
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use yii\helpers\Url;
use app\components\SiteHelper;

$page = SiteHelper::getCurrentPage();

$metaData = SiteHelper::metaData($this, [$model, $model->section], [
    'title' => \Yii::t('app', $model->name),
    'meta_title' => \Yii::t('app', $model->name),
    'meta_description' => '',
    'meta_keywords' => '',
    ]);
echo '<h1>'.$metaData['title'].'</h1>';
$this->params['breadcrumbs'] = isset($this->params['breadcrumbs'])?$this->params['breadcrumbs']:[];
$this->params['breadcrumbs'] = array_merge($this->params['breadcrumbs'], SiteHelper::getCurrentPageBreadcrumbs(true));
if(!$page)
{
    $this->params['breadcrumbs'][] = ['label' => $model->section->name, 'url'=> Url::to(['index', 'section_id' => $model->section->id])];
}
$this->params['breadcrumbs'][] = ['label' => $metaData['title']];
?>

<div class="news-detail">
    <div class="date"><?php echo \Yii::$app->formatter->asDate($model->date, 'php:d.m.Y');?></div>
    <div class="image">
        <?php
        if($model->image)
        {
            echo Html::img(\app\components\ImageHelper::thumbnail($model->getImagePath(), 250, 250));
        };
        ?>
    </div>
    <h3><?php echo Html::encode($model->name);?></h3>
    <?php echo HtmlPurifier::process($model->desc) ?>
    <?= app\widgets\PhotogalleryWidget::widget(['images' => $model->photogallery]) ?>
    <?php
    if($model->section)
    {
        $plural = mb_strtolower($model->section->name, 'UTF-8');

        $url = Url::to(['index', 'section_id' => $model->section->id]);
        echo Html::a(\Yii::t('app', 'All').' '.$plural, $url);
    }
    ?>
</div>
