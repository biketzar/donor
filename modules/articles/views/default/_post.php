<?php
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use yii\helpers\Url;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
//var_dump($model);
$url = Url::to(['view', 'id' => $model->id, 'page_id'=>\yii::$app->request->get('page_id')]);
?>
<div class="news-post">
    <div class="date"><?php echo \Yii::$app->formatter->asDate($model->date, 'php:d.m.Y');?></div>
    <div class="image">
        <?php
        if($model->image)
        {
            echo Html::a(Html::img(\app\components\ImageHelper::thumbnail($model->getImagePath(), 150, 150)), $url);
        };
        ?>
    </div>
    <h3><?php echo Html::a(Html::encode($model->name), $url);?></h3>
    <?php echo HtmlPurifier::process($model->short_desc?$model->short_desc:$model->desc) ?>
</div>