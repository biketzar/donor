<?php
return [
    'Article sections' => 'Разделы статей / новостей',
    'Article section' => 'Раздел статей / новостей',
    'Articles' => 'Статьи / Новости',
    'Article' => 'Статья / Новость',
    'Name' => 'Название',
    'Alias' => 'Псевдоним',
    'Published' => 'Опубликовано',
    'Desc' => 'Описание',
    'Section' => 'Раздел',
    'Image' => 'Изображение',
    'Short Desc' => 'Короткое описание',
    'Ordering' => 'Сортировка',
    'Date' => 'Дата',
    'Section is not found' => 'Раздел не найден',
    'Element is not found' => 'Элемент не найден',
    'News list' => 'Список статей / новостей',
    'archive' => 'архив',
    'News archive' => 'Архив новостей',
    'News' => 'Новости',
    
];
?>
