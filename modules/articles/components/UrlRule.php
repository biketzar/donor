<?php
namespace app\modules\articles\components;

use yii\web\UrlRuleInterface;
use yii\base\Object;
use app\modules\articles\models;

class UrlRule extends Object implements UrlRuleInterface
{

    public function createUrl($manager, $route, $params)
    {
        $url = '';

        if($route == 'articles/default/view')
        {

            $object = models\Article::find();
            if(isset($params['section_id']))
                $object = $object->andWhere('`section_id`=:section_id', [':section_id'=> $params['section_id']]);
            if(isset($params['alias']))
                $object = $object->andWhere('`alias`=:alias', [':alias'=> $params['alias']]);
            if(!isset($params['alias']) && isset($params['id']))
                $object = $object->andWhere('`id`=:id', [':id'=> $params['id']]);
            $object = $object->published()->limit(1)->one();
            
            if(isset($params['pageObject']))
            {
                $url .= $params['pageObject']->alias;
                unset($params['pageObject']);
            }
            
            if($object)
            {
                $url .= '/'.$object->alias;
                if(isset($params['alias'])) unset($params['alias']);
                if(isset($params['id'])) unset($params['id']);
                if(isset($params['section_id'])) unset($params['section_id']);
                if (!empty($params) && ($query = http_build_query($params)) !== '') {
                    $url .= '?' . $query;
                }
                return $url;
            }
        }
        if($route == 'articles/default/index')
        {
            if(isset($params['pageObject']))
            {
                $url .= $params['pageObject']->alias;
                if(isset($params['section_id']) && isset($params['pageObject']->handler_data['section_id']) 
                        && ($params['section_id'] == $params['pageObject']->handler_data['section_id'])) 
                    unset($params['section_id']);
                unset($params['pageObject']);
            }
            $url .= '';
            
            if (!empty($params) && ($query = http_build_query($params)) !== '') {
                $url .= '?' . $query;
            }
            return $url;
        }
        if($route == 'articles/default/archive')
        {
            if(isset($params['pageObject']))
            {
                $url .= $params['pageObject']->alias;
                if($params['pageObject']->handler != $route)
                    $url .= '/archive';
                if(isset($params['section_id']) && isset($params['pageObject']->handler_data['section_id']) 
                        && ($params['section_id'] == $params['pageObject']->handler_data['section_id'])) 
                    unset($params['section_id']);
                unset($params['pageObject']);
            } else {
                $url .= 'archive';
            }
            if (!empty($params) && ($query = http_build_query($params)) !== '') {
                $url .= '/?' . $query;
            }
            return $url;
        }
        
        return false;  // this rule does not apply
    }
    
    public function parseRequest($manager, $request)
    {

        $pathInfo = $request->getPathInfo();
        
        $pathInfo = trim($pathInfo, '/');


        if($pathInfo == 'archive')
            return ['articles/default/archive', []];
        if($pathInfo == '')
            return ['articles/default/index', []];
        $parts = explode('/', $pathInfo);
        
        foreach($parts as $part)
        {
            $object = models\Article::find()->where('`alias`=:alias', [':alias'=> $part])->published()->limit(1)->one();
            if($object)
            {
                $getParams = \yii::$app->request->getQueryParams();
                $getParams['id'] = $object->id;
                \yii::$app->request->setQueryParams($getParams);
            }
        }
        if(\yii::$app->request->get('id'))
            return ['articles/default/view', ['id' => \yii::$app->request->get('id')]];
        else
            return false;
    }
}
?>