<?php
namespace app\modules\articles\models;

use app\modules\articles\Module;

use \app\components\CommonActiveRecord as ActiveRecord;
use \app\components\CommonActiveQuery;
use yii\db\Query;
use yii\helpers\Url;

class Article extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%articles}}';
    }
    
    public function init()
    {
        parent::init();
        Module::registerTranslations();
        $this->_privateAdminName = \yii::t('modules/articles/app', $this->_privateAdminName);
        $this->_privatePluralName = \yii::t('modules/articles/app', $this->_privatePluralName);
        
    }
    
    /*public function behaviors()
    {
        $behaviors = parent::behaviors();
        return array_merge($behaviors, [
            'ml' => [
                'class' => \app\components\MultilingualBehavior::className(),
                //'attributes' => [
                    
                    'localizedAttributes' => [
                        'name', 'desc', 'short_desc'
                    ],
                    'strict' => false,
                //],
            ],
        ]);
    }
    */
    /*public function beforeSave($insert)
    {
        if(trim($this->alias) == '')
        {
            $this->alias = \app\components\Translite::str2url($this->name);
        }
        else
        {
            $this->alias = \app\components\Translite::str2url($this->alias);
        }
        return parent::beforeSave($insert);
    }*/
    
    public function afterFind()
    {
        parent::afterFind();
        
    }
    
    public function getSection()
    {
        return $this->hasOne(ArticleSection::className(), ['id' => 'section_id']);
    }
    
    public function getPhotogallery()
    {
        return $this->hasMany(\app\models\Photogallery::className(), ['item_id' => 'id'])
                ->where('model = :modelName', ['modelName' => self::className()]);
    }
    public function getMetadata()
    {
        return $this->hasOne(\app\models\Metadata::className(), ['item_id' => 'id'])
                ->where('model = :modelName', ['modelName' => self::className()]);
    }
    
    public static function find()
    {
        $query = new ArticleActiveQuery(get_called_class());
        return $query;
    }
    
    public function attributeLabels() {
        
        return [
            'name' => 'Название',
            'alias' => 'Псевдоним',
            'published' => 'Опубликовано',
            'desc' => 'Описание',
            'section' => 'Тип',
            'image' => 'Изображение',
            'short_desc' => 'Кр. описание',
            'ordering' => 'Сортировка',
            'date' => 'Дата',
            
            'section_id'=>'Тип',
        ];
        
    }
    
    public function rules()
    {
        return [
            [['id','name', 'alias' ,'short_desc', 'desc', 'published', 'section_id', 'ordering', 'date'], 'safe'],
            //[['name', 'alias', 'route', 'metatitle'], 'string', ['max'=>256]],
            [['short_desc', 'desc'], 'safe', 'on'=>['insert', 'update']],
            [['published', 'section_id', 'ordering'], 'number', 'integerOnly' => true, 'on'=>['insert', 'update']],
            [['alias'], 'app\components\AliasValidator', 'on'=>['insert', 'update'], 'skipOnEmpty'=>false],
            [['alias'], 'unique', 'targetAttribute' => ['alias'], 'on'=>['insert', 'update']],
            [['name', 'alias'], 'required', 'on'=>['insert', 'update']],
            [['image'], 'app\components\CustomImageValidator', 'on'=>['insert', 'update']],
            
            [['id', 'name', 'alias', 'short_desc', 'desc', 'published', 'section_id', 'date'], 'safe', 'on'=>'search'],
        ];
    }
    
    public static function getYears(\yii\db\ActiveQuery $orig_query)
    {
        $query = clone $orig_query;
        $query->select = ['DISTINCT(YEAR(date)) as year'];
        $query->orderBy = ['date' => 'DESC'];
        $result = array();
        foreach($query->asArray()->all() as $row)
        {
            $result[$row['year']] = $row['year'];
        }
        
        return $result;
    }
    
    public static function getMonths(\yii\db\ActiveQuery $orig_query)
    {
        $query = clone $orig_query;
        $query->select = ['DISTINCT(MONTH(date)) as month'];
        $query->orderBy = ['date' => 'DESC'];
        $result = array();
        foreach($query->asArray()->all() as $row)
        {
            $row['month'] = ($row['month']>10)?$row['month']:'0'.$row['month'];
            $result[$row['month']] = $row['month'];
        }
        
        return $result;
    }
    
    public static function getLastItems($section_alias = '', $count = 5)
    {
        $query = self::find()->select(['id', 'alias', 'section_id', 'name', 'image', 'date'])
                ->orderBy(['date'=>SORT_DESC])
                ->published()
                ->limit($count);
        if($section_alias)
        {
            if(is_numeric($section_alias))
            {
                $query->andWhere('section_id=:section_id', [':section_id' => $section_alias]);
            } else {
                $section = ArticleSection::find()->andWhere('alias=:alias', [':alias' => $section_alias])->select('id')->asArray()->one();
                if($section)
                {
                    $query->andWhere('section_id=:section_id', [':section_id' => $section['id']]);
                }
            }
        }
        return $query->all();
    }
    
    public static function getItemUrl(Article $item)
    {
        return Url::to(['/articles/default/view', 'id' => $item->id, 'page_id'=>\app\components\SiteHelper::getPageIdByHandler('articles/default/index', ['section_id' => $item->section_id])]);
    }
    public function getUrl()
    {
        return static::getItemUrl($this);
    }
    
    
    /*for crud admin*/
    public $_privateAdminName = 'Article'; // One item name, used in breadcrums, in header in actionAdmin
    public $_privatePluralName = 'Articles';// Item set name
    public $_privateAdminRepr = 'name';// method or property used to retrieve model string represantation. It's close to __toString but only for admin
    
    
    public function getAdminFields()
    {
            return array(
                    'name',
                    'date',
                    'published',
                    'ordering',
                    
            );
            
    }

    public function getFieldsDescription()
    {
            return array(
                    //'section_id' => array('RDbRelation', 'section'),
                    'name' => 'RDbText',
                    'alias' => 'RDbText',
                    'date' => 'RDbDate',
                    'image' => 'RDbFile',
                    'short_desc' => array('RDbText', 'forceText'=>'forceText'),
                    'desc' => 'RDbText',
                    'published' => 'RDbBoolean',
                    'ordering' => 'RDbText',
                    
            );
    }
    
    public function relationReferenceOptions()
    {
            return array(
                    'photogallery' => array('controller' => 'photo-gallery', 'relationName'=>'item', 'icon'=>'picture-o', 'urlOptions' => ['model' => self::className()]),
                    'metadata' => array('controller' => 'metadata', 'relationName'=>'item', 'icon'=>'tag', 'color'=>'lilac', 'urlOptions' => ['model' => self::className()]),
            );
    }
    
    public function getImageDirectory()
    {
        return \yii::getAlias('@web2').'/files/articles';
    }
    
    public function getImagePath()
    {
        if(!$this->image)
            return '';
        return $this->getImageDirectory().'/'.$this->image;
    }

    public function getNextArticle($id){
        return $this->find()->where('id=:id', [':id' => ++$id])->published()->one();
    }
    public function getPrevArticle($id){
        return $this->find()->where('id=:id', [':id' => --$id])->published()->one();
    }

    /**
     * @return Query
    */
    public function lastArticles(){
        return $this->find()->where('date>=:curdate',[':curdate'=>date('Y-m-d')])->orderBy(['id'=>'DESC']);
    }

}
class ArticleActiveQuery extends CommonActiveQuery
{
    public function section($section_id = 0)
    {
        return $this->andWhere(['section_id' => $section_id]);
    }
    public function year($year = 0)
    {
        return $this->andWhere(['YEAR(date)' => $year]);
    }
    public function month($month = 0)
    {
        return $this->andWhere(['MONTH(date)' => $month]);
    }
    public function published($state = 1)
    {
        $sections = ArticleSection::find()->published()->select('id')->asArray()->column();
        
        return $this->andWhere(['published' => $state, 'section_id' => $sections]);
    }
}
?>
