<?php
namespace app\modules\articles\models;

use app\modules\articles\Module;

use \app\components\CommonActiveRecord as ActiveRecord;

class ArticleSection extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%article_sections}}';
    }
    
    public function init()
    {
        parent::init();
        Module::registerTranslations();
        $this->_privateAdminName = \yii::t('modules/articles/app', $this->_privateAdminName);
        $this->_privatePluralName = \yii::t('modules/articles/app', $this->_privatePluralName);
    }
    
    public function attributeLabels() {
        
        return [
            'name' => \yii::t('modules/articles/app', 'Name'),
            'alias' => \yii::t('modules/articles/app', 'Alias'),
            'published' => \yii::t('modules/articles/app', 'Published'),
            'desc' => \yii::t('modules/articles/app', 'Desc'),
            
            
        ];
        
    }
    
    public function afterFind()
    {
        parent::afterFind();
        
    }
    
    public function getChildren()
    {
        return $this->hasMany(Article::className(), ['section_id' => 'id']);
    }
    
    public function rules()
    {
        return [
            [['id', 'name', 'alias', 'desc', 'published'], 'safe'],
            //[['name', 'alias', 'route', 'metatitle'], 'string', ['max'=>256]],
            [['published'], 'number', 'integerOnly' => true, 'on'=>['insert', 'update']],
            [['name'], 'required', 'on'=>['insert', 'update']],
            [['alias'], 'app\components\AliasValidator', 'on'=>['insert', 'update']],
            [['id', 'name', 'alias', 'desc', 'published'], 'safe', 'on'=>'search'],
        ];
    }
    
    
    /*for crud admin*/
    public $_privateAdminName = 'Article section'; // One item name, used in breadcrums, in header in actionAdmin
    public $_privatePluralName = 'Article sections';// Item set name
    public $_privateAdminRepr = 'name';// method or property used to retrieve model string represantation. It's close to __toString but only for admin
    
    
    public function getAdminFields()
    {
            return array(
                    'name',
                    
                    //'published',
            );
            
    }

    public function getFieldsDescription()
    {
            return array(
                    'name' => 'RDbText',
                    
                    'desc' => 'RDbText',
                    'show_on_main'=>'RDbBoolean',
                    //'published' => 'RDbBoolean',
            );
    }

}
?>
