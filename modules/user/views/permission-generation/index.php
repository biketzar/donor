<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

?>
<?php $form = ActiveForm::begin([
	'id'=>strtolower($model->formName()).'-form',
	'enableAjaxValidation' => false,
	
	'enableClientValidation' => false,
]); ?>
<?php echo $form->errorSummary($model); ?>
<?php
foreach($items as $item)
{
    echo '<div class="row">';
    echo Html::input('checkbox', $model->formName().'[items][]', htmlspecialchars($item), array('id'=>$model->formName().'-items-'.str_replace('.', '-', $item)));
    echo ' '.Html::label($item, $model->formName().'-items-'.str_replace('.', '-', $item));
    echo '</div>';
}
?>
<?= Html::submitButton(\Yii::t('modules/user/app', 'Save'), ['class' => 'btn btn-primary', 'name'=>'save']) ?>
<?php ActiveForm::end() ?>