<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->params['breadcrumbs'] = [
    ['label' => \Yii::t('modules/user/app', 'Users'), 'url'=> ['admin']],
    \Yii::t('modules/user/app', 'All')
    
];
$this->params['menu'] = [
			['label'=> \Yii::t('modules/user/app', 'Create').': ' . \Yii::t('modules/user/app', 'User'), 'url'=>['create'], 'visible' => \Yii::$app->user->can('/user/user/create'), 'template' => '<a href="{url}" class="btn btn-primary"><i class="fa fa-user-plus"></i> {label}</a>'],
		];;

$this->title = \Yii::t('modules/user/app', 'Users');

/*
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('".strtolower( get_class($model))."-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
*/

echo GridView::widget(array(
        'id'=>'settings-grid',
        'dataProvider'=>$dataProvider,
        'filterModel'=>$model,
        'layout' => "{items}\n<div class=\"middle-block\">{summary}</div>\n<div class=\"middle-block text-right\">{pager}</div>",
        'tableOptions' => ['class' => 'table table-striped table-hover'],
        'columns'=> [
            ['attribute' => 'id', 'filterInputOptions' => ['class'=>''],],
            ['attribute' => 'username', 'filterInputOptions' => ['class'=>''],],
            ['attribute' => 'email', 'filterInputOptions' => ['class'=>''],],
            ['attribute' => 'status', 'class' => 'app\modules\privatepanel\components\CustomDataColumn','filter' => \app\models\UserIndentity::getStatuses(), 'value'=>function($data){
                $list = \app\models\UserIndentity::getStatuses(); 
                if(isset($list[$data->status]))
                    return $list[$data->status];
                else
                    return '';
            }, 'filterInputOptions' => ['class'=>''],],
            ['attribute' => 'last_visit', 'filterInputOptions' => ['class'=>''],],
            ['class' => 'yii\grid\ActionColumn',
			//'header'=>\Yii::t('modules/user/app', 'Actions'),
			'template'=>'{view} {update} {delete}',
			'buttons'=> [
                            'view' => function ($url, $model, $key) {
                                if(!\Yii::$app->user->can('/user/user/view', ['id' => $model->name]))
                                    return;
                                $options = array_merge([
                                    'title' => \Yii::t('modules/user/app', 'View'),
                                    'aria-label' => \Yii::t('modules/user/app', 'View'),
                                    'data-pjax' => '0',
                                    'class' => 'btn btn-info btn-sm',
                                ], []);
                                return Html::a('<i class="fa fa-eye"></i>', $url, $options);
                            },
                            'update' => function ($url, $model, $key) {
                                if(!\Yii::$app->user->can('/user/user/update', ['id' => $model->name]))
                                    return;
                                $options = array_merge([
                                    'title' => \Yii::t('modules/user/app', 'Update'),
                                    'aria-label' => \Yii::t('modules/user/app', 'Update'),
                                    'data-pjax' => '0',
                                    'class' => 'btn btn-success btn-sm',
                                ], []);
                                return Html::a('<i class="fa fa-edit"></i>', $url, $options);
                            },
                            'delete' => function ($url, $model, $key) {
                                if(!\Yii::$app->user->can('/user/user/delete', ['id' => $model->name]))
                                    return;
                                $options = array_merge([
                                    'title' => \Yii::t('modules/user/app', 'Delete'),
                                    'aria-label' => \Yii::t('modules/user/app', 'Delete'),
                                    'data-pjax' => '0',
                                    'class' => 'btn btn-danger btn-sm',
                                ], []);
                                return Html::a('<i class="fa fa-trash-o"></i>', $url, $options);
                            }
			],
                    'buttonOptions'=>[]]
        ],

)); 
	
$pagiation = $dataProvider->getPagination();

if($pagiation && $dataProvider->getTotalCount() > 0)
{
    $form = ActiveForm::begin([
        'action' => Url::current([$pagiation->pageSizeParam => null]), 'method' =>'get'
    ]);
    \yii::$app->getModule('private');
    echo Html::label(\yii::t('modules/privatepanel/app', 'Show by').': ', $pagiation->pageSizeParam).' ';
    echo Html::textInput($pagiation->pageSizeParam, $pagiation->getPageSize(), ['size' => 4, 'class' => '']).' ';
    echo Html::submitButton(\yii::t('modules/privatepanel/app', 'Set param'), ['class' => 'btn']);
    ActiveForm::end();
}