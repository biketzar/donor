<?php

use yii\widgets\DetailView;

use app\modules\user\components\AuthManagerHelper;


$this->params['breadcrumbs'] = [
    ['label' => \Yii::t('modules/user/app', 'Users'), 'url'=> ['admin']],
    ['label' => \Yii::t('modules/user/app','View') .' «'.$model->username.'»']
];

$this->params['menu'] = [
    ['label'=> \Yii::t('modules/user/app', 'Manage').': ' . \Yii::t('modules/user/app', 'Users'), 'url'=>['admin'], 'visible' => \Yii::$app->user->can('/user/user/admin'), 'template' => '<a href="{url}" class="btn btn-lilac"><i class="fa fa-users"></i> {label}</a>'],
    ['label'=> \Yii::t('modules/user/app', 'Create').': ' . \Yii::t('modules/user/app', 'User'), 'url'=>['create'], 'visible' => \Yii::$app->user->can('/user/user/create'), 'template' => '<a href="{url}" class="btn btn-success"><i class="fa fa-user-plus"></i> {label}</a>'],
    ['label'=> \Yii::t('modules/user/app', 'Update').': ' . $model->username, 'url'=>['update', 'id'=>$model->getPrimaryKey()], 'visible' => \Yii::$app->user->can('/user/user/update'), 'template' => '<a href="{url}" class="btn btn-primary"><i class="fa fa-edit"></i> {label}</a>'],
    //['label'=> \Yii::t('modules/user/app', 'Delete').': ' . $this->getRepr(), 'url'=>[['view'], ['id'=>$this->_model->getPrimaryKey()]], 'visible' => $this->checkAccess('view')],
    ['label'=> \Yii::t('modules/user/app', 'Delete').': ' . $model->username, 'url'=>['delete','id'=>$model->getPrimaryKey(), 'token'=>\Yii::$app->request->getCsrfToken()], 'linkOptions'=>['onclick'=>'confirm("'.\Yii::t('modules/user/app','Are you sure you want to delete {this_item}?', ['this_item'=>$model->username]).'");'], 'visible' => \Yii::$app->user->can('/user/user/delete'), 'template' => '<a href="{url}" class="btn btn-danger"><i class="fa fa-user-times"></i> {label}</a>'],
];

$this->title = \Yii::t('modules/user/app', 'User').' «'.$model->username.'»';
?>

<?php 
$view_roles = [];
foreach($roles_user as $r)
{
    if(isset($roles[$r]))
        $view_roles[] = (isset($roles[$r]->description)?$roles[$r]->description:$roles[$r]->name);
}
echo DetailView::widget([
    'model' => $model,
    'attributes' => 
    [
        'id',
        'username',
        'email',
        [
            'attribute' => 'status',
            'value'=>$model->statusName,
        ],
        [
            'format' => 'raw',
            'label' => \yii::t('modules/user/app', 'Roles'), // can be omitted, as it is the default
            'value' => implode('<br/>', $view_roles)
        ],
        'last_visit',
        'create_time',
        'update_time',
    ]
    ,
]);
?>
