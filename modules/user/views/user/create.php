<?php
$this->params['breadcrumbs'] = [
    ['label' => \Yii::t('modules/user/app', 'Users'), 'url'=> ['admin']],
    ['label'=> \Yii::t('modules/user/app', 'Create').': ' . \Yii::t('modules/user/app', 'User')],
];

$this->params['menu'] = [
    ['label'=> \Yii::t('modules/user/app', 'Manage').': ' . \Yii::t('modules/user/app', 'Users'), 'url'=>['admin'], 'visible' => \Yii::$app->user->can('/user/user/admin')],
];

$this->title = \Yii::t('modules/user/app', 'User');

echo $this->context->renderPartial('_form', ['model'=>$model, 'roles'=>$roles, 'roles_user'=>$roles_user, 'managers'=>$managers]); 
?>