<?php
$this->params['breadcrumbs'] = [
    ['label' => \Yii::t('modules/user/app', 'Users'), 'url'=> ['admin']],
    ['label' => \Yii::t('modules/user/app','Update') .' «'.$model->username.'»']
];

$this->params['menu'] = [
    ['label'=> \Yii::t('modules/user/app', 'Manage').': ' . \Yii::t('modules/user/app', 'Users'), 'url'=>['admin'], 'visible' => \Yii::$app->user->can('/user/user/admin'), 'template' => '<a href="{url}" class="btn btn-lilac"><i class="fa fa-users"></i> {label}</a>'],
    ['label'=> \Yii::t('modules/user/app', 'Create').': ' . \Yii::t('modules/user/app', 'User'), 'url'=>['create'], 'visible' => \Yii::$app->user->can('/user/user/create'), 'template' => '<a href="{url}" class="btn btn-success"><i class="fa fa-user-plus"></i> {label}</a>'],
    ['label'=> \Yii::t('modules/user/app', 'View').': ' . $model->username, 'url'=>['view', 'id'=>$model->getPrimaryKey()], 'visible' => \Yii::$app->user->can('/user/user/view'), 'template' => '<a href="{url}" class="btn btn-primary"><i class="fa fa-eye"></i> {label}</a>'],
    ['label'=> \Yii::t('modules/user/app', 'Delete').': ' . $model->username, 'url'=>['delete','id'=>$model->getPrimaryKey(), 'token'=>\Yii::$app->request->getCsrfToken()], 'linkOptions'=>['onclick'=>'confirm("'.\Yii::t('modules/user/app','Are you sure you want to delete {this_item}?', ['this_item'=>$model->username]).'");'], 'visible' => \Yii::$app->user->can('/user/user/delete'), 'template' => '<a href="{url}" class="btn btn-danger"><i class="fa fa-user-times"></i> {label}</a>'],
];

$this->title = \Yii::t('modules/user/app', 'User').' «'.$model->username.'»';
echo $this->context->renderPartial('_form', ['model'=>$model, 'roles'=>$roles, 'roles_user'=>$roles_user, 'managers'=>$managers]); 
?>