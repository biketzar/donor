<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\View;
use app\modules\user\components\AuthManagerHelper;

?>
<?php $form = ActiveForm::begin([
	'id'=>strtolower($model->formName()).'-form',
	'enableAjaxValidation' => $this->context->enableAjaxValidation,
	//'enableAjaxValidation' => false,
	'enableClientValidation' => false,
	'options' => array(
		'enctype' => 'multipart/form-data',
	)
]); ?>

<?php echo $form->errorSummary($model); ?>
    <table cellspacing="1" cellpadding="3" border="0" class="table table-striped table-editing">
        <thead>
            <tr>
                <th>&nbsp;</th>
                <th><?php echo \yii::t('modules/user/app', $model->isNewRecord ? 'Create' : 'Update')?></th>
            </tr>
        </thead>
    <tbody>
<?php 

echo $form->field($model, 'username', ['template' => '<td>{label}</td><td>{input}</td>',
                        'errorOptions' => ['class' => 'error-block'], 'options' => ['class' => '', 'tag' => 'tr']]);
echo $form->field($model, 'email', ['template' => '<td>{label}</td><td >{input}</td>',
			'errorOptions' => ['class' => 'error-block'], 'options' => ['class' => '', 'tag' => 'tr']]);
echo $form->field($model, 'password', ['template' => '<td>{label}</td><td >{input}</td>',
			'errorOptions' => ['class' => 'error-block'], 'options' => ['class' => '', 'tag' => 'tr']])->passwordInput(['value' => '']);
echo $form->field($model, 'password_repeat', ['template' => '<td>{label}</td><td >{input}</td>',
			'errorOptions' => ['class' => 'error-block'], 'options' => ['class' => '', 'tag' => 'tr']])->passwordInput(['value' => '']);
echo $form->field($model, 'name', ['template' => '<td>{label}</td><td >{input}</td>',
			'errorOptions' => ['class' => 'error-block'], 'options' => ['class' => '', 'tag' => 'tr']]);
echo $form->field($model, 'status', ['template' => '<td>{label}</td><td >{input}</td>',
			'inputOptions' => ['class' => ''], 'errorOptions' => ['class' => 'error-block']])->dropDownList(\app\models\UserIndentity::getStatuses());


$new_roles = [];
foreach($roles as $item)
{
    $new_roles[$item->name] = ($item->description)?$item->description:$item->name;
}

$itemOptions = ['separator' =>'<br/>'];

echo '<tr><th>';
echo Html::label(\Yii::t('modules/user/app', 'Roles'), $model->formName().'[roles]');
echo '</th><td >';
echo Html::checkboxList($model->formName().'[roles]', $roles_user, $new_roles, $itemOptions);
echo '</td></tr>';

?>

</tbody>      
    <tfoot>
        <tr>
            <td>&nbsp;</td>
            <td>
        <?= Html::submitButton('<i class="fa fa-floppy-o"></i> '.\Yii::t('modules/user/app', $model->isNewRecord ? 'Create' : 'Save'), ['class' => 'btn btn-success', 'name'=>'save']) ?>&nbsp;
        <?= Html::submitButton('<i class="fa fa-check-circle-o"></i> '.\Yii::t('modules/user/app', 'Save & continue edit'), ['class' => 'btn btn-primary', 'onclick'=>'jQuery(this).closest("form").find("[name=\"savetype\"]").val("apply")', 'name'=>'apply']) ?>&nbsp;
        <?php echo Html::a('<i class="fa fa-times-circle-o"></i> '.\Yii::t('modules/user/app', 'Cancel'), ['admin'], ['class' => 'btn btn-gray']); ?>
            </td>
        </tr>
    </tfoot>
</table>


<?php ActiveForm::end() ?>
