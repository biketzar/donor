<?php
$this->params['breadcrumbs'] = [
    ['label' => \Yii::t('modules/user/app', 'Permissions'), 'url'=> ['admin']],
    ['label' => \Yii::t('modules/user/app','Update') .' «'.$this->context->getViewName($model).'»']
];

$this->params['menu'] = [
    ['label'=> \Yii::t('modules/user/app', 'Manage').': ' . \Yii::t('modules/user/app', 'Permissions'), 'url'=>['admin'], 'visible' => \Yii::$app->user->can('/user/permission/admin'), 'template' => '<a href="{url}" class="btn btn-lilac"><i class="fa fa-bars"></i> {label}</a>'],
    ['label'=> \Yii::t('modules/user/app', 'Create').': ' . \Yii::t('modules/user/app', 'Permission'), 'url'=>['create'], 'visible' => \Yii::$app->user->can('/user/permission/create'), 'template' => '<a href="{url}" class="btn btn-success"><i class="fa fa-plus"></i> {label}</a>'],
    ['label'=> \Yii::t('modules/user/app', 'View').': ' . $this->context->getViewName($model), 'url'=>['view', 'id'=>$model->name], 'visible' => \Yii::$app->user->can('/user/permission/view', ['id' => $model->name]), 'template' => '<a href="{url}" class="btn btn-primary"><i class="fa fa-eye"></i> {label}</a>'],
    //['label'=> \Yii::t('modules/user/app', 'Access levels').': ' . $this->context->getViewName($model), 'url'=>['access-levels', 'id'=>$model->name], 'visible' => \Yii::$app->user->can('/user/permission/access-levels', ['id' => $model->name])],
    ['label'=> \Yii::t('modules/user/app', 'Delete').': ' . $this->context->getViewName($model), 'url'=>['delete','id'=>$model->name, 'token'=>\Yii::$app->request->getCsrfToken()], 'linkOptions'=>['onclick'=>'confirm("'.\Yii::t('modules/user/app','Are you sure you want to delete {this_item}?', ['this_item'=>$this->context->getViewName($model)]).'");'], 'visible' => \Yii::$app->user->can('/user/permission/delete', ['id' => $model->name]), 'template' => '<a href="{url}" class="btn btn-danger"><i class="fa fa-remove"></i> {label}</a>'],
];

$this->title = \Yii::t('modules/user/app', 'Permission').' «'.$this->context->getViewName($model).'»';

echo $this->context->renderPartial('_form', ['model'=>$model, 'itemid' => $itemid, 'permissions' => $permissions]); 
?>