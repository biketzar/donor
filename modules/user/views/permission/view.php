<?php

use yii\widgets\DetailView;

use app\modules\user\components\AuthManagerHelper;

use app\modules\user\Module;

$this->params['breadcrumbs'] = [
    ['label' => \Yii::t('modules/user/app', 'Permissions'), 'url'=> ['admin']],
    ['label' => \Yii::t('modules/user/app','View') .' «'.$model->name.'»']
];

$this->params['menu'] = [
    ['label'=> \Yii::t('modules/user/app', 'Manage').': ' . \Yii::t('modules/user/app', 'Permissions'), 'url'=>['admin'], 'visible' => \Yii::$app->user->can('/user/permission/admin'), 'template' => '<a href="{url}" class="btn btn-lilac"><i class="fa fa-bars"></i> {label}</a>'],
    ['label'=> \Yii::t('modules/user/app', 'Create').': ' . \Yii::t('modules/user/app', 'Permission'), 'url'=>['create'], 'visible' => \Yii::$app->user->can('/user/permission/create'), 'template' => '<a href="{url}" class="btn btn-success"><i class="fa fa-plus"></i> {label}</a>'],
    ['label'=> \Yii::t('modules/user/app', 'Update').': ' .$this->context->getViewName($model), 'url'=>['update', 'id'=>$model->name], 'visible' => \Yii::$app->user->can('/user/permission/update', ['id' => $model->name]), 'template' => '<a href="{url}" class="btn btn-primary"><i class="fa fa-edit"></i> {label}</a>'],
    //['label'=> \Yii::t('modules/user/app', 'Delete').': ' . $this->getRepr(), 'url'=>[['view'], ['id'=>$this->_model->getPrimaryKey()]], 'visible' => $this->checkAccess('view')],
    ['label'=> \Yii::t('modules/user/app', 'Delete').': ' . $this->context->getViewName($model), 'url'=>['delete','id'=>$model->name, 'token'=>\Yii::$app->request->getCsrfToken()], 'linkOptions'=>['onclick'=>'confirm("'.\Yii::t('modules/user/app','Are you sure you want to delete {this_item}?', ['this_item'=>$this->context->getViewName($model)]).'");'], 'visible' => \Yii::$app->user->can('/user/permission/delete', ['id' => $model->name]), 'template' => '<a href="{url}" class="btn btn-danger"><i class="fa fa-remove"></i> {label}</a>'],
];

$this->title = \Yii::t('modules/user/app', 'Permission').' «'.$this->context->getViewName($model).'»';
?>

<?php 
$children_lines = [];

foreach($children['path'] as $path)
{
    $children_lines[] = implode(' -> ', $path);
}
$parents_lines = [];
foreach($parents['path'] as $path)
{
    $parents_lines[] = implode(' -> ', $path);
}
echo DetailView::widget([
    'model' => $model,
    'attributes' => 
    [
        ['attribute' => 'name', 'label' => Module::t('app', 'Name')],
        ['attribute' => 'description', 'label' => Module::t('app', 'Description')],
        ['attribute' => 'ruleName', 'label' => Module::t('app', 'Rule Name')],
        [
            'format' => 'raw',
            'label' => \Yii::t('modules/user/app', 'Children'), // can be omitted, as it is the default
            'value' => implode('<br/>', $children_lines),
        ],
        [
            'format' => 'raw',
            'label' => \Yii::t('modules/user/app', 'Parent'), // can be omitted, as it is the default
            'value' => implode('<br/>', $parents_lines),
        ],
    ]
    ,
]);
?>
