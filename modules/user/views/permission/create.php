<?php
$this->params['breadcrumbs'] = [
    ['label' => \Yii::t('modules/user/app', 'Permissions'), 'url'=> ['admin']],
    ['label'=> \Yii::t('modules/user/app', 'Create').': ' . \Yii::t('modules/user/app', 'Permission')],
];

$this->params['menu'] = [
    ['label'=> \Yii::t('modules/user/app', 'Manage').': ' . \Yii::t('modules/user/app', 'Permissions'), 'url'=>['admin'], 'visible' => \Yii::$app->user->can('/user/permission/admin')],
];

$this->title = \Yii::t('modules/user/app', 'Permission');

echo $this->context->renderPartial('_form', ['model'=>$model, 'itemid' => $itemid, 'permissions' => $permissions]); 
?>