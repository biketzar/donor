<?php
$this->params['breadcrumbs'] = [
    ['label' => \Yii::t('modules/user/app', 'Access levels'), 'url'=> ['admin']],
    ['label' => \Yii::t('modules/user/app','Update') .' «'.$this->context->getViewName($model).'»']
];

$this->params['menu'] = [
    ['label'=> \Yii::t('modules/user/app', 'Manage').': ' . \Yii::t('modules/user/app', 'Access levels'), 'url'=>['admin'], 'visible' => \Yii::$app->user->can('/user/access-level/admin')],
    ['label'=> \Yii::t('modules/user/app', 'Create').': ' . \Yii::t('modules/user/app', 'Access level'), 'url'=>['create'], 'visible' => \Yii::$app->user->can('/user/access-level/create')],
    ['label'=> \Yii::t('modules/user/app', 'View').': ' . $this->context->getViewName($model), 'url'=>['view', 'id'=>$model->name], 'visible' => \Yii::$app->user->can('/user/access-level/view')],
    ['label'=> \Yii::t('modules/user/app', 'Permissions').': ' . $this->context->getViewName($model), 'url'=>['permissions', 'id'=>$model->name], 'visible' => \Yii::$app->user->can('/user/access-level/permissions')],
    ['label'=> \Yii::t('modules/user/app', 'Delete').': ' . $this->context->getViewName($model), 'url'=>['delete','id'=>$model->name, 'token'=>\Yii::$app->request->getCsrfToken()], 'linkOptions'=>['onclick'=>'confirm("'.\Yii::t('modules/user/app','Are you sure you want to delete {this_item}?', ['this_item'=>$this->context->getViewName($model)]).'");'], 'visible' => \Yii::$app->user->can('/user/access-level/delete')],
];

$this->title = \Yii::t('modules/user/app', 'Access level').' «'.$this->context->getViewName($model).'»';

echo $this->context->renderPartial('_form', ['model'=>$model, 'itemid' => $itemid]); 
?>