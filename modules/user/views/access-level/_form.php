<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\View;
use app\modules\user\components\AuthManagerHelper;

?>
<?php $form = ActiveForm::begin([
	'id'=>strtolower($model->formName()).'-form',
	'enableAjaxValidation' => $this->context->enableAjaxValidation,
	
	'enableClientValidation' => true,
	/*'clientOptions' => array(
		'validateOnSubmit' => false,
		'validateOnChange' => true,
	),*/
	'options' => array(
		'enctype' => 'multipart/form-data',
	)
]); ?>

<?php echo $form->errorSummary($model); ?>

<?php 
echo $form->field($model, 'name');
echo $form->field($model, 'description');

?>

<?= Html::submitButton(\Yii::t('modules/user/app','Save'), ['class' => 'btn btn-primary', 'name'=>'save']) ?>
<?= Html::submitButton(\Yii::t('modules/user/app', 'Save & continue edit'), ['class' => 'btn', 'onclick'=>'jQuery(this).closest("form").find("[name=\"savetype\"]").val("apply")', 'name'=>'apply']) ?>
<?php echo Html::a(\Yii::t('modules/user/app', 'Cancel'), array('admin')) ?>
<?php echo Html::hiddenInput('savetype', 'save');?>

<?php /*$this->registerJs(<<<JS
var submited = false;
$("#{$form->id}").bind('submit', function(){
if (!submited){submited = true;}else{return false;}
});
JS
,
View::POS_END, 
'create-from'
)*/?>

<?php ActiveForm::end() ?>
