<?php
$this->params['breadcrumbs'] = [
    ['label' => \Yii::t('modules/user/app', 'Access levels'), 'url'=> ['admin']],
    ['label' => \Yii::t('modules/user/app','Create Access level')]
];

$this->params['menu'] = [
    ['label'=> \Yii::t('modules/user/app', 'Manage').': ' . \Yii::t('modules/user/app', 'Access levels'), 'url'=>['admin'], 'visible' => \Yii::$app->user->can('/user/access-level/admin')],
];

$this->title = \Yii::t('modules/user/app', 'Access level');

echo $this->context->renderPartial('_form', ['model'=>$model, 'itemid' => $itemid]); 
?>