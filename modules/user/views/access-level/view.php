<?php

use yii\widgets\DetailView;

use app\modules\user\components\AuthManagerHelper;

$this->params['breadcrumbs'] = [
    ['label' => \Yii::t('modules/user/app', 'Access levels'), 'url'=> ['admin']],
    ['label' => \Yii::t('modules/user/app','View') .' «'.$this->context->getViewName($model).'»']
];

$this->params['menu'] = [
    ['label'=> \Yii::t('modules/user/app', 'Manage').': ' . \Yii::t('modules/user/app', 'Access levels'), 'url'=>['admin'], 'visible' => \Yii::$app->user->can('/user/access-level/admin')],
    ['label'=> \Yii::t('modules/user/app', 'Create').': ' . \Yii::t('modules/user/app', 'Access level'), 'url'=>['create'], 'visible' => \Yii::$app->user->can('/user/access-level/create')],
    ['label'=> \Yii::t('modules/user/app', 'Update').': ' . $this->context->getViewName($model), 'url'=>['update', 'id'=>$model->name], 'visible' => \Yii::$app->user->can('/user/access-level/update')],
    ['label'=> \Yii::t('modules/user/app', 'Permissions').': ' . $this->context->getViewName($model), 'url'=>['permissions', 'id'=>$model->name], 'visible' => \Yii::$app->user->can('/user/access-level/permissions')],
    ['label'=> \Yii::t('modules/user/app', 'Delete').': ' . $this->context->getViewName($model), 'url'=>['delete','id'=>$model->name, 'token'=>\Yii::$app->request->getCsrfToken()], 'linkOptions'=>['onclick'=>'confirm("'.\Yii::t('modules/user/app','Are you sure you want to delete {this_item}?', ['this_item'=>$this->context->getViewName($model)]).'");'], 'visible' => \Yii::$app->user->can('/user/access-level/delete')],
];

$this->title = \Yii::t('modules/user/app', 'Access level').' «'.$this->context->getViewName($model).'»';
?>

<?php 
$permissions = AuthManagerHelper::getPermissionsByAccessLevelDirect($model->name); 
$permission_lines = array();
foreach($permissions as $p)
{
    $permission_lines[] = $p->description?$p->description:$p->name;
}
echo DetailView::widget([
    'model' => $model,
    'attributes' => 
    [
        'name',
        'description',
        [
            'format' => 'raw',
            'label' => \Yii::t('modules/user/app', 'Permissions'),
            'value' => implode('<br/>', $permission_lines)
        ]
    ]
    ,
]);
?>
