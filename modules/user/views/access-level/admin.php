<?php

use yii\grid\GridView;
use yii\helpers\Html;

$this->params['breadcrumbs'] = [
    ['label' => \Yii::t('modules/user/app', 'Access levels'), 'url'=> ['admin']],
    \Yii::t('modules/user/app', 'All')
    
];
$this->params['menu'] = [
			['label'=> \Yii::t('modules/user/app', 'Create').': ' . \Yii::t('modules/user/app', 'Access level'), 'url'=>['create'], 'visible' => \Yii::$app->user->can('/user/access-level/create'), 'template' => '<a href="{url}" class="btn btn-primary"><i class="fa fa-plus"></i> {label}</a>'],
		];;

$this->title = \Yii::t('modules/user/app', 'Access levels');

/*
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('".strtolower( get_class($model))."-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
*/

echo GridView::widget(array(
        'id'=>'settings-grid',
        'dataProvider'=>$dataProvider,
        'layout' => "{items}\n<div class=\"middle-block\">{summary}</div>\n<div class=\"middle-block text-right\">{pager}</div>",
        'columns'=> [
            ['attribute' => 'name'],
            ['attribute' => 'description'],
            ['class' => 'yii\grid\ActionColumn',
			//'header'=>\Yii::t('modules/user/app', 'Actions'),
			'template'=>'{permissions} {view} {update} {delete}',
			'buttons'=> [
                            'permissions' => function($url, $model, $key) {
                                $options = array_merge([
                                    'title' => \Yii::t('modules/user/app', 'Permissions'),
                                    'aria-label' => \Yii::t('modules/user/app', 'Permissions'),
                                    'data-pjax' => '0',
                                ], []);
                                return Html::a('<span class="glyphicon glyphicon-cog"></span>', $url, $options);
                            }
			],
                    'buttonOptions'=>[]]
        ],

)); 
	
 