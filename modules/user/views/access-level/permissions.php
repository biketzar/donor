<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

use app\modules\user\components\AuthManagerHelper;
global $permissionMapTemp;
$permissionMapTemp = $permissionMap;
global $modelTemp;
$modelTemp = $model;

$this->params['breadcrumbs'] = [
    ['label' => \Yii::t('modules/user/app', 'Access levels'), 'url'=> ['admin']],
    ['label' => \Yii::t('modules/user/app','Permissions') .' «'.$this->context->getViewName($model).'»']
];

$this->params['menu'] = [
    ['label'=> \Yii::t('modules/user/app', 'Manage').': ' . \Yii::t('modules/user/app', 'Access levels'), 'url'=>['admin'], 'visible' => \Yii::$app->user->can('/user/access-level/admin')],
    ['label'=> \Yii::t('modules/user/app', 'View').': ' . $this->context->getViewName($model), 'url'=>['view', 'id'=>$model->name], 'visible' => \Yii::$app->user->can('/user/access-level/view')],
    ['label'=> \Yii::t('modules/user/app', 'Update').': ' . $this->context->getViewName($model), 'url'=>['update', 'id'=>$model->name], 'visible' => \Yii::$app->user->can('/user/access-level/update')],
    ['label'=> \Yii::t('modules/user/app', 'Delete').': ' . $this->context->getViewName($model), 'url'=>['delete','id'=>$model->name, 'token'=>\Yii::$app->request->getCsrfToken()], 'linkOptions'=>['onclick'=>'confirm("'.\Yii::t('modules/user/app','Are you sure you want to delete {this_item}?', ['this_item'=>$this->context->getViewName($model)]).'");'], 'visible' => \Yii::$app->user->can('/user/access-level/delete')],
];
$this->title = \Yii::t('modules/user/app', 'Permissions').' «'.$this->context->getViewName($model).'»';
?>
<?php \yii\widgets\Pjax::begin(); ?>
<?php
echo GridView::widget(array(
        'id'=>'settings-grid',
        'dataProvider'=>$dataProvider,
        'columns'=> [
            [
                'format' => 'raw',
                'class' => 'app\modules\privatepanel\components\CustomDataColumn',
                'label' => \Yii::t('modules/user/app', 'Name'),
                'value' => function ($value){
                    global $permissionMapTemp;
                    global $modelTemp;
                    $name = $this->context->getViewName($value);
                    $info = '';
                    if(isset($permissionMapTemp[$value->name]))
                    {
                        if(isset($permissionMapTemp[$value->name][$modelTemp->name]))
                        {
                            
                        } else {
                            $parents = [];
                            foreach($permissionMapTemp[$value->name] as $p)
                                $parents[] = $this->context->getViewName($p);
                            $info .= ' '.Html::tag('span', '('.\Yii::t('modules/user/app','inherited').')', ['title' => implode(', ', $parents), 'class' => 'not-set']);
                        }
                    } else {
                        
                    }
                    return $name.$info;
                }
            ],
            [
                'format' => 'raw',
                'class' => 'app\modules\privatepanel\components\CustomDataColumn',
                'label' => \Yii::t('modules/user/app', 'Action'),
                'value' => function ($value){
                    global $permissionMapTemp;
                    global $modelTemp;
                    ob_start();
                    echo Html::beginForm('', 'post', ['id' => strtolower('Permission-'.$value->name).'-form' , 'class' => 'permission-form']);
                    if(isset($permissionMapTemp[$value->name]))
                    {
                        if(isset($permissionMapTemp[$value->name][$modelTemp->name]))
                        {
                            echo Html::hiddenInput('delete', '');
                            echo Html::submitButton(\Yii::t('modules/user/app','Delete link'), ['class' => 'btn', 'name'=>'deleteButton']);
                        } else {
                            echo Html::hiddenInput('add', '');
                            echo Html::submitButton(\Yii::t('modules/user/app','Add strict link'), ['class' => 'btn', 'name'=>'addButton']);
                        }
                    } else {
                        echo Html::hiddenInput('add', '');
                        echo Html::submitButton(\Yii::t('modules/user/app','Add link'), ['class' => 'btn', 'name'=>'addButton']);
                    }
                    echo Html::hiddenInput('permission', $value->name);
                    echo Html::endForm();
                    $text = ob_get_contents();
                    ob_end_clean();
                    return $text;
                }
            ],
        ],

)); 
\yii\widgets\Pjax::end();        
$jsText = <<<JS
        jQuery(document).on("submit", '.permission-form', function (e) {
            e.preventDefault();
            var form = $(this);
            $.ajax({
                url: form.attr('action'),
                type: form.attr('method'),
                data: form.serialize(),
                success: function (result) {
                    //console.log(result);
                    jQuery.pjax.reload({container:'#settings-grid'});
                }
            });
        });
JS;
$this->registerJs($jsText, \yii\web\View::POS_READY);
?>
