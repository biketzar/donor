<?php
$this->params['breadcrumbs'] = [
    ['label' => \Yii::t('modules/user/app', 'Roles'), 'url'=> ['admin']],
    ['label'=> \Yii::t('modules/user/app', 'Create').': ' . \Yii::t('modules/user/app', 'Role')],
];

$this->params['menu'] = [
    ['label'=> \Yii::t('modules/user/app', 'Manage').': ' . \Yii::t('modules/user/app', 'Roles'), 'url'=>['admin'], 'visible' => \Yii::$app->user->can('/user/user/admin')],
];

$this->title = \Yii::t('modules/user/app', 'Role');

echo $this->context->renderPartial('_form', ['model'=>$model, 'itemid' => $itemid, 'roles' => $roles]); 
?>