<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

use app\modules\user\components\AuthManagerHelper;
global $accessLevelMapTemp;
$accessLevelMapTemp = $accessLevelMap;
global $modelTemp;
$modelTemp = $model;

$this->params['breadcrumbs'] = [
    ['label' => \Yii::t('modules/user/app', 'Roles'), 'url'=> ['admin']],
    ['label' => \Yii::t('modules/user/app','Access levels') .' «'.$this->context->getViewName($model).'»']
];

$this->params['menu'] = [
    ['label'=> \Yii::t('modules/user/app', 'Manage').': ' . \Yii::t('modules/user/app', 'Roles'), 'url'=>['admin'], 'visible' => \Yii::$app->user->can('/user/role/admin')],
    ['label'=> \Yii::t('modules/user/app', 'View').': ' . $this->context->getViewName($model), 'url'=>['view', 'id'=>$model->name], 'visible' => \Yii::$app->user->can('/user/role/view', ['id' => $model->name])],
    ['label'=> \Yii::t('modules/user/app', 'Update').': ' . $this->context->getViewName($model), 'url'=>['update', 'id'=>$model->name], 'visible' => \Yii::$app->user->can('/user/role/update', ['id' => $model->name])],
    ['label'=> \Yii::t('modules/user/app', 'Delete').': ' . $this->context->getViewName($model), 'url'=>['delete','id'=>$model->name, 'token'=>\Yii::$app->request->getCsrfToken()], 'linkOptions'=>['onclick'=>'confirm("'.\Yii::t('modules/user/app','Are you sure you want to delete {this_item}?', ['this_item'=>$this->context->getViewName($model)]).'");'], 'visible' => \Yii::$app->user->can('/user/role/delete', ['id' => $model->name])],
];
$this->title = \Yii::t('modules/user/app', 'Access levels').' «'.$this->context->getViewName($model).'»';
?>
<?php
\yii\widgets\Pjax::begin(); 
echo GridView::widget(array(
        'id'=>'settings-grid',
        'dataProvider'=>$dataProvider,
        'columns'=> [
            [
                'format' => 'raw',
                'class' => 'yii\grid\DataColumn',
                'label' => \Yii::t('modules/user/app', 'Name'),
                'value' => function ($value){
                    global $accessLevelMapTemp;
                    global $modelTemp;
                    $name = $this->context->getViewName($value);
                    $info = '';
                    if(isset($accessLevelMapTemp[$value->name]))
                    {
                        if(isset($accessLevelMapTemp[$value->name][$modelTemp->name]))
                        {
                            
                        } else {
                            $parents = [];
                            foreach($accessLevelMapTemp[$value->name] as $p)
                                $parents[] = $this->context->getViewName($p);
                            $info .= ' '.Html::tag('span', '('.\Yii::t('modules/user/app','inherited').')', ['title' => implode(', ', $parents), 'class' => 'not-set']);
                        }
                    } else {
                        
                    }
                    return $name.$info;
                }
            ],
            [
                'format' => 'raw',
                'class' => 'yii\grid\DataColumn',
                'label' => \Yii::t('modules/user/app', 'Action'),
                'value' => function ($value){
                    global $accessLevelMapTemp;
                    global $modelTemp;
                    ob_start();
                    echo Html::beginForm('', 'post', ['id' => strtolower('AccessLevel-'.$value->name).'-form', 'class'=>'accesslevel-form']);
                    if(isset($accessLevelMapTemp[$value->name]))
                    {
                        if(isset($accessLevelMapTemp[$value->name][$modelTemp->name]))
                        {
                            echo Html::hiddenInput('delete', '');
                            echo Html::submitButton(\Yii::t('modules/user/app','Delete link'), ['class' => 'btn', 'name'=>'deleteButton']);
                        } else {
                            echo Html::hiddenInput('add', '');
                            echo Html::submitButton(\Yii::t('modules/user/app','Add strict link'), ['class' => 'btn', 'name'=>'addButton']);
                        }
                    } else {
                        echo Html::hiddenInput('add', '');
                        echo Html::submitButton(\Yii::t('modules/user/app','Add link'), ['class' => 'btn', 'name'=>'addButton']);
                    }
                    echo Html::hiddenInput('access-level', $value->name);
                    echo Html::endForm();
                    $text = ob_get_contents();
                    ob_end_clean();
                    return $text;
                }
            ],
        ],

)); 

\yii\widgets\Pjax::end();        
$jsText = <<<JS
        jQuery(document).on("submit", '.accesslevel-form', function (e) {
            e.preventDefault();
            var form = $(this);
            $.ajax({
                url: form.attr('action'),
                type: form.attr('method'),
                data: form.serialize(),
                success: function (result) {
                    //console.log(result);
                    jQuery.pjax.reload({container:'#settings-grid'});
                }
            });
        });
JS;
$this->registerJs($jsText, \yii\web\View::POS_READY);