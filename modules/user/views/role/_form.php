<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\View;
use app\modules\user\components\AuthManagerHelper;

?>
<?php $form = ActiveForm::begin([
	'id'=>strtolower($model->formName()).'-form',
	'enableAjaxValidation' => $this->context->enableAjaxValidation,
	
	'enableClientValidation' => true,
	/*'clientOptions' => array(
		'validateOnSubmit' => false,
		'validateOnChange' => true,
	),*/
	'options' => array(
		'enctype' => 'multipart/form-data',
	)
]); ?>

<?php echo $form->errorSummary($model); ?>
    <table cellspacing="1" cellpadding="3" border="0" class="table table-striped table-editing">
        <thead>
            <tr>
                <th>&nbsp;</th>
                <th><?php echo \yii::t('modules/user/app', !$model->name ? 'Create' : 'Update')?></th>
            </tr>
        </thead>
    <tbody>
<?php 
echo $form->field($model, 'name', ['template' => '<td>{label}</td><td class="dark">{input}{error}</td>',
			'errorOptions' => ['class' => 'error-block'], 'options' => ['class' => '', 'tag' => 'tr']]);
echo $form->field($model, 'description', ['template' => '<td>{label}</td><td class="light">{input}{error}</td>',
			'errorOptions' => ['class' => 'error-block'], 'options' => ['class' => '', 'tag' => 'tr']]);

$new_roles = [];
foreach($roles as $item)
{
    $new_roles[$item->name] = ($item->description)?$item->description:$item->name;
}
echo '<tr><td>';
echo Html::label(\Yii::t('modules/user/app', 'Children'), $model->formName().'[children]');
echo '</td><td>';
echo Html::checkboxList($model->formName().'[children]', $model->children, $new_roles, ['separator' =>'<br/>', 'unselect'=>'']);
echo '</td></tr>';

echo '<tr><td>';
echo Html::label(\Yii::t('modules/user/app', 'Parents'), $model->formName().'[parents]');
echo '</td><td>';
echo Html::checkboxList($model->formName().'[parents]', $model->parents, $new_roles, ['separator' =>'<br/>', 'unselect'=>'']);
echo '</td></tr>';


/*$access_levels = AuthManagerHelper::getAccessLevels(); 

echo Html::label(\Yii::t('modules/user/app', 'Access levels'), $model->formName().'[accessLevels]');
echo Html::checkboxList($model->formName().'[accessLevels]', $model->accessLevels, $access_levels, ['separator' =>'<br/>', 'unselect'=>'']);
 * 
 */
?>

</tbody>      
    <tfoot>
        <tr>
            <td>&nbsp;</td>
            <td>
        <?= Html::submitButton('<i class="fa fa-floppy-o"></i> '.\Yii::t('modules/user/app', !$model->name ? 'Create' : 'Save'), ['class' => 'btn btn-success', 'name'=>'save']) ?>&nbsp;
        <?= Html::submitButton('<i class="fa fa-check-circle-o"></i> '.\Yii::t('modules/user/app', 'Save & continue edit'), ['class' => 'btn btn-primary', 'onclick'=>'jQuery(this).closest("form").find("[name=\"savetype\"]").val("apply")', 'name'=>'apply']) ?>&nbsp;
        <?php echo Html::a('<i class="fa fa-times-circle-o"></i> '.\Yii::t('modules/user/app', 'Cancel'), ['admin'], ['class' => 'btn btn-gray']); ?>
            </td>
        </tr>
    </tfoot>
</table>

<?php ActiveForm::end() ?>
