<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

use app\modules\user\Module;

$this->params['breadcrumbs'] = [
    ['label' => \Yii::t('modules/user/app', 'Roles'), 'url'=> ['admin']],
    \Yii::t('modules/user/app', 'All')
    
];
$this->params['menu'] = [
        ['label'=> \Yii::t('modules/user/app', 'Create').': ' . \Yii::t('modules/user/app', 'Role'), 'url'=>['create'], 'visible' => \Yii::$app->user->can('/user/role/create'), 'template' => '<a href="{url}" class="btn btn-primary"><i class="fa fa-plus"></i> {label}</a>'],
];

$this->title = \Yii::t('modules/user/app', 'Roles');

/*
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('".strtolower( get_class($model))."-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
*/

echo GridView::widget(array(
        'id'=>'settings-grid',
        'dataProvider'=>$dataProvider,
        'layout' => "{items}\n<div class=\"middle-block\">{summary}</div>\n<div class=\"middle-block text-right\">{pager}</div>",
        'tableOptions' => ['class' => 'table table-striped table-hover'],
        'columns'=> [
            ['attribute' => 'name', 'label' => Module::t('app', 'Name'), 'filterInputOptions' => ['class'=>''],],
            ['attribute' => 'description', 'label' => Module::t('app', 'Description'), 'filterInputOptions' => ['class'=>''],],
            ['class' => 'yii\grid\ActionColumn',
			//'header'=>\Yii::t('modules/user/app', 'Actions'),
			'template'=>'{permissions} {view} {update} {delete}',
			'buttons'=> [
                            'permissions' => function($url, $model, $key) {
                                if(!\Yii::$app->user->can('/user/role/permissions', ['id' => $model->name]))
                                    return;
                                $options = array_merge([
                                    'title' => \Yii::t('modules/user/app', 'Permissions'),
                                    'aria-label' => \Yii::t('modules/user/app', 'Permissions'),
                                    'data-pjax' => '0',
                                    'class' => 'btn btn-warning btn-sm',
                                ], []);
                                return Html::a('<i class="fa fa-cogs"></i>', $url, $options);
                            },
                            'view' => function ($url, $model, $key) {
                                if(!\Yii::$app->user->can('/user/role/view', ['id' => $model->name]))
                                    return;
                                $options = array_merge([
                                    'title' => \Yii::t('modules/user/app', 'View'),
                                    'aria-label' => \Yii::t('modules/user/app', 'View'),
                                    'data-pjax' => '0',
                                    'class' => 'btn btn-info btn-sm',
                                ], []);
                                return Html::a('<i class="fa fa-eye"></i>', $url, $options);
                            },
                            'update' => function ($url, $model, $key) {
                                if(!\Yii::$app->user->can('/user/role/update', ['id' => $model->name]))
                                    return;
                                $options = array_merge([
                                    'title' => \Yii::t('modules/user/app', 'Update'),
                                    'aria-label' => \Yii::t('modules/user/app', 'Update'),
                                    'data-pjax' => '0',
                                    'class' => 'btn btn-success btn-sm',
                                ], []);
                                return Html::a('<i class="fa fa-edit"></i>', $url, $options);
                            },
                            'delete' => function ($url, $model, $key) {
                                if(!\Yii::$app->user->can('/user/role/delete', ['id' => $model->name]))
                                    return;
                                $options = array_merge([
                                    'title' => \Yii::t('modules/user/app', 'Delete'),
                                    'aria-label' => \Yii::t('modules/user/app', 'Delete'),
                                    'data-pjax' => '0',
                                    'class' => 'btn btn-danger btn-sm',
                                ], []);
                                return Html::a('<i class="fa fa-trash-o"></i>', $url, $options);
                            }
			],
                    'buttonOptions'=>[

                    ]]
        ],

)); 
	
$pagiation = $dataProvider->getPagination();

if($pagiation && $dataProvider->getTotalCount() > 0)
{
    $form = ActiveForm::begin([
        'action' => Url::current([$pagiation->pageSizeParam => null]), 'method' =>'get'
    ]);
    \yii::$app->getModule('private');
    echo Html::label(\yii::t('modules/privatepanel/app', 'Show by').': ', $pagiation->pageSizeParam).' ';
    echo Html::textInput($pagiation->pageSizeParam, $pagiation->getPageSize(), ['size' => 4, 'class' => '']).' ';
    echo Html::submitButton(\yii::t('modules/privatepanel/app', 'Set param'), ['class' => 'btn']);
    ActiveForm::end();
}