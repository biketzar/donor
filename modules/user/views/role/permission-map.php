<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

use app\modules\user\components\AuthManagerHelper;

global $rolePermissionMapTemp;

$this->params['breadcrumbs'] = [
    ['label' => \Yii::t('modules/user/app', 'Roles'), 'url'=> ['admin']],
    ['label' => \Yii::t('modules/user/app','Role permission map')]
];

$this->params['menu'] = [
    ['label'=> \Yii::t('modules/user/app', 'Manage').': ' . \Yii::t('modules/user/app', 'Roles'), 'url'=>['admin'], 'visible' => \Yii::$app->user->can('/user/role/permission-map')],
];
$this->title = \Yii::t('modules/user/app', 'Role permission map');
?>
<?php
$columns = [];
$columns[] = [
    'format' => 'raw',
    'class' => 'app\modules\privatepanel\components\CustomDataColumn',
    'label' => \Yii::t('modules/user/app', 'Name'),
    'value' => function ($value){
        return $this->context->getViewName($value);
    }
];

foreach($roles as $role)
{
    $rolePermissionMapTemp[$role->name] = array();
    foreach($dataProvider->getModels() as $value)
    {
        $name = '';
        $info = '';
        if(isset($rolePermissionMap[$role->name][$value->name]))
        {
            $name = Html::tag('i', '', ['class'=>'fa fa-check text-success']);
            if(isset($rolePermissionMap[$role->name][$value->name][$role->name]))
            {

            } else {
                $parents = [];
                foreach($rolePermissionMap[$role->name][$value->name] as $p)
                    $parents[] = $this->context->getViewName($p);
                //$info .= ' '.Html::tag('span', implode(', ', $parents), ['class' => 'not-set small']);
            }
        } else {
           $name = Html::tag('i', '', ['class'=>'fa fa-minus text-danger']);
        }
        $rolePermissionMapTemp[$role->name][$value->name] = $name.$info;
    }
    
    $columns[] = [
        'format' => 'raw',
        'class' => 'app\modules\privatepanel\components\CustomDataColumn',
        'label' => $this->context->getViewName($role),
        //'id' => $role->name,
        'value' => function ($value, $key, $index, $column){
            global $rolePermissionMapTemp;
            return $rolePermissionMapTemp[$column->options['role']][$value->name];
        },
        'options' => ['role' => $role->name]
    ];
}
echo GridView::widget(array(
        'id'=>'settings-grid',
        'dataProvider'=>$dataProvider,
        'columns'=> $columns,
        'layout' => "{items}\n<div class=\"middle-block\">{summary}</div>\n<div class=\"middle-block text-right\">{pager}</div>",
)); 
