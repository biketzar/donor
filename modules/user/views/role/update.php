<?php
$this->params['breadcrumbs'] = [
    ['label' => \Yii::t('modules/user/app', 'Roles'), 'url'=> ['admin']],
    ['label' => \Yii::t('modules/user/app','Update') .' «'.$this->context->getViewName($model).'»']
];

$this->params['menu'] = [
    ['label'=> \Yii::t('modules/user/app', 'Manage').': ' . \Yii::t('modules/user/app', 'Roles'), 'url'=>['admin'], 'visible' => \Yii::$app->user->can('/user/role/admin'), 'template' => '<a href="{url}" class="btn btn-lilac"><i class="fa fa-bars"></i> {label}</a>'],
    ['label'=> \Yii::t('modules/user/app', 'Create').': ' . \Yii::t('modules/user/app', 'Role'), 'url'=>['create'], 'visible' => \Yii::$app->user->can('/user/role/create'), 'template' => '<a href="{url}" class="btn btn-success"><i class="fa fa-plus"></i> {label}</a>'],
    ['label'=> \Yii::t('modules/user/app', 'View').': ' . $this->context->getViewName($model), 'url'=>['view', 'id'=>$model->name], 'visible' => \Yii::$app->user->can('/user/role/view', ['id' => $model->name]), 'template' => '<a href="{url}" class="btn btn-primary"><i class="fa fa-eye"></i> {label}</a>'],
    ['label'=> \Yii::t('modules/user/app', 'Permissions').': ' . $this->context->getViewName($model), 'url'=>['permissions', 'id'=>$model->name], 'visible' => \Yii::$app->user->can('/user/role/permissions', ['id' => $model->name]), 'template' => '<a href="{url}" class="btn btn-orange"><i class="fa fa-cog"></i> {label}</a>'],
    ['label'=> \Yii::t('modules/user/app', 'Delete').': ' . $this->context->getViewName($model), 'url'=>['delete','id'=>$model->name, 'token'=>\Yii::$app->request->getCsrfToken()], 'linkOptions'=>['onclick'=>'confirm("'.\Yii::t('modules/user/app','Are you sure you want to delete {this_item}?', ['this_item'=>$this->context->getViewName($model)]).'");'], 'visible' => \Yii::$app->user->can('/user/role/delete', ['id' => $model->name]), 'template' => '<a href="{url}" class="btn btn-danger"><i class="fa fa-remove"></i> {label}</a>'],
];

$this->title = \Yii::t('modules/user/app', 'Role').' «'.$this->context->getViewName($model).'»';

echo $this->context->renderPartial('_form', ['model'=>$model, 'itemid' => $itemid, 'roles' => $roles]); 
?>