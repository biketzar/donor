<?php

namespace app\modules\user;
//use yii\filters\AccessControl;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\user\controllers';
    
    public function init()
    {
        parent::init();
        
        // custom initialization code goes here
        $this->registerTranslations();
    }
    
    public function registerTranslations()
    {
        \Yii::$app->i18n->translations['modules/user/*'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'sourceLanguage' => 'en-US',
            'basePath' => '@app/modules/user/messages',
            'fileMap' => [
                'modules/user/app' => 'app.php',
                'modules/user/app' => 'app.php',
            ],
        ];
    }
    
    public static function t($category, $message, $params = [], $language = null)
    {
        return \Yii::t('modules/user/' . $category, $message, $params, $language);
    }
}
