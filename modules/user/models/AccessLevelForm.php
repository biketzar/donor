<?php

namespace app\modules\user\models;

class AccessLevelForm extends \yii\base\Model
{
    public $name;
    
    public $type = \app\components\AccessLevel::TYPE_ACCESS_LEVEL;
    
    public $description;
    
    public $ruleName = 'Manager';
    
    public function rules()
    {
        return [
            [['name', 'description'], 'safe', 'on' => 'search'],
            [['name', 'description'], 'safe'],
            [['name', 'description'], 'safe', 'on' => ['insert', 'update']],
            
            [['name'], 'required', 'on' => ['insert', 'update']],
            [['name'], 'trim', 'on' => ['insert', 'update']],
            [['name'], 'app\components\AliasValidator', 'on'=>['insert', 'update']],
        ];    
    }
    
    public function getViewname()
    {
        return isset($this->description)?$this->description:$this->name;
    }
    
    public function loadData($model)
    {
        $this->name = $model->name;
        $this->description = $model->description;
        $this->ruleName = $model->ruleName;
    }
    
    
}
?>
