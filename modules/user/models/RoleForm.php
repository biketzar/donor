<?php

namespace app\modules\user\models;

use app\modules\user\components\AuthManagerHelper;

use app\modules\user\Module;

class RoleForm extends \yii\base\Model
{
    public $name;
    
    public $type = \yii\rbac\Item::TYPE_ROLE;
    
    public $description;
    
    public $children = [];
    
    public $parents = [];
    
    public $accessLevels = [];
    
    public $ruleName = 'Manager';
    
    public function rules()
    {
        return [
            [['name', 'description'], 'safe', 'on' => 'search'],
            [['name', 'description', 'children', 'parents', 'accessLevels'], 'safe'],
            [['children', 'parents', 'accessLevels'], 'filter', 'filter' => function ($value) {
                if(!is_array($value))
                    return [];
                else
                    return $value;
            }],
            
            [['name', 'description', 'children', 'parents'], 'safe', 'on' => ['insert', 'update']],
            [['accessLevels'], 'safe', 'on' => ['access-levels']],
            [['name'], 'required', 'on' => ['insert', 'update']],
            [['name'], 'trim', 'on' => ['insert', 'update']],
            [['name'], 'app\components\AliasValidator', 'on'=>['insert', 'update']],
        ];    
    }
    
    public function getViewname()
    {
        return isset($this->description)?$this->description:$this->name;
    }
    
    public function loadData($model)
    {
        $this->name = $model->name;
        $this->description = $model->description;
        $this->ruleName = $model->ruleName;
        $children = AuthManagerHelper::getChildrenObjects($model, \yii\rbac\Item::TYPE_ROLE);
        $this->children = array_keys($children['children']);
        $parents = AuthManagerHelper::getParentsObjects($model, \yii\rbac\Item::TYPE_ROLE);
        $this->parents = array_keys($parents['parents']);
        /*$levels = AuthManagerHelper::getRoleAccessLevels($this->name);
        $this->accessLevels = array_keys($levels);*/
    }
    public function attributeLabels() {
        
        return [
            'name' => Module::t('app', 'Name'),
            'description' => Module::t('app', 'Description'),
            'ruleName' => Module::t('app', 'Rule Name'),
            
        ];
        
    }
    
    
}
?>
