<?php

namespace app\modules\user\models;

use app\modules\user\components\AuthManagerHelper;

use app\modules\user\Module;

class PermissionForm extends \yii\base\Model
{
    public $name;
    
    public $type = \yii\rbac\Item::TYPE_PERMISSION;
    
    public $description;
    
    public $children = [];
    
    public $parents = [];
    
    public $ruleName = 'Manager';
    
    public function rules()
    {
        return [
            [['name', 'description'], 'safe', 'on' => 'search'],
            [['name', 'description', 'children', 'parents'], 'safe'],
            ['ruleName', 'in', 'range' => array_keys(static::getRules()), 'on' => ['insert', 'update']],
            [['children', 'parents'], 'filter', 'filter' => function ($value) {
                if(!is_array($value))
                    return [];
                else
                    return $value;
            }],
            
            [['name', 'description', 'children', 'parents'], 'safe', 'on' => ['insert', 'update']],
            [['name'], 'required', 'on' => ['insert', 'update']],
            [['name'], 'trim', 'on' => ['insert', 'update']],
            [['name'], 'app\modules\user\components\PermissionNameValidator', 'on'=>['insert', 'update']],
        ];    
    }
    
    public function getViewname()
    {
        return isset($this->description)?$this->description:$this->name;
    }
    
    public function loadData($model)
    {
        $this->name = $model->name;
        $this->description = $model->description;
        $this->ruleName = $model->ruleName;
        $children = AuthManagerHelper::getChildrenObjects($model, \yii\rbac\Item::TYPE_PERMISSION);
        $this->children = array_keys($children['children']);
        $parents = AuthManagerHelper::getParentsObjects($model, \yii\rbac\Item::TYPE_PERMISSION);
        $this->parents = array_keys($parents['parents']);
    }
    
    public static function getRules()
    {
        $rules = \yii::$app->authManager->getRules();
        $result = [];
        foreach($rules as $key=>$rule)
            $result[$key] = $rule->name;
        
        return $result;
    }
    
    public function attributeLabels() {
        
        return [
            'name' => Module::t('app', 'Name'),
            'description' => Module::t('app', 'Description'),
            'ruleName' => Module::t('app', 'Rule Name'),
            
        ];
        
    }
    
    
}
?>
