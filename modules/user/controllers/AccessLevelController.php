<?php

namespace app\modules\user\controllers;

use yii\web\Controller;

use app\components\AccessControl;
use yii\widgets\ActiveForm;
use app\modules\user\components\AuthManagerHelper;
use yii\data\ArrayDataProvider;
use app\modules\user\models;

class AccessLevelController extends Controller
{
    public $_logAction = true;
    public $enableAjaxValidation = true;
    
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    ['class' => 'app\components\ActionRule']
                ]
                
            ],
        ];
    }
    
    /**
     * Установка сообщения для пользователя
     * @param string $msg
     * @param boolean $ctrl 
     */
    public function setAdminMessage($msg, $type = 'info')
    {
        \Yii::$app->session->setFlash('admin_message_'.$type, $msg);
    }

    /**
     * Получение сообщения для пользователя
     * @param boolean $ctrl 
     * @return string
     */
    public function getAdminMessage($type = 'info')
    {
        if ( $this->hasAdminMessage($type) )
            return \Yii::$app->session->getFlash('admin_message_'.$type);
        else
            return '';
    }
	
	/**
     * Проверка наличия сообщения для польщователя
     * @param boolean $ctrl
     * @return boolean 
     */
    public function hasAdminMessage($type = 'info')
    {
        return \Yii::$app->session->hasFlash('admin_message_'.$type);
    }
    
    public function actionIndex()
    {
        return $this->actionAdmin();
    }
    
    public function actionAdmin()
    {
        $session_key = 'navigation_'.$this->module->id.'_'.$this->id.'_'.$this->action->id;
        $session = \Yii::$app->session->get($session_key, array());
	$data = \Yii::$app->request->getQueryParams();
	//$search_params = \Yii::$app->request->get();
        if(isset($data)) {
                $session['filter'] = $data;
        } elseif(isset($session['filter'])) {
                $data = $session['filter'];
        }
        $countOnPage = $this->getCountOnPage($session);
        
        $items = AuthManagerHelper::getAccessLevelObjects();
        
        $dataProvider = new ArrayDataProvider([
            'allModels' => $items,
            'pagination'=> [
                'pageSize' => $countOnPage,
            ]
        ]);
        
        \Yii::$app->session->set($session_key, $session);
        
        return $this->render('admin',array(
            'dataProvider'=>$dataProvider,
        ));
    }
    
    public function actionView()
    {
        $roles = AuthManagerHelper::getAccessLevelObjects(); 
	
        $model = isset($roles[\Yii::$app->request->get('id', '')])?$roles[\Yii::$app->request->get('id', '')]:null;
        
        if($model)
        {
            return $this->render('view',
                [
                    'model'=>$model,
                ]
            );
        } else {
            $this->setAdminMessage(\Yii::t('modules/user/app', 'There is no record with this primary key', 'error'));
            $params = ['admin'];
            $this->redirect($params);
        }
    }
    
    public function actionCreate()
    {
        $this->logOff();
	$roles = AuthManagerHelper::getAccessLevelObjects(); 
	
        $model = new \app\components\AccessLevel;
        
        $form = new models\AccessLevelForm;
        
        $form->loadData($model);
	
        if($this->enableAjaxValidation && \Yii::$app->request->isAjax && \Yii::$app->request->post('ajax', null) === strtolower($form->formName()).'-form')
        {
            return $this->performAjaxValidation($form);
        }

        if(\Yii::$app->request->isPost)
        {
            $form->load(\Yii::$app->request->post());
            if($form->validate())
            {
                $valid = true;
                if($form->name != $model->name)
                {
                    if(isset($roles[$form->name]))
                    {
                        $form->addError('name', \Yii::t('modules/user/app', 'This name has already been taken.'));
                        $valid = false;
                    } 
                } 
                if($valid)
                {
                    
                    $model->name = $form->name;
                    $model->type = $form->type;
                    $model->description = $form->description;
                    $model->ruleName = $form->ruleName;

                    $manager = AuthManagerHelper::getManager();

                    if($manager->add($model))
                    {
                        $this->logOn();
                        $id = $model->name;
                        $this->setAdminMessage(\Yii::t('modules/user/app', 'Success create'), 'success');
                        if(\Yii::$app->request->post('savetype', '')  == 'apply')
                        {
                            $params = ['update', 'id'=>$id];
                            $this->redirect($params);
                            return;
                        } else {
                            $params = ['admin'];
                            $this->redirect($params);
                            return;
                        }


                    }
                }

            }
        }
        
        return $this->render('create',
            [
                'model'=>$form,
                'itemid'=>'',
            ]
        );
    }
    
    
    public function actionUpdate()
    {
        $this->logOff();
	$roles = AuthManagerHelper::getAccessLevelObjects(); 
	
        $model = isset($roles[\Yii::$app->request->get('id', '')])?$roles[\Yii::$app->request->get('id', '')]:new \yii\rbac\Role;
        
        $form = new models\AccessLevelForm;
        
        $form->loadData($model);
        
        if($model)
        {	
            if($this->enableAjaxValidation && \Yii::$app->request->isAjax && \Yii::$app->request->post('ajax', null) === strtolower($form->formName()).'-form')
            {
                return $this->performAjaxValidation($form);
            }

            if(\Yii::$app->request->isPost)
            {
                $form->load(\Yii::$app->request->post());
                if($form->validate())
                {
                    $valid = true;
                    if($form->name != $model->name)
                    {
                        if(isset($roles[$form->name]))
                        {
                            $form->addError('name', \Yii::t('modules/user/app', 'This name has already been taken.'));
                            $valid = false;
                        } 
                    } 
                    if($valid)
                    {
                            
                        $currentName = $model->name;

                        $model->name = $form->name;
                        $model->type = $form->type;
                        $model->description = $form->description;
                        $model->ruleName = $form->ruleName;

                        $manager = AuthManagerHelper::getManager();

                        if($manager->update($currentName, $model))
                        {
                            $this->logOn();
                            $id = $model->name;
                            $this->setAdminMessage(\Yii::t('modules/user/app', 'Success update'), 'success');
                            if(\Yii::$app->request->post('savetype', '')  == 'apply')
                            {
                                $params = ['update', 'id'=>$id];
                                $this->redirect($params);
                                return;
                            } else {
                                $params = ['admin'];
                                $this->redirect($params);
                                return;
                            }
                            
                            
                        }
                    }
                
                }
            }

            return $this->render('update',
                [
                    'model'=>$form,
                    'itemid'=>$model->name,
                ]
            );
        } else {
            $this->setAdminMessage(\Yii::t('modules/user/app', 'There is no record with this primary key', 'error'));
            $params = ['admin'];
            $this->redirect($params);
        }
    }
    /*
    public function actionPermissions()
    {
	$levels = AuthManagerHelper::getAccessLevelObjects(); 
	
        $model = isset($levels[\Yii::$app->request->get('id', '')])?$levels[\Yii::$app->request->get('id', '')]:new \app\components\AccessLevel;
        
        if($model)
        {
            
            if(\Yii::$app->request->isPost && \Yii::$app->request->validateCsrfToken(\Yii::$app->request->post('_csrf', '')))
            {
                $data = \Yii::$app->request->post();
                if(isset($data['add']))
                {
                    AuthManagerHelper::addChild($model, $data['permission']);
                }
                if(isset($data['delete']))
                {
                    AuthManagerHelper::removeChild($model, $data['permission']);
                }
                $this->setAdminMessage(\Yii::t('modules/user/app', 'Success update'), 'success');
                
            }
            
            $permissions = AuthManagerHelper::getPermissionObjects(); 
            
            $permissions_map = AuthManagerHelper::getAccessLevelPermissionsMap($model->name);

            $dataProvider = new ArrayDataProvider([
                'allModels' => $permissions,
                'pagination'=> false
            ]);
            if(\Yii::$app->request->isAjax)
            {
                return $this->renderPartial('permissions',
                    [
                        'dataProvider' => $dataProvider,
                        'permissionMap' => $permissions_map,
                        'model'=>$model,
                    ]
                ); 
            } else {
                return $this->render('permissions',
                    [
                        'dataProvider' => $dataProvider,
                        'permissionMap' => $permissions_map,
                        'model'=>$model,
                    ]
                );
            }
        } else {
            $this->setAdminMessage(\Yii::t('modules/user/app', 'There is no record with this primary key', 'error'));
            $params = ['admin'];
            $this->redirect($params);
        }
    }
    */
    public function actionDelete()
    {
        $this->logOff();
        $roles = AuthManagerHelper::getAccessLevelObjects(); 
        if(\Yii::$app->request->isPost || \Yii::$app->request->isGet && \Yii::$app->request->validateCsrfToken(\Yii::$app->request->get('token', '')))
        {
            $model = isset($roles[\Yii::$app->request->get('id', '')])?$roles[\Yii::$app->request->get('id', '')]:null;
            
            if($model)
            {
                $this->logOn();
                $manager = AuthManagerHelper::getManager();
                $manager->remove($model);
            }
		
            if(\Yii::$app->request->get('ajax', ''))
            {
            } else {
                $params = ['admin'];
                $this->redirect($params);
            }
            
        }
        else
            throw new \yii\web\NotFoundHttpException(\Yii::t('modules/user/app', 'Invalid request. Please do not repeat this request again.'));
    }
    
    public function afterAction($action, $result)
    {
        $result = parent::afterAction($action, $result);

        if($this->_logAction){
                $this->whiteLog();
        }
        return $result;
    }
    
    protected function performAjaxValidation($model)
    {
        
        $model->load(\Yii::$app->request->post());
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return ActiveForm::validate($model);
            
        
    }
    
    protected function whiteLog()
    {
        $id = \Yii::$app->request->get('id', 0);
        \Yii::$app->db->createCommand()->insert('{{%private_log}}',[
            'user_id' => \Yii::$app->user->getId(),
            'controller' => $this->id,
            'action' => $this->action->id,
            'model' => 'yii\\rbac\\Role',
            'item_id' => $id,
            'is_post' => \Yii::$app->request->isPost?1:0,
            'datetime' => date('Y-m-d H:i:s'),
        ])->execute();
    }
    
    protected function logOff()
    {
            $this->_logAction = false;
    }

    protected function logOn()
    {
            $this->_logAction = true;
    }
    
    protected function getCountOnPage($session)
    {
        $count = \Yii::$app->request->get('countOnPage', null);
        if(isset($count) && ($count > 0 || $count == -1)){
            $countOnPage = min((int)$count, 999);
            $session['countOnPage'] = $countOnPage;
        }else{
            if(isset($session['countOnPage'])){
                $countOnPage = $session['countOnPage'];
            } else {
                $countOnPage = 20;
                $session['countOnPage'] = $countOnPage;
            }
        }
        return $countOnPage;
    }
    public function getViewName($object)
    {
        return $object->description?$object->description:$object->name;
    }
}
?>
