<?php

namespace app\modules\user\controllers;

use yii\web\Controller;
use yii\rbac\Item;
use app\components\AccessControl;


class PermissionGenerationController extends Controller
{
    
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    ['class' => 'app\components\ActionRule']
                ]
                
            ],
        ];
    }
    
    public function actionIndex()
    {
        
        $items = [];
        $perm_modules = [];
        $perm_tasks = [];
        $perm_operations = [];
        $folder = \yii::getAlias('@'.str_replace('\\', '/', \yii::$app->controllerNamespace));
        
        if(file_exists($folder))
        {
            $module_id = \Yii::$app->id;
            $perm_modules[] = '';
            $files = scandir($folder);

            foreach($files as $file)
            {
                if($file == '.' || $file == '..')
                    continue;
                if(!is_file($folder.'/'.$file))
                    continue;
                $pi = pathinfo($file);
                if(!preg_match("/^([a-zA-Z0-9]*)Controller$/s", $pi['filename'], $m))
                    continue;
                $conrtollerfile = str_replace('Controller', '', $pi['filename']);
                $controller = '';
                for($i = 0; $i < strlen($conrtollerfile); $i++)
                {
                    if($i > 0 && strtoupper($conrtollerfile[$i]) == $conrtollerfile[$i])
                        $controller .= '-';
                    $controller .= strtolower($conrtollerfile[$i]);
                }
                $items[] = '/'.$module_id.'/'.$controller.'/*';
                $perm_tasks['/'.$module_id.'/'.$controller.'/*'] = '';
                $methods = get_class_methods(\yii::$app->controllerNamespace.'\\'.$pi['filename']);

                foreach($methods as $method)
                {
                    if(!preg_match("/^action([a-zA-Z0-9]*)$/s", $method, $m))
                        continue;
                    if($method == 'actions')
                        continue;
                    $method = str_replace('action', '', $method);
                    $action = '';
                    for($i = 0; $i < strlen($method); $i++)
                    {
                        if($i > 0 && strtoupper($method[$i]) == $method[$i])
                            $action .= '-';
                        $action .= strtolower($method[$i]);
                    }
                    $items[] = '/'.$module_id.'/'.$controller.'/'.$action;
                    $perm_operations['/'.$module_id.'/'.$controller.'/'.$action] = $controller.'/*';

                }

            }
        }
        $modules = \yii::$app->getModules();
        
        foreach($modules as $moduleID => $module)
        {
            
            if(!is_object($module))
            {
                $moduleClassName = $module['class'];
                $module = \Yii::$app->getModule($moduleID);
            }
            $items[] = '/'.$moduleID.'/*';
            $perm_modules[] = '/'.$moduleID.'/*';
            //$controllers = $module->controllerMap;
            $folder = \yii::getAlias('@'.str_replace('\\', '/', $module->controllerNamespace));
            
            if(file_exists($folder))
            {
                $files = scandir($folder);
                
                foreach($files as $file)
                {
                    if($file == '.' || $file == '..')
                        continue;
                    if(!is_file($folder.'/'.$file))
                        continue;
                    $pi = pathinfo($file);
                    if(!preg_match("/^([a-zA-Z0-9]*)Controller$/s", $pi['filename'], $m))
                        continue;
                    $conrtollerfile = str_replace('Controller', '', $pi['filename']);
                    $controller = '';
                    for($i = 0; $i < strlen($conrtollerfile); $i++)
                    {
                        if($i > 0 && strtoupper($conrtollerfile[$i]) == $conrtollerfile[$i])
                            $controller .= '-';
                        $controller .= strtolower($conrtollerfile[$i]);
                    }
                    $items[] = '/'.$moduleID.'/'.$controller.'/*';
                    $perm_tasks['/'.$moduleID.'/'.$controller.'/*'] = '/'.$moduleID.'/*';
                    $methods = get_class_methods($module->controllerNamespace.'\\'.$pi['filename']);
                    
                    foreach($methods as $method)
                    {
                        if(!preg_match("/^action([a-zA-Z0-9]*)$/s", $method, $m))
                            continue;
                        if($method == 'actions')
                            continue;
                        $method = str_replace('action', '', $method);
                        $action = '';
                        for($i = 0; $i < strlen($method); $i++)
                        {
                            if($i > 0 && strtoupper($method[$i]) == $method[$i])
                                $action .= '-';
                            $action .= strtolower($method[$i]);
                        }
                        $items[] = '/'.$moduleID.'/'.$controller.'/'.$action;
                        $perm_operations['/'.$moduleID.'/'.$controller.'/'.$action] = '/'.$moduleID.'/'.$controller.'/*';
                        
                    }
                    
                }
            }
            foreach($module->controllerMap as $controller => $controllerFile)
            {
                $items[] = '/'.$moduleID.'/'.$controller.'/*';
                $perm_tasks['/'.$moduleID.'/'.$controller.'/*'] = '/'.$moduleID.'/*';
                $methods = get_class_methods($controllerFile);

                foreach($methods as $method)
                {
                    if(!preg_match("/^action([a-zA-Z0-9]*)$/s", $method, $m))
                        continue;
                    if($method == 'actions')
                        continue;
                    $method = str_replace('action', '', $method);
                    $action = '';
                    for($i = 0; $i < strlen($method); $i++)
                    {
                        if($i > 0 && strtoupper($method[$i]) == $method[$i])
                            $action .= '-';
                        $action .= strtolower($method[$i]);
                    }
                    $items[] = '/'.$moduleID.'/'.$controller.'/'.$action;
                    $perm_operations['/'.$moduleID.'/'.$controller.'/'.$action] = '/'.$moduleID.'/'.$controller.'/*';

                }
            }
        }
        
        $manager = \Yii::$app->authManager;
        
        $exists_items = array();
        
        $perms = $manager->getPermissions();
        
        $roles = $manager->getRoles();
        
        foreach($perms as $perm)
        {
            $exists_items[] = $perm->name;
        }
        
        $items = array_diff($items, $exists_items);
        
        $model = new \app\modules\user\models\GenerateForm;
        
        if(\Yii::$app->request->isPost)
        {
            $model->load(\Yii::$app->request->post());
            if($model->validate())
            {
                foreach($model->items as $newperm)
                {
                    if(!in_array($newperm, $exists_items))
                    {
                            
                        $permission = $manager->createPermission($newperm);
                        $permission->description = '';

                        $permission->ruleName = 'Manager';

                        $manager->add($permission);
                        $perms[$newperm] = $permission;
                    } else {
                        $permission = $perms[$newperm];
                    }
                    
                    if(in_array($newperm, $perm_modules))
                    {
                        $exists_items[] = $newperm;
                        if(!$manager->hasChild($roles['superadmin'], $permission))
                            $manager->addChild($roles['superadmin'], $permission);
                    }
                    if(isset($perm_tasks[$newperm]) && in_array($perm_tasks[$newperm], $exists_items))
                    {
                        if(!$manager->hasChild($perms[$perm_tasks[$newperm]], $permission))
                            $manager->addChild($perms[$perm_tasks[$newperm]], $permission);
                        if(!$manager->hasChild($roles['superadmin'], $permission))
                            $manager->addChild($roles['superadmin'], $permission);
                        $exists_items[] = $newperm;
                    }
                    if(isset($perm_operations[$newperm]) && in_array($perm_operations[$newperm], $exists_items))
                    {
                        if(!$manager->hasChild($perms[$perm_operations[$newperm]], $permission))
                            $manager->addChild($perms[$perm_operations[$newperm]], $permission);
                    }
                    
                }
                
            }
            $params = ['index'];
            $this->redirect($params);
        }
        
        return $this->render('index', array(
                'model'=>$model,
                'items'=>$items,
                
        ));
        
    }
}
