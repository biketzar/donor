<?php

namespace app\modules\user\controllers;

use yii\web\Controller;

use yii\widgets\ActiveForm;
use app\modules\user\components\AuthManagerHelper;
use app\modules\user\models;
use yii\data\ArrayDataProvider;

class PermissionController extends Controller
{
    public $_logAction = true;
    public $enableAjaxValidation = true;
    
    
    public function behaviors()
    {
        return [
            'access' => [
                'class' => 'app\components\AccessControl',
                'rules' => [
                    ['class' => 'app\components\ActionRule']
                ]
                
            ],
        ];
    }
    
        /**
     * Установка сообщения для пользователя
     * @param string $msg
     * @param boolean $ctrl 
     */
    public function setAdminMessage($msg, $type = 'info')
    {
        \Yii::$app->session->setFlash('admin_message_'.$type, $msg);
    }

    /**
     * Получение сообщения для пользователя
     * @param boolean $ctrl 
     * @return string
     */
    public function getAdminMessage($type = 'info')
    {
        if ( $this->hasAdminMessage($type) )
            return \Yii::$app->session->getFlash('admin_message_'.$type);
        else
            return '';
    }
	
	/**
     * Проверка наличия сообщения для польщователя
     * @param boolean $ctrl
     * @return boolean 
     */
    public function hasAdminMessage($type = 'info')
    {
        return \Yii::$app->session->hasFlash('admin_message_'.$type);
    }
    
    public function actionIndex()
    {
        return $this->actionAdmin();
    }
    
    public function actionAdmin()
    {
        $session_key = 'navigation_'.$this->module->id.'_'.$this->id.'_'.$this->action->id;
        $session = \Yii::$app->session->get($session_key, array());
	$data = \Yii::$app->request->getQueryParams();
	//$search_params = \Yii::$app->request->get();
        if(isset($data)) {
                $session['filter'] = $data;
        } elseif(isset($session['filter'])) {
                $data = $session['filter'];
        }
        $session = $this->initAdminSession($session);
        
        $countOnPage = $session['per-page'];
        
        $manager = AuthManagerHelper::getManager();
        $items = $manager->getPermissions();
        
        $dataProvider = new ArrayDataProvider([
            'allModels' => $items,
            'pagination'=> [
                'pageSize' => $countOnPage,
            ]
        ]);
        
        \Yii::$app->session->set($session_key, $session);
        
        return $this->render('admin',array(
            'dataProvider'=>$dataProvider,
        ));
    }
    
    public function actionView()
    {
        $manager = AuthManagerHelper::getManager();
        
        $model = $manager->getPermission(\Yii::$app->request->get('id', ''));
        
        if($model)
        {
            $children = AuthManagerHelper::getChildrenObjects($model, \yii\rbac\Item::TYPE_PERMISSION);
            $parents = AuthManagerHelper::getParentsObjects($model, \yii\rbac\Item::TYPE_PERMISSION);
            return $this->render('view',
                [
                    'model'=>$model,
                    'children' => $children,
                    'parents' => $parents,
                ]
            );
        } else {
            $this->setAdminMessage(\Yii::t('modules/user/app', 'There is no record with this primary key', 'error'));
            $params = ['admin'];
            $this->redirect($params);
        }
    }
    
    public function actionCreate()
    {
        $this->logOff();
	$manager = AuthManagerHelper::getManager();
	
        $model = new \yii\rbac\Permission;
        
        $form = new models\PermissionForm;
        
        $form->loadData($model);
        
        $form->setScenario('insert');
	
        $old_children = $form->children;
        $old_parents = $form->parents;
        //$old_accessLevels = $form->accessLevels;
        
	if($this->enableAjaxValidation && \Yii::$app->request->isAjax && \Yii::$app->request->post('ajax', null) === strtolower($form->formName()).'-form')
        {
            return $this->performAjaxValidation($form);
        }

        if(\Yii::$app->request->isPost)
        {
            $form->load(\Yii::$app->request->post());
            if($form->validate())
            {
                $valid = true;
                if($form->name != $model->name)
                {
                    if($manager->findItem($form->name))
                    {
                        $form->addError('name', \Yii::t('modules/user/app', 'This name has already been taken.'));
                        $valid = false;
                    } 
                } 
                if($valid)
                {

                    $model->name = $form->name;
                    $model->type = $form->type;
                    $model->description = $form->description;
                    $model->ruleName = $form->ruleName;

                    $manager = AuthManagerHelper::getManager();

                    if($manager->add($model))
                    {
                        $this->logOn();
                        $this->processChildren($form, $model, $old_children);
                        $this->processParents($form, $model, $old_parents);
                        //$form->processAccessLevels($old_accessLevels);

                        $id = $model->name;
                        $this->setAdminMessage(\Yii::t('modules/user/app', 'Success create'), 'success');
                        if(\Yii::$app->request->post('savetype', '')  == 'apply')
                        {
                            $params = ['update', 'id'=>$id];
                            $this->redirect($params);
                            return;
                        } else {
                            $params = ['admin'];
                            $this->redirect($params);
                            return;
                        }


                    }
                }

            }
        }
        
        return $this->render('create',
            [
                'model'=>$form,
                'itemid'=>'',
                'permissions'=>$manager->getPermissions(),
            ]
        );
    }
    
    
    public function actionUpdate()
    {
        $this->logOff();
	$manager = AuthManagerHelper::getManager();
        
	$model = $manager->getPermission(\Yii::$app->request->get('id', ''));
        
        $form = new models\PermissionForm;
        
        $form->loadData($model);
        
        $form->setScenario('update');
        
        $old_children = $form->children;
        $old_parents = $form->parents;
        //$old_accessLevels = $form->accessLevels;
        
	if($model)
        {	
            if($this->enableAjaxValidation && \Yii::$app->request->isAjax && \Yii::$app->request->post('ajax', null) === strtolower($form->formName()).'-form')
            {
                return $this->performAjaxValidation($form);
            }

            if(\Yii::$app->request->isPost)
            {
                $form->load(\Yii::$app->request->post());
                if($form->validate())
                {
                    $valid = true;
                    if($form->name != $model->name)
                    {
                        if($manager->findItem($form->name))
                        {
                            $form->addError('name', \Yii::t('modules/user/app', 'This name has already been taken.'));
                            $valid = false;
                        } 
                    } 
                    if($valid)
                    {
                            
                        $currentName = $model->name;

                        $model->name = $form->name;
                        $model->type = $form->type;
                        $model->description = $form->description;
                        $model->ruleName = $form->ruleName;

                        if($manager->update($currentName, $model))
                        {
                            $this->logOn();
                            $this->processChildren($form, $model, $old_children);
                            $this->processParents($form, $model, $old_parents);
                            
                            //$form->processAccessLevels($old_accessLevels);
                            
                            $id = $model->name;
                            $this->setAdminMessage(\Yii::t('modules/user/app', 'Success update'), 'success');
                            if(\Yii::$app->request->post('savetype', '')  == 'apply')
                            {
                                $params = ['update', 'id'=>$id];
                                $this->redirect($params);
                                return;
                            } else {
                                $params = ['admin'];
                                $this->redirect($params);
                                return;
                            }
                            
                            
                        }
                    }
                
                }
            }
            $permissions = $manager->getPermissions();
            if(isset($permissions[$model->name]))
                unset($permissions[$model->name]);
            return $this->render('update',
                [
                    'model'=>$form,
                    'itemid'=>$model->name,
                    'permissions'=>$permissions,
                ]
            );
        } else {
            $this->setAdminMessage(\Yii::t('modules/user/app', 'There is no record with this primary key', 'error'));
            $params = ['admin'];
            $this->redirect($params);
        }
    }
    
    public function actionDelete()
    {
        $this->logOff();
        if(\Yii::$app->request->isPost || \Yii::$app->request->isGet && \Yii::$app->request->validateCsrfToken(\Yii::$app->request->get('token', '')))
        {
            $manager = AuthManagerHelper::getManager();
        
            $model = $manager->getPermission(\Yii::$app->request->get('id', ''));
            
            if($model)
            {
                $this->logOn();
                $manager->remove($model);
            }
		
            if(\Yii::$app->request->get('ajax', ''))
            {
            } else {
                $params = ['admin'];
                $this->redirect($params);
            }
            
        }
        else
            throw new \yii\web\NotFoundHttpException(\Yii::t('modules/user/app', 'Invalid request. Please do not repeat this request again.'));
    }
    
    
    public function afterAction($action, $result)
    {
        $result = parent::afterAction($action, $result);

        if($this->_logAction){
                $this->whiteLog();
        }
        return $result;
    }
    
    
    protected function performAjaxValidation($model)
    {
        
        $model->load(\Yii::$app->request->post());
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return ActiveForm::validate($model);
            
        
    }
    
    protected function whiteLog()
    {
        $id = \Yii::$app->request->get('id', 0);
        \Yii::$app->db->createCommand()->insert('{{%private_log}}',[
            'user_id' => \Yii::$app->user->getId(),
            'controller' => $this->id,
            'action' => $this->action->id,
            'model' => 'yii\\rbac\\Item',
            'item_id' => $id,
            'is_post' => \Yii::$app->request->isPost?1:0,
            'datetime' => date('Y-m-d H:i:s'),
        ])->execute();
    }
    
    protected function logOff()
    {
            $this->_logAction = false;
    }

    protected function logOn()
    {
            $this->_logAction = true;
    }
    
    public function getViewName($object)
    {
        return $object->description?$object->description:$object->name;
    }
    
    
    protected function processChildren($element, $model, $old_children = [])
    {
        $new_children = $element->children;
        
        $diff = array_diff($new_children, $old_children);
        foreach($diff as $item)
        {
            AuthManagerHelper::addChild($model, $item);
        }
        $diff = array_diff($old_children, $new_children);
        foreach($diff as $item)
        {
            AuthManagerHelper::removeChild($model, $item);
        }
    }
    protected function processParents($element, $model, $new_parents = [], $old_parents = [])
    {
        $new_parents = $element->parents;
        
        $diff = array_diff($new_parents, $old_parents);
        foreach($diff as $item)
        {
            AuthManagerHelper::addChild($item, $model);
        }
        $diff = array_diff($old_parents, $new_parents);
        foreach($diff as $item)
        {
            AuthManagerHelper::removeChild($item, $model);
        }
    }
    protected function initAdminSession($session)
    {
        $pageParam = 'page';
        $page = \Yii::$app->request->get($pageParam);
        if($page)
        {
            $session[$pageParam] = $page;
        } else {
            if(isset($session[$pageParam])){
                $getParams = \yii::$app->request->getQueryParams();
                $getParams[$pageParam] = $session[$pageParam];
                \yii::$app->request->setQueryParams($getParams);
            } else {
            }
        }
        
        $count = \Yii::$app->request->get('per-page', null);
        if(isset($count) && ($count > 0 || $count == -1)){
            $countOnPage = min((int)$count, 999);
            $session['per-page'] = $countOnPage;
        }else{
            if(isset($session['per-page'])){
                $countOnPage = $session['per-page'];
            } else {
                $countOnPage = 20;
                $session['per-page'] = $countOnPage;
            }
        }
        return $session;
    }
    
}
?>
