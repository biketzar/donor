<?php

namespace app\modules\user\controllers;

use yii\web\Controller;

use yii\widgets\ActiveForm;
use app\modules\user\components\AuthManagerHelper;
use app\components\SiteHelper;

class UserController extends Controller
{
    public $_logAction = true;
    public $enableAjaxValidation = true;
    
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['access']= 
                    [
                        'class' => 'app\components\AccessControl',
                        'rules' => [
                            ['class' => 'app\modules\user\components\UserAdminRule'],
                        ]
                    ];
        
        return $behaviors;
    }
    
    /**
     * Установка сообщения для пользователя
     * @param string $msg
     * @param boolean $ctrl 
     */
    public function setAdminMessage($msg, $type = 'info')
    {
        \Yii::$app->session->setFlash('admin_message_'.$type, $msg);
    }

    /**
     * Получение сообщения для пользователя
     * @param boolean $ctrl 
     * @return string
     */
    public function getAdminMessage($type = 'info')
    {
        if ( $this->hasAdminMessage($type) )
            return \Yii::$app->session->getFlash('admin_message_'.$type);
        else
            return '';
    }
	
	/**
     * Проверка наличия сообщения для польщователя
     * @param boolean $ctrl
     * @return boolean 
     */
    public function hasAdminMessage($type = 'info')
    {
        return \Yii::$app->session->hasFlash('admin_message_'.$type);
    }
    
    public function actionIndex()
    {
        return $this->actionAdmin();
    }
    
    public function actionAdmin()
    {
        $model = \Yii::createObject(['class' => 'app\models\UserIndentity', 'scenario' => 'search']);
	
        $session_key = 'navigation_'.$this->module->id.'_'.$this->id.'_'.$this->action->id;
        $session = \Yii::$app->session->get($session_key, array());
	$data = \Yii::$app->request->get();
	//$search_params = \Yii::$app->request->get();
        if(isset($data)) {
                $model->load($data);
                $session['filter'] = $data;
        } elseif(isset($session['filter'])) {
                $model->load($session['filter']);
        }
        $session = $this->initAdminSession($session);
        
        $countOnPage = $session['per-page'];
        
        $query = $model->searchQuery($data);
        
        //$query = $model::find();
        
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $countOnPage,
            ],
        ]);
	
        \Yii::$app->session->set($session_key, $session);
        
        return $this->render('admin',array(
            'dataProvider'=>$dataProvider,
            'model'=>$model,
            
        ));
    }
    
    public function actionView()
    {
        $model = \Yii::createObject(['class' => 'app\models\UserIndentity']);
	
        $model = \app\models\UserIndentity::findOne(\Yii::$app->request->get('id', ''));
        
        $authmanager = AuthManagerHelper::getManager();
        
        if($model)
        {
            $roles_user = array_keys($authmanager->getRolesByUser($model->primaryKey));
            
            return $this->render('view',
                [
                    'model'=>$model,
                    'roles_user'=>$roles_user,
                    'roles' => $authmanager->getRoles(),
                ]
            );
        } else {
            $this->setAdminMessage(\Yii::t('modules/user/app', 'There is no record with this primary key', 'error'));
            $params = ['admin'];
            $this->redirect($params);
        }
    }
    
    public function actionCreate()
    {
        $this->logOff();
	$model = \Yii::createObject(['class' => '\app\models\UserIndentity', 'scenario' => 'insert']);
        
        $authmanager = AuthManagerHelper::getManager();
	
        if($this->enableAjaxValidation && \Yii::$app->request->isAjax && \Yii::$app->request->post('ajax', null) === strtolower($model->formName()).'-form')
        {
            return $this->performAjaxValidation($model);
        }
        $managers = \yii::$app->authManager->getUsersByRole('manager');
        if(\Yii::$app->request->isPost)
        {
            $model->load(\Yii::$app->request->post());
            
            $data = \Yii::$app->request->post($model->formName());
            
            if($model->save())
            {
                //$this->saveAvatar($model, $data);
                $this->logOn();
                $this->setAdminMessage(\Yii::t('modules/user/app', 'Success create'), 'success');
                $id = $model->getPrimaryKey;
                if(isset($data['roles']))
                {
                    $new_roles = $data['roles'];

                    foreach($new_roles as $role)
                    {
                        AuthManagerHelper::assignRole($role, $id);
                    }
                }

                if(\Yii::$app->request->post('savetype', '')  == 'apply')
                {
                    $params = ['update', 'id'=>$id];
                    $this->redirect($params);
                    return;
                } else {
                    $params = ['admin'];
                    $this->redirect($params);
                    return;
                }
                
            }
            
        }
        
        return $this->render('create',
            [
                'model'=>$model,
                'roles_user' => [],
                'roles' => $authmanager->getRoles(),
                'managers' => $managers,
            ]
        );
    }
    
    
    public function actionUpdate()
    {
        $this->logOff();
	$model = \app\models\UserIndentity::findOne(\Yii::$app->request->get('id', ''));
        
        $authmanager = AuthManagerHelper::getManager();
        
	if($model)
        {
            $model->setScenario('update');
        
            $id = $model->getPrimaryKey();
            
            if($this->enableAjaxValidation && \Yii::$app->request->isAjax && \Yii::$app->request->post('ajax', null) === strtolower($model->formName()).'-form')
            {
                return $this->performAjaxValidation($model);
            }
            $current_roles = array_keys($authmanager->getRolesByUser($id));
            $managers = \yii::$app->authManager->getUsersByRole('manager');
            if(\Yii::$app->request->isPost)
            {
                $model->load(\Yii::$app->request->post());
                
                $data = \Yii::$app->request->post($model->formName());
                
                if($model->save())
                {
                    //$this->saveAvatar($model, $data);
                    $this->logOn();
                    $this->setAdminMessage(\Yii::t('modules/user/app', 'Success update'), 'success');
                    
                    if(isset($data['roles']))
                    {
                        $new_roles = $data['roles'];
                        $roles_changed = false;
                        $diff = array_diff($new_roles, $current_roles);
                        foreach($diff as $role)
                        {
                            AuthManagerHelper::assignRole($role, $id);
                            $roles_changed = true;
                        }
                        $diff = array_diff($current_roles, $new_roles);
                        foreach($diff as $role)
                        {
                            AuthManagerHelper::revokeRole($role, $id);
                            $roles_changed = true;
                        }
                        if($roles_changed)
                        {
                            //$model->saveRoleToWI();
                            $model->afterRoleChange($current_roles);
                            
                        }
                    }

                    if(\Yii::$app->request->post('savetype', '')  == 'apply')
                    {
                        $params = ['update', 'id'=>$id];
                        $this->redirect($params);
                        return;
                    } else {
                        $params = ['admin'];
                        $this->redirect($params);
                        return;
                    }
                    
                }
            }
            
            return $this->render('update',
                [
                    'model'=>$model,
                    'roles_user' => $current_roles,
                    'roles' => $authmanager->getRoles(),
                    'managers' => $managers,
                ]
            );
        } else {
            $this->setAdminMessage(\Yii::t('modules/user/app', 'There is no record with this primary key', 'error'));
            $params = ['admin'];
            $this->redirect($params);
        }
    }
    
    protected function saveAvatar($model, $data)
    {
        
        $file = \yii\web\UploadedFile::getInstance($model, 'avatar');
            
            $path = $model->getAvatarDirectory();
			
            $path = $path.((substr($path,-1) != '/')?'/':'');
            
            //$path = str_replace(\Yii::getAlias('@web2'), \Yii::getAlias('@webroot2'), $path);

            if($file)
            {

                /*if(!is_dir($path))
                {

                    $path_parts = str_replace(\Yii::getAlias('@webroot').'', '', $path);

                    $path_parts = explode('/',$path_parts);
                    $path_temp = '';
                    for($i = 0; $i<count($path_parts); $i++)
                    {
                        if(!$path_parts[$i])
                            continue;
                        $path_temp.= $path_parts[$i].'/';
                        
                        if(!file_exists(\Yii::getAlias('@webroot').''.$path_temp))
                        {
                            mkdir(\Yii::getAlias('@webroot').''.$path_temp, 0755);
                        }
                    }
                    
                }*/
                SiteHelper::checkFolderPath($path);

                $filename = SiteHelper::translit($file->baseName);
                
                $k = 1;
                
                while(file_exists($path.$filename. '.' . $file->extension))
                {
                    $filename = SiteHelper::translit(trim($file->baseName)).'-'.($k++);
                }
                
                $file->saveAs(\Yii::getAlias('@webroot').$path.$filename . '.' . $file->extension);

                $model->avatar = $filename . '.' . $file->extension;
            } else {
                if($model->avatar)
                    $model->avatar = $model->avatar;
                elseif($model->getOldAttribute('avatar'))
                    $model->avatar = $model->getOldAttribute('avatar');
                else
                    $model->avatar = '';
            }
            if(isset($data['avatar_attr']['watermark']) && $model->avatar)
            {
                $watermark = \Yii::$app->constants->get('watermark');
                if($watermark)
                {
                    \app\components\ImageHelper::watermark($model->getAvatarDirectory().$model->avatar, 
                            \Yii::$app->constants->get('watermark'),
                            $path.$model->avatar
                            );
                }
            }
            
            if(isset($data['avatar_attr']['del']))
                $model->avatar = '';
            $model->update(false, ['avatar']);
    }
    
    public function actionDelete()
    {
        $this->logOff();
        if(\Yii::$app->request->isPost || \Yii::$app->request->isGet && \Yii::$app->request->validateCsrfToken(\Yii::$app->request->get('token', '')))
        {
            $this->logOn();
            $model = \app\models\UserIndentity::findOne(\Yii::$app->request->get('id', ''));
            
            $model->delete();
		
            if(\Yii::$app->request->get('ajax', ''))
            {
            } else {
                $params = ['admin'];
                $this->redirect($params);
            }
            
        }
        else
            throw new \yii\web\NotFoundHttpException(\Yii::t('modules/user/app', 'Invalid request. Please do not repeat this request again.'));
    }
    
    public function actionSuggest()
    {
        $this->logOn();
        if(\Yii::$app->request->get('field') || \Yii::$app->request->get('term'))
        {
            $field = \Yii::$app->request->get('field');
            $term = \Yii::$app->request->get('term');
            $modelName = \app\models\UserIndentity;
            
            $model = \Yii::createObject(['class' => $modelName, 'scenario' => 'search']);
	
            $query = $modelName::find();
        
            $query->andFilterWhere(['like', $field, $term]);
            
            $dataProvider = new ActiveDataProvider([
                'query' => $query,
                'pagination' => [
                    'pageSize' => 100,
                ],
            ]);
            $result = [];
            foreach($dataProvider->getModels() as $object)
            {
                $result[] = array(
                        'id' => $object->primaryKey,
                        'label' => $object->$field,
                        'value' => $object->$field,
                );
            }
            
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return $result;
            
        } else {
            throw new \yii\web\NotFoundHttpException(\Yii::t('modules/user/app', 'Invalid request. Please do not repeat this request again.'));
        }
    }
    
    public function afterAction($action, $result)
    {
        $result = parent::afterAction($action, $result);

        if($this->_logAction){
                $this->whiteLog();
        }
        return $result;
    }
    
    protected function performAjaxValidation($model)
    {
        $model->load(\Yii::$app->request->post());
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return ActiveForm::validate($model);
            
        
    }
    
    protected function whiteLog()
    {
        $id = \Yii::$app->request->get('id', 0);
        \Yii::$app->db->createCommand()->insert('{{%private_log}}',[
            'user_id' => \Yii::$app->user->getId(),
            'controller' => $this->id,
            'action' => $this->action->id,
            'model' => 'app\\models\\UserIndentity',
            'item_id' => $id,
            'is_post' => \Yii::$app->request->isPost?1:0,
            'datetime' => date('Y-m-d H:i:s'),
        ])->execute();
    }
    
    protected function logOff()
    {
            $this->_logAction = false;
    }

    protected function logOn()
    {
            $this->_logAction = true;
    }
    
    
    protected function initAdminSession($session)
    {
        $pageParam = 'page';
        $page = \Yii::$app->request->get($pageParam);
        if($page)
        {
            $session[$pageParam] = $page;
        } else {
            if(isset($session[$pageParam])){
                $getParams = \yii::$app->request->getQueryParams();
                $getParams[$pageParam] = $session[$pageParam];
                \yii::$app->request->setQueryParams($getParams);
            } else {
            }
        }
        
        $count = \Yii::$app->request->get('per-page', null);
        if(isset($count) && ($count > 0 || $count == -1)){
            $countOnPage = min((int)$count, 999);
            $session['per-page'] = $countOnPage;
        }else{
            if(isset($session['per-page'])){
                $countOnPage = $session['per-page'];
            } else {
                $countOnPage = 20;
                $session['per-page'] = $countOnPage;
            }
        }
        return $session;
    }
}
?>
