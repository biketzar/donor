<?php
return [
    'Users' => 'Пользователи',
    'User' => 'Пользователь',
    'Username' => 'Логин',
    'Email' => 'Email',
    'Status' => 'Статус',
    'Password' => 'Пароль',
    'Password repeat' => 'Повторить пароль',
    'Last Visit' => 'Последнее посещение',
    
    'This email address has already been taken' => 'Email адрес уже используется',
    'Roles' => 'Роли',
    'Create Time' => 'Дата создания',
    'Update Time' => 'Дата обновления',
    'no records in list'=>'В списке нет записей. Предлагаем добавить.',
    'no' => 'нет',
    'yes' => 'да',
    'All' => 'Все',
    'Create' => 'Добавить',
    'Manage' => 'Список',
    'View' => 'Просмотр',
    'Delete' => 'Удалить',
    'Update' => 'Редактировать',
    'Clone' => 'Клонировать',
    'Change log' => 'Лог изменений',
    'Cancel' => 'Отмена',
    'Save' => 'Сохранить',
    'Save & continue edit' => 'Применить',
    'Max allowed size' => 'Максимальный размер файла',
    'Success update' => 'Успешно обновлено',
    'Success create' => 'Успешно добавлено',
    'Delete file' => 'Удалить файл',
    'Add watermark' => 'Добавить водяной знак',
    'Change log' => 'Лог изменений',
    'Cannot move file' => 'Не удалось переместить файл',
    'Multiple upload' => 'Множественная загрузка',
    'Remember Me' => 'Запомнить меня',
    'Login' => 'Авторизация',
    'Login button' => 'Войти',
    'Name' => 'Название',
    'Description' => 'Описание',
    'Permissions' => 'Разрешения',
    'Children' => 'Дочерние',
    'Parent' => 'Родительские',
    'Role' => 'Роль',
    'Permission' => 'Разрешение',
    'Rule Name' => 'Правило',
    'Parents' => 'Родительские',
    'Action' => 'Действие',
    'Add link' => 'добавить связь',
    'Add strict link' => 'добавить прямую связь',
    'Delete link' => 'удалить связь',
    'Person Name' => 'ФИО',
    'Company' => 'Компания',
    'City Id' => 'Город',
    'Phone' => 'Телефон',
    'Skype' => 'Skype',
    'Country Id' => 'Страна',
    'Region Id' => 'Регион',
    'Select country' => 'Выберите страну',
    'Select region' => 'Выберите регион',
    'Select city' => 'Выберите город',
    'Verify Code' => 'Введите код на картинке',
    'Avatar' => 'Аватар',
    'Old Password' => 'Старый пароль',
    'New Password' => 'Новый пароль',
    'Manager' => 'Менеджер',
    'Ax User Id' => 'Ax ID',
    'Role permission map' => 'Схема прав ролей',
];
?>
