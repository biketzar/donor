<?php

namespace app\modules\user\components;


class AuthManagerHelper extends \yii\base\Object
{
    static protected $_accesslevelobjects = null;
    static protected $_accesslevels = null;
    static protected $_permissionobjects = null;
    static protected $_permissions = null;
    
    public static function getManager()
    {
        $authManager = \yii::$app->authManager;
        return $authManager;
    }
    
    public static function assignRole($role, $user_id)
    {
        $authManager = self::getManager();
        
        if(is_string($role))
            $role = $authManager->getRole($role);
                
        if(!$role instanceof \yii\rbac\Role)
            return;
        $roles = $authManager->getRolesByUser($user_id);
        if(!isset($roles[$role->name]))            
            $authManager->assign($role, $user_id);
        
    }
    public static function revokeRole($role, $user_id)
    {
        $authManager = self::getManager();
        
        if(is_string($role))
            $role = $authManager->getRole($role);
                
        if(!$role instanceof \yii\rbac\Role)
            return;
        
        $authManager->revoke($role, $user_id);
        
    }
    
    protected static function getChildrenObjectsRecursive($parent, $type)
    {
        $authManager = self::getManager();
        
        $children = $authManager->getChildrenOfTypeDirect($parent->name, $type);
        
        $result = ['item' => $parent, 'children' => [], 'path' => []];
        foreach($children as $key => $item)
        {
            $result['children'][$key] = self::getChildrenObjectsRecursive($item, $type);
            if(count($result['children'][$key]['path']))
            {
                foreach($result['children'][$key]['path'] as $p)
                    $result['path'][] = array_merge([$key], $p);
            } else {
                $result['path'][] = [$key];
            }
        }

        return $result;
    }
    
    public static function getChildrenObjects($parent, $type = Item::TYPE_PERMISSION)
    {
        static $chains = [];
        
        if(!isset($chains[$parent->name]))
        {
            $chains[$parent->name] = self::getChildrenObjectsRecursive($parent, $type);
            
        }
        $result = ['item'=>null, 'children'=>[], 'path'=>[]];
           
        if(isset($chains[$parent->name]))
            $result = $chains[$parent->name];
        
        return $result;
    }
    
    protected static function getParentsObjectsRecursive($child, $type)
    {
        $authManager = self::getManager();
        
        $parents = $authManager->getParentOfTypeDirect($child->name, $type);
        
        $result = ['item' => $child, 'parents' => [], 'path' => []];
        foreach($parents as $key => $item)
        {
            $result['parents'][$key] = self::getParentsObjectsRecursive($item, $type);
            if(count($result['parents'][$key]['path']))
            {
                foreach($result['parents'][$key]['path'] as $p)
                    $result['path'][] = array_merge($p, [$key]);
            } else {
                $result['path'][] = [$key];
            }
            
        }

        return $result;
        
    }
    
    public static function getParentsObjects($child, $type = Item::TYPE_PERMISSION)
    {
        static $chains = [];
        
        if(!isset($chains[$child->name]))
        {
            $chains[$child->name] = self::getParentsObjectsRecursive($child, $type);
            
        }
        $result = ['item'=>null, 'parents'=>[], 'path'=>[]];
           
        if(isset($chains[$child->name]))
            $result = $chains[$child->name];
        
        return $result;
    }
    
    /*public static function getAccessLevels($refresh = false)
    {
        if(is_array(self::$_accesslevels) && !$refresh)
            return self::$_accesslevels;
        $authManager = self::getManager();
        $items = $authManager->getAccessLevels();
        
        self::$_accesslevels = [];
        foreach($items as $model)
            self::$_accesslevels[$model->name] = ($model->description)?$model->description:$model->name;
        return self::$_accesslevels;
    }
    public static function getAccessLevelObjects($refresh = false)
    {
        if(is_array(self::$_accesslevelobjects) && !$refresh)
            return self::$_accesslevelobjects;
        $authManager = self::getManager();
        $items = $authManager->getAccessLevels();
        
        self::$_accesslevelobjects = [];
        foreach($items as $model)
            self::$_accesslevelobjects[$model->name] = $model;
        return self::$_accesslevelobjects;
    }
    
    public static function getPermissionsByAccessLevel($level_name)
    {
        $authManager = self::getManager();
        return $authManager->getChildrenOfType($level_name, Item::TYPE_PERMISSION);
    }
    
    public static function getPermissionsByAccessLevelDirect($level_name)
    {
        $authManager = self::getManager();
        return $authManager->getChildrenOfTypeDirect($level_name, Item::TYPE_PERMISSION);
    }
    
    public static function getRoleAccessLevels($role)
    {
        $authManager = self::getManager();
        return $authManager->getRoleAccessLevels($role);
        
    }
    
    public static function getRoleAccessLevelsMap($role)
    {
        $authManager = self::getManager();
        return $authManager->getRoleAccessLevelsMap($role);
    }
    
    public static function getAccessLevelPermissionsMap($role)
    {
        $authManager = self::getManager();
        return $authManager->getAccessLevelPermissionsMap($role);
    }
    */
    public static function getRolePermissionsMap($role)
    {
        $authManager = self::getManager();
        return $authManager->getRolePermissionsMap($role);
    }
    
    public static function addChild($parent, $child)
    {
        $authManager = self::getManager();
        
        if(is_string($parent))
        {
            if(!($parent = $authManager->findItem($parent)))
                return;
        }
        if(is_string($child))
        {
            if(!($child = $authManager->findItem($child)))
                return;
        }
        if(!$authManager->hasChild($parent, $child))
            $authManager->addChild($parent, $child);
        
    }
    public static function removeChild($parent, $child)
    {
        $authManager = self::getManager();
        
        if(is_string($parent))
        {
            if(!($parent = $authManager->findItem($parent)))
                return;
        }
        if(is_string($child))
        {
           if(!($child = $authManager->findItem($child)))
                return;
        }
        if($authManager->hasChild($parent, $child))
            $authManager->removeChild($parent, $child);
        
    }
}
?>
