<?php
namespace app\modules\user\components;

use app\components\ActionRule;
use yii\web\User;
use yii\di\Instance;


/**
 * Checks if authorID matches user passed via params
 */
class RoleAdminRule extends ActionRule
{
    public $name = 'role_admin_rule';

    private $_user = 'user';
    
    /**
     * @param string|integer $user the user ID.
     * @param Item $item the role or permission that this rule is associated with
     * @param array $params parameters passed to ManagerInterface::checkAccess().
     * @return boolean a value indicating whether the rule permits the role or permission it is associated with.
     */
    public function execute($user, $item, $params)
    {
        $authManager = \yii::$app->authManager;
        
        $action = $item->name;
        $action = str_replace('/*', '', $action);
        $action = explode('/', $action);
        $action = $action[count($action)-1];
        $role_id = isset($params['id'])?$params['id']:null;
        
        $assignments = $authManager->getAssignments($user);
        
        /*if($role_id && in_array($role_id, ['superadmin']))
        {
            if($action == 'permissions')
                return false;
        } */
        if($role_id && in_array($role_id, ['superadmin', 'guest']))
        {
            if($action == 'update' || $action == 'delete')
                return false;
        }  
        
        if($role_id && !\yii::$app->user->can('right-to-change-role-'.$role_id))
            return false;
        return parent::execute($user, $item, $params);
        
    }
    
    public function allows($action, $user, $request)
    {
        $authManager = \yii::$app->authManager;
        $assignments = $authManager->getAssignments($user->id);
        
        $role_id = $request->get('id', '');
        
        /*if($role_id && in_array($role_id, ['superadmin']))
        {
            if($action->id == 'permissions')
                return false;
        } */
        if($role_id && in_array($role_id, ['superadmin', 'guest']))
        {
            if($action->id == 'update' || $action->id == 'delete')
                return false;
        } 
        if($role_id && !$user->can('right-to-change-role-'.$role_id))
            return false;
        return parent::allows($action, $user, $request);
    }
    
    public function getUser()
    {
        if (!$this->_user instanceof UserIndentity) {
            $this->_user = Instance::ensure($this->_user, UserIndentity::className());
        }
        return $this->_user;
    }
    
    
    
}
?>
