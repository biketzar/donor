<?php
namespace app\modules\user\components;

use app\components\ActionRule;
use yii\web\User;
use yii\di\Instance;


/**
 * Checks if authorID matches user passed via params
 */
class UserAdminRule extends ActionRule
{
    public $name = 'user_admin_rule';

    private $_user = 'user';
    
    /**
     * @param string|integer $user the user ID.
     * @param Item $item the role or permission that this rule is associated with
     * @param array $params parameters passed to ManagerInterface::checkAccess().
     * @return boolean a value indicating whether the rule permits the role or permission it is associated with.
     */
    public function execute($user, $item, $params)
    {
        return true;
    }
    
    public function allows($action, $user, $request)
    {
        $authManager = \yii::$app->authManager;
        $assignments = $authManager->getAssignments($user->id);
        
        if(in_array('superadmin', $assignments))
            return true;
        
        $user_id = $request->get('id', '');
        
        if($user_id)
        {
            $assignments2 = $authManager->getAssignments($user_id);
            foreach($assignments2 as $key => $assignment)
            {
                if(!$user->can('right-to-change-role-'.$key))
                    return false;
            }
            
        } 
        return parent::allows($action, $user, $request);
    }
    
    public function getUser()
    {
        if (!$this->_user instanceof UserIndentity) {
            $this->_user = Instance::ensure($this->_user, UserIndentity::className());
        }
        return $this->_user;
    }
    
    
    
}
?>
