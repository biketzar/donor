<?php
namespace app\modules\form\components;

use yii\web\UrlRuleInterface;
use yii\base\Object;
use app\modules\form\models;

class UrlRule extends Object implements UrlRuleInterface
{

    public function createUrl($manager, $route, $params)
    {
        $url = '';
        
        if($route == 'form/default/index')
        {
            if(isset($params['pageObject']))
            {
                $url .= $params['pageObject']->alias;
                if(isset($params['form_id']) && isset($params['pageObject']->handler_data['form_id']) 
                        && ($params['form_id'] == $params['pageObject']->handler_data['form_id'])) 
                    unset($params['form_id']);
                unset($params['pageObject']);
            }
            $url .= '';
            
            if (!empty($params) && ($query = http_build_query($params)) !== '') {
                $url .= '?' . $query;
            }
            return $url;
        }
        
        return false;  // this rule does not apply
    }
    
    public function parseRequest($manager, $request)
    {


        $pathInfo = $request->getPathInfo();

        $pathInfo = trim($pathInfo, '/');

        if($pathInfo == '')
            return ['form/default/index', []];
        
        return false;
    }
}
?>