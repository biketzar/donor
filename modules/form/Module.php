<?php

namespace app\modules\form;

use yii\helpers\ArrayHelper;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\form\controllers';
    
    public function init()
    {
        parent::init();
        // custom initialization code goes here
        self::registerTranslations();
    }
    
    public static function registerTranslations()
    {
        \Yii::$app->i18n->translations['modules/form/*'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'sourceLanguage' => 'en-US',
            'basePath' => '@app/modules/form/messages',
            'fileMap' => [
                'modules/form/app' => 'app.php',
                'modules/form/app' => 'app.php',
            ],
        ];
    }
    
    public function getHandlers()
    {
        return [
            'form/default/index' => \yii::t('modules/form/app', 'Form'),
        ];
    }
    
    public function getHandlerData()
    {
        $result = ['form/default/index' => []];
        
        $forms = models\Form::find()->asArray()->all();
        $result['form/default/index']['form_id'] = ['RDbSelect', 'data' => ArrayHelper::map($forms, 'id', 'name'), 'label' => \yii::t('modules/form/app', 'Form')];
        
        return $result;
    }
    public function getHandlerRules()
    {
        $result = ['form/default/index' => [
            ['class' => 'app\\modules\\form\\components\\UrlRule'],
        ]];
        
        return $result;
    }
    
    public function privateMenuLinks()
    {
        return [
            'content' => ['items' => [['label' => \yii::t('modules/form/app', 'Forms'), 'url'=> ['/private/form/admin'], 'visible' => \yii::$app->user->can('/private/form/admin')]]],
        ];
    }
    public function privateControllerMap()
    {
        return [
            'form' => '\app\modules\form\controllers\privatepanel\FormController',
            'form-field' => '\app\modules\form\controllers\privatepanel\FormFieldController',
            'form-result' => '\app\modules\form\controllers\privatepanel\FormResultController',
        ];
    }
    
    
}
