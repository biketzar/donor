<?php

namespace app\modules\form\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\modules\form\models;

use yii\data\ActiveDataProvider;

class DefaultController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /*public function actions()
    {
        return [
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
    */
    public function actionIndex()
    {
        $form_id = Yii::$app->request->get('form_id', 0);
        
        $form = null;
        
        if($form_id)
        {
            $form = models\Form::find()->where('id=:id', [':id' => $form_id])->published()->one();
        }
        
        if(!$form)
        {
            throw new \yii\web\NotFoundHttpException(\yii::t('app', 'Form is not found'));
        }
        
        $model = $form->createFormModel();
        
        $message = '';
        
        if(\Yii::$app->request->isPost && \Yii::$app->request->post($model->formName()) && $model->load(\Yii::$app->request->post()) && $model->validate())
        {
            
                $resulttext = array();
                $resultfiletext = array();
                $files = array();
                foreach ($form->activeFields as $field)
                {
                    $value = $form->getValue($model, $field);
                    if($value !== false)
                        $resulttext[]= $field->name.': '.$value;
                    
                    $value = $form->getFileValue($model, $field);
                    if($value !== false)
                    {
                        $files= array_merge($files, $value);
                        foreach($value as $k=>$v)
                        {
                            //$v = str_replace(\Yii::getAlias('@root'),'',$v);
                            $resultfiletext[]= '<a href="'.$v.'">'.$k.'</a>';
                        }
                    }
                }
                
                if($form->send_to_email)
                {
                    $mailer = new \app\components\MailHelper;
                    $mailer->to = $form->email;
                    $mailer->subject = $form->subject;
                    $mailer->mailTemplate = 'form';
                    $mailer->params = ['TEXT' => implode('<br/>', $resulttext)];
                    $mailer->files = $files;
                   

                    $mailer->Send();
                }
                if($form->send_to_db)
                {
                    $dbsubmit = \Yii::createObject(['class' => 'app\modules\form\models\FormResult', 'scenario' => 'insert']);
                    $dbsubmit->form_id = $form->id;
                    $dbsubmit->date = date('Y-m-d H:i:s');
                    $dbsubmit->text = implode('<br/>', array_merge($resulttext, $resultfiletext));
                    $dbsubmit->save();
                }
                $message = $form->success_text?$form->success_text:\yii::t('modules/form/app', 'Message was sent successfully');
                \Yii::$app->session->setFlash('form_message', $message);
        }
        

        //$items = $provider->getModels();
        return $this->render('index',
                [
                    'form'   => $form,
                    'model'   => $model,
                    
                ]);
    }
    
}
