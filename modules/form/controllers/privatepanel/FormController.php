<?php

namespace app\modules\form\controllers\privatepanel;

use app\modules\privatepanel\components\PrivateController;

class FormController extends PrivateController
{
    protected $_modelName = 'app\modules\form\models\Form';
}
?>
