<?php
namespace app\modules\form\models;

use app\modules\form\Module;

use \app\components\CommonActiveRecord as ActiveRecord;
use \app\components\CommonActiveQuery;

class FormField extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%form_fields}}';
    }
    
    public function init()
    {
        parent::init();
        Module::registerTranslations();
        $this->_privateAdminName = \yii::t('modules/form/app', $this->_privateAdminName);
        $this->_privatePluralName = \yii::t('modules/form/app', $this->_privatePluralName);
        
    }
    
    public function afterFind()
    {
        parent::afterFind();
        
    }
    
    public function getForm()
    {
        return $this->hasOne(Form::className(), ['id' => 'form_id']);
    }
    
    public static function getTypes()
    {
        return [
            'text' => \yii::t('modules/form/app', 'Text field'),
            'email' => \yii::t('modules/form/app', 'E-mail field'),
            'date' => \yii::t('modules/form/app', 'Date field'),
            'textarea' => \yii::t('modules/form/app', 'Textarea field'),
            'checkbox' => \yii::t('modules/form/app', 'Checkbox field'),
            'group' => \yii::t('modules/form/app', 'Group field'),
            'dropdown' => \yii::t('modules/form/app', 'Dropdown field'),
            'file' => \yii::t('modules/form/app', 'File field'),
            'separator' => \yii::t('modules/form/app', 'Separator field'),
        ];
    }
    
    public function attributeLabels() {
        
        return [
            'form_id' => \yii::t('modules/form/app', 'Form'),
            'name' => \yii::t('modules/form/app', 'Name'),
            'width' => \yii::t('modules/form/app', 'Width'),
            'height' => \yii::t('modules/form/app', 'Height'),
            'values' => \yii::t('modules/form/app', 'Values'),
            'required' => \yii::t('modules/form/app', 'Required'),
            'multiple' => \yii::t('modules/form/app', 'Multiple'),
            'default_value' => \yii::t('modules/form/app', 'Default value'),
            'type' => \yii::t('modules/form/app', 'Field type'),
            'ordering' => \yii::t('modules/form/app', 'Ordering'),
            'published' => \yii::t('modules/form/app', 'Published'),
        ];
        
    }
    
    public function rules()
    {
        return [
            [['id', 'form_id', 'name', 'width', 'height', 'values', 'required', 'multiple', 'default_value', 'type', 'ordering', 'published'], 'safe'],
            [['published', 'form_id', 'ordering', 'multiple', 'width', 'height', 'required'], 'number', 'integerOnly' => true, 'on'=>['insert', 'update']],
            [['name', 'type'], 'required', 'on'=>['insert', 'update']],
            [['id', 'form_id', 'name', 'width', 'height', 'values', 'required', 'multiple', 'default_value', 'type', 'ordering'], 'safe', 'on'=>'search'],
        ];
    }
    
    
    
    /*for crud admin*/
    public $_privateAdminName = 'Form field'; // One item name, used in breadcrums, in header in actionAdmin
    public $_privatePluralName = 'Form fields';// Item set name
    public $_privateAdminRepr = 'name';// method or property used to retrieve model string represantation. It's close to __toString but only for admin
    
    public function getAdminFields()
    {
            return array(
                    //'form_id',
                    'name',
                    'type',
                    'ordering',
                    'published',
            );
            
    }

    public function getFieldsDescription()
    {
            return array(
                    'form_id' => ['RDbRelation', 'form'],
                    'name' => 'RDbText',
                    'type' => ['RDbSelect', 'data' => static::getTypes()],
                    'required' => 'RDbBoolean',
                    'multiple' => 'RDbBoolean',
                    'default_value' => 'RDbText',
                    'width' => 'RDbText',
                    'height' => 'RDbText',
                    'values' => ['RDbText', 'forceTextArea'=>true, 'htmlOptions'=>['rows'=>10]],
                    'ordering' => 'RDbText',
                    'published' => 'RDbBoolean',
            );
    }
    
    public function relationParentReferenceOptions()
    {
            return [
                    'form' => array('controller' => 'form', 'relationName'=>'form', 'icon'=>'book'),
            ];
    }
    
}

?>
