<?php
namespace app\modules\form\models;

use app\modules\form\Module;

use \app\components\CommonActiveRecord as ActiveRecord;
use \app\components\CommonActiveQuery;

use yii\data\Sort;

class FormResult extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%form_results}}';
    }
    
    public function init()
    {
        parent::init();
        Module::registerTranslations();
        $this->_privateAdminName = \yii::t('modules/form/app', $this->_privateAdminName);
        $this->_privatePluralName = \yii::t('modules/form/app', $this->_privatePluralName);
        
    }
    
    public function afterFind()
    {
        parent::afterFind();
        
    }
    
    public function getForm()
    {
        return $this->hasOne(Form::className(), ['id' => 'form_id']);
    }
    
    public function attributeLabels() {
        
        return [
            'form_id' => \yii::t('modules/form/app', 'Form'),
            'text' => \yii::t('modules/form/app', 'Form result text'),
            'date' => \yii::t('modules/form/app', 'Date'),
        ];
        
    }
    
    public function rules()
    {
        return [
            [['id', 'form_id', 'date', 'text'], 'safe'],
            [['form_id'], 'number', 'integerOnly' => true, 'on'=>['insert', 'update']],
            [['form_id', 'date'], 'required', 'on'=>['insert', 'update']],
            [['id', 'form_id', 'date', 'text'], 'safe', 'on'=>'search'],
        ];
    }
    
    
    
    /*for crud admin*/
    public $_privateAdminName = 'Form result'; // One item name, used in breadcrums, in header in actionAdmin
    public $_privatePluralName = 'Form results';// Item set name
    public $_privateAdminRepr = 'date';// method or property used to retrieve model string represantation. It's close to __toString but only for admin
    
    
    public function getAdminFields()
    {
        return [
            //'form_id',
            'text',
            'date',
        ];
            
    }

    public function getFieldsDescription()
    {
        return [
            'form_id' => array('RDbRelation', 'form'),
            'date' => ['RDbDate', 'format' => 'php:Y-m-d H:i:s'],
            'text' => array('RDbText', 'truncateWords' => 100000),
        ];
    }
    public function relationParentReferenceOptions()
    {
            return [
                    'form' => array('controller' => 'form', 'relationName'=>'form', 'icon'=>'book'),
            ];
    }
    
    public function getGridButtonColumns($columnParams)
    {
        $columnParams['template'] = '{view} {delete}';
        return $columnParams;
    }
    
    public function getAdminMenu($privateModel, $menu = [])
    {
        
        return [];

    }
    public function getViewMenu($privateModel, $menu = [])
    {
        $result = [];
        
        foreach($menu as $m)
        {
            if(!isset($m['url'][0]) || $m['url'][0] != 'admin' && $m['url'][0] != 'delete')
                continue;
            $result[] = $m;
        }
        
        return $result;

    }
    
    public function searchQuery($data)
    {
        $query = parent::searchQuery($data);
        $query->orderBy(['date' => SORT_DESC]);
        return $query;
    }
}

?>
