<?php
namespace app\modules\form\models;

use app\modules\form\Module;
use yii\helpers\Html;
use yii\web\UploadedFile;
use app\components\SiteHelper;

use \app\components\CommonActiveRecord as ActiveRecord;

class Form extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%forms}}';
    }
    
    public function init()
    {
        parent::init();
        Module::registerTranslations();
        $this->_privateAdminName = \yii::t('modules/form/app', $this->_privateAdminName);
        $this->_privatePluralName = \yii::t('modules/form/app', $this->_privatePluralName);
    }
    
    public function attributeLabels() {
        
        return [
            'name' => \yii::t('modules/form/app', 'Name'),
            'alias' => \yii::t('modules/form/app', 'Alias'),
            'email' => \yii::t('modules/form/app', 'Email'),
            'subject' => \yii::t('modules/form/app', 'Mail subject'),
            'captcha' => \yii::t('modules/form/app', 'Captcha'),
            'onsubmit' => \yii::t('modules/form/app', 'JS on submit'),
            'send_to_email' => \yii::t('modules/form/app', 'Send to email'),
            'send_to_db' => \yii::t('modules/form/app', 'Send to DB'),
            'published' => \yii::t('modules/form/app', 'Published'),
            'desc' => \yii::t('modules/form/app', 'Desc'),
            'success_text' => \yii::t('modules/form/app', 'Success text'),
            'form_title' => \yii::t('modules/form/app', 'Form title'),
        ];
        
    }
    
    public function afterFind()
    {
        parent::afterFind();
        
    }
    
    public function getFields()
    {
        return $this->hasMany(FormField::className(), ['form_id' => 'id']);
    }
    public function getActiveFields()
    {
        return $this->hasMany(FormField::className(), ['form_id' => 'id'])->published();
    }
    
    public function getResults()
    {
        return $this->hasMany(FormResult::className(), ['form_id' => 'id']);
    }
    
    public function rules()
    {
        return [
            [['id', 'name', 'alias', 'email', 'desc', 'subject', 'onsubmit', 'send_to_email', 'send_to_db', 'published', 'success_text', 'form_title'], 'safe'],
            [['published', 'send_to_email', 'send_to_db', 'captcha'], 'number', 'integerOnly' => true, 'on'=>['insert', 'update']],
            [['name'], 'required', 'on'=>['insert', 'update']],
            [['alias'], 'app\components\AliasValidator', 'on'=>['insert', 'update'], 'skipOnEmpty'=>false],
            [['id', 'name', 'alias', 'email', 'desc', 'subject', 'onsubmit', 'send_to_email', 'send_to_db', 'published', 'success_text', 'form_title'], 'safe', 'on'=>'search'],
        ];
    }
    
    
    /*for crud admin*/
    public $_privateAdminName = 'Form'; // One item name, used in breadcrums, in header in actionAdmin
    public $_privatePluralName = 'Forms';// Item set name
    public $_privateAdminRepr = 'name';// method or property used to retrieve model string represantation. It's close to __toString but only for admin
    
    
    public function getAdminFields()
    {
            return array(
                    'name',
                    'alias',
                    'published',
            );
            
    }

    public function getFieldsDescription()
    {
            return [
                    'name' => 'RDbText',
                    'alias' => 'RDbText',
                    'email' => 'RDbText',
                    'subject' => 'RDbText',
                    'form_title' => 'RDbText',
                    'success_text'=>['RDbText', 'forceTextArea' => true],
                    'onsubmit'=>['RDbText', 'forceTextArea' => true],
                    'captcha' => 'RDbBoolean',
                    'send_to_email' => 'RDbBoolean',
                    'send_to_db' => 'RDbBoolean',
                    'desc' => 'RDbText',
                    'published' => 'RDbBoolean',
            ];
    }
    
    public function relationReferenceOptions()
    {
        return [
            'fields' => array('controller' => 'form-field', 'relationName'=>'form', 'icon'=>'list', 'color'=>'orange'),
            'results' => array('controller' => 'form-result', 'relationName'=>'form', 'icon'=>'comments'),
        ];
    }
    
    public function createFormModel()
    {
        $vars = [];
        
        $fields = $this->fields;
        foreach($fields as $field)
        {
            $vars[] = 'param'.$field->id;
        }
        if($this->captcha)
        {
            $vars[] = 'captcha';
        }
        
        $model = new \yii\base\DynamicModel($vars);
        
        foreach($fields as $field)
        {
            if($field->required)
                $model->addRule('param'.$field->id, 'required', ['message' => str_replace ('{attribute}', $field->name, \Yii::t('yii', '{attribute} cannot be blank.'))]);
            switch($field->type):
                case 'email':
                    $model->addRule('param'.$field->id, 'email', ['message' => str_replace ('{attribute}', $field->name, \Yii::t('yii', '{attribute} is not a valid email address.'))]);
                    break;
                case 'file':
                    $model->addRule('param'.$field->id, 'file', ['maxSize' => 1024*1024/*, 'message' => str_replace ('{attribute}', $field->name, \Yii::t('yii', '{attribute} is not a valid email address.'))*/]);
                    break;
            endswitch;
            $model->addRule('param'.$field->id, 'safe');
        }
        if($this->captcha)
        {
            $model->addRule('captcha', 'required', ['message' => str_replace ('{attribute}', \yii::t('modules/form/app', 'Captcha'), \Yii::t('yii', '{attribute} cannot be blank.'))]);
            $model->addRule('captcha', 'captcha', ['skipOnEmpty' => false, 'captchaAction' => '/site/captcha', 'except'=>'ajax']);
            
        }
        
        return $model;
        //$model->attributeLabels();
    }
    
    public function getFieldLabel($widget, $model, $field, $htmlOptions = [])
    {
        $fieldname = 'param'.$field->id;
        if($field->type != 'separator')
        {
            $htmlOptions['label'] = $field->name;
            return Html::activeLabel($model, $fieldname, $htmlOptions);
        }
        return '';
    }
    
    public function getFieldError($widget, $model, $field, $htmlOptions = [])
    {
        $fieldname = 'param'.$field->id;
        if($field->type != 'separator')
        {
            $htmlOptions['label'] = $field->name;
            return Html::error($model, $fieldname, $htmlOptions);
        }
        return '';
    }
    
    public function getFieldOutput($widget, $model, $field, $inputOptions = [])
    {
        $fieldname = 'param'.$field->id;
        //$inputOptions = [];
        $inputOptions['class'] = '';
        $options = ['class' => 'form-group clearfix'];
        
        $required_sign = '<sup>*</sup>';
        switch($field->type)
        {
            case 'text':
            case 'email':
            {
                if($field->width)
                    $inputOptions['width'] = $field->width;
                if($field->default_value)
                    $inputOptions['placeholder'] = $field->default_value;
                $inputOptions['type'] = $field->type;
                return $widget->field($model, $fieldname,[
                    'labelOptions' => ['label' => $field->name.($field->required?' '.$required_sign:'').':'],
                    'inputOptions' => $inputOptions,
                    'options' => $options,
                ])->textInput();
                //return Html::activeTextInput($model, $fieldname, $params['htmlOptions']);
            }
            case 'textarea':
            {
                if($field->width)
                    $inputOptions['cols'] = $field->width;
                if($field->height)
                    $inputOptions['rows'] = $field->height;
                if($field->default_value)
                    $inputOptions['placeholder'] = $field->default_value;
                
                return $widget->field($model, $fieldname,[
                    'labelOptions' => ['label' => $field->name.($field->required?' '.$required_sign:'').':'],
                    'inputOptions' => $inputOptions,
                    'options' => $options,
                ])->textArea();
            }
            case 'date':
            {
                $lang = \Yii::$app->language;
                $lang = explode('-', $lang);
                $lang = $lang[0];
                if($field->default_value)
                    $inputOptions['placeholder'] = $field->default_value;
                
                return $widget->field($model, $fieldname,[
                    'labelOptions' => ['label' => $field->name.($field->required?' '.$required_sign:'').':'],
                    'inputOptions' => $inputOptions,
                    'options' => $options,
                ])->widget(\yii\jui\DatePicker::classname(), [
                    'language' => $lang,
                    'dateFormat' => 'yyyy-MM-dd',
                    'clientOptions' => [
                        'defaultDate' => date('Y-m-d'),
                        'showOn'=> "both",
                        'buttonImageOnly'=> 1,
                        'buttonImage'=> \Yii::getAlias('@web2')."/../assets/privatepanel/calendar.png",
                    ]
                ]);
                
            }
            case 'checkbox':
            {
                if($field->default_value)
                    $inputOptions['checked'] = true;
                
                return $widget->field($model, $fieldname,[
                    'labelOptions' => ['label' => $field->name],
                    'inputOptions' => $inputOptions,
                    'options' => $options,
                ])->checkbox([], false);
            }
            case 'group':
            {
                $values = explode("\n", $field->values);
                $data = array();
                foreach($values as $v)
                {
                    $v = trim($v);
                    $data[$v] = ($v);
                }

                unset($inputOptions['class']);
                
                if($field->multiple)
                {
                    return $widget->field($model, $fieldname,[
                        'labelOptions' => ['label' => $field->name.($field->required?' '.$required_sign:'').':'],
                        'inputOptions' => $inputOptions,
                        'options' => $options,
                        
                    ])->checkboxList($data, []);
                    //return Html::activeCheckboxList($model, $fieldname,$data, $htmlOptions);
                } else {
                    return $widget->field($model, $fieldname,[
                        'labelOptions' => ['label' => $field->name.($field->required?' '.$required_sign:'').':'],
                        'inputOptions' => $inputOptions,
                        'options' => $options,
                    ])->radioList($data, []);
                    //return Html::activeRadioList($model, $fieldname,$data, $htmlOptions);
                }

            }
            case 'dropdown':
            {
                $values = explode("\n", $field->values);
                $data = array();
                if(!$field->required && $field->default_value && !in_array($field->default_value, $values))
                {
                    $data[''] = $field->default_value;
                } else {
                    if($field->default_value && in_array($field->default_value, $values))
                    {
                        $model->$fieldname = $field->default_value;
                    }
                }
                foreach($values as $v)
                {
                    $v = trim($v);
                    $data[$v] = ($v);
                }

                if($field->multiple)
                {
                    $inputOptions['multiple'] = true;
                    //$fieldname .= '[]';
                } else {

                }
                return $widget->field($model, $fieldname,[
                    'labelOptions' => ['label' => $field->name.($field->required?' '.$required_sign:'').':'],
                    'inputOptions' => $inputOptions,
                    'options' => $options,
                ])->dropDownList($data);

            }
            case 'file':
            {
                //return Html::activeFileInput($model, $fieldname);
                $inputOptions['class'] = 'file_js';
                return $widget->field($model, $fieldname,[
                    'labelOptions' => ['label' => $field->name.($field->required?' '.$required_sign:'').':'],
                    'inputOptions' => $inputOptions,
                    'options' => $options,
                ])->fileInput();
            }
            case 'separator':
            {
                return '<hr/>';
            }
        }
         
        return '';
    }
    
    public function getParamFilesDirectory()
    {
            return \yii::getAlias('@web2').'/'.'files/formupload/';
    }
    public function getFilesDirectory()
    {
            return \yii::getAlias('@web2').'/'.'files/formupload';
    }
    
    public function getValue($model, $field)
    {
        $fieldname = 'param'.$field->id;
        
        switch($field->type)
        {
            case 'text':
            case 'textarea':
            case 'date':
            {
                return isset($model->$fieldname)?$model->$fieldname:false;
            }
            case 'checkbox':
            {
                return (isset($model->$fieldname) && $model->$fieldname)?\Yii::t('yii', 'Yes'):\Yii::t('yii', 'No');
            }
            case 'group':
            case 'dropdown':
            {
                if($field->multiple)
                {
                    if(is_array($model->$fieldname))
                    {
                        $r = array();
                        foreach($model->$fieldname as $v)
                            if(trim($v))
                                $r[] = $v;
                        return isset($model->$fieldname)?implode(', ',$r):false;
                    } else
                        return isset($model->$fieldname)?$model->$fieldname:false;
                } else {
                    return isset($model->$fieldname)?$model->$fieldname:false;
                }

            }
            case 'file':
            {
                /*$file = UploadedFile::getInstance($model, $fieldname);
                if($file)
                {
                    $path = $this->getFilesDirectory();

                    if(!is_dir($path))
                    {
                        $path_parts = str_replace(\Yii::getAlias('@webroot').'/', '', $path);
                        $path_parts = explode('/',$path_parts);

                        $path_temp = '';
                        for($i = 0; $i<count($path_parts); $i++)
                        {
                            $path_temp.= $path_parts[$i].'/';
                            if(!file_exists(\Yii::getAlias('@webroot').'/'.$path_temp))
                            {
                                mkdir(\Yii::getAlias('@webroot').'/'.$path_temp, 0755);
                            }
                        }
                    }

                    $filename = SiteHelper::translit($file->baseName);

                    $k = 1;

                    while(file_exists($path.'/'.$filename. '.' . $file->extension))
                    {
                        $filename = SiteHelper::translit(trim($file->baseName)).'-'.($k++);
                    }
                    $file->saveAs($this->getFilesDirectory().$filename . '.' . $file->extension);

                    $model->$fieldname = $this->getFilesDirectory().$filename . '.' . $file->extension;
                } else {
                    if($model->getOldAttribute($field))
                        $model->$fieldname = $model->getOldAttribute($field);
                    else
                        $model->$fieldname = '';
                }*/
                return false;
                //return $model->$fieldname;
                //return $this->$fieldname->getName();
                //return false;
            }
        }
        return false;
    }
    
    public function getFileValue($model, $field)
    {
        $fieldname = 'param'.$field->id;
        /*$pathfunc = 'getParam'.$field->id.'Path';
        
        if($field->type == 'file')
        {
            $file = isset($this->$fieldname)?Yii::getPathOfAlias('root').'/'.$this->$pathfunc():false;
            if($file)
            {
                if(isset($_FILES[get_class($this)]) && count($_FILES[get_class($this)]))
                {
                    if(isset($_FILES[get_class($this)]['name'][$fieldname]))
                        return array($_FILES[get_class($this)]['name'][$fieldname]=>$file);
                }
                return array(str_replace(dirname($file),'',$file)=>$file);
            }
            
        }
        return false;*/
        
        if($field->type == 'file'):
            
            $value = false;
            
            $file = UploadedFile::getInstance($model, $fieldname);
            
            if($file)
            {
                $path = $this->getFilesDirectory();
                $path = $path.((substr($path,-1) != '/')?'/':'');
                $path1 = $path;
                
                $path = str_replace(\Yii::getAlias('@web2'), \Yii::getAlias('@webroot2'), $path);
                
                SiteHelper::checkFolderPath($path);
                /*if(!is_dir($path))
                {
                    $path_parts = str_replace(\Yii::getAlias('@webroot2').'/', '', $path);
                    
                    $path_parts = explode('/',$path_parts);

                    $path_temp = '';
                    for($i = 0; $i<count($path_parts); $i++)
                    {
                        $path_temp.= $path_parts[$i].'/';
                        if(!file_exists(\Yii::getAlias('@webroot2').'/'.$path_temp))
                        {
                            mkdir(\Yii::getAlias('@webroot2').'/'.$path_temp, 0755);
                        }
                    }
                }*/
                
                $filename = SiteHelper::translit($file->baseName);

                $k = 1;

                while(file_exists($path.$filename. '.' . $file->extension))
                {
                    $filename = SiteHelper::translit(trim($file->baseName)).'-'.($k++);
                }
                
                $file->saveAs($path.$filename . '.' . $file->extension);

                $value = [$file->baseName => $path1.$filename . '.' . $file->extension];
            } else {
                /*if($model->getOldAttribute($field))
                    $model->$fieldname = $model->getOldAttribute($field);
                else*/
                    $value = [];
            }

            return $value;
        else:
            return false;
        endif;
    }

}
?>
