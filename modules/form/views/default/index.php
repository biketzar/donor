<?php

use yii\widgets\ActiveForm;
use yii\helpers\Json;
use app\components\SiteHelper;
use yii\helpers\Url;
use yii\helpers\Html;
//use yii\bootstrap\Alert;

$page = SiteHelper::getCurrentPage();

$this->params['breadcrumbs'] = isset($this->params['breadcrumbs'])?$this->params['breadcrumbs']:[];
$this->params['breadcrumbs'] = array_merge($this->params['breadcrumbs'], SiteHelper::getCurrentPageBreadcrumbs(false));

$metaData = SiteHelper::metaData($this, [$form], [
    'title' => $page? $page->name:\Yii::t('app', 'Form'),
    'meta_title' => $page? $page->name:\Yii::t('app', 'Form'),
    'meta_description' => '',
    'meta_keywords' => '',
    ]);
//echo '<h1>'.$metaData['title'].'</h1>';
$this->params['pageTitle'] = $metaData['title'];
echo '<div class="content padtopbot20">';
//$this->params['breadcrumbs'][] = ['label' => $metaData['title']];
echo '<div class="col-xs-16">';
echo $form->desc;
echo '</div>';
echo '<div class="clearfix"></div>';
if(\Yii::$app->session->hasFlash('form_message'))
{
    /*echo Alert::widget([
        'options' => ['class' => 'alert-success'],
        'body' => \Yii::$app->session->getFlash('form_message'),
    ]);*/
    echo '<p class="text-success">'.\Yii::$app->session->getFlash('form_message').'</p>';
    if($form->onsubmit)
    {
        $this->registerJS($form->onsubmit, \yii\web\View::POS_LOAD);
    }
}

?>

<?php $widget = ActiveForm::begin([
	'id'=>strtolower($model->formName()).'-form',
	'enableClientValidation' => true,
	'options' => array(
		'enctype' => 'multipart/form-data',
		'class' => 'padtopbot30 col-sm-14 col-md-10 center-block',
	),

]);
if($form->form_title):
?>
<div class="form-group clearfix">
    <div class="col-sm-10">
      <legend><?php echo $form->form_title;?></legend>
    </div>
  </div>
<?php endif;?>
<?php echo $widget->errorSummary($model); ?>

<?php

foreach($form->activeFields as $k=>$field):

    //echo $form->getFieldLabel($widget, $model, $field);
    echo $form->getFieldOutput($widget, $model, $field);
    //echo $form->getFieldError($widget, $model, $field);

endforeach;

if($form->captcha)
{
    echo $widget->field($model, 'captcha',[
            'labelOptions' => ['label' => \yii::t('modules/form/app', 'Captcha Label').' <sup>*</sup>:'],
            'template' => "<div class=\"col-sm-4\">{label}</div>\n{input}\n{hint}\n{error}",
            'inputOptions' => ['class' => ''],
            'options' => ['class' => 'form-group clearfix'],
        ])->widget(\yii\captcha\Captcha::classname(), [
        'captchaAction' => '/site/captcha',
            'template' => '<div class="col-xs-8 col-sm-4">{input}</div> <div class="col-xs-8 col-sm-2">{image}</div>',

        // configure additional widget properties here
    ]);

}

?>
<div class="form-group clearfix">
    <div class="col-sm-4"></div>
    <div class="col-sm-5">
      <button type="submit" class="btn btn-success btn-block"><?php echo \yii::t('modules/form/app', 'Send');?></button>
    </div>
  </div>

<?php ActiveForm::end();
echo '</div>';
?>
