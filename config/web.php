<?php

define('ALKOHOST', preg_match("/alkohost/i", $_SERVER['HTTP_HOST']));

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    //'sourceLanguage' => 'en',
    'language' => 'ru-RU',
    'sourceLanguage' => 'en-US',
    
    'charset' => 'utf-8',
    'name' => 'Alchemy.Yii2',
    //'timeZone' => 'UTC',
    //'viewPath' => '@app/views',
	'aliases' => [
        '@web2' => '/web',
        '@webroot2' => dirname(__FILE__).'/../web',
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '2fy8lbANWACWO5NlY76xn8ZPdbAUQMgb',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'class' => 'app\models\User',
            'identityClass' => 'app\models\UserIndentity',
            'enableAutoLogin' => true,
            'loginUrl' => ['user/login'],
            'authTimeout' => 24*3600*30,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            //'useFileTransport' => true,
            'fileTransportPath' => '@runtime/mail',
            'htmlLayout' => '@app/views/mail/layout.php',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                    'enableRotation' => true,
                    'maxFileSize' => 2048,
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        'authManager' => [
            'class' => 'app\components\DbManager', // or use 'yii\rbac\DbManager'
            'defaultRoles' => ['guest'],
        ],
        'urlManager' => [
            'class' => 'app\components\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            /*'enableStrictParsing' => false,*/
            'rules' => [
				'login' => 'user/login',
                ['class' => 'app\components\PageUrlRule'],
                
            ],
        ],
        'session' => [
            'class' => 'yii\web\DbSession',
            'cookieParams' => ['httponly' => true, 'lifetime' => 3600],
            'timeout' => 3600,
            'useCookies' => true,
        ],
        'constants' => [
            'class' => 'app\components\ConstantHelper',
        ],
        'i18n' => [
            'translations' => [
                'app*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'fileMap' => [
                        'app' => 'app.php',
                        'app/layout' => 'layout.php',
                        'app/site' => 'site.php',
                        //'app' => 'app.php',
                    ],
                    //'on missingTranslation' => ['app\components\TranslationEventHandler', 'handleMissingTranslation']
                ],
                'common' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'fileMap' => [
                        'common' => 'common.php',
                    ],
                    //'on missingTranslation' => ['app\components\TranslationEventHandler', 'handleMissingTranslation']
                ],
                'modules/user/*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'sourceLanguage' => 'en-US',
                    'basePath' => '@app/modules/user/messages',
                    'fileMap' => [
                        'modules/user/app' => 'app.php',
                    ],
                ],
            ],
        ],
        
    ],
    'controllerMap' => [
        'elfinder' => [
            'class' => 'mihaildev\elfinder\Controller',
            'access' => ['@'], //глобальный доступ к фаил менеджеру @ - для авторизорованных , ? - для гостей , чтоб открыть всем ['@', '?']
            'disabledCommands' => ['netmount'], //отключение ненужных команд https://github.com/Studio-42/elFinder/wiki/Client-configuration-options#commands
            'roots' => [
                [
                    'baseUrl'=>'@web2',
                    'basePath'=>'@webroot2',
                    'path' => 'files',
                    'name' => 'Global',
                ],
                /*[
                    'class' => 'mihaildev\elfinder\UserPath',
                    'path'  => 'files/user_{id}',
                    'name'  => 'My Documents'
                ],
                [
                    'path' => 'files/some',
                    'name' => ['category' => 'my','message' => 'Some Name'] //перевод Yii::t($category, $message)
                ],
                [
                    'path'   => 'files/some',
                    'name'   => ['category' => 'my','message' => 'Some Name'], // Yii::t($category, $message)
                    'access' => ['read' => '*', 'write' => 'UserFilesAccess'] // * - для всех, иначе проверка доступа в даааном примере все могут видет а редактировать могут пользователи только с правами UserFilesAccess
                ]*/
            ],
            /*'watermark' => [
                    'source'         => __DIR__.'/logo.png', // Path to Water mark image
                     'marginRight'    => 5,          // Margin right pixel
                     'marginBottom'   => 5,          // Margin bottom pixel
                     'quality'        => 95,         // JPEG image save quality
                     'transparency'   => 70,         // Water mark image transparency ( other than PNG )
                     'targetType'     => IMG_GIF|IMG_JPG|IMG_PNG|IMG_WBMP, // Target image formats ( bit-field )
                     'targetMinPixel' => 200         // Target image minimum pixel size
            ]*/
        ],
        /*'tinymce' => [
            'class' => 'dosamigos\tinymce\TinyMce',
            'clientOptions' => []
        ]*/
    ],
    'params' => $params,
    'modules' => [
        /*'useradmin' => [
            'class' => 'mdm\admin\Module',
            'layout' => 'left-menu',
        ],*/
        'private' => [
            'class' => 'app\modules\privatepanel\Module',
            'layout' => 'main.php',
        ],
        'privateuser' => [
            'class' => 'app\modules\user\Module',
            //'layout' => '../../../../modules/privatepanel/views/layouts/main.php',
            'layout' => '@app/modules/privatepanel/views/layouts/main.php',
            /*'layout' => 'app\modules\privatepanel\views\layouts\main.php',
            'viewPath' => 'app\modules\user\views',*/
        ],
        'articles' => [
            'class' => 'app\modules\articles\Module',
        ],
        'form' => [
            'class' => 'app\modules\form\Module',
        ],
    ],
    'on beforeRequest' => function ($event) {
        //var_dump($event);
    },
    /*'as access' => [
        'class' => 'app\components\AccessControl',
        'rules' => [
            //['class' => 'app\components\ManagerRule'],
            ['class' => 'app\components\ActionRule'],
        ]
    ],*/
    
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
	//if($_SERVER['REMOTE_ADDR'] == '31.134.191.119')
	//{
	//	$config['bootstrap'][] = 'debug';
	//	$config['modules']['debug'] = [
	//			'class' => 'yii\debug\Module',
	//			'allowedIPs' => ['31.134.191.119']
	//		];
	//}

    /*$config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        //'allowedIPs' => ['127.0.0.1', '::1', '192.168.0.*', '192.168.178.20'] // adjust this to your needs
    ];*/
}

return $config;
