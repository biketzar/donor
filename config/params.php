<?php

return [
    'adminEmail' => 'admin@example.com',
    'breadcrumbs' => [],
    'translatedLanguages' => ['ru' => 'Russian', 'en' => 'English'],
    'defaultLanguage' => 'ru',
    'nonTranslatedModules' => ['private', 'privateuser', 'debug'],
    'projectSettings'=>json_decode(file_get_contents(CONFIG_FILE), FALSE) ? json_decode(file_get_contents(CONFIG_FILE), TRUE) : [],
];
