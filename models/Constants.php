<?php
namespace app\models;


class Constants extends \app\components\CommonActiveRecord
{
    
    const TYPE_TEXT = 'text';
    const TYPE_TEXTAREA = 'textarea';
    const TYPE_WYSIWYG = 'editor';
    const TYPE_INT = 'int';
    const TYPE_FLOAT = 'float';
    const TYPE_FILE = 'file';
    const TYPE_CHECKBOX = 'checkbox';
    const TYPE_EMAIL = 'mail';
    const TYPE_DATE = 'date';
        
    public static function tableName()
    {
        return '{{%constants}}';
    }
    
    public function init()
    {
        parent::init();
        
        $this->_privateAdminName = \yii::t('app', $this->_privateAdminName);
        $this->_privatePluralName = \yii::t('app', $this->_privatePluralName);
    }
    
    public function rules()
    {
        $result = [
            [['id', 'name', 'title', 'desc', 'value', 'type'], 'safe'],
            [['id', 'name', 'title', 'desc', 'value', 'type'], 'safe', 'on'=>['insert', 'update']],
            [['name', 'title'], 'required'],
            [['name'], 'app\components\AliasValidator'],
            [['type'], 'in', 'range' => array_keys(static::getTypes())],
            [['id', 'name', 'title', 'desc', 'value', 'type'], 'safe', 'on'=>'search'],
        ];
        switch ($this->type) {
                case self::TYPE_INT:
                        $rules[] = ['value', 'number', 'integerOnly'=>true];
                        break;
                case self::TYPE_FLOAT:
                        $rules[] = ['value', 'number'];
                        break;
                case self::TYPE_FILE:
                        $rules[] = ['value', 'file'];
                        break;
                case self::TYPE_CHECKBOX:
                        $rules[] = ['value', 'boolean'];
                        break;
                case self::TYPE_EMAIL:
                        $rules[] = ['value', 'email'];
                        break;
                case self::TYPE_DATE:
                        $rules[] = ['value', 'date', 'format' => 'yyyy-MM-dd'];
                        break;
                default:
                        break;
        }
        return $result;
    }
    
    public static function getTypes()
    {
        return [
            self::TYPE_TEXT => \Yii::t('app', 'String'),
            self::TYPE_TEXTAREA => \Yii::t('app', 'Text'),
            self::TYPE_WYSIWYG => \Yii::t('app', 'HTML Text'),
            self::TYPE_INT => \Yii::t('app', 'Integer'),
            self::TYPE_FLOAT => \Yii::t('app', 'Float'),
            self::TYPE_CHECKBOX => \Yii::t('app', 'Checkbox'),
            self::TYPE_EMAIL => \Yii::t('app', 'Email'),
            self::TYPE_DATE => \Yii::t('app', 'Date'),
            self::TYPE_FILE => \Yii::t('app', 'File'),
        ];
    }
    
    public function attributeLabels() {
        
        return [
            'name' => \yii::t('app', 'Name'),
            'title' => \yii::t('app', 'Title'),
            'desc' => \yii::t('app', 'Description'),
            'value' => \yii::t('app', 'Value'),
            'type' => \yii::t('app', 'Type'),
            'published' => \yii::t('app', 'Published'),
        ];
        
    }
    
    
    /*for crud admin*/
    public $_privateAdminName = 'Constant'; // One item name, used in breadcrums, in header in actionAdmin
    public $_privatePluralName = 'Constants';// Item set name
    public $_privateAdminRepr = 'title';// method or property used to retrieve model string represantation. It's close to __toString but only for admin
    
    
    public function getAdminFields()
    {
            return array(
                    'name',
                    'title',
                    'desc',
                    'value',
                    'type',
            );
            
    }

    public function getFieldsDescription()
    {
            $result = array(
                    'name' => 'RDbText',
                    'title' => 'RDbText',
                    'desc' => 'RDbText',
                    'type' => array('RDbSelect', 'data'=> static::getTypes()),
            );
            switch($this->type)
            {
                case self::TYPE_TEXT:
                case self::TYPE_INT:
                case self::TYPE_FLOAT:
                case self::TYPE_EMAIL:
                default:
                    $result['value'] = array('RDbText','forceTextInput' => true);
                    break;
                case self::TYPE_TEXTAREA:
                    $result['value'] = array('RDbText','forceTextArea' => true);
                    break;
                case self::TYPE_WYSIWYG:
                    $result['value'] = array('RDbText');
                    break;
                case self::TYPE_FILE:
                    $result['value'] = array('RDbFile');
                    break;
                case self::TYPE_CHECKBOX:
                    $result['value'] = array('RDbBoolean');
                    break;
                case self::TYPE_DATE:
                    $result['value'] = array('RDbDate');
                    break;
            }
            return $result;
    }
    
    public function getValueDirectory()
    {
        return 'files/constants';
    }
    
    public function getValuePath()
    {
        if(!$this->value)
            return '';
        return 'files/constants/'.$this->value;
    }
    
    public function searchQuery($data)
    {
        $className = $this->className();
        $formName = $this->formName();
        
        /*if(isset($data[$formName]))
            $data = $data[$formName];*/
        $query = $className::find();
        
        $this->load($data);
        
        $query->andFilterWhere(['like', 'name', $this->name]);
        $query->andFilterWhere(['like', 'title', $this->title]);
        $query->andFilterWhere(['like', 'title', $this->desc]);
        $query->andFilterWhere(['type' => $this->type]);
        
        $getParams = \Yii::$app->request->getQueryParams(); 
        
        if(isset($getParams))
        {
            foreach($getParams as $param=>$value)
            {
                if(isset($value))
                {
                    if($relation = $this->getRelation($param, false))
                    {
                        /*$p = array_values($relation->link);
                        if(!$relation->multiple)
                        {
                            foreach($relation->link as $a=>$b)
                                $query->andFilterWhere([$b => $value]);
                            
                        } else {
                            
                            $query->with($param)->andFilterWhere([$param => $value]);
                            
                        }
                        $createUrlParams[$param] = $value;*/
                    } else {
                        if($this->hasAttribute($param))
                        {
                            $this->$param = $value;
                            $query->andFilterWhere([$param => $value]);
                        }
                        
                    }
                }
            }
            foreach($getParams as $param=>$value)
            {
                if(isset($value))
                {
                    if($relation = $this->getRelation($param, false))
                    {
                        $p = array_values($relation->link);
                        if(!$relation->multiple)
                        {
                            foreach($relation->link as $a=>$b)
                                $query->andFilterWhere([$b => $value]);
                            
                        } else {
                            
                            $query->with($param)->andFilterWhere([$param => $value]);
                            
                        }
                        $createUrlParams[$param] = $value;
                    } else {
                        /*if($this->hasAttribute($param))
                            $query->andFilterWhere([$param => $value]);
                        */
                    }
                }
            }
        }
        
        $query->orderBy(['title' => SORT_ASC]);
        
        
        
        return $query;
    }

}
?>
