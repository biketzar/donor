<?php
namespace app\models;

use app\components\CommonActiveRecord as ActiveRecord;

class MailTemplate extends ActiveRecord
{
    public $to = false;
    public static function tableName()
    {
        return '{{%mail_templates}}';
    }
    
    public function init()
    {
        parent::init();
        
        $this->_privateAdminName = \yii::t('app', $this->_privateAdminName);
        $this->_privatePluralName = \yii::t('app', $this->_privatePluralName);
    }
    
    public function rules()
    {
        return [
            [['id', 'name', 'alias', 'from', 'subject', 'content', 'published'], 'safe'],
            
            [['id', 'published'], 'number', 'integerOnly' => true],
            [['name'], 'required', 'on'=>['insert', 'update']],
            [['alias'], 'app\components\AliasValidator', 'on'=>['insert', 'update']],
            //[['alias'], 'unique'],
            [['id', 'name', 'alias', 'from', 'subject', 'content', 'published'], 'safe', 'on'=>'search'],
        ];
    }
    
    public function attributeLabels() {
        
        return [
            'name' => \yii::t('app', 'Name'),
            'alias' => \yii::t('app', 'Alias'),
            'from' => \yii::t('app', 'Mail From'),
            'content' => \yii::t('app', 'Mail Content'),
            'subject' => \yii::t('app', 'Mail Subject'),
            'published' => \yii::t('app', 'Published'),
        ];
        
    }
    
    
    /*for crud admin*/
    public $_privateAdminName = 'Mail template'; // One item name, used in breadcrums, in header in actionAdmin
    public $_privatePluralName = 'Mail templates';// Item set name
    public $_privateAdminRepr = 'name';// method or property used to retrieve model string represantation. It's close to __toString but only for admin

    public function getAdminFields()
    {
            return array(
                    'name',
                    'alias',
                    'published',
            );
            
    }

    public function getFieldsDescription()
    {
            return array(
                    'name' => 'RDbText',
                    'alias' => 'RDbText',
                    'from' => 'RDbText',
                    'subject' => 'RDbText',
                    'content' => 'RDbText',
                    'published' => 'RDbBoolean',
            );
    }

}
?>
