<?php
namespace app\models;

use app\components\CommonActiveRecord as ActiveRecord;

class Metadata extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%metadata}}';
    }
    
    public function init()
    {
        parent::init();
        $this->_privateAdminName = \yii::t('app', $this->_privateAdminName);
        $this->_privatePluralName = \yii::t('app', $this->_privatePluralName);
        
    }
    
    public function rules()
    {
        return [
            [['id', 'title', 'meta_title', 'meta_description', 'meta_keywords', 'model', 'item_id'], 'safe'],
            [['model', 'item_id'], 'required', 'on'=>['insert', 'update']],
            [['id', 'title', 'meta_title', 'meta_description', 'meta_keywords', 'model', 'item_id', 'item'], 'safe', 'on'=>'search'],
        ];
    }
    
    public function attributeLabels() {
        
        return [
            'title' => \yii::t('app', 'Title'),
            'meta_title' => \yii::t('app', 'Meta Title'),
            'meta_description' => \yii::t('app', 'Meta Description'),
            'meta_keywords' => \yii::t('app', 'Meta Keywords'),
            
        ];
        
    }
    
    public function getItem()
    {
        if($this->model && class_exists($this->model))
        {
            return $this->hasOne($this->model, ['id' => 'item_id']);
        } else
            return $this->hasOne(self::className(), ['id' => 'id']);
    }
    
    /*for crud admin*/
    public $_privateAdminName = 'Meta data'; // One item name, used in breadcrums, in header in actionAdmin
    public $_privatePluralName = 'Meta data';// Item set name
    public $_privateAdminRepr = 'title';
    
    public function getAdminFields()
    {
            return array(
                    'title',
                    'meta_title',
                    'meta_description',
                    'meta_keywords',
            );
            
    }

    public function getFieldsDescription()
    {
            return array(
                    'item_id' => 'RDbHidden',
                    'model' => 'RDbHidden',
                    'title' => 'RDbText',
                    'meta_title' => 'RDbText',
                    'meta_description' => array('RDbText', 'forceTextArea' => true),
                    'meta_keywords' => array('RDbText', 'forceTextArea' => true),
            );
    }
    
    public function getViewExcludedFields()
    {
        return ['item_id', 'model'];
    }
    
    public function relationParentReferenceOptions()
    {
        $result = [];
        if($this->model)
        {
            $privateModelAdmin = \Yii::createObject(['class' => 'app\modules\privatepanel\components\PrivateModelAdmin'],[$this]);
            $result = array_merge($result, $privateModelAdmin->resolveParent($this->model, 'item'));
        }
        return $result;
    }
    
    public function getCreateMenu($privateModel, $menu = [])
    {
        return [];
    }
    public function getUpdateMenu($privateModel, $menu = [])
    {
        return [];
    }
    
    
    
}
?>
