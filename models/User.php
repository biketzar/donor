<?php

namespace app\models;

use app\components\CommonActiveQuery;

class User extends \yii\web\User
{

    public function getStatusName()
    {
        $items = self::getStatuses();
        return (isset($items[$this->getIdentity()->status])?$items[$this->getIdentity()->status]:'');
    }
    
    public function getFullName()
    {
        return ($this->getIdentity())?(($this->getIdentity()->name)?$this->getIdentity()->name:(($this->getIdentity()->username)?$this->getIdentity()->username:$this->getIdentity()->email)):'';
    }
    
    protected function beforeLogin($identity, $cookieBased, $duration)
    {
        //if($identity->status != UserIndentity::STATUS_ACTIVE)
        //    return false;
        return parent::beforeLogin($identity, $cookieBased, $duration);
    }
    protected function afterLogin($identity, $cookieBased, $duration)
    {
        $identity->last_visit = date('Y-m-d H:i:s');
        $identity->update(false, ['last_visit']);
        return parent::afterLogin($identity, $cookieBased, $duration);
    }
    
}
