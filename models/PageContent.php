<?php
namespace app\models;

//use yii\db\ActiveRecord;

class PageContent extends \app\components\CommonActiveRecord
{
    public static function tableName()
    {
        return '{{%page_content}}';
    }
    
    public function init()
    {
        parent::init();
        
        $this->_privateAdminName = \yii::t('app', $this->_privateAdminName);
        $this->_privatePluralName = \yii::t('app', $this->_privatePluralName);
    }
    
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        return array_merge($behaviors, [
            'ml' => [
                'class' => \app\components\MultilingualBehavior::className(),
                //'attributes' => [
                    
                    'localizedAttributes' => [
                        'name',
                        'text'
                    ],
                    'strict' => false,
                //],
            ],
        ]);
    }
    
    public static function find()
    {
        $query = new PageContentActiveQuery(get_called_class());
        return $query;
    }
    
    public function afterFind()
    {
        parent::afterFind();
        $this->params = json_decode($this->params, true);    
        
    }
    
    public function getPage()
    {
        return $this->hasOne(Page::className(), ['id' => 'page_id']);
    }
    /*public function getHandler()
    {
        return $this->hasOne(Handler::className(), ['id' => 'handler_id']);
    }
    */
    public function rules()
    {
        return [
            [['id', 'name', 'page_id', 'text', 'published', 'ordering', 'params'], 'safe'],
            
            [['id', 'page_id'], 'integer'],
            [['name', 'page_id'], 'required', 'on'=>['insert', 'update']],
            [['text'], 'safe', 'on'=>['insert', 'update']],
            //[['inmap', 'insearch'], 'default', 'value'=>1],
            
            [['id', 'name', 'page_id', 'text', 'published', 'ordering', 'params'], 'safe', 'on'=>'search'],
        ];
    }
    
    public function attributeLabels() {
        
        return [
            'name' => \yii::t('app', 'Name'),
            'alias' => \yii::t('app', 'Alias'),
            'published' => \yii::t('app', 'Published'),
            'text' => \yii::t('app', 'Text'),
            
            'ordering' => \yii::t('app', 'Ordering'),
        ];
        
    }
    
    
    /*for crud admin*/
    public $_privateAdminName = 'Page Content'; // One item name, used in breadcrums, in header in actionAdmin
    public $_privatePluralName = 'Page Contents';// Item set name
    public $_privateAdminRepr = 'name';// method or property used to retrieve model string represantation. It's close to __toString but only for admin

    public function getAdminFields()
    {
            return array(
                    'name',
                    'published',
                    'ordering'
            );
            
    }

    public function getFieldsDescription()
    {
            return array(
                    'page_id' => array('RDbRelation', 'page', 'treeParentRel'=>'parent', 'treeChildRel' => 'children'),
                    'name' => 'RDbText',
                    'text' => 'RDbText',
                    'published' => 'RDbBoolean',
                    'ordering' => 'RDbText',
            );
    }

    public function relationParentReferenceOptions()
    {
            return array(
                    'page' => array('controller' => 'page', 'relationName'=>'page', 'icon'=>'book'),
            );
    }

    /**
     * Добавление в конце столбца подписей
     * @return array 
     */
    /*public function getChangedFieldsForClone() {
        return array(
            'header' => '_clone',
            'title' => '_clone',
        );
    }*/
}

class PageContentActiveQuery extends \app\components\CommonActiveQuery
{
    public function page($page_id = 1)
    {
        return $this->andWhere('page_id=:page_id', [':page_id' => intval($page_id)]);
    }
}
?>
