<?php
namespace app\models;

class Dcard extends \app\components\CommonActiveRecord
{
    public static function tableName()
    {
        return '{{%dcard}}';
    }
    
    public static function find() {
        return parent::find()->orderBy('surname, name');
    }
    
    public function init()
    {
        parent::init();
        
        $this->_privateAdminName = \yii::t('app', $this->_privateAdminName);
        $this->_privatePluralName = \yii::t('app', $this->_privatePluralName);
    }    
    
    public function getMetadata()
    {
        return $this->hasOne(\app\models\Metadata::className(), ['item_id' => 'id'])
                ->where('model = :modelName', ['modelName' => self::className()]);
    }
    
    public function rules()
    {
        return [
            [['name','surname', 'pname', 'blood_count', 'id', 'department_id', 'is_budget'], 'safe', 'on' => 'search'],
            [['name','surname', 'pname'], 'string', 'max' => 255, 'on' => ['insert', 'update']],
            [['group_num'], 'string', 'max' => 64, 'on' => ['insert', 'update']],
            [['blood_count', 'department_id', 'is_budget'], 'integer', 'on' => ['insert', 'update']],
        ];
    }
    
    
    public function beforeSave($insert) {
        if(parent::beforeSave($insert)){
            $this->name = mb_convert_case(mb_strtolower($this->name, 'UTF-8'), MB_CASE_TITLE, "UTF-8");
            $this->surname = mb_convert_case(mb_strtolower($this->surname, 'UTF-8'), MB_CASE_TITLE, "UTF-8");
            $this->pname = mb_convert_case(mb_strtolower($this->pname, 'UTF-8'), MB_CASE_TITLE, "UTF-8");
            return true;
        }
        return false;
}
    
    
    public function attributeLabels() {
        
        return [
            'name' => 'Имя',
            'surname' => 'Фамилия',
            'pname' => 'Отчество',
            'blood_count' => 'Всего сдал',
            'group_num' => 'Номер группы',
            'department_id' => 'Подразделение',
            'is_budget' => 'Бюджет',
        ];
        
    }


    /*for crud admin*/
    public $_privateAdminName = 'Карточка донора'; // One item name, used in breadcrums, in header in actionAdmin
    public $_privatePluralName = 'Карточки донора';// Item set name
    public $_privateAdminRepr = 'name';// method or property used to retrieve model string represantation. It's close to __toString but only for admin
    
    public function getAdminReprString()
    {
        return $this->surname.' '.$this->name.' '.$this->pname;
    }

    public function getAdminFields()
    {
        return array(
                'surname',
                'name',
                'pname',
                'department_id',
                'group_num',
                'is_budget',
        );
    }
    public function getDepartament()
    {
        return $this->hasOne(Departments::className(), ['id' => 'department_id']);
    }

    public function getFieldsDescription()
    {
        return [
            'surname' => 'RDbText',
            'name' => 'RDbText',
            'pname' => 'RDbText',
            'department_id' => ['RDbRelation', 'departament'],
            'group_num' => 'RDbText',
            'is_budget' => 'RDbBoolean',
            //'blood_count' => 'RDbText',
            ];
    }

    public function relationReferenceOptions()
    {
            return array(
                    //'contents' => array('controller' => 'page-content', 'relationName'=>'page', 'icon'=>'book'),
                    
            );
    }
}
