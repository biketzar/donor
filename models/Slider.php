<?php
namespace app\models;

use app\components\CommonActiveRecord as ActiveRecord;

class Slider extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%slider}}';
    }
    
    public function init()
    {
        parent::init();
        $this->_privateAdminName = \yii::t('app', $this->_privateAdminName);
        $this->_privatePluralName = \yii::t('app', $this->_privatePluralName);
        
    }
    
    public function rules()
    {
        return [
            [['id', 'name', 'dsc', 'ordering', 'published'], 'safe'],
            [['published', 'ordering'], 'number', 'integerOnly' => true, 'on'=>['insert', 'update']],
            [['name', 'link'], 'required', 'on'=>['insert', 'update']],
            [['image'], 'app\components\CustomImageValidator', 'on'=>['insert', 'update']],
            
            [['id', 'name', 'dsc', 'link','published', 'ordering'], 'safe', 'on'=>'search'],
        ];
    }
    
    public function attributeLabels() {
        
        return [
            'name' => \yii::t('app', 'Name'),
            'link' => 'Ссылка',
            'published' => \yii::t('app', 'Published'),
            'image' => \yii::t('app', 'Image'),
            'ordering' => \yii::t('app', 'Ordering'),
            'dsc' => 'Текст',
        ];
        
    }
    
    /*for crud admin*/
    public $_privateAdminName = 'Сладйер'; // One item name, used in breadcrums, in header in actionAdmin
    public $_privatePluralName = 'Слайдер';// Item set name
    public $_privateAdminRepr = 'name';
    
    public function getAdminFields()
    {
            return array(
                    'name',
                    'link',
                    'dsc',
                    'published',
                    'ordering',
            );
            
    }

    public function getFieldsDescription()
    {
            return array(
                    'name' => 'RDbText',
                    'image' => 'RDbFile',
                    'dsc' => 'RDbText',
                    'link' => 'RDbText',
                    'published' => 'RDbBoolean',
                    
                    'ordering' => 'RDbText',
            );
    }
    
    
    public function getImageDirectory()
    {
        
        return \yii::getAlias('@web2').'/files/slider';
    }
    
    public function getImagePath()
    {
        if(!$this->image)
            return '';
		
        return $this->getImageDirectory().'/'.$this->image;
    }
    
    
    public function beforeDelete() {
        
        if(file_exists($this->getImagePath()))
            unlink($this->getImagePath());
        return parent::beforeDelete();
    }
    
}
?>
