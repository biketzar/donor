<?php

namespace app\models;

use Yii;
use yii\base\Model;

class SearchForm extends Model
{
	public $query;
	public $message;
	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return [
			['query', 'required'],
			['query', 'string', 'min' => 2],
		];
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return [
			'query'=>\yii::t('app', 'Enter search words'),
		];
	}

}