<?php

namespace app\models;

use yii\db\ActiveRecord;

use \app\components\CommonActiveQuery;

class Donor extends \app\components\CommonActiveRecord implements \yii\web\IdentityInterface
{
    //public $id;
    //public $username;
    //public $password;
    //public $authKey;
    //public $access_token;
    const STATUS_INACTIVE = '0';
    const STATUS_ACTIVE = '1';
    const STATUS_BLOCKED = '2';
    const STATUS_SPRING2016 = '20161';
    
    public $password_repeat = null;
    public static function tableName()
    {
        return '{{%donors}}';
    }
    
    public function init()
    {
        parent::init();
        
        $this->_privateAdminName = \yii::t('app', $this->_privateAdminName);
        $this->_privatePluralName = \yii::t('app', $this->_privatePluralName);
    }
    
    public static function find()
    {
        $query = new DonorActiveQuery(get_called_class());
        return $query;
    }
    
    public function rules()
    {
        return [
            [['dcard_id'], 'safe', 'on'=>'dcard'],
            [['name','surname', 'department_id', 'donorstatus', 'blood_status', 'is_budget', 'sex', 'marrow_status','pname', 'gradebook', 'group_num', 'phone', 'email', 'status', 'last_visit', 'weight', 'typing'], 'safe', 'on' => 'search'],
            [['pass_id', 'pass_series', 'inn', 'snils', 'subdiv_code', 'name','surname', 'pname', 'group_num', 'reg_addr', 'birth_place', 'sex'],'filter','filter'=>'\yii\helpers\HtmlPurifier::process','on' => ['insert', 'update', 'setdate']],
            [['name', 'email', 'status', 'password_repeat', 'is_budget', 'group_num', 'sex'], 'safe'],
            [['name',  'email', 'status', 'password', 'email', 'is_budget', 'group_num', 'issued', 'reg_addr', 'birth_place', 'sex', 'unsubscribe'], 'safe', 'on' => ['insert', 'update']],
            [['email','name','surname', 'gradebook','phone','department_id','pass_id','pass_series','birth_date','issued_date','inn','snils','subdiv_code', 'weight'], 'required', 'on' => ['insert', 'update']],
            [['password'], 'required', 'on' => ['insert']],
            [['dcard_id', 'blood_status', 'marrow_status','donorstatus', 'department_id', 'sex', 'weight', 'typing'], 'integer', 'on' => ['insert']],
            ['password', 'compare', 'on' => ['insert', 'update'], 'message' =>'Пароли не совпадают'],
            ['status', 'default', 'value' => self::STATUS_ACTIVE, 'on' => ['insert', 'update']],
            ['status', 'in', 'range' => array_keys(self::getStatuses())],
            [['pass_id'], 'string', 'length' => 6, 'on' => ['insert', 'update']],
			[['pass_series'], 'string', 'length' => 4, 'on' => ['insert', 'update']],
			[['birth_date'], 'date', 'format' => 'dd.MM.yyyy', 'on' => ['insert', 'update']],
			[['issued_date'], 'date', 'format' => 'dd.MM.yyyy', 'on' => ['insert', 'update']],
			[['inn'], 'string', 'length' => 12, 'on' => ['insert', 'update']],
			[['snils'], 'string', 'max' => 20, 'min' => 11, 'on' => ['insert', 'update']],
			[['subdiv_code'], 'string', 'max' => 16, 'min' => 3, 'on' => ['insert', 'update']],
			[['password'], 'string', 'max' => 255, 'min' => 6, 'on' => ['insert', 'update']],
            [['name','surname', 'pname'], 'string', 'max' => 255, 'on' => ['insert', 'update']],
            //[['name','surname', 'pname'], 'unique', 'targetAttribute' => ['name','surname', 'pname'], 'on' => ['insert', 'update']],
            //[['username'], 'match', 'pattern' => '/^[a-zA-Z0-9]+$/', 'message' => 'Логин может состоять только из букв английского алфавита и цифр', 'on' => ['insert', 'update']],
            [['email'], 'email', 'on' => ['insert', 'update']],
            ['email', 'unique', 'targetClass' => '\app\models\Donor', 'message' => \Yii::t('app','This email address has already been taken.')],
        ];    
    }
    
    public function attributeLabels() {
        
        return [
            'name' => \yii::t('modules/user/app', 'Person Name'),
            'username' => \yii::t('modules/user/app', 'Username'),
            'email' => \yii::t('modules/user/app', 'Email'),
            'status' => \yii::t('modules/user/app', 'Status'),
            'password' => \yii::t('modules/user/app', 'Password'),
            'password_repeat' => \yii::t('modules/user/app', 'Password repeat'),
            'last_visit' => \yii::t('modules/user/app', 'Last Visit'),
            'roles' => \yii::t('modules/user/app', 'Roles'),
            'create_time' => \yii::t('modules/user/app', 'Create Time'),
            'update_time' => \yii::t('modules/user/app', 'Update Time'),
			'is_budget' => 'Бюджет',
			'department_id' => 'Подразделение',
            'name' => 'Имя',
            'surname' => 'Фамилия',
            'pname' => 'Отчество',
            'group_num' => 'Номер группы',
            'gradebook' => 'Номер зачетки',
            'phone' => 'Телефон',
            'dcard_id' => 'Карточка донора',
            'donorstatus' => 'Статус донора',
            'blood_status' => 'Статус крови',
            'marrow_status' => 'Донор костного мозга',
            'inn' => 'ИНН',
            'snils' => 'СНИЛС',
            'pass_id' => 'Номер паспорта',
            'pass_series' => 'Серия паспорта',
            'birth_date' => 'Дата рождения',
            'birth_place' => 'Место рождения',
            'issued' => 'Паспорт выдан',
            'issued_date' => 'Дата выдачи',
            'subdiv_code' => 'Код подразделения',
            'reg_addr' => 'Адрес регистрации по паспорту',
            'sex' => 'Пол',
            'weight' => 'Вес',
            'typing' => 'Сдача на выезде'
        ];
        
    }
    
    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }

    /**
     * Finds user by username
     *
     * @param  string      $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        //return static::findOne(['username' => $username]);
        return static::find()->andWhere('email=:username', [':username' => $username])->one();
    }
    
    /**
     * Finds active user by username
     *
     * @param  string      $username
     * @return static|null
     */
    public static function findActiveByUsername($username)
    {
        return static::find()->andWhere('email=:username', [':username' => $username])->andWhere(['status' => 1])->one();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param  string  $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return \Yii::$app->getSecurity()->validatePassword($password, $this->password);
    }
    
    public function beforeSave($insert)
    {
        $this->name = mb_convert_case(mb_strtolower($this->name, 'UTF-8'), MB_CASE_TITLE, "UTF-8");
        $this->surname = mb_convert_case(mb_strtolower($this->surname, 'UTF-8'), MB_CASE_TITLE, "UTF-8");
        $this->pname = mb_convert_case(mb_strtolower($this->pname, 'UTF-8'), MB_CASE_TITLE, "UTF-8");
        $dirtyAttributes = $this->getDirtyAttributes();
        if(isset($dirtyAttributes['password']) && $dirtyAttributes['password'])
        {
            $this->password = \Yii::$app->getSecurity()->generatePasswordHash($dirtyAttributes['password']);
        } else {
            $this->password = $this->getOldAttribute('password');
        }
        
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->access_token = \Yii::$app->getSecurity()->generateRandomString();
            }
            return true;
        }
        return false;
    }
    
    
    
    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->access_token = \Yii::$app->security->generateRandomString();
    }
    
    public function generatePassword()
    {
        $this->password = \Yii::$app->security->generateRandomString(8);
    }

    /**
     * Removes password reset token
     */
    public function removeAccessToken()
    {
        $this->access_token = null;
    } 
    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        $expire = \Yii::$app->params['user.passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int) end($parts);
        if ($timestamp + $expire < time()) {
            // token expired
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }
    
    
    public static function getStatuses()
    {
        return [
            self::STATUS_ACTIVE => \Yii::t('app', 'Active'),
            self::STATUS_INACTIVE => \Yii::t('app', 'Inactive'),
            self::STATUS_BLOCKED => \Yii::t('app', 'Blocked'),
        ];
    }
    
    public function getStatusName()
    {
        $items = self::getStatuses();
        return (isset($items[$this->status])?$items[$this->status]:'');
    }
    
    public function getFullName()
    {
        return $this->name;
    }
    
    public function afterFind() {
        $this->adminnRepresentstion = $this->getAdminReprString();
        parent::afterFind();
    }




    /*for crud admin*/
    public $_privateAdminName = 'User'; // One item name, used in breadcrums, in header in actionAdmin
    public $_privatePluralName = 'Users';// Item set name
    public $_privateAdminRepr = 'surname';
    public $adminnRepresentstion = '';
    public function getAdminReprString()
    {
        return $this->surname.' '.$this->name.' '.$this->pname.' '.$this->group_num;
    }
    
    public function getDcard()
    {
        return $this->hasOne(Dcard::className(), ['id' => 'dcard_id']);
    }
    
    public function getDonorstatus()
    {
        return $this->hasOne(Donorstatus::className(), ['id' => 'donorstatus']);
    }
	
	public function getDepartament()
    {
        return $this->hasOne(Departments::className(), ['id' => 'department_id']);
    }
	
	public static function getDepartments()
	{
		$departments = Departments::find()->all();
		$result = [];
		foreach($departments as $department)
		{
			$result[$department->id] = $department->name;
		}
		return $result;
	}
    
    public function getAdminFields()
	{
		return array(
                        'donorstatus',
                        'surname',
			'name',
			'pname',
                        'gradebook',
			'group_num',
                        'phone',
                        'email',
                        'dcard_id',
						'department_id', 
						'is_budget',
		);
	}
    
    
    public function getFieldsDescription()
    {
        return array(
            'donorstatus'=>['RDbRelation', 'donorstatus'],
            'surname' => 'RDbText',
            'name' => 'RDbText',
            'pname' => 'RDbText',
			'department_id' => ['RDbRelation', 'departament'],
			'is_budget' => 'RDbBoolean',
            'group_num' => 'RDbText',
            'phone' => 'RDbText',
            'email' => 'RDbText',
            'gradebook' => 'RDbText',
            'dcard_id' => ['RDbRelation', 'dcard'],
            'blood_status' => ['RDbSelect', 'data'=>[
                '0'=>'нет данных', 
                '1'=>'активна',
                '2'=>'нуждается в активации',
                '3'=>'в ожидании активации',
                ]],
            'marrow_status' => 'RDbBoolean',
        );
    }
    
}

class DonorActiveQuery extends CommonActiveQuery
{
    public function status($status)
    {
        if($status !== null)
            return $this->andWhere(['status=:status', [':status' => intval($status)]]);
        else
            return $this;
    }
    
}
