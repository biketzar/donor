<?php
namespace app\models;

class Departments extends \app\components\CommonActiveRecord
{
    public static function tableName()
    {
        return '{{%departments}}';
    }
    
    public function init()
    {
        parent::init();
        
        $this->_privateAdminName = \yii::t('app', $this->_privateAdminName);
        $this->_privatePluralName = \yii::t('app', $this->_privatePluralName);
    }    
    
    
    public function rules()
    {
        return [
            [['name'], 'safe', 'on' => 'search'],
            [['name'], 'string', 'max' => 64, 'on' => ['insert', 'update']],
        ];
    }    
    
    public function attributeLabels() {
        
        return [
            'name' => 'Название',
        ];
        
    }


    /*for crud admin*/
    public $_privateAdminName = 'Подразделения'; // One item name, used in breadcrums, in header in actionAdmin
    public $_privatePluralName = 'Подразделения';// Item set name
    public $_privateAdminRepr = 'name';// method or property used to retrieve model string represantation. It's close to __toString but only for admin
    

    public function getAdminFields()
    {
                    return array(
                            'name',

                    );
    }

    public function getFieldsDescription()
    {
        return [
            'name' => 'RDbText',
            ];
    }
}
