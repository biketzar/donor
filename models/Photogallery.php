<?php
namespace app\models;

use app\components\CommonActiveRecord as ActiveRecord;

class Photogallery extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%photogallery}}';
    }
    
    public function init()
    {
        parent::init();
        $this->_privateAdminName = \yii::t('app', $this->_privateAdminName);
        $this->_privatePluralName = \yii::t('app', $this->_privatePluralName);
        
    }
    
    public function rules()
    {
        return [
            [['id', 'name', 'model', 'item_id', 'ordering', 'published'], 'safe'],
            [['published', 'ordering'], 'number', 'integerOnly' => true, 'on'=>['insert', 'update']],
            [['model', 'item_id'], 'required', 'on'=>['insert', 'update']],
            [['image'], 'app\components\CustomImageValidator', 'on'=>['insert', 'update']],
            
            [['id', 'name', 'model', 'item_id','published', 'item', 'ordering'], 'safe', 'on'=>'search'],
        ];
    }
    
    public function attributeLabels() {
        
        return [
            'name' => \yii::t('app', 'Name'),
            'alias' => \yii::t('app', 'Alias'),
            'published' => \yii::t('app', 'Published'),
            'image' => \yii::t('app', 'Image'),
            'ordering' => \yii::t('app', 'Ordering'),
        ];
        
    }
    
    public function getItem()
    {
        if($this->model && class_exists($this->model))
        {
            return $this->hasOne($this->model, ['id' => 'item_id']);
        } else
            return $this->hasOne(self::className(), ['id' => 'id']);
    }
    
    /*for crud admin*/
    public $_privateAdminName = 'Image'; // One item name, used in breadcrums, in header in actionAdmin
    public $_privatePluralName = 'Photo galleries';// Item set name
    public $_privateAdminRepr = 'name';
    
    public function getAdminFields()
    {
            return array(
                    'name',
                    'image',
                    'published',
                    'ordering',
            );
            
    }

    public function getFieldsDescription()
    {
            return array(
                    'item_id' => 'RDbHidden',
                    'model' => 'RDbHidden',
                    'name' => 'RDbText',
                    'image' => 'RDbFile',
                    'published' => 'RDbBoolean',
                    'ordering' => 'RDbText',
            );
    }
    
    public function getViewExcludedFields()
    {
        return ['item_id', 'model'];
    }
    
    public function getImageDirectory()
    {
        $model = $this->model;
        $model = explode('\\', $model);
        $model = $model[count($model)-1];
        
        return \yii::getAlias('@web2').'/files/photogallery/'.$model.'/'.$this->item_id;
    }
    
    public function getImagePath()
    {
        if(!$this->image)
            return '';
		
        return $this->getImageDirectory().'/'.$this->image;
    }
    
    public function relationParentReferenceOptions()
    {
        $result = [];
        if($this->model)
        {
            $privateModelAdmin = \Yii::createObject(['class' => 'app\modules\privatepanel\components\PrivateModelAdmin'],[$this]);
            $result = array_merge($result, $privateModelAdmin->resolveParent($this->model, 'item'));
        }
        return $result;
    }
    
    public function getAdminMenu($privateModel, $menu = [])
    {
        
        $menu[] = 
            ['label'=> \yii::t("app", 'Multiple upload').': ' . $privateModel->getAdminName(), 'url'=>array_merge(['multipleupload'], $privateModel->additionalUrlParams), 'visible' => $privateModel->checkAccess('multipleupload')]
        ;
        return $menu;

    }
    public function getCreateMenu($privateModel, $menu = [])
    {
        $menu[] = 
            ['label'=> \yii::t("app", 'Multiple upload').': ' . $privateModel->getAdminName(), 'url'=>array_merge(['multipleupload'], $privateModel->additionalUrlParams), 'visible' => $privateModel->checkAccess('multipleupload')]
        ;
        return $menu;
    }
    
    public function beforeDelete() {
        
        if(file_exists($this->getImagePath()))
            unlink($this->getImagePath());
        return parent::beforeDelete();
    }
    
}
?>
