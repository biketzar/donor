<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 */
class RecoveryPasswordForm extends Model
{
    public $password;
    public $password_repeat;
    
    
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['password'], 'required'],
            ['password', 'compare', 'message'=>'Пароли не совпадают'],
            [['password'], 'string', 'max' => 255, 'min' => 6, 'on' => ['insert', 'update']],
            ['password_repeat', 'safe'],
        ];
    }
    
    public function attributeLabels() {
        
        return [
            'password' => \yii::t('modules/user/app', 'Password'),
            'password_repeat' => \yii::t('modules/user/app', 'Password repeat'),
            
        ];
        
    }
    
    public function requiredUsername()
    {
        if (!$this->hasErrors()) {
            if(!$this->username && !$this->email)
            {
                $this->addError('username', \Yii::t('app', 'Need to enter username or email.'));
            }
        }
    }

}
