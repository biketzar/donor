<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 */
class AuthForm extends Model
{
    public $email;
    public $password;
    public $rememberMe = true;

    private $_user = false;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['email', 'password'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }
    
    public function attributeLabels() {
        
        return [
            'email' => 'Email',
            'password' => \yii::t('modules/user/app', 'Password'),
            'rememberMe' => \yii::t('modules/user/app', 'Remember Me'),
            
        ];
        
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Неверный email или пароль');
            } else {
                $user->last_visit = date('Y-m-d H:i:s');
                $user->update(false, ['last_visit']);
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            //return Yii::$app->donor->login($this->getUser(), $this->rememberMe ? 3600*24*30 : 0);
            $session = new \yii\web\Session;
            $session->open();
            if(isset($this->getUser()->id))
            {
                $session['user_id'] = $this->getUser()->id;
                return TRUE;
            }
            else
            {
                return FALSE;
            }
        } else {
            return false;
        }
    }
    
    public static function getDonorId()
    {
        $session = new \yii\web\Session;
        $session->open();
        return $session['user_id'];
    }
    
    public static function logout()
    {
        $session = new \yii\web\Session;
        $session->open();
        $session->remove('user_id');
    }
    
    public static function setAuth($id)
    {
        $session = new yii\web\Session;
        $session->open();
        $session['user_id'] = $id;
    }


    public static function isAuth()
    {
        $session = new \yii\web\Session;
        $session->open();
        if(isset($session['user_id']) && Donor::find()->where('id=:id', [':id' => $session['user_id']])->one())
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = Donor::findByUsername($this->email);
        }

        return $this->_user;
    }
}
