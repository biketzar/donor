<?php
namespace app\models;

use app\components\CommonActiveRecord as ActiveRecord;

use yii\data\Sort;

class TextPlace extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%text_places}}';
    }
    
    public function init()
    {
        parent::init();
        
        $this->_privateAdminName = \yii::t('app', $this->_privateAdminName);
        $this->_privatePluralName = \yii::t('app', $this->_privatePluralName);
    }
    
    public function getContents()
    {
        return $this->hasMany(TextPlaceContent::className(), ['place_id' => 'id'])->orderBy(['ordering' => SORT_ASC]);
    }
    
    public function getPageContents($page_id = 0)
    {
        $pages = \Yii::$app->db->createCommand('SELECT content_id FROM {{%text_place_content_page}} WHERE page_id=:page_id')
             ->bindValue(':page_id', $page_id)->queryColumn();
        
        return $this->hasMany(TextPlaceContent::className(), ['place_id' => 'id'])
                ->where('all_pages')->orWhere(['id'=> $pages])->orderBy(['ordering' => SORT_ASC]);
    }
    
    public function rules()
    {
        return [
            [['id', 'name', 'alias', 'published'], 'safe'],
            
            [['id', 'published'], 'number', 'integerOnly' => true],
            [['name'], 'required', 'on'=>['insert', 'update']],
            [['alias'], 'app\components\AliasValidator', 'on'=>['insert', 'update']],
            [['alias'], 'unique'],
            [['id', 'name', 'alias', 'published'], 'safe', 'on'=>'search'],
        ];
    }
    
    public function attributeLabels() {
        
        return [
            'name' => \yii::t('app', 'Name'),
            'alias' => \yii::t('app', 'Alias'),
            'published' => \yii::t('app', 'Published'),
        ];
        
    }
    
    
    /*for crud admin*/
    public $_privateAdminName = 'Text place'; // One item name, used in breadcrums, in header in actionAdmin
    public $_privatePluralName = 'Text places';// Item set name
    public $_privateAdminRepr = 'name';// method or property used to retrieve model string represantation. It's close to __toString but only for admin

    public function getAdminFields()
    {
            return array(
                    'name',
                    'alias',
                    'published',
            );
            
    }

    public function getFieldsDescription()
    {
            return array(
                    'name' => 'RDbText',
                    'alias' => 'RDbText',
                    'published' => 'RDbBoolean',
            );
    }

}
?>
