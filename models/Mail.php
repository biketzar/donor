<?php
namespace app\models;

use app\components\CommonActiveRecord as ActiveRecord;

class Mail extends ActiveRecord
{
    public $donorstatus = [];
    public $donor = [];
    public $is_budget = '';
    public $blood_type = [];
    public static function tableName()
    {
        return '{{%mail}}';
    }
    
    public function rules()
    {
        return [
            [['donorstatus', 'donor', 'is_budget', 'blood_type'], 'safe', 'on'=>'insert'],
            [['body', 'subject'], 'required', 'on'=>['insert']],            
            [['body', 'subject', 'date', 'params', ], 'safe', 'on'=>'search'],
        ];
    }
    
    public function attributeLabels() {
        
        return [
            'subject' => 'Тема',
            'body' => 'Сообщение',
            'donor' => 'Доноры',
            'donorstatus' => 'Статусы доноров',
            'is_budget' => 'Бюджет/контракт',
          'blood_type' => 'Группа крови'
        ];
        
    }
    
    /*for crud admin*/
    public $_privateAdminName = 'Сообщение'; // One item name, used in breadcrums, in header in actionAdmin
    public $_privatePluralName = 'Сообщения';// Item set name
    public $_privateAdminRepr = 'subject';
    
    public function getAdminFields()
    {
            return array(
                    'subject',
                    'body',
                    'params'
            );
            
    }
    
    public static function getDonors()
    {
        $items = [];
        $donors = Donor::find()->select('id, surname, name, pname, donorstatus')->orderBy('surname ASC')->asArray()->all();
        foreach ($donors as $donor)
        {
            $items[$donor['id']] = "{$donor['surname']} {$donor['name']} {$donor['pname']}";
        }
        return $items;
    }
    
    public static function getDonorstatuses()
    {
        $items = [];
        $statuses = Donorstatus::find()->asArray()->all();
        foreach($statuses as $status)
        {
            $items[$status['id']] = $status['title'];
        }
        return $items;
    }
    
    public static function getIsBudget()
    {
        return $items = ['1'=>'Бюджет','0'=> 'Контракт'];
        
    }

    public function getFieldsDescription()
    {
            return array(
                'donorstatus' => ['RDbSelect', 'data' => self::getDonorstatuses(), 'htmlOptions' =>['multiple' =>TRUE]],
                'donor' => ['RDbSelect', 'data' => self::getDonors(), 'htmlOptions' =>['multiple' =>TRUE]],
                'is_budget' => ['RDbSelect', 'data' => self::getIsBudget()],
                'blood_type' => ['RDbSelect', 'data' => Donorlite::getBloodTypeList(), 'htmlOptions' =>['multiple' =>TRUE]],
                'subject' => 'RDbText',
                'body' => 'RDbText',
                'params' => 'RDbText',
            );
    }
    
    public function getFormExcludedFields()
    {
        return [
                'params'
            ];
    }
    
    public function beforeSave($insert) {
        if($insert)
        {
            $this->params = "donorstatus:{$this->donorstatus};"
            . "donor:{$this->donor};"
            . "is_budget:{$this->is_budget}"
            . "blood_type:{$this->blood_type}";
        }
        return parent::beforeSave($insert);
    }
    
    public function afterSave($insert, $changedAttributes) {
        if($insert)
        {
            set_time_limit(0);
            if(is_string($this->donorstatus))
            {
                $this->donorstatus = json_decode($this->donorstatus, TRUE);
            }
            if(is_string($this->donor))
            {
                $this->donor = json_decode($this->donor, TRUE);
            }
            if(is_string($this->blood_type))
            {
                $this->blood_type = json_decode($this->blood_type, TRUE);
            }
            $query = Donor::find();
            if(is_array($this->donorstatus))
            {
                $list = array();
                foreach ($this->donorstatus as $status)
                {
                    if(is_numeric($status) && $status > 0)
                    {
                        $list[] = $status;
                    }
                }
                if(count($list) > 0)
                {
                    $query->andWhere(['in', 'donorstatus', $list]);
                }
            }
            if(is_array($this->donor))
            {
                $list = array();
                foreach ($this->donor as $donor)
                {
                    if(is_numeric($donor) && $donor > 0)
                    {
                        $list[] = $donor;
                    }
                }
                if(count($donor) > 0)
                {
                    $query->andWhere(['in', 'id', $list]);
                }
            }
            if(is_numeric($this->is_budget) && ($this->is_budget == 1 || $this->is_budget == 0))
            {
                $query->andWhere("is_budget=:is_budget", [':is_budget' => $this->is_budget]);
            }
            if($this->blood_type && is_array($this->blood_type))
            {
              $query->andWhere(['IN', 'blood_type', $this->blood_type]);
            }
            $query->andWhere('unsubscribe!=1');
            $donors = $query->select('id')->asArray()->all();
            foreach ($donors as $donor)
            {
                $queueModel = new MailQueue();
                $queueModel->id_donor = $donor['id'];
                $queueModel->id_mail = $this->id;
                $queueModel->save();
                //лог
            }
        }
        
        return parent::afterSave($insert, $changedAttributes);
    }
}

