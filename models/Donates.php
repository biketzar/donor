<?php
namespace app\models;

//use yii\db\ActiveRecord;
//use app\components\CommonActiveRecord;
//use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\Url;

class Donates extends \app\components\CommonActiveRecord
{    
    public static function tableName()
    {
        return '{{%donates}}';
    }
    
    public function init()
    {
        parent::init();
        
        $this->_privateAdminName = \yii::t('app', $this->_privateAdminName);
        $this->_privatePluralName = \yii::t('app', $this->_privatePluralName);
    }    
    
    public function rules()
    {
        return [
            [['id_donor','donate_date', 'count', 'id'], 'safe', 'on'=>['search']],
            [['id_donor','donate_date', 'count', 'id'], 'safe'],
            [['id_donor', 'donate_date'], 'required', 'on' => ['insert', 'update']],
            [['id_donor', 'count'], 'integer', 'on' => ['insert', 'update']],
        ];
    }
    
    public static function find() {
        return parent::find()->orderBy('donate_date DESC');
    }
   
    
    public function attributeLabels() {
        
        return [
            'id_donor' => 'Донор',
            'donate_date' => 'Дата',
            'count' => 'Количество крови',
        ];
        
    }
    
    public function afterSave($insert, $changedAttributes) {
        if($insert)
        {
            $donor = Donorlite::find()->where('id=:id', [':id'=>$this->id_donor])->one();
            if($donor)
            {
                $donor->donorstatus = 4;
                $donor->save();
            }
        }
        return parent::afterSave($insert, $changedAttributes);
    }


    public function getDonor()
    {
        return $this->hasOne(Donor::className(), ['id' => 'id_donor']);
    }


    /*for crud admin*/
    public $_privateAdminName = 'Сдача крови'; // One item name, used in breadcrums, in header in actionAdmin
    public $_privatePluralName = 'Сдача крови';// Item set name
    public $_privateAdminRepr = 'id_donor';// method or property used to retrieve model string represantation. It's close to __toString but only for admin

    public function getAdminFields()
    {
        return array(
                'id_donor',
                'donate_date',
        );
    }

    public function getFieldsDescription()
    {
        return [
            'id_donor' => ['RDbRelation', 'donor'],
            'donate_date' => 'RDbDate',
            'count' => 'RDbText',
            ];
    }

    public function relationReferenceOptions()
    {
            return array(
                    //'contents' => array('controller' => 'page-content', 'relationName'=>'page', 'icon'=>'book'),
                    
            );
    }
}
