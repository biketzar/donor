<?php
namespace app\models;

class Donorlist extends \app\components\CommonActiveRecord
{
    public static function tableName()
    {
        return '{{%donorlist}}';
    }
    
    public static function find() {
        return parent::find();
    }
    
    public function init()
    {
        parent::init();
        
        $this->_privateAdminName = \yii::t('app', $this->_privateAdminName);
        $this->_privatePluralName = \yii::t('app', $this->_privatePluralName);
    }    
    
    public function rules()
    {
        return [
            [['id_donor','date', 'date_id', 'type', 'approved',], 'safe', 'on' => 'search'],
            [['type', 'approved',], 'required', 'on' => ['insert', 'update']],
            [['id_donor'], 'required', 'on' => ['insert']],
            [['id_donor'], 'integer', 'on' => ['insert']],
            [['date_id', 'type', 'approved',], 'integer', 'on' => ['insert', 'update']],
            [['date'], 'safe', 'on' => ['insert', 'update']],
        ];
    }
    
    
    public function attributeLabels() {
        
        return [
            'id_donor' => 'Донор',
            'date_id' => 'Время',
            'date' => 'Время для выездных',
            'type' => 'Тип донора',
            'approved' => 'Статус',
        ];
        
    }


    /*for crud admin*/
    public $_privateAdminName = 'Список доноров'; // One item name, used in breadcrums, in header in actionAdmin
    public $_privatePluralName = 'Список доноров';// Item set name
    public $_privateAdminRepr = 'id_donor';// method or property used to retrieve model string represantation. It's close to __toString but only for admin
    
    public function getAdminReprString()
    {
        return Donorlite::find()->where('id=:id', [':id' => $this->id_donor])->one()->getAdminReprString();
    }

    public function getAdminFields()
    {
        return array(
                'id_donor',
                'date_id',
                'type',
                //'date',
                'approved',
        );
    }
    public function getDonor()
    {
        return $this->hasOne(Donor::className(), ['id' => 'id_donor']);
    }
    
    public function getTimes()
    {
        return $this->hasOne(Times::className(), ['id' => 'date_id']);
    }
    
    public static function getApproved()
    {
        return [0 => 'не утверждён', 1 => 'утверждён', 2 => 'отказано'];
    }
    
    public static function getTypes()
    {
        return [0 => 'политех', 1 => 'выезд'];
    }
	
	public static function getTimesList()
    {
        $list = [];
        $times = Times::find()->all();
        foreach($times as $time)
        {
            $list[$time->id] = $time->dtime.($time->typing ? ' T ' : '');
        }
        return $list;
    }

    public function getFieldsDescription()
    {
        return [
            'id_donor' => ['RDbRelation', 'donor'],
            //'date' => 'RDbText',
            'date_id' => ['RDbSelect', 'data' => self::getTimesList()],
            'type' => ['RDbSelect', 'data' => self::getTypes()],
            'approved' => ['RDbSelect', 'data' => self::getApproved()],
            ];
    }
    
    public function beforeSave($insert)
    {
        if($this->type == 0)
        {
            $date = Times::findOne(['id' => $this->date_id, 'typing' => 0]);
            if(!$date)
            {
                $this->addError('date_id', 'Время и место сдачи несовместимы');
                return FALSE;
            }
        }
        else
        {
            $date = Times::findOne(['id' => $this->date_id, 'typing' => 1]);
            if(!$date)
            {
                $this->addError('date_id', 'Время и место сдачи несовместимы');
                return FALSE;
            }
        }
        return parent::beforeSave($insert);
    }
    
    public function afterSave($insert, $changedAttributes)
    {
        if($this->approved == 1)
        {
            $donor = Donorlite::findOne(['id' => $this->id_donor]);
            $time = Times::findOne(['id' => $this->date_id]);
            if($donor && $time)
            {
                $donor->setScenario('setdate');
                $donor->donorstatus = 3;//одобрена дата сдачи
                $donor->donate_day = $time->dtime;
                $donor->save();
            }
        }
        if($this->approved == 2)
        {
            $donor = Donorlite::findOne(['id' => $this->id_donor]);
            $time = Times::findOne(['id' => $this->date_id]);
            if($donor && $time)
            {
                $donor->setScenario('setdate');
                $donor->donorstatus = 7;//не учитывать
                $donor->donate_day = NULL;
                $donor->save();
            }
        }
        if($this->approved == 0)
        {
            $donor = Donorlite::findOne(['id' => $this->id_donor]);
            $time = Times::findOne(['id' => $this->date_id]);
            if($donor && $time)
            {
                $donor->setScenario('setdate');
                $donor->donorstatus = 6;//выбран роботом
                $donor->donate_day = NULL;
                $donor->save();
            }
        }
        return parent::afterSave($insert, $changedAttributes);
    }
    
    public function getFormExcludedFields()
    {
        return ['id'];
    }
}
