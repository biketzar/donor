<?php
namespace app\models;

use app\components\CommonActiveRecord as ActiveRecord;

class Times extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%times}}';
    }
    
    public function rules()
    {
        return [
            [['id', 'dtime', 'hide', 'max_donor_count'], 'safe'],
            [['dtime', 'typing', 'hide', 'max_donor_count'], 'safe', 'on'=>['insert', 'update']], 
            [['max_donor_count'], 'integer', 'min' => 0, 'on'=>['insert', 'update'] ], 
            [['dtime', 'typing', 'hide', 'max_donor_count'], 'safe', 'on'=>'search'],
        ];
    }
    
    public function attributeLabels() {
        
        return [
            'dtime' => 'Дата и время',
            'typing' => 'Возможна сдача на типирование',
          'hide' => 'Остановить регистрацию',
          'max_donor_count' => 'Максимальное кол-во доноров'
        ];
        
    }
    
    /*for crud admin*/
    public $_privateAdminName = 'Время сдачи'; // One item name, used in breadcrums, in header in actionAdmin
    public $_privatePluralName = 'Время сдачи';// Item set name
    public $_privateAdminRepr = 'dtime';
    
    public function getAdminFields()
    {
            return array(
                    'dtime',
                    'typing',
              'hide'
            );
            
    }

    public function getFieldsDescription()
    {
            return array(
                    'dtime' => 'RDbDateTime',
                'typing' => 'RDbBoolean',
                'max_donor_count' => 'RDbText',
              'hide' => 'RDbBoolean',
            );
    }
    
    public function afterDelete() {
        $models = Eventday::find()->where('date_id=:date_id', [':date_id' => $this->id])->all();
        foreach ($models as $model)
        {
            //$models->date_id = -1;
            $model->delete();
        }
        return parent::afterDelete();
    }
    
    public static function find() {
        return parent::find()->orderBy('dtime ASC');
    }
    
    public function afterSave($insert, $changedAttributes) {
        \yii\caching\TagDependency::invalidate(\Yii::$app->cache, 'task-cache-dependency');
        return parent::afterSave($insert, $changedAttributes);
    }
    
}
