<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 */
class ForgetPasswordForm extends Model
{
    public $username;
    public $email;
    public $captcha;
    
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'email'], 'safe'],
            [['email'], 'email'],
            [['email'], 'required'],
            //['captcha', 'captcha'],
            //['captcha', 'required'],
        ];
    }
    
    public function attributeLabels() {
        
        return [
            'username' => \yii::t('modules/user/app', 'Username'),
            'email' => \yii::t('modules/user/app', 'Email'),
            
        ];
        
    }
    
    public function requiredUsername()
    {
        if (!$this->hasErrors()) {
            if(!$this->username && !$this->email)
            {
                $this->addError('username', \Yii::t('app', 'Need to enter username or email.'));
            }
        }
    }

}
