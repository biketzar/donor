<?php

namespace app\models;

use yii\helpers\Html;
use \app\components\CommonActiveQuery;
use app\components\CommonActiveRecord as ActiveRecord;

class Donorlite extends ActiveRecord
{
    
    public $roditName = '';
    
    public $inPoliteh = '';
    
    public $money = 3000;

    public static function tableName()
    {
        return '{{%donors}}';
    }
    
    public function init()
    {
        parent::init();
        
        $this->_privateAdminName = \yii::t('app', $this->_privateAdminName);
        $this->_privatePluralName = \yii::t('app', $this->_privatePluralName);
    }
    
    public static function find()
    {
        $query = new DonorliteActiveQuery(get_called_class());
        return $query->orderBy('id DESC');
    }
	
	public static function getDepartments()
	{
		$departments = Departments::find()->all();
		$result = [];
		foreach($departments as $department)
		{
			$result[$department->id] = $department->name;
		}
		return $result;
	}
    
    public function rules()
    {
        return [
            [['dcard_id'], 'safe', 'on'=>'dcard'],
            [['pass_id', 'pass_series', 'inn', 'snils', 'subdiv_code', 'name','surname', 'pname', 'group_num', 'reg_addr', 'birth_place', 'sex'],'filter','filter'=>'\yii\helpers\HtmlPurifier::process','on' => ['insert', 'update', 'setdate']],
            [['printDate','department_id','name','inPoliteh','surname','pname','phone','pass_id','pass_series','birth_date','issued_date','inn','snils','subdiv_code','issued', 'birth_place', 'reg_addr', 'weight', 'typing'], 'safe', 'on'=>'print'],
            [['donate_day','name','surname','pname','phone','birth_date','issued_date','inn','snils','subdiv_code','issued', 'birth_place', 'reg_addr', 'weight', 'typing'], 'safe', 'on'=>'setdate'],
            [['name','surname', 'blood_status', 'donate_day', 'sex', 'marrow_status', 'is_budget', 'donorstatus', 'pname', 'gradebook', 'group_num', 'phone', 'email', 'is_vip', 'weight', 'typing','blood_type'], 'safe', 'on' => 'search'],
            [['name', 'email', 'marrow_status','sex', ], 'safe'],
            [['name','email','is_budget','donate_day','department_id','sex', 'issued', 'birth_place', 'reg_addr','blood_type'], 'safe', 'on' => ['insert', 'update', 'setdate']],
            [['email','name','surname', 'gradebook','phone',], 'required', 'on' => ['insert', 'update', 'setdate']],
            [['gradebook', 'dcard_id', 'donorstatus', 'blood_status', 'marrow_status', 'is_vip', 'weight', 'typing'], 'integer', 'on' => ['insert', 'update', 'setdate']],
            [['name','surname', 'pname', 'group_num'], 'string', 'max' => 255, 'on' => ['insert', 'update', 'setdate']],
			[['pass_id'], 'string', 'length' => 6, 'on' => ['insert', 'update', 'setdate']],
			[['pass_series'], 'string', 'length' => 4, 'on' => ['insert', 'update', 'setdate']],
			[['birth_date'], 'date', 'format' => 'dd.MM.yyyy', 'on' => ['insert', 'update', 'setdate']],
			[['issued_date'], 'date', 'format' => 'dd.MM.yyyy', 'on' => ['insert', 'update', 'setdate']],
			[['inn'], 'string', 'length' => 12, 'on' => ['insert', 'update', 'setdate']],
			[['snils'], 'string', 'max' => 20, 'min' => 11, 'on' => ['insert', 'update', 'setdate']],
			[['subdiv_code'], 'string', 'max' => 16, 'min' => 3, 'on' => ['insert', 'update', 'setdate']],
            //[['username'], 'match', 'pattern' => '/^[a-zA-Z0-9]+$/', 'message' => 'Логин может состоять только из букв английского алфавита и цифр', 'on' => ['insert', 'update']],
            [['email'], 'email', 'on' => ['insert', 'update']],
            [['weight'], 'required', 'on' => ['insert', 'update']],
            ['email', 'unique', 'targetClass' => '\app\models\Donor', 'message' => \Yii::t('app','This email address has already been taken.')],
        ];    
    }
    
    public function attributeLabels() {
        
        return [
            'name' => \yii::t('modules/user/app', 'Person Name'),
            'username' => \yii::t('modules/user/app', 'Username'),
            'email' => \yii::t('modules/user/app', 'Email'),
            'status' => \yii::t('modules/user/app', 'Status'),
            'password' => \yii::t('modules/user/app', 'Password'),
            'password_repeat' => \yii::t('modules/user/app', 'Password repeat'),
            'last_visit' => \yii::t('modules/user/app', 'Last Visit'),
            'roles' => \yii::t('modules/user/app', 'Roles'),
            'create_time' => \yii::t('modules/user/app', 'Create Time'),
            'update_time' => \yii::t('modules/user/app', 'Update Time'),
            'name' => 'Имя',
            'surname' => 'Фамилия',
            'pname' => 'Отчество',
            'group_num' => 'Номер группы',
            'gradebook' => 'Номер зачетки',
            'phone' => 'Телефон',
            'dcard_id' => 'Карточка донора',
            'donorstatus' => 'Статус донора',
            'donateDate' => 'Желаемые даты',
            'blood_status' => 'Статус крови',
            'marrow_status' => 'Донор костного мозга',
            'donate_day' => 'Дата сдачи',
            'is_budget' => 'Бюджет',
            'department_id' => 'Подразделение',
            'inn' => 'ИНН',
            'snils' => 'СНИЛС',
            'pass_id' => 'Номер паспорта',
            'pass_series' => 'Серия паспорта',
            'birth_date' => 'Дата рождения',
            'birth_place' => 'Место рождения',
            'issued' => 'Паспорт выдан',
            'issued_date' => 'Дата выдачи',
            'subdiv_code' => 'Код подразделения',
            'reg_addr' => 'Адрес регистрации по паспорту',
            'sex' => 'Пол',
            'roditName' => 'Имя в род.падеже',
            'printDate' => 'Дата',
            'inPoliteh' => 'сдал официально',
            'is_vip' => 'Вне очереди и приоритета',
            'weight' => 'Вес',
            'typing' => 'Сдача на выезде',
			'blood_type' => 'Группа крови'
        ];
        
    }
    
    public function beforeSave($insert)
    {
        $this->name = mb_convert_case(mb_strtolower($this->name, 'UTF-8'), MB_CASE_TITLE, "UTF-8");
        $this->surname = mb_convert_case(mb_strtolower($this->surname, 'UTF-8'), MB_CASE_TITLE, "UTF-8");
        $this->pname = mb_convert_case(mb_strtolower($this->pname, 'UTF-8'), MB_CASE_TITLE, "UTF-8");
        $dirtyAttributes = $this->getDirtyAttributes();
        if(isset($dirtyAttributes['password']) && $dirtyAttributes['password'])
        {
            $this->password = \Yii::$app->getSecurity()->generatePasswordHash($dirtyAttributes['password']);
        } else {
            $this->password = $this->getOldAttribute('password');
        }
        
        if($this->donate_day != '' && strtotime($this->donate_day)>time() && $this->donorstatus != 3)
        {
            //$this->donorstatus = 3;
        }
        if(($this->donorstatus == 285 && $this->oldAttributes['donorstatus'] != 285) || ($this->donorstatus == 286 && $this->oldAttributes['donorstatus'] != 286))
        {
            $history = Donorhistory::find()->where('id_donor=:id_donor AND date>:date', [':id_donor' => $this->id, ':date' => date('Y-m-d H:i:s', strtotime('-1 month'))])->one();
            if($history)
            {
                $history->delete();
            }
        }
        
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->access_token = \Yii::$app->getSecurity()->generateRandomString();
            }
            return true;
        }
        return false;
    }
    
    
    
    public static function getStatuses()
    {
        return [
            self::STATUS_ACTIVE => \Yii::t('app', 'Active'),
            self::STATUS_INACTIVE => \Yii::t('app', 'Inactive'),
            self::STATUS_BLOCKED => \Yii::t('app', 'Blocked'),
        ];
    }
	
	public static function getBloodTypeList()
	{
		return [
		1 => 'первая-положительная',
		2 => 'первая-отрицательная',
		3 =>'вторая-положительная',
		4 =>'вторая-отрицательная',
		5 =>'третья-положительная',
		6 =>'третья-отрицательная',
		7 =>'четвертая-положительная',
		8 =>'четвертая-отрицательная',
		];
	}
    
    public function getStatusName()
    {
        $items = self::getStatuses();
        return (isset($items[$this->status])?$items[$this->status]:'');
    }
    
    public function getFullName()
    {
        return $this->name;
    }
    
    public static function getTimes()
    {
        static $items;
        if(is_array($items))
        {
            return $items;
        }
        $times = Times::find()->asArray()->all();
        $items = [];
        foreach($times as $time)
        {
            $items[$time['id']] = $time['dtime'];
        }
        return $items;
    }
    
    public function afterFind() {
        
        
        $this->adminnRepresentstion = $this->getAdminReprString();
        parent::afterFind();
    }
    
    public function getDonateDate()
    {
        $dayModels = Eventday::find()->where('id_donor=:id_donor', [':id_donor'=>$this->id])->all();
        $donateDate = "";
        $times = self::getTimes();
        foreach($dayModels as $model)
        {
            $donateDate .= (isset($times[$model->date_id]) ? $times[$model->date_id] : '')."<br>\n";
        }
        return $donateDate;
    }
    
    public function __get($name) {
        if($name == 'donateDate')
        {
            return $this->getDonateDate();
        }
        return parent::__get($name);
    }




    /*for crud admin*/
    public $_privateAdminName = 'Донор'; // One item name, used in breadcrums, in header in actionAdmin
    public $_privatePluralName = 'Доноры';// Item set name
    public $_privateAdminRepr = 'surname';
    public $adminnRepresentstion = '';
    public function getAdminReprString()
    {
        return $this->surname.' '.$this->name.' '.$this->pname.' '.$this->group_num;
    }
    
    public function getDcard()
    {
        return $this->hasOne(Dcard::className(), ['id' => 'dcard_id']);
    }
    
    public function getAdminFields()
    {
            return array(
            'id',
                    'donorstatus',
                    'surname',
                    'name',
                    'pname',
                    'gradebook',
                    'group_num',
                    'phone',
                    'email',
                    'dcard_id',
                    'donateDate',
                    'department_id', 
                    'is_budget',
                    'weight',
                    'typing',
					'blood_type'
            );
    }
    
    public static function getWeightItems()
    {
        return [
            1 => 'больше 58 кг',
            0 => 'от 50 до 58 кг'
        ];
    }
    
    public $printDate = '';
    public $iof = '';
    public function getFieldsDescription()
    {
      $roleList = array_keys(\app\modules\user\components\AuthManagerHelper::getManager()
          ->getRolesByUser(\Yii::$app->user->getId()));
      if(in_array('admin', $roleList))
      {
        return array(
          'donorstatus'=>['RDbRelation', 'donorstatusrel'],
          'blood_type' => ['RDbSelect', 'data' => self::getBloodTypeList()],
        );
      }
        if($this->getScenario() == 'print')
        {
            return array(
                'surname' => 'RDbText',
                'name' => 'RDbText',
                'pname' => 'RDbText',
                'department_id' => ['RDbRelation', 'departament'],
                'inn' => 'RDbText',
                'pass_id' => 'RDbText',
                'pass_series' => 'RDbText',
                'issued' => ['RDbText', 'forceTextArea'=>'forceTextArea'],
                'issued_date' => 'RDbText',
                'subdiv_code' => 'RDbText',
                'birth_date' => 'RDbText',
                'phone' => 'RDbText',
                'printDate' => 'RDbText',
                'inPoliteh' => 'RDbBoolean',
                'typing' => 'RDbBoolean',
				'blood_type' => ['RDbSelect', 'data' => self::getBloodTypeList()],
                'weight' => ['RDbSelect', 'data' => self::getWeightItems()]
                );
        }
        
        if($this->getScenario() == 'setdate')
        {
             return array(
                'donorstatus'=>['RDbRelation', 'donorstatusrel'],
                'donateDate' => ['RDbLabel', 'forceTextArea'=>'forceTextArea', 'style'=>'height:90px'],
                'donate_day'=> 'RDbDateTime',
                'surname' => 'RDbText',
                'name' => 'RDbText',
                'pname' => 'RDbText',
                'inn' => 'RDbText',
                'pass_id' => 'RDbText',
                'pass_series' => 'RDbText',
                'issued' => ['RDbText', 'forceTextArea'=>'forceTextArea'],
                'issued_date' => 'RDbText',
                'subdiv_code' => 'RDbText',
                'birth_date' => 'RDbText',
                'phone' => 'RDbText',
                'weight' => ['RDbSelect', 'data' => self::getWeightItems()],
				'blood_type' => ['RDbSelect', 'data' => self::getBloodTypeList()],
                'typing' => 'RDbBoolean'
                );
        }
        return array(
            'is_vip' => 'RDbBoolean',
            'donorstatus'=>['RDbRelation', 'donorstatusrel'],
            'surname' => 'RDbText',
            'name' => 'RDbText',
            'pname' => 'RDbText',
            'donate_day' => 'RDbText',
            'group_num' => 'RDbText',
            'phone' => 'RDbText',
            'email' => 'RDbText',
            'gradebook' => 'RDbText',
            'department_id' => ['RDbRelation', 'departament'],
            'is_budget' => 'RDbBoolean',
            'dcard_id' => ['RDbRelation', 'dcard'],
            'donateDate' => 'RDbBase',
            'blood_status' => ['RDbSelect', 'data'=>[
                '0'=>'нет данных', 
                '1'=>'активна',
                '2'=>'нуждается в активации',
                '3'=>'в ожидании активации',
                ]],
            'marrow_status' => 'RDbBoolean',
            'inn' => 'RDbText',
            'snils' => 'RDbText',
            'pass_id' => 'RDbText',
            'pass_series' => 'RDbText',
            'birth_date' => 'RDbText',
            'birth_place' => ['RDbText', 'forceTextArea'=>'forceTextArea', ],
            'issued' => ['RDbText', 'forceTextArea'=>'forceTextArea'],
            'issued_date' => 'RDbText',
            'subdiv_code' => 'RDbText',
            'reg_addr' => ['RDbText', 'forceTextArea'=>'forceTextArea'],
            'sex' => ['RDbSelect', 'data'=>['0'=>'М', 
                '1'=>'Ж',]],
            'weight' => ['RDbSelect', 'data' => self::getWeightItems()],
            'typing' => 'RDbBoolean',
			'blood_type' => ['RDbSelect', 'data' => self::getBloodTypeList()],
        );
    }
    
    public function getFormExcludedFields()
    {
        if($this->getScenario() == 'setdate')
        {
            return [];
        }
        return ['donateDate'];
    }
	
	public function getDepartament()
    {
        return $this->hasOne(Departments::className(), ['id' => 'department_id']);
    }
    
    public function getGridButtonColumns($columnParams = array('buttons'=>array(), 'template'=>'')) {
            $columnParams['template'] = str_replace('{log}', '', $columnParams['template']);
            if(\Yii::$app->user->can(\Yii::$app->controller->getAccessOperationName('print')))
            {
                $columnParams['template'] = ' {print} '.$columnParams['template'];
                $columnParams['buttons']['print'] = function ($url, $model, $key) 
                {
                    $options = array_merge([
                        'title' => 'Печать',
                        'aria-label' => 'Печать',
                        'data-pjax' => $key,
                        'class' => 'btn btn-blue btn-sm',
                    ], []);
                    return Html::a('<i class="fa fa-print"></i>', $url, $options);
                };
            }
            if(\Yii::$app->user->can(\Yii::$app->controller->getAccessOperationName('change')))
            {
                $columnParams['template'] = ' {change} '.$columnParams['template'];
                $columnParams['buttons']['change'] = function ($url, $model, $key) 
                {
                    $options = array_merge([
                        'title' => 'Дата сдачи',
                        'aria-label' => 'Дата сдачи',
                        'data-pjax' => $key,
                        'class' => 'btn btn-blue btn-sm',
                    ], []);
                    return Html::a('<i class="fa fa-heartbeat"></i>', $url, $options);
                };
            }
            return $columnParams;
    }
    
    public function getDonorstatusrel()
    {
        return $this->hasOne(Donorstatus::className(), ['id' => 'donorstatus']);
    }
    
}

class DonorliteActiveQuery extends CommonActiveQuery
{
    public function status($status)
    {
        if($status !== null)
            return $this->andWhere(['status=:status', [':status' => intval($status)]]);
        else
            return $this;
    }
    
}
