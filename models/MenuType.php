<?php
namespace app\models;

use app\components\CommonActiveRecord as ActiveRecord;

use yii\data\Sort;

class MenuType extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%menu_types}}';
    }
    
    public function init()
    {
        parent::init();
        
        $this->_privateAdminName = \yii::t('app', $this->_privateAdminName);
        $this->_privatePluralName = \yii::t('app', $this->_privatePluralName);
    }
    
    public function getPages()
    {
        return $this->hasMany(Page::className(), ['menu_id' => 'id'])->where(['parent_id'=>0])->orderBy(['ordering' => SORT_ASC]);
    }
    
    public function getActivePages($lang = null)
    {
        $pages = Page::find()->andWhere(['parent_id'=>0])->andWhere(['menu_id'=>$this->id])->orderBy(['ordering' => SORT_ASC])->published()->all();
        $result = [];
        foreach($pages as $page)
        {
            if(!empty($lang) && !in_array($lang, $page->lang) && (!empty(\yii::$app->params['translatedLanguages']) && count(\yii::$app->params['translatedLanguages'])>1))
                continue;
            $result[] = $page;
        }
        return $result;
    }
    
    public function rules()
    {
        return [
            [['id', 'name', 'alias', 'published'], 'safe'],
            
            [['id', 'published'], 'number', 'integerOnly' => true],
            [['name'], 'required', 'on'=>['insert', 'update']],
            [['alias'], 'app\components\AliasValidator', 'on'=>['insert', 'update']],
            [['alias'], 'unique'],
            [['id', 'name', 'alias', 'published'], 'safe', 'on'=>'search'],
        ];
    }
    
    public function attributeLabels() {
        
        return [
            'name' => \yii::t('app', 'Name'),
            'alias' => \yii::t('app', 'Alias'),
            'published' => \yii::t('app', 'Published'),
        ];
        
    }
    
    
    /*for crud admin*/
    public $_privateAdminName = 'Menu'; // One item name, used in breadcrums, in header in actionAdmin
    public $_privatePluralName = 'Menus';// Item set name
    public $_privateAdminRepr = 'name';// method or property used to retrieve model string represantation. It's close to __toString but only for admin

    public function getAdminFields()
    {
            return array(
                    'name',
                    'alias',
                    'published',
            );
            
    }

    public function getFieldsDescription()
    {
            return array(
                    'name' => 'RDbText',
                    'alias' => 'RDbText',
                    'published' => 'RDbBoolean',
            );
    }

}
?>
