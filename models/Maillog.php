<?php
namespace app\models;

use app\components\CommonActiveRecord as ActiveRecord;

class Maillog extends ActiveRecord
{
    public $donorstatus;
    public $donor;
    
    public static function tableName()
    {
        return '{{%maillog}}';
    }
    
    public function rules()
    {
        return [
            [['id_mail', 'id_donor', 'status'], 'safe', 'on'=>'insert'],
            [['id_mail', 'id_donor', 'status'], 'required', 'on'=>['insert']],            
            [['id_mail', 'id_donor', 'status', 'date'], 'safe', 'on'=>'search'],
        ];
    }
    
    public function attributeLabels() {
        
        return [
            'id_mail' => 'Сообщение',
            'id_donor' => 'Донор',
            'status' => 'Статус',
            'date' => 'Дата и время'
        ];
        
    }
    
    /*for crud admin*/
    public $_privateAdminName = 'Лог отправленных сообщений'; // One item name, used in breadcrums, in header in actionAdmin
    public $_privatePluralName = 'Логи отправленных сообщений';// Item set name
    public $_privateAdminRepr = 'id_mail';
    
    public function getAdminFields()
    {
            return array(
                    'id_mail',
                    'id_donor',
                'status',
                'date'
            );
            
    }
    
    public static function getDonors()
    {
        $items = [];
        $donors = Donor::find()->select('id, surname, name, pname, donorstatus')->orderBy('surname ASC')->asArray()->all();
        foreach ($donors as $donor)
        {
            $items[$donor['id']] = "{$donor['surname']} {$donor['name']} {$donor['pname']}";
        }
        return $items;
    }
    
    public function getMail()
    {
        return $this->hasOne(Mail::className(), ['id' => 'id_mail']);
    }

    public function getFieldsDescription()
    {
            return array(
                'id_mail' => ['RDbRelation', 'mail'],
                'id_donor' => ['RDbSelect', 'data' => self::getDonors()],
                'status' => 'RDbBoolean',
                'date' => 'RDbDateTime'
            );
    }
    
    public function getFormExcludedFields()
    {
        return [
                'id_mail',
                //'id_donor',
                'status',
                'date'
            ];
    }
    
    public function getGridButtonColumns($columnParams = array('buttons'=>array(), 'template'=>'')) 
    {
        $columnParams['template'] = str_replace('{delete}', '', $columnParams['template']);
        $columnParams['template'] = str_replace('{update}', '', $columnParams['template']);
        return $columnParams;
    }
}

