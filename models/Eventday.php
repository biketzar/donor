<?php
namespace app\models;

//use yii\db\ActiveRecord;
//use app\components\CommonActiveRecord;
//use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\Url;

class Eventday extends \app\components\CommonActiveRecord
{
    
    public $dtime = 0;
    
    public function afterFind() {
        $this->dtime = $this->date_id;
        return parent::afterFind();
    }
    
    public static function getDtimeItems()
    {
        static $items;
        if(!is_array($items))
        {
            $items = ['0' => 'не выбрано'];
            $donor = \app\models\Donorlite::find()->where('id=:id', [':id'=>\app\models\AuthForm::getDonorId()])->one();
            if($donor && ($donor->typing == 1 || $donor->weight == 0))//для типирования только определённые даты
            {
                $models = Times::find()
                    ->where('UNIX_TIMESTAMP(dtime) > :now AND typing=1', [':now' => time()])
                    ->andWhere(['hide' => 0])
                    ->asArray()->all();
            }
            else
            {
                $models = Times::find()
                    ->where('UNIX_TIMESTAMP(dtime) > :now AND typing=0', [':now' => time()])
                    ->andWhere(['hide' => 0])
                    ->asArray()->all();
            }
            foreach($models as $model)
            {
                $items[$model['id']] = date('d.m.Y H:i', strtotime($model['dtime']));
            }
        }
        return $items;
    }
    
    public static function tableName()
    {
        return '{{%eventday}}';
    }
    
    public function init()
    {
        parent::init();
        
        $this->_privateAdminName = \yii::t('app', $this->_privateAdminName);
        $this->_privatePluralName = \yii::t('app', $this->_privatePluralName);
    }    
    
    public function getMetadata()
    {
        return $this->hasOne(\app\models\Metadata::className(), ['item_id' => 'id'])
                ->where('model = :modelName', ['modelName' => self::className()]);
    }
    
    public function rules()
    {
        return [
            [['id_donor','date_id', 'priority', 'id'], 'safe'],
            [['id_donor','date_id', 'priority', 'id'], 'safe', 'on' => ['insert', 'update']],
            [['id_donor','date_id', 'priority', 'id'], 'safe', 'on' => ['search']],
            [['id_donor', 'priority'], 'integer', 'on' => ['insert', 'update']],
        ];
    }
   
    
    public function attributeLabels() {
        
        return [
            'id_donor' => 'Донор',
            'date_id' => 'Дата и время сдачи',
            'priority' => 'Приоритет',
            'dday' => 'День',
            'dtime' => 'Дата и время',
        ];
        
    }
    
    public function getDonor()
    {
        return $this->hasOne(Donor::className(), ['id' => 'id_donor']);
    }
    
    public function getTimes()
    {
        return $this->hasOne(Times::className(), ['id' => 'date_id']);
    }
    
    /**
     * возвращает время в указанном формате
     * @param string $format
     * @return string
     */
    public function getDateTime($format)
    {
        $time = Times::find()->where('id=:id', [':id' => $this->date_id])->one();
        if($time)
        {
            return date($format, strtotime($time->dtime));
        }
        else
        {
            return 'Не найдено';
        }
    }


    /*for crud admin*/
    public $_privateAdminName = 'Время сдачи'; // One item name, used in breadcrums, in header in actionAdmin
    public $_privatePluralName = 'Время сдачи';// Item set name
    public $_privateAdminRepr = 'id_donor';// method or property used to retrieve model string represantation. It's close to __toString but only for admin

    public function getAdminFields()
    {
        return array(
                'id_donor',
                'date_id',
                'priority',
        );
    }
    
    public static function getTimesList()
    {
        $list = [];
        $times = Times::find()->all();
        foreach($times as $time)
        {
            $list[$time->id] = $time->dtime.($time->typing ? ' T ' : '');
        }
        return $list;
    }
    

    public function getFieldsDescription()
    {
        return [
            'id_donor' => ['RDbRelation', 'donor'],
            'date_id' =>  ['RDbSelect', 'data' => self::getTimesList()],//['RDbDate', 'format'=>'yyyy-MM-dd HH:mm', 'inputFormat'=>'dd-MM-yyyy HH:mm'],
            'priority' => 'RDbText',
            ];
    }

    public function relationReferenceOptions()
    {
            return array(
                    //'contents' => array('controller' => 'page-content', 'relationName'=>'page', 'icon'=>'book'),
                    
            );
    }
    
    public function afterSave($insert, $changedAttributes) {
        \yii\caching\TagDependency::invalidate(\Yii::$app->cache, 'task-cache-dependency');
        return parent::afterSave($insert, $changedAttributes);
    }
}
