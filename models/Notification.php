<?php
namespace app\models;

use app\components\CommonActiveRecord as ActiveRecord;

class Notification extends ActiveRecord
{
    public $info = '';
    
    public static function tableName()
    {
        return '{{%notification}}';
    }
    
    public function rules()
    {
        return [
            [['date_id', 'status_id', 'subject', 'message', 'user_id', 'type_id'], 'safe', 'on'=>'insert'],
            [['date_id', 'status_id', 'subject', 'message', 'type_id'], 'required', 'on'=>['insert']],            
            [['date_id', 'status_id', 'subject', 'message', 'user_id', 'create_date', 'type_id'], 'safe', 'on'=>'search'],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'date_id' => 'Дата',
            'status_id' => 'Статус',
            'subject' => 'Тема',
            'message' => 'Сообщение',
            'user_id' => 'Отправитель',
            'create_date' => 'Дата отправки',
            'type_id' => 'Тип'
        ];
    }
    
    public $_privateAdminName = 'Уведомление'; // One item name, used in breadcrums, in header in actionAdmin
    public $_privatePluralName = 'Уведомления';// Item set name
    public $_privateAdminRepr = 'info';
    
    public function getAdminFields()
    {
        return array(
            'date_id',
            'status_id',
            'subject',
            'message',
            'user_id',
            'create_date',
            'type_id'
        );   
    }
    
    public function getFieldsDescription()
    {
        return [
            'status_id' => ['RDbSelect', 'data' => Donorlist::getApproved()],
            'date_id' => ['RDbSelect', 'data' => Donorlist::getTimesList()],
            'type_id' => ['RDbSelect', 'data' => Donorlist::getTypes()],
            'subject' => ['RDbText'],
            'message' => ['RDbText'],
            ];
    }
    
    public function afterFind()
    {
        $this->info = $this->getInfo();
        return parent::afterFind();
    }
    
    public function getInfo()
    {
        return $this->create_date.' '.$this->subject;
    }
    
    public function afterSave($insert, $changedAttributes)
    {
        if($insert)
        {
            $userIds = Donorlist::find()
                    ->select('id_donor')
                    ->where([
                        'date_id' => $this->date_id,
                        'type' => $this->type_id,
                        'approved' => $this->status_id,
                        ])->column();
            if($userIds)
            {
                $donors = Donor::find()->where(['IN', 'id', $userIds])->asArray()->all();
                $this->sendMessage($donors);
            }
        }
        return parent::afterSave($insert, $changedAttributes);
    }
    
    public function sendMessage($donors)
    {
        foreach ($donors as $donor)
        {
            $mailer = new \app\components\MailHelper;
            $mailer->to = $donor['email'];
            $mailer->subject = $this->subject;
            $mailer->content = $this->message;
            $mailer->from = 'no-reply@donor.spb.ru';
            $mailer->params = [
                'FULLNAME' => "{$donor['surname']} {$donor['name']} {$donor['pname']}",
                'NAME' => "{$donor['name']}"
                ];
            $result = $mailer->sendOne();
            $log = new NotificationLog;
            $log->notification_id = $this->id;
            $log->date_id = $this->date_id;
            $log->donor_id = $donor['id'];
            $log->status = (int)$result;
            $log->save();
        }
    }
    
    public function beforeSave($insert)
    {
        $this->user_id = \Yii::$app->user->getId();
        return parent::beforeSave($insert);
    }
    
    public static function inSendedNotification($donorId, $timeId)
    {
        static $list;
        if(!is_array($list))
        {
            $logs = NotificationLog::find()
                    ->where(['IN', 'date_id', array_keys(Donorlist::getTimesList())])
                    ->asArray()->all();
            $list = [];
            foreach ($logs as $log)
            {
                $list[$log['date_id']][$log['donor_id']] = (bool)$log['status'];
            }
        }
        return (isset($list[$timeId][$donorId]));
    }
}