<?php
namespace app\models;

use app\components\CommonActiveRecord;

class Handler extends \app\components\CommonActiveRecord
{
    public static function tableName()
    {
        return '{{%handlers}}';
    }
    
    public function getPages()
    {
        return $this->hasMany(Page::className(), ['handler_id' => 'id']);
    }
    
    public $_privateAdminRepr = 'name';
    
}
?>
