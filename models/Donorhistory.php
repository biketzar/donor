<?php
namespace app\models;

use app\components\CommonActiveRecord as ActiveRecord;

class Donorhistory extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%donorhistory}}';
    }
    
    public function init()
    {
        parent::init();
        $this->_privateAdminName = 'История';
        $this->_privatePluralName = 'Истории';
        
    }
    public static function find() {
        return parent::find()->orderBy('date DESC');
    }

        public function rules()
    {
        return [
            [['id', 'id_donor', 'date', 'is_politeh', 'date_id'], 'safe'],
            [['id', 'id_donor', 'is_politeh'], 'number', 'integerOnly' => true, 'on'=>['insert', 'update']],
            [['id_donor'], 'required', 'on'=>['insert', 'update']],
            [['id', 'id_donor', 'date', 'is_politeh'], 'safe', 'on'=>'search'],
        ];
    }
    
    public function attributeLabels() {
        
        return [
            'id' => 'Id',
            'id_donor' => 'Донор',
            'date' => 'Дата сдачи',
            'is_politeh' => 'Сдал в Политехе'
        ];
    }
    
    
    /*for crud admin*/
    public $_privateAdminName = 'История'; // One item name, used in breadcrums, in header in actionAdmin
    public $_privatePluralName = 'Истории';// Item set name
    public $_privateAdminRepr = 'id';
    
    public function getAdminFields()
    {
            return array(
                    'id_donor',
                    'date',
                    'is_politeh'
            );
            
    }
    
    public function getDonor()
    {
        return $this->hasOne(Donor::className(), ['id' => 'id_donor']);
    }

    public function getFieldsDescription()
    {
        if($this->getScenario() == 'update')
        {
            return ['is_politeh' => 'RDbBoolean'];
        }
            return array(
                    'id_donor' => ['RDbRelation', 'donor'],
                    'date' => ['RDbDate', 'format'=>'MM.yyyy', 'inputFormat'=>'MM.yyyy'],
                    'is_politeh' => 'RDbBoolean'
            );
    }
    
    public function getFormExcludedFields()
    {
        return ['date'];
    }
    
    public function getGridButtonColumns($columnParams = array('buttons'=>array(), 'template'=>'')) 
    {
        $columnParams['template'] = str_replace('{delete}', '', $columnParams['template']);
        return $columnParams;
    }
}

