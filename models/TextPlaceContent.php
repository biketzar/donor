<?php
namespace app\models;

use app\components\CommonActiveRecord as ActiveRecord;

use yii\data\Sort;

class TextPlaceContent extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%text_place_contents}}';
    }
    
    public function init()
    {
        parent::init();
        
        $this->_privateAdminName = \yii::t('app', $this->_privateAdminName);
        $this->_privatePluralName = \yii::t('app', $this->_privatePluralName);
    }
    public static function getTextBlock($id)
    {
        $model = TextPlaceContent::find()->where('id=:id', [':id'=>$id])->one();
        if($model)
        {
            return ['name'=>$model->name, 'text'=>$model->text];
        }
        else
        {
            return ['name'=>'', 'text'=>''];
        }
    }

        public function getPlace()
    {
        return $this->hasOne(TextPlace::className(), ['id' => 'place_id']);
    }
    public function getPages()
    {
        return $this->hasMany(Page::className(), ['id' => 'page_id'])->viaTable('{{%text_place_content_page}}', ['content_id' => 'id']);
    }
    
    public function rules()
    {
        return [
            [['id', 'name', 'place_id', 'published', 'text', 'ordering', 'all_pages'], 'safe'],
            [['id', 'name', 'place_id', 'published', 'text', 'ordering'], 'safe', 'on'=>['insert', 'update']],
            
            [['id', 'published', 'place_id', 'ordering', 'all_pages'], 'number', 'integerOnly' => true],
            [['name'], 'required', 'on'=>['insert', 'update']],
            
            [['id', 'name', 'place_id', 'published', 'text', 'ordering', 'all_pages', 'pages'], 'safe', 'on'=>'search'],
        ];
    }
    
    public function attributeLabels() {
        
        return [
            'name' => \yii::t('app', 'Name'),
            'alias' => \yii::t('app', 'Alias'),
            'text' => \yii::t('app', 'Text'),
            'ordering' => \yii::t('app', 'Ordering'),
            'published' => \yii::t('app', 'Published'),
            'place_id' => \yii::t('app', 'Text place'),
            'all_pages' => \yii::t('app', 'Show on all pages'),
            'pages' => \yii::t('app', 'Show on pages'),
        ];
        
    }
    
    
    /*for crud admin*/
    public $_privateAdminName = 'Text place content'; // One item name, used in breadcrums, in header in actionAdmin
    public $_privatePluralName = 'Text place contents';// Item set name
    public $_privateAdminRepr = 'name';// method or property used to retrieve model string represantation. It's close to __toString but only for admin

    public function getAdminFields()
    {
            return array(
                    'place_id',
                    'name',
                    'published',
            );
            
    }

    public function getFieldsDescription()
    {
            return array(
                    'place_id' => ['RDbRelation', 'place'],
                    'name' => 'RDbText',
                    'text' => 'RDbText',
                    'ordering' => 'RDbText',
                    'all_pages' => 'RDbBoolean',
                    'pages' => ['RDbRelation', 'pages'],
                    'published' => 'RDbBoolean',
            );
    }
}
?>
