<?php
namespace app\models;

use app\components\CommonActiveRecord as ActiveRecord;

class Documents extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%docs}}';
    }
    
    public function rules()
    {
        return [
            [['id', 'name'], 'safe'],
            [['name'], 'required', 'on'=>['insert', 'update']], 
            [['name'], 'safe', 'on'=>'search'],
        ];
    }
    
    public function attributeLabels() {
        
        return [
            'name' => 'Название',
        ];
        
    }
    
    /*for crud admin*/
    public $_privateAdminName = 'Документ'; // One item name, used in breadcrums, in header in actionAdmin
    public $_privatePluralName = 'Документы';// Item set name
    public $_privateAdminRepr = 'name';
    
    public function getAdminFields()
    {
            return array(
              'name'
            );
            
    }

    public function getFieldsDescription()
    {
            return array(
                'name' => 'RDbText',
            );
    }
    
    
}
