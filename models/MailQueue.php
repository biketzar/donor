<?php
namespace app\models;

use app\components\CommonActiveRecord as ActiveRecord;

class MailQueue extends ActiveRecord
{
  public static function tableName()
  {
      return '{{%mail_queue}}';
  }
  
  public function rules()
  {
    return [[['id_donor', 'id_mail'], 'safe']];
  }
  
  public function getDonor()
  {
    return $this->hasOne(Donor::className(), ['id' => 'id_donor']);
  }
  
  public function getMail()
  {
    return $this->hasOne(Mail::className(), ['id' => 'id_mail']);
  }
  
  public function send()
  {
    $donorModel = $this->donor;
    $mailBodyModel = $this->mail;
    if(!$donorModel)
    {
      return 'Донор не найден';
    }
    if(!$mailBodyModel)
    {
      return 'Сообщение не найдено';
    }
    $mailer = new \app\components\MailHelper;
    $mailer->to = $donorModel->email;
    $mailer->unsubscribeLink = \yii\helpers\Url::to(['/user/unsubscribe/', 'c' => md5($donorModel->email)], TRUE);
    $mailer->subject = 'День донора в СПбПУ - '.$mailBodyModel->subject;
    $mailer->content = $mailBodyModel->body;
    $mailer->from = 'no-reply@donor.spb.ru';
    $mailer->params = [
        'FULLNAME' => "{$donorModel->surname} {$donorModel->name} {$donorModel->pname}",
        'NAME' => "{$donorModel->name}"
        ];
    $result = $mailer->sendOne();
    $log = new Maillog;
    $log->id_mail = $mailBodyModel->id;
    $log->id_donor = $donorModel->id;
    $log->status = (int)$result;
    $log->save();
    return $result;
  }
  
}
