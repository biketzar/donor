<?php
namespace app\models;

//use yii\db\ActiveRecord;
//use app\components\CommonActiveRecord;
//use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\Url;

class Page extends \app\components\CommonActiveRecord
{
    public static function tableName()
    {
        return '{{%pages}}';
    }
    
    public function init()
    {
        parent::init();
        
        $this->_privateAdminName = \yii::t('app', $this->_privateAdminName);
        $this->_privatePluralName = \yii::t('app', $this->_privatePluralName);
    }
    
    
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        return array_merge($behaviors, [
            'ml' => [
                'class' => \app\components\MultilingualBehavior::className(),
                //'attributes' => [
                    
                    'localizedAttributes' => [
                        'name'
                    ],
                    //'languages' => isset(\Yii::$app->params['translatedLanguages'])?\Yii::$app->params['translatedLanguages']:['ru', 'en'],
                    //'defaultLanguage' => isset(\Yii::$app->params['defaultLanguage'])?\Yii::$app->params['defaultLanguage']:'ru',
                    //'langClassName' => 'Translation',
                    //'langTableName' => 'translation',
                    //'langForeignKey' => 'owner_id',
                    //'dynamicLangClass' => true,
                    'strict' => false,
                //],
            ],
        ]);
    }
    
    public function afterFind()
    {
        parent::afterFind();
        //$this->menu = json_decode($this->menu, true);    
        $this->handler_data = json_decode($this->handler_data, true); 
        
        if(!is_array($this->handler_data))
            $this->handler_data = [];
        
        $this->lang = json_decode($this->lang, true); 
        
        if(!is_array($this->lang))
            $this->lang = [];
        
    }
    public function beforeSave($insert)
    {
        if(!is_string($this->handler_data))
        {
            $r = $this->handler_data;
            if(isset($r['empty']))
                unset($r['empty']);
            $this->handler_data = $r;
            $this->handler_data = json_encode($this->handler_data);   
        }
        if(!is_string($this->lang))
        {
            $r = $this->lang;
            if(isset($r['empty']))
                unset($r['empty']);
            $this->lang = $r;
            $this->lang = json_encode($this->lang);   
        }
        /*if(!is_string($this->menu))
            $this->menu = json_encode($this->menu);   */
        return parent::beforeSave($insert);
    }
    
    public function getParent()
    {
        return $this->hasOne(Page::className(), ['id' => 'parent_id'])->where($this->getTableSchema()->primaryKey[0].' !=:id', [':id' => intval($this->primaryKey)]);
    }
    public function getChildren()
    {
        return $this->hasMany(Page::className(), ['parent_id' => 'id']);
    }
    
    public function getActiveChildren()
    {
        return $this->hasMany(Page::className(), ['parent_id' => 'id'])->published();
    }
    
    /*public function getHandler()
    {
        return $this->hasMany(Handler::className(), ['id' => 'handler_id'])->viaTable('{{%page_handler}}', ['page_id' => 'id']);;
    }*/
    
    public function getContents()
    {
        return $this->hasMany(PageContent::className(), ['page_id' => 'id']);
    }
    
    public function getMenu()
    {
        return $this->hasOne(MenuType::className(), ['id' => 'menu_id']);
    }
    
    public function getMenuIds()
    {
        return $this->getMenu()->select('id')->column();
    }
    
    
    public function getMetadata()
    {
        return $this->hasOne(\app\models\Metadata::className(), ['item_id' => 'id'])
                ->where('model = :modelName', ['modelName' => self::className()]);
    }
    
    public function rules()
    {
        return [
            [['id', 'name', 'alias', 'route', 'menu_id', 'handler', 'handler_data', 'metatitle', 'metadescription', 'metakeywords', 'inmap', 'insearch', 'ordering', 'published', 'lang'], 'safe'],
            //[['name', 'alias', 'route', 'metatitle'], 'string', ['max'=>256]],
            [['id', 'parent_id', 'menu_id', 'published', 'inmap', 'insearch', 'ordering'], 'number', 'integerOnly' => true, 'on'=>['insert', 'update']],
            [['handler_data'], 'safe', 'on'=>['insert', 'update'], 'skipOnEmpty'=>false],
            [['name'], 'required', 'on'=>['insert', 'update']],
            //[['inmap', 'insearch'], 'default', 'value'=>1],
            [['url_pic'], 'app\components\CustomImageValidator', 'watermark' => 1, 'on'=>['insert', 'update']],
            [['alias'], 'app\components\AliasValidator', 'on'=>['insert', 'update'], 'skipOnEmpty'=>false],
            [['alias'], 'unique', 'targetAttribute' => ['alias', 'lang'], 'on'=>['insert', 'update']],
            [['id', 'name', 'alias', 'route', 'metatitle', 'metadescription', 'metakeywords', 'handler_id', 'menu_id', 'handler', 'handler_data', 'inmap', 'ordering', 'published', 'lang'], 'safe', 'on'=>'search'],
        ];
    }
    
    public static function getLangs()
    {
        return isset(\yii::$app->params['translatedLanguages'])?\yii::$app->params['translatedLanguages']:[];
    }
    
    public static function getHandlers()
    {
        static $result = null;
        if(is_array($result))
            return $result;
        $result = [
            'site/page' => \yii::t('app', 'Type page'),
            'site/sitemap' => \yii::t('app', 'Site map'),
            'site/search' => \yii::t('app', 'Search'),
        ];
        
        $modules = \yii::$app->getModules();
        
        foreach($modules as $moduleID => $module)
        {
            
            if(!is_object($module))
            {
                $module = (\Yii::$app->hasModule($moduleID))?\Yii::$app->getModule($moduleID):null;
            }
            
            if($module && method_exists($module, 'getHandlers'))
            {
                $result = array_merge($result, $module->getHandlers());
            }
        }
        
        return $result;
    }
    
    public static function getHandlerData($handler)
    {
        static $result = null;
        if(is_array($result))
        {
            return (isset($result[$handler])?$result[$handler]:[]);
        }
        $result = [
            'site/page' => [],
            'site/sitemap' => [],
        ];
        
        $modules = \yii::$app->getModules();
        
        foreach($modules as $moduleID => $module)
        {
            
            if(!is_object($module))
            {
                $module = (\Yii::$app->hasModule($moduleID))?\Yii::$app->getModule($moduleID):null;
            }
            
            if($module && method_exists($module, 'getHandlerData'))
            {
                $result = array_merge($result, $module->getHandlerData());
            }
        }
        
        return (isset($result[$handler])?$result[$handler]:[]);
    }
    public static function getHandlerRules($handler)
    {
        static $result = null;
        if(is_array($result))
        {
            return (isset($result[$handler])?$result[$handler]:[]);
        }
        $result = [
            'site/page' => [],
            'site/sitemap' => [],
            'site/sitemap-xml' => [],
        ];
        
        $modules = \yii::$app->getModules();
        
        foreach($modules as $moduleID => $module)
        {
            
            if(!is_object($module))
            {
                $module = \Yii::$app->getModule($moduleID);
            }
            
            if($module && method_exists($module, 'getHandlerRules'))
            {
                $result = array_merge($result, $module->getHandlerRules());
            }
        }
        
        return (isset($result[$handler])?$result[$handler]:[]);
    }
    
    public static function getHandlerModule($handler)
    {
        static $result = null;
        if(is_array($result))
        {
            return (isset($result[$handler])?$result[$handler]:null);
        }
        $result = [
            'site/page' => null,
            'site/sitemap' => null,
            'site/sitemap-xml' => null,
        ];
        
        $modules = \yii::$app->getModules();
        
        foreach($modules as $moduleID => $module)
        {
            
            if(!is_object($module))
            {
                $module = (\Yii::$app->hasModule($moduleID))?\Yii::$app->getModule($moduleID):null;
            }
            
            if($module && method_exists($module, 'getHandlers'))
            {
                $handlers = $module->getHandlers();
                
                foreach($handlers as $h => $m)
                {
                    $result[$h] = $moduleID;
                }
            }
        }
        
        return (isset($result[$handler])?$result[$handler]:null);
    }
    
    public static function getSitemapByHandler($page_id, $handler, $data = [])
    {
        static $result = null;
        $hash = $handler.serialize($data);
        if(isset($result[$hash]))
        {
            return $result[$hash];
        }
        /*$result = [
            'site/page' => [],
            'site/sitemap' => [],
            'site/sitemap-xml' => [],
        ];
        */
        $modules = \yii::$app->getModules();
        if(!$result)
            $result = [];
        $result[$hash] = [];
        foreach($modules as $moduleID => $module)
        {
            
            if(!is_object($module))
            {
                $module = (\Yii::$app->hasModule($moduleID))?\Yii::$app->getModule($moduleID):null;
            }
            
            if($module && method_exists($module, 'getSitemapByHandler'))
            {
                $result[$hash] = array_merge($result[$hash], $module->getSitemapByHandler($page_id, $handler, $data));
            }
        }
        
        return (isset($result[$hash])?$result[$hash]:[]);
    }
    
    public static function getSearchResultsByHandler($form, $page_id, $handler, $data = [])
    {
        static $result = [];
        $hash = $page_id.$handler.serialize($data).serialize($form->attributes);
        
        if(isset($result[$hash]))
        {
            return $result[$hash];
        }
        $result[$hash] = [];
        /*$result = [
            'site/page' => [],
            'site/sitemap' => [],
            'site/sitemap-xml' => [],
        ];*/
        $url = Url::toRoute(['/site/page', 'page_id' => $page_id]);
        
        $page = Page::findOne($page_id);
        
        //var_dump(\app\components\SiteHelper::strtolower($form->query, 'utf-8'));
        if(strpos(\app\components\SiteHelper::strtolower($page->name), \app\components\SiteHelper::strtolower($form->query, 'utf-8')) !== false)
        {
            $result[$hash][$url] =[
                'title' => $page->name,
                'url' => $url,
                'content' => '',
                'view' => ''
            ];
        }
            
        if($handler == 'site/page')
        {
            if($page)
            {
                $query = PageContent::find()->published()->page($page_id)->andFilterWhere(['like' , 'text', $form->query]);

                $data = $query->limit(1)->one();
                
                if($data)
                {
                    
                    $result[$hash][$url] =[
                        'title' => $page->name,
                        'url' => $url,
                        'content' => '',
                        'view' => ''
                    ];
                }
            }
        } else {
            $modules = \yii::$app->getModules();
            foreach($modules as $moduleID => $module)
            {

                if(!is_object($module))
                {
                    $module = (\Yii::$app->hasModule($moduleID))?\Yii::$app->getModule($moduleID):null;
                }

                if($module && method_exists($module, 'getSearchResultsByHandler'))
                {
                    $result[$hash] = array_merge($result[$hash], $module->getSearchResultsByHandler($form, $page_id, $handler, $data));
                }
            }
        }
        
        return (isset($result[$hash])?$result[$hash]:[]);
    }
    
    public function attributeLabels() {
        
        return [
            'name' => \yii::t('app', 'Name'),
            'alias' => \yii::t('app', 'Alias'),
            'published' => \yii::t('app', 'Published'),
            'handler' => \yii::t('app', 'Handler'),
            'parent_id' => \yii::t('app', 'Parent page'),
            'menu_id' => \yii::t('app', 'Menu'),
            'route' => \yii::t('app', 'Route'),
            'inmap' => \yii::t('app', 'In map'),
            'insearch' => \yii::t('app', 'In search'),
            'ordering' => \yii::t('app', 'Ordering'),
            'date' => \yii::t('app', 'Date'),
            'lang' => \yii::t('app', 'Lang'),
        ];
        
    }


    /*for crud admin*/
    public $_privateAdminName = 'Page'; // One item name, used in breadcrums, in header in actionAdmin
    public $_privatePluralName = 'Pages';// Item set name
    public $_privateAdminRepr = 'name';// method or property used to retrieve model string represantation. It's close to __toString but only for admin
    
    
    public $treeParentAttribute = 'parent_id';
    public $treeChildRelation = 'children';

    public function getAdminFields()
    {
            if(isset($this->isTreeAdminView)){
                    return array(
                            /*array(
                                    'filter' => $this->_model->getField('header')->getFieldFilter(), 
                                    'name' => 'header', 
                                    'value' => '(string)$data->getField("header")',
                                    'cssClassExpression' => '"deep".$data->deep',
                            ),*/
                            'name',
                            'alias',
                            'handler',
                            //'parent_id',
                            'menu_id',
                            'inmap',
                            'ordering'
                    );
            }else{
                    return array(
                            'name',
                            'alias',
                            'handler',
                            //'parent_id',
                            'menu_id',
                             //'inmap',
                            'ordering',
                            'published'
                    );
            }
    }

    public function getFieldsDescription()
    {
        $handler_data = static::getHandlerData($this->handler);
        
        $result = [
                    'parent_id' => array('RDbRelation', 'parent', 'treeParentRel'=>'parent', 'treeChildRel' => 'children', 'condition'),
                    'name' => 'RDbText',
                    
                    'alias' => 'RDbText',
                    'route' => 'RDbText',
                    //'handler' => array('RDbRelation', 'handler'),
                    'handler' => array('RDbSelect', 'data' => self::getHandlers()),
        ];
        //$result['handler_data[empty]'] = 'RDbHidden';
        foreach($handler_data as $k => $hd)
        {
            $result['handler_data['.$k.']'] = $hd;
        }
        
        $result = array_merge($result, [
                    //'keywords' => array('RDbText', 'forceTextArea' => true),
                    //'description' => array('RDbText', 'forceTextArea' => true),
                    'menu_id' => array('RDbRelation', 'menu'),
                    'inmap' => 'RDbBoolean',
                    'insearch' => 'RDbBoolean',
                    /*'metatitle' => 'RDbText',
                    'metadescription' => array('RDbText', 'forceTextArea' => true),
                    'metakeywords' => array('RDbText', 'forceTextArea' => true),*/
                    'published' => 'RDbBoolean',
                    'ordering' => 'RDbText',
                    'lang' => array('RDbSelect', 'data' => self::getLangs(), 'htmlOptions' => ['multiple' => true]),
                    //'url_pic' => 'RDbFile',
                    //'tags' => array('RDbRelation', 'tags'),
        ]);
        
        return $result;
    }

    public function relationReferenceOptions()
    {
            return array(
                    'contents' => array('controller' => 'page-content', 'relationName'=>'page', 'icon'=>'book'),
                    /*'photos' => array('controller' => 'photo', 'label'=>'Фотогалерея', 'icon' => 'icon_photos.gif'),
                    'metaTag' => array('controller' => 'MetaTag', 'label'=>'Мета-теги', 'icon' => 'icon_metatag.gif'),*/
                    //'children' => array('controller' => 'page', 'disable' => true),
                    /*'handler' => array('controller' => 'handler'),
                    'parent' => array('controller' => 'page'),*/
            );
    }

    public function getGridButtonColumns($columnParams = array('buttons'=>array(), 'template'=>'')) {
            //$columnParams = parent::getGridButtonColumns();
            if(\Yii::$app->user->can(\Yii::$app->controller->getAccessOperationName('clone')))
            {
                $columnParams['template'] .= ' {clone}';
                $columnParams['buttons']['clone'] = function ($url, $model, $key) 
                {
                    $options = array_merge([
                        'title' => \Yii::t('app', 'Clone'),
                        'aria-label' => \Yii::t('app', 'Clone'),
                        'data-pjax' => $key,
                        'class' => 'btn btn-blue btn-sm',
                    ], []);
                    return Html::a('<i class="fa fa-copy"></i>', $url, $options);
                };
            }
            return $columnParams;
    }

    /**
     * Возвращает список запрещенный связей
     * @return array 
     */
    public function getProhibitedAttributesForClone() {
        return [
            'children',
        ];
    }
    public function geExtendedAttributesForClone() {
        return [
            
        ];
    }
    public function getRelationsForClone() {
        return [
            'contents',
            'menu',
        ];
    }

    /**
     * Добавление в конце столбца подписей
     * @return array 
     */
    /*public function getChangedFieldsForClone() {
        return array(
            'header' => '_clone',
            'title' => '_clone',
        );
    }*/
    
    public static function find()
    {
        $query = new PageActiveQuery(get_called_class());
        return $query;
    }
}
class PageActiveQuery extends \app\components\CommonActiveQuery
{
    public $defaultOrder = ['ordering' => SORT_ASC];
}
?>
