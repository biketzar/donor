<?php
namespace app\models;

use yii\db\ActiveRecord as ActiveRecord;

use yii\data\Sort;

class Privatelog extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%private_log}}';
    }
    
    public function init()
    {
        parent::init();
        $this->_privateAdminName = \yii::t('app', $this->_privateAdminName);
        $this->_privatePluralName = \yii::t('app', $this->_privatePluralName);
        
    }
    
    public function rules()
    {
        return [
            [['controller', 'action', 'model', 'datetime'], 'safe', 'on' =>['insert', 'update']],
            [['user_id', 'item_id', 'is_post'], 'number', 'integerOnly' => true, 'on'=>['insert', 'update']],
            [['user_id', 'controller', 'action', 'datetime'], 'required', 'on'=>['insert', 'update']],
            [['user_id', 'controller', 'action', 'model', 'datetime', 'item_id', 'is_post'], 'safe', 'on'=>'search'],
        ];
    }
    
    public function attributeLabels() {
        
        return [
            'controller' => \yii::t('app', 'Controller'),
            'action' => \yii::t('app', 'Action'),
            'model' => \yii::t('app', 'Model'),
            'datetime' => \yii::t('app', 'Date Time'),
            'user_id' => \yii::t('app', 'User'),
            'item_id' => \yii::t('app', 'Id item_id'),
            'is_post' => \yii::t('app', 'Is POST request'),
        ];
        
    }
    
    public function getItem()
    {
        if($this->model && class_exists($this->model))
        {
            return $this->hasOne($this->model, ['id' => 'item_id']);
        } else
            return $this->hasOne(self::className(), ['id' => 'id']);
    }
    
    public function getUser()
    {
        return $this->hasOne(\app\models\UserIndentity::className(), ['id' => 'user_id']);
    }
    
    public function searchQuery($data)
    {
        $className = $this->className();
        $formName = $this->formName();
        
        /*if(isset($data[$formName]))
            $data = $data[$formName];*/
        $query = $className::find();
        
        $this->load($data);
        
        $query->andFilterWhere(['controller' => $this->controller]);
        $query->andFilterWhere(['model' => $this->model]);
        $query->andFilterWhere(['item_id' => $this->item_id]);
        $query->andFilterWhere(['action' => $this->action]);
        $query->andFilterWhere(['datetime' => $this->datetime]);
        $query->andFilterWhere(['user_id' => $this->user_id]);
        
        $getParams = \Yii::$app->request->getQueryParams(); 
        
        if(isset($getParams))
        {
            foreach($getParams as $param=>$value)
            {
                if(isset($value))
                {
                    if($relation = $this->getRelation($param, false))
                    {
                        /*$p = array_values($relation->link);
                        if(!$relation->multiple)
                        {
                            foreach($relation->link as $a=>$b)
                                $query->andFilterWhere([$b => $value]);
                            
                        } else {
                            
                            $query->with($param)->andFilterWhere([$param => $value]);
                            
                        }
                        $createUrlParams[$param] = $value;*/
                    } else {
                        if($this->hasAttribute($param))
                        {
                            $this->$param = $value;
                            $query->andFilterWhere([$param => $value]);
                        }
                        
                    }
                }
            }
            foreach($getParams as $param=>$value)
            {
                if(isset($value))
                {
                    if($relation = $this->getRelation($param, false))
                    {
                        $p = array_values($relation->link);
                        if(!$relation->multiple)
                        {
                            foreach($relation->link as $a=>$b)
                                $query->andFilterWhere([$b => $value]);
                            
                        } else {
                            
                            $query->with($param)->andFilterWhere([$param => $value]);
                            
                        }
                        $createUrlParams[$param] = $value;
                    } else {
                        /*if($this->hasAttribute($param))
                            $query->andFilterWhere([$param => $value]);
                        */
                    }
                }
            }
        }
        
        $query->orderBy(['datetime' => SORT_DESC]);
        
        
        
        return $query;
    }
    
    /*for crud admin*/
    public $_privateAdminName = 'Private log'; // One item name, used in breadcrums, in header in actionAdmin
    public $_privatePluralName = 'Private log';// Item set name
    public $_privateAdminRepr = 'model';
    
    public function getAdminFields()
    {
            return array(
                    'user_id',
                    'action',
                    'datetime',
            );
            
    }

    public function getFieldsDescription()
    {
            return array(
                    'user_id' => array('RDbRelation', 'user'),
                    'action' => 'RDbText',
                    'datetime' => ['RDbDate', 'format' => 'php:Y-m-d H:i:s', 'dateFormat' => 'yyyy-MM-dd'],
            );
    }
    
    /*public function getViewExcludedFields()
    {
        return ['item_id', 'model'];
    }*/
    
    public function relationParentReferenceOptions()
    {
        $result = [];
        if($this->model)
        {
            $privateModelAdmin = \Yii::createObject(['class' => 'app\modules\privatepanel\components\PrivateModelAdmin'],[$this]);
            $result = array_merge($result, $privateModelAdmin->resolveParent($this->model, 'item'));
        }
        return $result;
    }
    
    public function getActionText()
    {
        return ($this->action)?\yii::t('app', 'Action '.$this->action):$this->action;
    }
    
    public function getGridButtonColumns($columnParams)
    {
        $columnParams['template'] = '';
        return $columnParams;
    }
    
    public function getAdminMenu($privateModel, $menu = [])
    {
        
        return [];

    }
    
}
?>
