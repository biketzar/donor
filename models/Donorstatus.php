<?php
namespace app\models;

use app\components\CommonActiveRecord as ActiveRecord;

class Donorstatus extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%donorstatus}}';
    }
    
    public function init()
    {
        parent::init();
        $this->_privateAdminName = 'Статус';
        $this->_privatePluralName = 'Статусы';
        
    }
    public static function find() {
        return parent::find()->orderBy('id DESC');
    }

        public function rules()
    {
        return [
            [['id', 'title'], 'safe'],
            [['id', 'active'], 'number', 'integerOnly' => true, 'on'=>['insert', 'update']],
            [['title'], 'required', 'on'=>['insert', 'update']],
            [['id', 'title', 'active'], 'safe', 'on'=>'search'],
        ];
    }
    
    public function attributeLabels() {
        
        return [
            'id' => 'Id',
            'title' => 'Название',
            'active' => 'Статус сезона'
        ];
    }
    
    public function beforeSave($insert) {
        if($this->active)
        {
            $models = self::find()->where('active=1')->all();
            foreach($models as $model)
            {
                $model->active = 0;
                $model->save();
            }
        }
        return parent::beforeSave($insert);
    }
    
    /*for crud admin*/
    public $_privateAdminName = 'Статус'; // One item name, used in breadcrums, in header in actionAdmin
    public $_privatePluralName = 'Статусы';// Item set name
    public $_privateAdminRepr = 'title';
    
    public function getAdminFields()
    {
            return array(
                    'id',
                    'title',
                    'active'
            );
            
    }

    public function getFieldsDescription()
    {
            return array(
                    'id' => 'RDbText',
                    'title' => 'RDbText',
                    'active' => 'RDbBoolean'
            );
    }
    
    public function getFormExcludedFields()
    {
        return [];
    }
    
    public function beforeDelete() {
        $donor = Donor::find()->where('donorstatus=:donorstatus', [':donorstatus' => $this->id])->one();
        if($donor)
        {
            return false;
        }
        return parent::beforeDelete();
    }
}

