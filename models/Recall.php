<?php
namespace app\models;

use app\components\CommonActiveRecord as ActiveRecord;

class Recall extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%recall}}';
    }
    
    public function init()
    {
        parent::init();
        $this->_privateAdminName = \yii::t('app', $this->_privateAdminName);
        $this->_privatePluralName = \yii::t('app', $this->_privatePluralName);
        
    }
    public static function find() {
        return parent::find()->orderBy('is_viewed ASC, cur_tme DESC');
    }

        public function rules()
    {
        return [
            [['id', 'name', 'email', 'dsc'], 'safe'],
            [['dsc'], 'safe', 'on'=>['insert', 'update']],
            [['is_viewed'], 'number', 'integerOnly' => true, 'on'=>['insert', 'update']],
            [['name', 'email'], 'required', 'on'=>['insert', 'update']],
            [['email'], 'email', 'on' => ['insert', 'update']],
            [['id', 'name', 'dsc', 'cur_tme','email', 'is_viewed'], 'safe', 'on'=>'search'],
        ];
    }
    
    public function attributeLabels() {
        
        return [
            'name' => 'Имя',
            'email' => 'Email',
            'is_viewed' => 'Обработан',
            'dsc' => 'Сообщение',
            'cur_tme' => 'Дата обращения',
        ];
        
    }
    public function beforeSave($insert) {
        $this->name = strip_tags($this->name);
        $this->dsc = strip_tags($this->dsc);
        return parent::beforeSave($insert);
    }
    
    private function sendMail($to, $subject, $replyto, $params = array())
    {
        $template = '<html>
                    <head>
                     <title>Обратная связь</title>
                    </head>
                    <body>
                    <table>
                    <tbody>
                    <tr><td>Имя:</td><td>[NAME]</td></tr>
                    <tr><td>Email:</td><td>[EMAIL]</td></tr>
                    <tr><td>Сообщение:</td><td>[MESSAGE]</td></tr>
                    </tbody>
                    </table>
                    </body>
                    </html>';
        /* тема/subject */
        foreach($params as $key=>$value)
        {
                $template = str_replace('['.$key.']', $value, $template);
        }
        $headers= "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=utf8\r\n";
        $headers .= "From: no-reply@donor.spb.ru\r\n";
        $headers .= "Reply-To: {$replyto}\r\n";
	mail('w1121@yandex.ru', $subject, $template, $headers);
        return mail($to, $subject, $template, $headers);
    }
    
    public function afterSave($insert, $changedAttributes) {
        if($insert)
        {
            $params = [];
            $params['NAME'] = $this->name;
            $params['EMAIL'] = $this->email;
            $params['MESSAGE'] = $this->dsc;
            $this->sendMail(\app\components\ConstantHelper::getValue('recall-mail', 'w1121@yandex.ru'), 'Обратная связь', $this->email, $params);
        }
        return parent::afterSave($insert, $changedAttributes);
    }
    
    /*for crud admin*/
    public $_privateAdminName = 'Обратная связь'; // One item name, used in breadcrums, in header in actionAdmin
    public $_privatePluralName = 'Обратная связь';// Item set name
    public $_privateAdminRepr = 'name';
    
    public function getAdminFields()
    {
            return array(
                    'name',
                    'email',
                    'dsc',
                    'is_viewed',
                    'cur_tme',
            );
            
    }

    public function getFieldsDescription()
    {
            return array(
                    'name' => 'RDbText',
                    'email' => 'RDbText',
                    'dsc' => ['RDbText', 'forceTextArea'=>'forceTextArea'],
                    'is_viewed' => 'RDbBoolean',
                    'cur_tme' => ['RDbDate', 'format'=>'dd-MM-yyyy HH:mm', 'inputFormat'=>'dd-MM-yyyy HH:mm'],
            );
    }
    
}
?>
