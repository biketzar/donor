<?php

namespace app\models;

use yii\db\ActiveRecord;

use \app\components\CommonActiveQuery;

class UserIndentity extends \app\components\CommonActiveRecord implements \yii\web\IdentityInterface
{
    //public $id;
    //public $username;
    //public $password;
    //public $authKey;
    //public $access_token;
    const STATUS_INACTIVE = '0';
    const STATUS_ACTIVE = '1';
    const STATUS_BLOCKED = '2';
    
    public $password_repeat = null;

    /*private static $users = [
        '100' => [
            'id' => '100',
            'username' => 'admin',
            'password' => 'admin',
            'authKey' => 'test100key',
            'accessToken' => '100-token',
        ],
        '101' => [
            'id' => '101',
            'username' => 'demo',
            'password' => 'demo',
            'authKey' => 'test101key',
            'accessToken' => '101-token',
        ],
    ];*/
    public static function tableName()
    {
        return '{{%users}}';
    }
    
    public function init()
    {
        parent::init();
        
        $this->_privateAdminName = \yii::t('app', $this->_privateAdminName);
        $this->_privatePluralName = \yii::t('app', $this->_privatePluralName);
    }
    
    public static function find()
    {
        $query = new UserIndentityActiveQuery(get_called_class());
        return $query;
    }
    
    public function rules()
    {
        return [
            [['name', 'username', 'email', 'status', 'last_visit'], 'safe', 'on' => 'search'],
            [['name', 'username', 'email', 'status', 'password_repeat'], 'safe'],
            [['name', 'username', 'email', 'status', 'password', 'email'], 'safe', 'on' => ['insert', 'update']],
            [['email', 'status'], 'required', 'on' => ['insert', 'update']],
            [['password'], 'required', 'on' => ['insert']],
            ['password', 'compare', 'on' => ['insert', 'update']],
            ['status', 'default', 'value' => self::STATUS_ACTIVE, 'on' => ['insert', 'update']],
            ['status', 'in', 'range' => array_keys(self::getStatuses())],
            [['username'], 'string', 'max' => 100, 'min' => 4, 'on' => ['insert', 'update']],
            //[['username'], 'match', 'pattern' => '/^[a-zA-Z0-9]+$/', 'message' => 'Логин может состоять только из букв английского алфавита и цифр', 'on' => ['insert', 'update']],
            [['email'], 'email', 'on' => ['insert', 'update']],
            ['email', 'unique', 'targetClass' => '\app\models\UserIndentity', 'message' => \Yii::t('app','This email address has already been taken.')],
            ['username', 'unique', 'targetClass' => '\app\models\UserIndentity', 'message' => \Yii::t('app','This username has already been taken.')],
        ];    
    }
    
    public function attributeLabels() {
        
        return [
            'name' => \yii::t('modules/user/app', 'Person Name'),
            'username' => \yii::t('modules/user/app', 'Username'),
            'email' => \yii::t('modules/user/app', 'Email'),
            'status' => \yii::t('modules/user/app', 'Status'),
            'password' => \yii::t('modules/user/app', 'Password'),
            'password_repeat' => \yii::t('modules/user/app', 'Password repeat'),
            'last_visit' => \yii::t('modules/user/app', 'Last Visit'),
            'roles' => \yii::t('modules/user/app', 'Roles'),
            'create_time' => \yii::t('modules/user/app', 'Create Time'),
            'update_time' => \yii::t('modules/user/app', 'Update Time'),
        ];
        
    }
    
    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }

    /**
     * Finds user by username
     *
     * @param  string      $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        //return static::findOne(['username' => $username]);
        return static::find()->andWhere('username=:username OR email=:username', [':username' => $username])->one();
    }
    
    /**
     * Finds active user by username
     *
     * @param  string      $username
     * @return static|null
     */
    public static function findActiveByUsername($username)
    {
        return static::find()->andWhere('username=:username OR email=:username', [':username' => $username])->andWhere(['status' => 1])->one();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param  string  $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return \Yii::$app->getSecurity()->validatePassword($password, $this->password);
    }
    
    public function beforeSave($insert)
    {
        $dirtyAttributes = $this->getDirtyAttributes();
        if(isset($dirtyAttributes['password']) && $dirtyAttributes['password'])
        {
            $this->password = \Yii::$app->getSecurity()->generatePasswordHash($dirtyAttributes['password']);
        } else {
            $this->password = $this->getOldAttribute('password');
        }
        
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->access_token = \Yii::$app->getSecurity()->generateRandomString();
            }
            return true;
        }
        return false;
    }
    
    
    
    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->access_token = \Yii::$app->security->generateRandomString();
    }
    
    public function generatePassword()
    {
        $this->password = \Yii::$app->security->generateRandomString(8);
    }

    /**
     * Removes password reset token
     */
    public function removeAccessToken()
    {
        $this->access_token = null;
    } 
    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        $expire = \Yii::$app->params['user.passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int) end($parts);
        if ($timestamp + $expire < time()) {
            // token expired
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }
    
    
    public static function getStatuses()
    {
        return [
            self::STATUS_ACTIVE => \Yii::t('app', 'Active'),
            self::STATUS_INACTIVE => \Yii::t('app', 'Inactive'),
            self::STATUS_BLOCKED => \Yii::t('app', 'Blocked'),
        ];
    }
    
    public function getStatusName()
    {
        $items = self::getStatuses();
        return (isset($items[$this->status])?$items[$this->status]:'');
    }
    
    public function getFullName()
    {
        return $this->name;
    }
    
    public function getAvatarDirectory()
    {
        
        return \yii::getAlias('@web2').'/files/avatar';
    }
    
    public function getAvatarPath()
    {
        if(!$this->avatar)
            return '';
		
        return $this->getImageDirectory().'/'.$this->avatar;
    }
    
    
    
    /*for crud admin*/
    public $_privateAdminName = 'User'; // One item name, used in breadcrums, in header in actionAdmin
    public $_privatePluralName = 'Users';// Item set name
    public $_privateAdminRepr = 'username';

    
}

class UserIndentityActiveQuery extends CommonActiveQuery
{
    public function status($status)
    {
        if($status !== null)
            return $this->andWhere(['status=:status', [':status' => intval($status)]]);
        else
            return $this;
    }
    
}
