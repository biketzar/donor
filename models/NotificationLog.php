<?php
namespace app\models;

use app\components\CommonActiveRecord as ActiveRecord;

class NotificationLog extends ActiveRecord
{
    public $ihfo = '';
    
    public static function tableName()
    {
        return '{{%notification_log}}';
    }
    
    public function rules()
    {
        return [
            [['donor_id', 'date_id', 'status', 'notification_id'], 'safe'],           
            [['donor_id', 'date_id', 'status', 'notification_id'], 'safe', 'on'=>'search'],
        ];
    }
}