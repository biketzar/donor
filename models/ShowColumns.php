<?php
namespace app\models;


use \app\components\CommonActiveRecord as ActiveRecord;

class ShowColumns extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%show_columns}}';
    }
   
    public function attributeLabels() {
        
        return [
            'column_names' => 'column_names',
        ];
    }
        
    public function rules()
    {
        return [
            [['tabl_name','column_names'], 'safe','on'=>['insert', 'update', 'search']],
        ];
    }
    

    /*for crud admin*/
    public $_privateAdminName = 'Настройка колонок'; // One item name, used in breadcrums, in header in actionAdmin
    public $_privatePluralName = 'Настройка колонок';// Item set name
    public $_privateAdminRepr = 'tabl_name';// method or property used to retrieve model string represantation. It's close to __toString but only for admin
    
    
    public function getAdminFields()
    {
            return array(
                    'column_names',
            );
            
    }

    public function getFieldsDescription()
    {
            return array(
                    'column_names' => ['RDbText', 'foreceText'=>TRUE],
            );
    }

}
?>
