<?php
namespace app\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use yii\bootstrap\ActiveForm;

class SiteSearchWidget extends Widget
{
    public function init()
    {
        parent::init();
        
    }

    public function run()
    {
        $model = \yii::createObject('\app\models\SearchForm');
            
        if(\yii::$app->request->get('query'))
        {
            $model->query = \yii::$app->request->get('query');
        }
        $view = $this->getView();
        
        ob_start();
        $dom_id = uniqid();
        $suggest_url = \yii\helpers\Url::toRoute('@web/site/search-suggest');
        $search_url = \yii\helpers\Url::toRoute('@web/site/search');
        $form = ActiveForm::begin(['action' => $search_url, 'id' => 'search-form'.$dom_id, 'method' =>'get', 'options' => ['role' => 'search']]);
        
        echo $form->errorSummary($model);
        
        echo \yii\jui\AutoComplete::widget([
         'name' => 'query',
         'clientOptions' => [
             'source' => $suggest_url,
            'minLength' => 2,
            'select' => new \yii\web\JsExpression(' function( event, ui ) {
                    if(ui.item.id){
                            location.href = ui.item.id;
                            return;
                    }
                    $("#search-form'.$dom_id.'").submit();
                }'),
         ],
     ]);
        /*echo $form->field($model, 'query', ['inputOptions'=> ['class'=>'search-input', 'name' => 'query']])->widget(\yii\jui\AutoComplete::classname(), [
    'clientOptions' => [
        //'source' => ['USA', 'RUS'],
        'source' => $suggest_url,
        'minLength' => 2,
        'select' => new \yii\web\JsExpression(' function( event, ui ) {
                if(ui.item.id){
			location.href = ui.item.id;
			return;
		}
		$("#search-form'.$dom_id.'").submit();
            }'),
    ],
]);*/?>
    <div class="form-group">
        <?= Html::submitButton(\yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
    </div>
        <?php
        ActiveForm::end();
        
        $content = ob_get_contents();
        ob_end_clean();
        
        return $content;
    }
}
