<?php
namespace app\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;

class TextBlockWidget extends Widget
{
    public $position = 'position';
    public $count = 1;
    public $order = 'order';
    public $class = '';
    
    protected $blocks = [];
    
    public function init()
    {
        parent::init();
        $place = \app\models\TextPlace::find()->where('id=:id', [':id' => $this->position])->published()->one();
        if(!$place)
        {
            $place = \app\models\TextPlace::find()->where('alias=:id', [':id' => $this->position])->published()->one();
        }
        if($place)
        {
            $query = $place->getPageContents(\yii::$app->request->get('page_id'))->limit($this->count);
            
            if($this->order == 'rand')
            {
                $query->orderBy('RAND()');
            }
            $this->blocks = $query->all();
            
        }
    }

    public function run()
    {
        $content = '';
        foreach($this->blocks as $block)
        {
            $content .= Html::tag('div', HtmlPurifier::process($block->text), ['class' => $this->class]);
        }
        return $content;
    }
}
