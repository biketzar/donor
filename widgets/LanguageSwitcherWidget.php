<?php
namespace app\widgets;

use yii\base\Widget;
use yii\helpers\Html;

class LanguageSwitcherWidget extends Widget
{
    public function run()
    {
        $currentUrl = ltrim(\Yii::$app->request->getPathInfo());
        
        $links = array();
        $lang = \yii::$app->language;
        $langs = isset(\yii::$app->params['translatedLanguages'])?\yii::$app->params['translatedLanguages']:[];
        $default = (isset(\yii::$app->params['defaultLanguage']))?\yii::$app->params['defaultLanguage']:\yii::$app->language;
        foreach ($langs as $suffix => $name){
            $class = array();
            
            if($suffix == $lang)
                $class[]= ' active';
            
            /*if ($suffix === $default) {
                $suffix = '';
                
            } else {
                //$suffix = '_' . $suffix;
                
            }
            */
            //$url = ($suffix ? trim($suffix, '_') . '/' : '') . $currentUrl;
            $class[] = $suffix;
            
            //$links[] = CHtml::tag('li', array('class'=>implode(' ',$class)), CHtml::link($name, $url));
            $links[] = Html::a($name, ['//'.$currentUrl, 'lang'=>$suffix], array('class'=>implode(' ',$class)));
        }
        echo '<div class="lang">';
        //echo CHtml::tag('ul', array('class'=>'language'), implode("\n", $links)); 
        echo implode("\n",$links);
        echo '</div>';
    }
}
?>
