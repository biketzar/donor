<?php
use yii\helpers\Html;
if(count($images)):
?>
<div class="photo-gallery">
<?php
foreach ($images as $image): ?>
    <a href="<?php echo $image->getImagePath();?>" rel="lightbox[<?php echo $gallery;?>]" class="fancybox" title="<?php echo Html::encode($image->name);?>">
        <?php echo Html::img(\app\components\ImageHelper::thumbnail($image->getImagePath(), $width, $height)); ?>
    </a>
<?php endforeach;
?>
</div>
<?php
endif;
?>