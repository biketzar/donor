<?php
namespace app\widgets;

use yii\base\Widget;
use yii\helpers\Html;

class PhotogalleryWidget extends Widget
{
    public $width = 200;
    public $height = 200;
    public $type = 'simple';
    
    public $gallery = '';
    
    public $images = [];
    
    public function init()
    {
        parent::init();
        if(!$this->gallery)
                $this->gallery = 'gallery'.uniqid();
        
    }

    public function run()
    {
        
        return $this->render('photogallery-'.$this->type, [
            'images' => $this->images,
            'width' => $this->width,
            'height' => $this->height,
            'gallery' => $this->gallery,
        ]);
    }
}
