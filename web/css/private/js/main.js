if(window["jQuery"]){
	var initHelpSystem = function(){
		$("a.helpButton").bind('mouseover', {}, function(){
			$message = $('#'+$(this).attr('rel'));
			$message.show();
		}).bind('mouseout', {}, function(){
			$message = $('#'+$(this).attr('rel'));
			if(!$message.hasClass("byClick")){
				$message.hide();
			}
		}).click(function(){
			$message = $('#'+$(this).attr('rel'));
			if($message.hasClass("byClick")){
				$message.hide();
				$message.removeClass("byClick");
			}else{
				$message.show();
				$message.addClass("byClick");
			}
		});
	}
	$(document).ready(initHelpSystem);
}

if(window["jQuery"]){
(function($) {
	$.fn.gridSortableTable = function(options) {
		// Default settings
		var defaults = {
			handle: '',
			placeholder: 'sortable-placeholder',
			csrfToken: ''
		};

		// Merge the options with the defaults
		var settings = $.extend(defaults, options);

		// Run this for each selected element
		return this.each(function() {

			var $this = $(this);
			var $tbody = $this.find('tbody');

			// Apply jui sortable on the element
			$tbody.sortable({
				axis: 'y',
				containment: 'parent',
				cursor: 'pointer',
				delay: 100,
				distance: 5,
				forceHelperSize: true,
				forcePlaceholderSize: true,
				tolerance: 'pointer',
				handle: settings.handle,
				placeholder: settings.placeholder,
				// Helper function to set correct column widths while dragging
				helper: function(e, tr) {
					var $helper = tr.clone();
					$helper.children().each(function(index) {
						$(this).width(tr.children().eq(index).width());
					});
					return $helper;
				},
				// Actions to be taken when the row is dropped
				update: function(e, ui) {
					// Run an Ajax request to save the new weights
					$.post(settings.url, {
						result: $tbody.sortable('toArray')
					});
				},
				// Actions to be taken when sorting is stopped
				stop: function(e, ui) {
					// Update the row classes
					$tbody.children().each(function(index) {
						index%2===0 ? $(this).removeClass('even').addClass('odd') : $(this).removeClass('odd').addClass('even');
					});
				}
			})
			.disableSelection();
		});
	};
})(jQuery);
}

if(window["jQuery"] && window["jQuery"].widget){
(function( $ ) {
	$.widget( "ui.combobox", {
		_create: function() {
			var input,
				self = this,
				select = this.element.hide(),
				selected = select.children( ":selected" ),
				value = selected.val() ? selected.text() : "",
				wrapper = $( "<span>" )
					.addClass( "ui-combobox" )
					.insertAfter( select )
					.css({whiteSpace: "nowrap"}),
				opts = {
					suggestUrl: "/address/SuggestGood",
					width: "90%"
				};

			input = $( "<input>" )
				.appendTo( wrapper )
				.val( value )
				//.addClass( "ui-state-default" )
				.attr("title", "Начните вводить название, а потом выберите вариант из списка")
				.autocomplete({
					delay: 0,
					minLength: 2,
					source: opts.suggestUrl,
					select: function( event, ui ) {
						select.append($("<option></options").html(ui.item.value).attr("value", ui.item.id).attr("selected", true));
						self._trigger( "selected", event, {
							item: ui.item.option
						});
					},
					change: function( event, ui ) {
					},
					search: function( event, ui ) {
						select.empty();
					},
					open: function( event, ui) {
						console.log(ui);
					}
				})
				.addClass( "ui-widget ui-widget-content" )
				.css({width: opts.width})
				.keydown(function( event ){
					if($(this).val().length == 0){
						select.empty();
					}
				});
			// подставляем собственный метод рендеринга одного варианта, что бы в названии выделить совпадение.
			input.data( "autocomplete" )._renderItem = function( ul, item ) {
				var term = input.val(), 
					text = item.label.replace(
						new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + $.ui.autocomplete.escapeRegex(term) + ")(?![^<>]*>)(?![^&;]+;)", "gi"), 
						"<strong>$1</strong>" 
					);
				return $( "<li></li>" )
					.data( "item.autocomplete", item )
					.append( "<a>" + text + "</a>" )
					.appendTo( ul );
			};
			// подставляем собственный метод рендеринга всех вариантов, что в случае если у нас один единственный вариант то он выбирался автоматически.
			input.data( "autocomplete" )._renderMenu = function( ul, items ) {
				var self = this;
				$.each( items, function( index, item ) {
					self._renderItem( ul, item );
				});
				if(items.length == 1){
					select.append($("<option></options").html(items[0].value).attr("value", items[0].id).attr("selected", true));
				}
			},
			
			
			$( "<a>" )
				.attr( "tabIndex", -1 )
				.attr( "title", "Show All Items" )
				.appendTo( wrapper )
				.button({
					icons: {
						primary: "ui-icon-triangle-1-s"
					},
					text: false
				})
				.removeClass( "ui-corner-all" )
				.addClass( "ui-corner-right ui-button-icon" )
				.click(function() {
					// close if already visible
					if ( input.autocomplete( "widget" ).is( ":visible" ) ) {
						input.autocomplete( "close" );
						return;
					}

					// work around a bug (likely same cause as #5265)
					$( this ).blur();

					// pass empty string as value to search for, displaying all results
					input.autocomplete( "search", "" );
					input.focus();
				}).hide();
			
			$( "<span>&nbsp...</span>" )
				.appendTo( wrapper );
		},

		destroy: function() {
			this.wrapper.remove();
			this.element.show();
			$.Widget.prototype.destroy.call( this );
		}
	});
	
	
	$.widget( "ui.multicombobox", {
		_create: function() {
			var input,
				selectedItems,
				addValue,
				self = this,
				selectName = this.element.prop("name");
				select = this.element.hide().prop("name", ""),
				selected = select.children( ":selected" ),
				value = "",
				wrapper = $( "<span>" )
					.addClass( "ui-combobox" )
					.insertAfter( select )
					.css({whiteSpace: "nowrap"}),
				opts = {
					suggestUrl: "/address/SuggestGood",
					width: "90%"
				};
			
				
			addValue = function(value, text){
				selectedItems
				.append($("<li>")
					.append($("<input>").prop("type", "hidden").prop("name", selectName).prop("value", value))
					.append($("<span>").html(text + "&nbsp;"))
					.append($("<a>").html("[X]").click(function(){$(this).parent().remove();}).css({cursor: "pointer"}).attr("title", "Убрать вариант"))
				);
			}	
			
			
			
			input = $( "<input>" )
				.appendTo( wrapper )
				.val( value )
				//.addClass( "ui-state-default" )
				.attr("title", "Начните вводить название, а потом выберите вариант из списка")
				.autocomplete({
					delay: 0,
					minLength: 2,
					source: opts.suggestUrl,
					select: function( event, ui ) {
					    addValue(ui.item.id, ui.item.value);
						self._trigger( "selected", event, {
							item: ui.item.option
						});
						setTimeout(function(){input.val("")}, 200);
					},
					search: function( event, ui ) {
						select.empty();
					}
				})
				.addClass( "ui-widget ui-widget-content" )
				.css({width: opts.width})
				.keydown(function( event ){
					if($(this).val().length == 0){
						select.empty();
					}
				});
			
			input.data( "autocomplete" )._renderItem = function( ul, item ) {
				var term = input.val(), 
					text = item.label.replace(
						new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + $.ui.autocomplete.escapeRegex(term) + ")(?![^<>]*>)(?![^&;]+;)", "gi"), 
						"<strong>$1</strong>" 
					);
				return $( "<li></li>" )
					.data( "item.autocomplete", item )
					.append( "<a>" + text + "</a>" )
					.appendTo( ul );
			};
			
			$( "<span>&nbsp...</span>" )
				.appendTo( wrapper );
				
			selectedItems = $( "<ul>" )
				.appendTo( wrapper )
				.css({margin: 0, padding: "0 0 0 15px"});
			
			selected.each(function(){
				var item = $(this);
				addValue(item.prop("value"), item.html());
				item.remove();
			});	
		},

		destroy: function() {
			this.wrapper.remove();
			this.element.show();
			$.Widget.prototype.destroy.call( this );
		}
	});
})( jQuery );
}