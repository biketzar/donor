var map = new Array();
var marker = new Array();
    
function initGoogleMap(mapid, tempid, id, latitude, longtutude, related)
{
    function initialize() {
      var mapOptions = {
        zoom: 8,
        center: new google.maps.LatLng(latitude, longtutude),
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };
      map[mapid] = new google.maps.Map(document.getElementById(mapid),
          mapOptions);
      var myLatlng = new google.maps.LatLng(latitude, longtutude);
      marker[mapid] = new google.maps.Marker({
        position: myLatlng,
        map: map[mapid],
        draggable:true,
        
        title: 'Здесь!'
    });
    
         google.maps.event.addListener(marker[mapid], 'dragend', function(event){
             var point = marker[mapid].getPosition();
             document.getElementById(tempid+'_latitude').value = point.lat();
             document.getElementById(tempid+'_longitude').value = point.lng();
             document.getElementById(id).value =point.lat()+';'+point.lng();
         });
        if(!latitude && !longtutude && related != '')
        {
            related = related.split(',');
            address = '';
            for(i = 0; i<related.length; i++)
            {
                if($('#'+related[i]).length > 0)
                {
                    if($('#'+related[i]).find('option').length > 0)
                        address += $('#'+related[i]+' :selected').text()+' ';
                    else
                        address += $('#'+related[i]).val()+' ';
                } else {
                    address += related[i]+' ';
                }
            }
            
            geocoder = new google.maps.Geocoder();
            geocoder.geocode( { 'address': address}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    marker[mapid].setPosition(results[0].geometry.location);
                    map[mapid].setCenter(results[0].geometry.location);
                    document.getElementById(tempid+'_latitude').value = results[0].geometry.location.lat();
                    document.getElementById(tempid+'_longitude').value = results[0].geometry.location.lng();
                    document.getElementById(id).value =results[0].geometry.location.lat()+';'+results[0].geometry.location.lng();
                } else {
                  //alert('Geocode was not successful for the following reason: ' + status);
                }
              });
        }

    }

    google.maps.event.addDomListener(window, 'load', initialize);
    
    $(document).ready(function () {
        $('#'+tempid+'_latitude, #'+tempid+'_longitude').on('change', function(){
            var myLatlng = new google.maps.LatLng($('#'+tempid+'_latitude').val(), $('#'+tempid+'_longitude').val());
            marker[mapid].setPosition(myLatlng);
            document.getElementById(id).value =$('#'+tempid+'_latitude').val()+';'+$('#'+tempid+'_longitude').val();
        });
        $('#'+tempid+'_searchbutton').on('click', function(){
            geocoder = new google.maps.Geocoder();
            address = $('#'+tempid+'_search').val();
            geocoder.geocode( { 'address': address}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    marker[mapid].setPosition(results[0].geometry.location);
                    map[mapid].setCenter(results[0].geometry.location);
                    document.getElementById(tempid+'_latitude').value = results[0].geometry.location.lat();
                    document.getElementById(tempid+'_longitude').value = results[0].geometry.location.lng();
                    document.getElementById(id).value =results[0].geometry.location.lat()+';'+results[0].geometry.location.lng();
                } else {
                  alert('Geocode was not successful for the following reason: ' + status);
                }
              });

        });
    });
    ;

}