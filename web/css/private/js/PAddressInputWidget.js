PAddressInputWidget = function(div_id, opts){
	var self = this,
		$conteiner = $(div_id), 
		relations = {"country": ["region", "city"], "region": ["city"]};
	
	this.opts = ( typeof opts == 'object') ? opts : {};
	
	this.change = function(){
		if(!$(this).hasClass("loading")){
			var param = this.getAttribute("rel"), value = this.value;
			if (param == 'region' && value == '') {
				param = 'country';
				value = $('select[rel=country]').val();
			}
			if(self.opts.onAjaxStart){
				self.opts.onAjaxStart($conteiner);
			}
			self.sendAjax(param, value);
		}
	};
	
	this.sendAjax = function (param, value){
		if(relations[param]){
			$("select", $conteiner).prop("disabled", true);
			for(var i=0; i < relations[param].length; i++){
				$("select[rel="+relations[param][i]+"]").addClass("loading");
			}
			$.get("/address/suggestAddress", {get: relations[param], param: param, value: value}, function(data){
				$("select", $conteiner).prop("disabled", false).removeClass("loading");
				self.suggestSelectOptions(relations[param], data);
			}, "JSON");
		}
	};
	
	this.suggestSelectOptions = function(targets, data){
		for(var i = 0; i < targets.length; i++){
			self.appendOptions(targets[i], data[targets[i]]);
		}
	};

	this.appendOptions = function(param, data){
		var $input = $("select[rel="+param+"]", $conteiner);
		if($input.prop("tagName") == "SELECT"){
			$input.empty();
			$input.append($("<option value=\"\"></option>"));
			for(var i=0; i < data.length; i++){
				$input.append($("<option value=\""+data[i].key+"\">"+data[i].value+"</option>"));
			}
		}
		if(self.opts.onChange){
			self.opts.onChange($conteiner);
		}
		if(self.opts.onAjaxEnd){
			self.opts.onAjaxEnd($conteiner);
		}
	};
	
	for(var key in relations){
		$("select[rel="+key+"]", $conteiner).change(self.change);
	}
}