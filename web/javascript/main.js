jQuery(document).ready(function () {
	$('.has-tooltip').tooltip();
	//содержание
	$('.row-offcanvas-right').find('button.btn-xs').click(function(){$('#sidebar').toggleClass('show-sub-menu')});
	$('#sidebar').find('a').click(function(){$(this).parents('#sidebar').removeClass('show-sub-menu');});
    $("a.list-group-item").click(function () {
        var elementClick = $(this).attr("href");
        var destination = $(elementClick).offset().top - 60;
        $('html, body').animate({ scrollTop: destination }, 500);
        return false; 
    });
    $("[name=phone]").mask("+7(999)999-9999");
    //личный кабинет
    $('a[href=#no-button]').click(function(){
        $.ajax({
                url: "/user/ajax2/",
                cache: false,
                type: 'POST',
                dataType: 'json',
                data: $('form').serialize()+'&marrow_status=0',
                success: function(data){
                  if(data != undefined && data.status == 0)
                  {
                      $('a[href=#no-button]').removeClass('btn-gray');
                      $('a[href=#yes-button]').addClass('btn-gray');
                  }
                }
              });
        return false;
    });
    $('a[href=#yes-button]').click(function(){
        $.ajax({
                url: "/user/ajax2/",
                cache: false,
                type: 'POST',
                dataType: 'json',
                data: $('form').serialize()+'&marrow_status=1',
                success: function(data){
                  if(data != undefined && data.status == 1)
                  {
                      $('a[href=#no-button]').addClass('btn-gray');
                      $('a[href=#yes-button]').removeClass('btn-gray');
                  }
                }
              });
        return false;
    });
    $('.fancybox').fancybox();
    $('#recall-form').submit(function(){saveRecall(this);return false;});
});

function checkPass()
{
    if($("[name=password]").val() != $("[name=password_repeat]").val())
    {
        $("#repeatPassError")[0].innerHTML = 'Пароли не совпадают';
        return false;
    }
    else
    {
        $("#repeatPassError")[0].innerHTML = '';
    }
    if($("[name=password]").val().length < 6)
    {
        $("#passError")[0].innerHTML = 'Длина пароля менее 6 символов';
        return false;
    }
    else
    {
        $("#passError")[0].innerHTML = '';
    }
    return true;
}

function saveAllDate()
    {
        $('.errorSummary').html('');
        $.ajax({
                url: "/user/ajax/",
                cache: false,
                type: 'POST',
                dataType: 'json',
                data: $('form').serialize(),
                success: function(data){
                  if(data !== undefined && data.html !== undefined)
                  {
                      $('div.jumbotron').find('div.container').html(data.html);
                  }
                  else
                  {
                      $('.errorSummary').html('<p style="color:red">Произошла ошибка. Просим вас повторить позже.</p>');
                  }
                }
              });
    }
var recallSended = false;
function saveRecall(form)
{
    if(recallSended === true)
    {
        return false;
    }
    recallSended = true;
    $('.errorSummary').html('');
    $.ajax({
        url: "/user/recall/",
        cache: false,
        type: 'POST',
        dataType: 'json',
        data: $(form).serialize(),
        success: function(data){
          $('#recall').find('.errorSummary').html();
          $('#recall').find('.form-control').css({border : '1px solid #ccc'});
          if(data !== undefined && data.error !== null && data.error === false )
          {
              $('#recall').find('.form-group-recall').css({display : 'none'});
              $('#recall').find('h2').html('Сообщение отправлено');
              $('#recall').find('.succes-message').html('<p>Мы свяжемся с вами в ближайшее время</p>');
          }
          else if(data !== undefined && data.error !== null && data.error === true )
          {
              for(var i in data.errors)
              {
                  $('#recall').find('.errorSummary').html($('#recall').find('.errorSummary').html() + data.errors[i]);
                  $('#recall').find('[name='+i+']').css({border : '1px solid #FF8080'});
              }
          }
        },
        error: function(data){
            $('#recall').find('.errorSummary').html('Произошла ошибка. Попробуйте написать позже.');
        }
      });
}