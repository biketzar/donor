<?php
if(!isset($_GET['check']))
{
	//echo file_get_contents('runtime/index.html');
	//die;
}
error_reporting(0);
define('CONFIG_FILE', __DIR__.'/config/config.json');
$projectSettings = json_decode(file_get_contents(CONFIG_FILE), FALSE) ? json_decode(file_get_contents(CONFIG_FILE), TRUE) : [];
$projectSettings['debug_mode']['value'] = (isset($projectSettings['debug_mode']['value']) ? boolval($projectSettings['debug_mode']['value']) : FALSE);
if($projectSettings['debug_mode']['value'])
{
    defined('YII_DEBUG') or define('YII_DEBUG', true);
    defined('YII_ENV') or define('YII_ENV', 'dev');
} else {
    defined('YII_DEBUG') or define('YII_DEBUG', false);
    defined('YII_ENV') or define('YII_ENV', 'prod');
}
// include __DIR__.'/components/';



// comment out the following two lines when deployed to production
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');

require(__DIR__ . '/vendor/autoload.php');
require(__DIR__ . '/vendor/yiisoft/yii2/Yii.php');
require(__DIR__ . '/autoload.php');

$config = require(__DIR__ . '/config/web.php');


(new yii\web\Application($config))->run();

