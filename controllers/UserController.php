<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ForgetPasswordForm;
use app\models\RecoveryPasswordForm;
use app\modules\user\components\AuthManagerHelper;
use yii\widgets\ActiveForm;

use app\components\SiteHelper;

use yii\helpers\Url;

class UserController extends Controller
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['access']= 
                    [
                        'class' => 'app\components\AccessControl',
                        'rules' => [
                            [
                                'actions' => ['index', 'login', 'error', 
                                  'datetime', 'main', 'ajax2', 'recall', 'addinfo',
                                    'register', 'registration-confirm', 'forget-password', 
                                  'recovery-password', 'logout', 'ajax', 'unsubscribe'],
                                'allow' => true,
                                'roles' => ['@', '?'],
                            ],
                            [
                                'actions' => ['event',],
                                'allow' => true,
                                'roles' => ['@', '?'],
                            ],
                            //['class' => 'app\components\ManagerControl'],
                        ]
                    ];
        $behaviors['verbs']= 
                    [
                        'class' => VerbFilter::className(),
                        'actions' => [
                            'logout' => ['post', 'get'],
                        ]
                    ];
        
        return $behaviors;
    
    }
    
    public function actionMain()
    {
        if(\app\models\AuthForm::isAuth())
        {
            $userId = \app\models\AuthForm::getDonorId();
            $donor = \app\models\Donorlite::find()->where('id=:id', [':id'=> $userId])->one();
            $donates = \app\models\Donorhistory::find()->where('id_donor=:id', [':id'=> $userId])->orderBy('date DESC')->all();
            $eventDay = \app\models\Eventday::find()->where('id_donor=:id_donor', [':id_donor'=>$userId])->all();
            $timeIds = [];
            foreach($eventDay as $eventDayModel)
            {
                $timeIds[] = $eventDayModel->date_id;
            }
            $message = "";
            if($timeIds)
            {
                $docList = \yii\helpers\ArrayHelper::map(\app\models\Documents::find()->asArray()->all(), 'id', 'name');
                $needDocs = (new \yii\db\Query())->from('yii2_link_doc')
                    ->where(['donor_id' => $userId])
                    ->andWhere(['IN', 'time_id', $timeIds])
                    ->column();
                if($needDocs)
                {
                    $message .= "<br/>Необходимо донести следующие документы: ".implode('; ', array_map(function($elem) use($docList) {return \yii\helpers\ArrayHelper::getValue($docList, $elem);}, $needDocs));
                }
            }
            if($message)
            {
                \Yii::$app->session->setFlash('date-form-error', $message);
            }
            return $this->render('private', ['donor'=>$donor,
                    'donates'=>$donates,
                    'eventDay'=>$eventDay,
                    'message' => $message
                    ]);
            
        }
        else
        {
            $this->redirect('/user/login/');
        }
    }
    
    private function saveEventDay($donor, $time, $priority)
    {
        if($time == 0)
        {
             $model = \app\models\Eventday::find()->where('id_donor=:id_donor and priority=:priority',
                    [':id_donor' => $donor,
                     ':priority' => $priority,
                    ])->one();
            if($model)
            {
                return (bool)$model->delete();
            }
            else
            {
                return FALSE;
            }
        }
        $timeModel = \app\models\Times::find()->where('id=:id', [':id'=>$time])->asArray()->one();
        if(!$timeModel)
        {
            return FALSE;
        }
        else {
            $model = \app\models\Eventday::find()->where('id_donor=:id_donor and priority=:priority',
                    [':id_donor' => $donor,
                     ':priority' => $priority,
                    ])->one();
            if(!$model)
            {
                $model = new \app\models\Eventday();
                $model->id_donor = $donor;
            }
            $model->date_id = $time;
            $model->priority = $priority;
            if($model->save())
            {
                return TRUE;
            }
            else
            {
                return FALSE;
            }
        }
    }
    
    public function actionAjax2()
    {
        $data = Yii::$app->request->post();
        if(\app\models\AuthForm::isAuth() && isset($data['marrow_status']))
        {
            $donor = \app\models\Donorlite::find()->where('id=:id', [':id'=>\app\models\AuthForm::getDonorId()])->one();
            if($donor)
            {
                $donor->marrow_status = ($data['marrow_status']);
                if($donor->save())
                {
                    echo json_encode(['error'=>FALSE, 'status'=>$donor->marrow_status]);
                }
                else {
                    echo json_encode(['error'=>TRUE]);
                }
            }
            else
            {
                echo json_encode(['error'=>TRUE]);
            }
        }
        else
        {
            echo json_encode(['error'=>TRUE]);
        }
        die();
    }
    
    public function actionAjax()
    {
        if(\app\models\AuthForm::isAuth())
        {
            $userId = \app\models\AuthForm::getDonorId();
            $data = Yii::$app->request->post();
            $result = FALSE;
            $error = 'Необходимо выбрать день и время';
            foreach($data['priority'] as $key=>$value)
            {
                if(isset($data['priority'][$key]) && isset($data['dtime'][$key]))
                {
                    $error = '';
                    $result = $this->saveEventDay($userId, $data['dtime'][$key], $data['priority'][$key]) || $result;
                }
                
            }
            if($result)
            {
                \Yii::$app->session->setFlash('date-form-success', 'Данные успешно сохранены.<br/>Для перехода в <b>личный кабинет</b> нажмите на <a href="/user/main/">ссылку</a>');
            }
            if($result === FALSE)
            {
                \Yii::$app->session->setFlash('date-form-error', 'Произошла ошибка.<br/>'.$error);
            }
            $eventDay = \app\models\Eventday::find()->where('id_donor=:id_donor', [':id_donor'=>$userId])->all();
            $forms = $this->renderPartial('dateForms', ['models'=>$eventDay]);
            echo json_encode(['error'=>FALSE, 'html'=>$forms]);
        }
        else
        {
            $eventDay = [];
            //$forms = $this->renderPartial('dateForms', ['models'=>$eventDay]);
            $forms = "<script>location.reload();</script>";
            echo json_encode(['error'=>TRUE, 'html'=>$forms]);
        }
        die();
    }
    
    public function actionDatetime()
    {
        if(\app\models\AuthForm::isAuth())
        {
            $userId = \app\models\AuthForm::getDonorId();
            $eventDay = \app\models\Eventday::find()->where('id_donor=:id_donor', [':id_donor'=>$userId])->all();
            $forms = $this->renderPartial('dateForms', ['models'=>$eventDay]);
            return $this->render('event', ['forms'=>$forms]);
        }
        else
        {
            $this->redirect('/user/login/');
        }
    }

    public function actionIndex()
    {
        if(\app\models\AuthForm::isAuth())
        {
            $this->redirect('/user/main/');
        }
        else
        {
            $this->redirect('/user/login/');
        }
    }
    
    public function actionLogin()
    {
        //var_dump(!\Yii::$app->user->isGuest);die();
        if (\app\models\AuthForm::isAuth()) {
            $this->redirect('/user/main/');
        }
        $model = new \app\models\AuthForm();
        
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            $this->redirect('/user/main/');
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }
	
	public function actionAddinfo()
	{		
		$model = \app\models\Donorlite::find()->where('id=:id', [':id'=>\app\models\AuthForm::getDonorId()])->one();
		if(!$model)
		{
			$this->redirect('/user/main/');
		}
		$model->setScenario('update');
		if(\Yii::$app->request->isAjax && \Yii::$app->request->post('ajax', null) === strtolower($model->formName()).'-form')
        {
            $model->load(\Yii::$app->request->post());
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return yii\bootstrap\ActiveForm::validate($model);
        }
        
        if ($model->load(Yii::$app->request->post())) 
        {
			if($model->save())
			{
				\Yii::$app->session->setFlash('register-edit', \yii::t('app', 'Данные успешно сохранены.<br/>Для перехода в <b>личный кабинет</b> нажмите на <a href="/user/main/">ссылку</a>'));
			}
			
            //'model' => $model,
		}
		return $this->render('register_edit', [
            'model' => $model,
        ]);
	}
    
    
    public function actionRegister()
    {
        if (\app\models\AuthForm::isAuth()) {
            $this->redirect('/user/main/');
        }
        
        $model = \Yii::createObject(['class' => '\app\models\Donor']);
        
        $model->setScenario('insert');
        
        if(\Yii::$app->request->isAjax && \Yii::$app->request->post('ajax', null) === strtolower($model->formName()).'-form')
        {
            $model->load(\Yii::$app->request->post());
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return yii\bootstrap\ActiveForm::validate($model);
        }
        
        if ($model->load(Yii::$app->request->post())) 
        {
            
//            $mailParams = [
//                'USERNAME' => $model->username,
//                'EMAIL' => $model->email,
//                'PASSWORD' => $model->password,
//            ];
            
            $model->status = \app\models\Donor::STATUS_INACTIVE;
            //$model->donorstatus = 1;
            if($model->save())
            {
                $dcard = \app\models\Dcard::find()->where('surname=:surname and name=:name', [
                    ':surname' => $model->surname,
                    ':name' => $model->name,
                ])->one();
                if($dcard)
                {
                    $newModel = \app\models\Donor::findOne($model->getPrimaryKey());
                    $newModel->setScenario('dcard');
                    $newModel->dcard_id = $dcard->getPrimaryKey();
                    $newModel->save();
                }
                \app\models\AuthForm::setAuth($model->getPrimaryKey());
                if(\app\models\AuthForm::isAuth())
                {
                    $this->redirect('/user/datetime/');
                }
                else {
                    $this->redirect('/user/login');
                }
//                AuthManagerHelper::assignRole('user', $model->getPrimaryKey());
//                
//                $mailParams['ACCESS_TOKEN'] = $model->access_token;
//                $mailParams['CONFIRM_URL'] = Url::to(['/user/registration-confirm', 'token' => $model->access_token], true);
//                $mailer = new \app\components\MailHelper;
//                $mailer->to = $model->email;
//                $mailer->subject = \yii::t('app', 'Success register');
//                $mailer->mailTemplate = 'registration';
//                $mailer->params = $mailParams;
//                $mailer->Send();
                
                \Yii::$app->session->setFlash('register', \yii::t('app', 'Success register'));
            }
            
            //return $this->goBack();
        } 
        return $this->render('register', [
            'model' => $model,
        ]);
        
    }
    
    public function actionRegistrationConfirm()
    {
        if (\app\models\AuthForm::isAuth()) {
            $this->redirect('/user/main/');
        }
        if(\yii::$app->request->get('token'))
        {
            $model = \app\models\User::find()->status(\app\models\User::STATUS_INACTIVE)
                    ->where('access_token=:token', [':token' => \yii::$app->request->get('token')])->one();
            
            if($model)
            {
                $model->status = \app\models\User::STATUS_ACTIVE;
                $model->access_token = '';
                $model->save(true, ['status', 'access_token', 'update_time']);
                \Yii::$app->session->setFlash('registration-confirm-success', \yii::t('app', 'Success confirm of registration'));
            } else {
                \Yii::$app->session->setFlash('registration-confirm-error', \yii::t('app', 'User is not found'));
            }
        }
        return $this->render('registration-confirm', [
            //'model' => $model,
        ]);
        
    }
    
    private function sendMail($to, $subject, $params = array())
    {
        $template = '<html>
                    <head>
                     <title>Восcтановление доступа</title>
                    </head>
                    <body>
                    <p>Для восстановления пароля перейдите по ссылке: <a href="[LINK]">[LINK]</a></p>
                    </body>
                    </html>';
        /* тема/subject */
        foreach($params as $key=>$value)
        {
                $template = str_replace('['.$key.']', $value, $template);
        }
        $result = \Yii::$app->mailer->compose()
            ->setTo($to)
            ->setFrom('no-reply@donor.spb.ru')
            ->setSubject($subject)
            ->setHtmlBody($template)
            ->send();
        return $result;//mail($to, $subject, $template, $headers);
    }
    
    public function actionForgetPassword()
    {
        if (\app\models\AuthForm::isAuth()) {
            $this->redirect('/user/main/');
        }

        $model = new ForgetPasswordForm();
        
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $user = \app\models\Donor::find()
                    ->where('email=:email', [':email' => $model->email])->one();
            if($user)
            {
                $user->generatePasswordResetToken();
                $user->save(true, ['access_token']);
                $param = [];
                $param['LINK'] = Url::home(true).'user/recovery-password/?token='.$user->access_token;
                $this->sendMail($user->email, 'Восcтановление доступа', $param);
                
                \Yii::$app->session->setFlash('forget-password-success', \yii::t('app', 'E-mail with information about recovery password was sent'));
            } else {
                \Yii::$app->session->setFlash('forget-password-error', \yii::t('app', 'User is not found'));
            }
            //return $this->goBack();
        } 
        return $this->render('forget-password', [
            'model' => $model,
        ]);
        
    }
    public function actionRecoveryPassword()
    {
        if (\app\models\AuthForm::isAuth()) {
            $this->redirect('/user/main/');
        }
        
        $model = null;
        if(\yii::$app->request->get('token'))
        {
            
            $user = \app\models\Donor::find()
                    ->where('access_token=:token', [':token' => \yii::$app->request->get('token')])->one();
            
            if($user)
            {
                $model = new RecoveryPasswordForm();
                if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                    $user->password = $model->password;
                    $user->access_token = '';
                    $user->save(true, ['password', 'access_token', 'update_time']);
//                    $mailParams = [
//                        'EMAIL' => $user->email,
//                    ];
//                    $mailer = new \app\components\MailHelper;
//                    $mailer->to = $user->email;
//                    $mailer->subject = 'Восстановление доступа';
//                    $mailer->mailTemplate = 'recovery-password';
//                    $mailer->params = $mailParams;
//                    $mailer->Send();
                    \Yii::$app->session->setFlash('recovery-password-success', \yii::t('app', 'Set new password'));
                    //$this->redirect('/user/login/');
                    return $this->actionLogin();
                }
                
            } else {
                sleep(5);
                \Yii::$app->session->setFlash('recovery-password-error', 'Неверный токен');
            }
        } else {
            \Yii::$app->session->setFlash('recovery-password-error', 'Отсутствует токен');
        }
        
        return $this->render('recovery-password', [
            'model' => $model,
        ]);
        
    }

    public function actionLogout()
    {
        \app\models\AuthForm::logout();
        return $this->redirect('/');
    }
    
    
    public function actionRecall()
    {
        $model = new \app\models\Recall();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) 
        {
            if($model->save())
            {
                echo json_encode(['error'=>FALSE]);
            }
            else{
                echo json_encode(['error'=>TRUE, 'errors'=>$model->getFirstErrors() ]);
            }
        }
        else
        {
            echo json_encode(['error'=>TRUE, 'errors'=>$model->getFirstErrors()]);
        }
        die();
    }
    
    public function actionUnsubscribe()
    {
      $message = "Пользователь не найден";
      if(\app\models\AuthForm::isAuth())
      {
        $userId = \app\models\AuthForm::getDonorId();
        $donor = \app\models\Donor::find()->where(['id' => $userId])->one();
        if($donor)
        {
          if($donor->unsubscribe)
          {
            $message = "Вы ранее отписались от рассылки";
          }
          elseif(($donor->unsubscribe = 1) && $donor->save())
          {
            $message = "Вы успешно отписались от рассылки";
          }
        }
      }
      else
      {
        $email = \Yii::$app->request->get('c');
        $donor = \app\models\Donor::find()
            ->where("MD5(email)=:email", [':email' => $email])
            ->one();
        if($donor)
        {
          if($donor->unsubscribe)
          {
            $message = "Вы ранее отписались от рассылки";
          }
          elseif(($donor->unsubscribe = 1) && $donor->save())
          {
            $message = "Вы успешно отписались от рассылки";
          }
        }
      }
      return $this->render('unsubscribe', [
        'message' => $message
      ]);
      
    }
}
