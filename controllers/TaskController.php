<?php
namespace app\controllers;
use yii\web\Controller;

class TaskController extends Controller
{
    public function actionIndex()
    {
        $cacheKey = 'task-cache-key';
        if(\Yii::$app->cache->get($cacheKey) === FALSE)
        {
            $helper = new \app\components\TaskHelper();
            $helper->start();
            \Yii::$app->cache->set($cacheKey, 1, 3600, new \yii\caching\TagDependency(['tags' => 'task-cache-dependency']));
        }
        return '';
    }
}