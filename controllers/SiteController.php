<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;


use app\components\SiteHelper;

use yii\helpers\Url;

class SiteController extends Controller
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['access']= 
                    [
                        'class' => 'app\components\AccessControl',
                        'rules' => [
                            [
                                'actions' => ['index', 'error', 'page', 'captcha', 
                                    'sitemap', 'sitemap-xml',
                                    'about', 'day', 'donate', 'contraind', 'recommend'],
                                'allow' => true,
                                'roles' => ['@', '?'],
                            ],
                            //['class' => 'app\components\ManagerControl'],
                        ]
                    ];
        /*$behaviors['verbs']= 
                    [
                        'class' => VerbFilter::className(),
                        'actions' => [
                            'logout' => ['post'],
                        ]
                    ];*/
        
        return $behaviors;
    
        /*return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];*/
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
    
    public function actionRules()
    {
        return $this->render('rules');
    }
    
    public function actionAbout()
    {
        $about = \app\models\About::find()->where('published=1')->all();
        return $this->render('about',
                [
                    'about' => $about,
                ]);
    }
    
    public function actionDonate()
    {
        return $this->render('donate');
    }
    
    public function actionDay()
    {
        return $this->render('day');
    }
    
    public function actionRecommend()
    {
        return $this->render('recommend');
    }
    
    public function actionContraind()
    {
        return $this->render('contraind');
    }
	
	public function actionTyping()
	{
		return $this->render('typing');
	}
	
	 private function declOfNum($number, $titles)
    {
        $cases = array (2, 0, 1, 1, 1, 2);
        return $number." ".$titles[ ($number%100 > 4 && $number %100 < 20) ? 2 : $cases[min($number%10, 5)] ];
    }
    
    public function actionIndex()
    {
		$page = SiteHelper::getCurrentPage();
        $donateCount = \app\models\Donorhistory::find()->count();
		$strArr = ['человек', 'человека', 'человек'];
        $donateCount = $this->declOfNum($donateCount, $strArr);
        $slider = \app\models\Slider::find()->where('published=1')->orderBy('ordering')->all();
        return $this->render('index',
                [
                    'slider'=>$slider,
                    'donateCount'=>$donateCount,
					'model' => $page
                ]);
    }
    
    public function actionPage()
    {
        $page = SiteHelper::getCurrentPage();
        if($page->id == 11)
        {
            return $this->actionRules();
        }
        if($page->id == 12)
        {
            return $this->actionAbout();
        }
		if($page->id == 13)
        {
            return $this->render('donate',[
            'model' => $page
        ]);
        }
		if($page->id == 14)
        {
            return $this->render('day',[
            'model' => $page
        ]);
        }
		if($page->id == 15)
        {
            return $this->render('recommend',[
            'model' => $page
        ]);
        }
		if($page->id == 16)
        {
            return $this->render('contraind',[
            'model' => $page
        ]);
        }
		if($page->id == 17)
        {
            return $this->render('typing',[
            'model' => $page
        ]);
        }
        return $this->render('page',[
            'model' => $page
        ]);
    }
    
    public function actionSitemap()
    {
        $pages = \app\components\Sitemap::getSiteMap();
        
        return $this->render("sitemap", array(
                "pages" => $pages,
        ));
    }
    public function actionSitemapXml()
    {
        $text = \app\components\Sitemap::getSiteMapXml();
        
        //header("Content-type: text/xml");
        //header('Content-Disposition: inline; filename="sitemap.xml"');
        Yii::$app->response->content = $text;
        Yii::$app->response->format = \yii\web\Response::FORMAT_XML;
        
        //\Yii::$app->end();
    }

    public function actionSearch()
    {
        $form = \yii::createObject('\app\models\SearchForm');
            
        if(\yii::$app->request->get('query'))
        {
            $form->query = \yii::$app->request->get('query');
        }
        if(\yii::$app->request->get($form->formName()))
        {
            $form->load(\yii::$app->request->get());
        }
        
        $items = [];
        
        $lang = \yii::$app->language;
        $lang = explode('-', $lang);
        $lang = $lang[0];
        
        if($form->query && $form->validate())
        {
            $key = serialize([$form->getAttributes(), $lang]);
            
            $data = \yii::$app->cache->get($key);
            $data = false;
            if ($data === false) {
                
                $items = \app\components\Search::getResults($form, $lang);
                \yii::$app->cache->set($key, $items, 3600);
                // $data is expired or is not found in the cache
            } else {
                $items = $data;
            }
        }
        
        $provider = new \yii\data\ArrayDataProvider([
            'allModels' => $items,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                /*'defaultOrder' => [
                    'date' => SORT_DESC,
                    'id' => SORT_DESC, 
                ]*/
            ],
        ]);
        
        return $this->render("search", array(
                "dataProvider" => $provider,
                "model" => $form,
        ));
    }
    public function actionSearchSuggest()
    {
        $form = \yii::createObject('\app\models\SearchForm');
            
        if(\yii::$app->request->get('term'))
        {
            $form->query = \yii::$app->request->get('term');
        }
        $items = [];
        
        $lang = \yii::$app->language;
        $lang = explode('-', $lang);
        $lang = $lang[0];
        
        if($form->query && $form->validate())
        {
            $key = serialize([$form->getAttributes(), $lang]);
            $data = \yii::$app->cache->get($key);
            if ($data === false) {
                
                $items = \app\components\Search::getResults($form, $lang);
                \yii::$app->cache->set($key, $items, 3600);
                // $data is expired or is not found in the cache
            } else {
                $items = $data;
            }
        }
        $result = [];
        foreach($items as $item)
            $result[] = ['id' => $item['url'], 'label' => $item['title'], 'value' => $item['title']];
        $result = array_slice($result, 0, 10);
        \yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        return json_encode($result);
    }

    
    public function allowAction()
    {
        return ['index', 'about', 'contact', 'logout', 'login'];
    }
}
