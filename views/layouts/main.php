<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
AppAsset::register($this);
$this->registerJsFile('/web/js/task.js');
?>
<?php $this->beginPage() ?>
﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<?= Yii::$app->language ?>">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?= Html::encode($this->title) ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<meta property="og:type" content="website" />
<meta property="og:title" content="День донора СПбПУ" />
<meta property="og:url" content="http://donor.spb.ru/" />
<meta property="og:image" content="http://donor.spb.ru/web/images/logo_opengraph_f1.png" />
<meta property="og:description" content="Акция проходит круглый год с большими событиями осенью и весной."/>
<meta content="http://donor.spb.ru/web/images/logo_opengraph_f1.png" itemprop="image">
<link href="/web/css/font-awesome.min.css" rel="stylesheet" />
<link href="/web/css/styles.css" rel="stylesheet" />
<link href="/web/css/my.css" rel="stylesheet" />
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->
<meta name="language" content="ru" />
<link rel="icon" href="/web/favicon.ico" />
<link rel="shortcut icon" href="/web/favicon.ico" />
<?= Html::csrfMetaTags() ?>
<?php $this->head(); ?>
</head>
<body>
    <?php $this->beginBody() ?>
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container" id="nav">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/">
                    <img width="40" height="40" alt="День донора Политех логотип" src="/web/images/logo_daydonor.jpg">
                </a>
            </div>
            <div id="navbar" class="collapse navbar-collapse">
                <ul class="nav navbar-nav">

                    <li class="<?=(strpos($_SERVER['REQUEST_URI'], 'user/') ? 'active' : '')?>">
                        <a href="/user/main/">Личный кабинет</a>
                    </li>
                    <li class="<?=(strpos($_SERVER['REQUEST_URI'], 'donate') ? 'active' : '')?>">
                        <a href="/donate/">О донорстве</a>
                    </li>
                    <li class="<?=(strpos($_SERVER['REQUEST_URI'], 'day') ? 'active' : '')?>">
                        <a href="/day/">Об акции</a>
                    </li>
                    <li class="<?=(strpos($_SERVER['REQUEST_URI'], 'recommend') ? 'active' : '')?>">
                        <a href="/recommend/">Рекомендации</a>
                    </li>
                    <li class="<?=(strpos($_SERVER['REQUEST_URI'], 'contraind') ? 'active' : '')?>">
                        <a href="/contraind/">Противопоказания</a>
                    </li>
                    <li class="<?=(strpos($_SERVER['REQUEST_URI'], 'about') ? 'active' : '')?>">
                        <a href="/about/">О нас</a>
                    </li>
                    <li>
                        <a href="#recall" class="fancybox">Обратная связь</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <?= $content ?>
    
    <footer class="footer">
            <div class="container">
                <div class="row">
                    <NOINDEX>
                    <div class="col-sm-offset-3 col-sm-4">
                        <h5>При участии:</h5>
                        <a class="brand" href="http://www.spbstu.ru" target="_blank">
                            <img alt="Санкт-Петербургский политехнический университет Петра Великого" src="/web/images/logo_polytech.png" titile="Санкт-Петербургский политехнический университет Петра Великого">
                        </a>
                        <a class="brand" href="https://vk.com/so_politeh" target="_blank">
                            <img width="40" height="40" alt="Студенческие отряды Политеха" src="/web/images/logo_st.jpg" titile="Студенческие отряды Политеха">
                        </a>
                        <a class="brand" href="https://fonddonorov.ru/" target="_blank">
                            <img width="40" height="40" alt="Фонд доноров" src="/web/images/logo_fond.png" titile="Фонд доноров">
                        </a>
                        <a class="brand" href="http://yadonorspb.ru/" target="_blank">
                            <img alt="Санкт-Петербургская городская станция переливания крови" src="/web/images/logo_bs.png" titile="Санкт-Петербургская городская станция переливания крови">
                        </a>
                        <a class="brand" href="https://vk.com/studg" target="_blank">
                            <img width="40" height="40" alt="Студсовет студгородка" src="/web/images/logo_ss.jpg" titile="Студсовет студгородка">
                        </a>
                    </div>
						</NOINDEX>
                    <div class="col-sm-offset-0 col-sm-2">
                        <h5>Соцсети:</h5>
    				    <a class="brand" href="https://www.vk.com/polydonor" target="_blank">
                            <img alt="День донора СПбПУ | ВКонтакте - Vk" src="/web/images/logo_vk.png" titile="День донора СПбПУ | ВКонтакте - Vk">
                        </a>
                        <a class="brand" href="https://www.instagram.com/polydonor" target="_blank">
                            <img alt="День донора СПбПУ | Instagram" src="/web/images/logo_insta.png" titile="День донора СПбПУ | Instagram">
                        </a>
                    </div>
                </div>       
            </div>
        </footer>
    <div id="recall" class="recall-form" style="display:none">
        <?php 
        $model = new \app\models\Recall();
        $form = yii\bootstrap\ActiveForm::begin([
                    'id'=>'recall-form',
                    'options' => ['class' => 'form-horizontal'],
            ]); ?>
        <div class="recall-header"><h2>Обратная связь</h2></div>
        <div class="errorSummary" style="color:red">
        </div>
        <div class="succes-message">
        </div>
        <div class="form-group-recall">
            <div class="recall-input">
                <?= Html::activeLabel($model, 'name') ?>
                <?= Html::activeTextInput($model, 'name', ['class'=>"form-control ",
                                                              'required'=>'required',
                                                               'placeholder'=>'',
                                                               'value'=>'',
                    ]) ?>
             </div>
        </div>
        <div class="form-group-recall">
            <div class="recall-input">
                <?= Html::activeLabel($model, 'email') ?>
                <?= Html::activeTextInput($model, 'email', ['class'=>"form-control ",
                                                              'required'=>'required',
                                                               'placeholder'=>'',
                                                              'type'=>'email',
                                                               'value'=>'',
                    ]) ?>
             </div>
        </div>
        <div class="form-group-recall">
             <div class="recall-input">
                <?= Html::activeLabel($model, 'dsc') ?>
                <?= Html::activeTextarea($model, 'dsc', ['class'=>"form-control ",
                                                              'required'=>'required',
                                                              'placeholder'=>'',
                                                                'value'=>'',
                    ]) ?>
             </div>
        </div>
        <div class="form-group-clear"></div>
        <div class="form-group-recall">
            <div class="recall-div-button">
                <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
    <?php yii\bootstrap\ActiveForm::end() ?>
    </div>
<?php $this->endBody() ?>
    <script src="/web/javascript/bootstrap.min.js"></script>
    <script src="/web/javascript/jquery.mask.js"></script>
    <script src="/web/javascript/main.js"></script>
    <script src="/web/fancybox/jquery.fancybox.pack.js"></script>
    <link href="/web/fancybox/jquery.fancybox.css" rel="stylesheet"/>
<?php echo app\components\ConstantHelper::getValue('counter-scripts', ''); ?>
<div id="toTop"> Наверх </div>
</body>
</html>
<?php $this->endPage() ?>
