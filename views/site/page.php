<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use app\components\SiteHelper;
use yii\helpers\Url;

if(!isset($this->params['breadcrumbs']))
    $this->params['breadcrumbs'] = [];
$this->params['breadcrumbs'] = array_merge($this->params['breadcrumbs'], SiteHelper::getCurrentPageBreadcrumbs(false));

$metaData = SiteHelper::metaData($this, [$model], [
    'title' => $model->name,
    'meta_title' => $model->name,
    'meta_description' => '',
    'meta_keywords' => '',
    ]);
echo '<h1>'.$metaData['title'].'</h1>';
?>
<?php 
foreach($model->contents as $content)
{
    echo HtmlPurifier::process($content->text);
}

?>