<?php
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use yii\helpers\Url;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="search-result-item">
    <?php if(isset($model['view']) && $model['view']):?>
    <?php echo $this->context->renderPartial($model['view'], ['model' => $model]);?>
    <?php else:?>
    <a href="<?=$model['url'];?>"><?=$model['title'];?></a>
    <?=$model['content'];?>
    <?php endif;?>
    
</div>