<?php
use yii\helpers\Html;
$this->registerMetaTag(['name' => 'keywords', 'content' => 'День Донора, регистрация, политех, СПбПУ']);
$this->registerMetaTag(['name' => 'description', 'content' => 'ССО Орион, Социальный отдел, Штаб студенческих отрядов Политеха, Городская станция переливания, День Донора 11-13 октября 2016 Санкт-Петербургский Политехнический Университет Петра Великого Выставочный комплекс']);
/* @var $this yii\web\View */
$this->title = 'О нас';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <div class="jumbotron">
        <div class="container">
            <?php $text = \app\models\TextPlaceContent::getTextBlock(8); ?>
            <h1><?=$text['name'];?></h1>
            <?=$text['text'];?>
        </div>
    </div>
    <?php foreach ($about as $key => $block):?>

        <div class="row featurette">
            <div class="col-md-7 <?=(($key % 2 == 1) ? 'col-md-push-5' : '')?>">
                <h2 class="featurette-heading">
                    <?=$block->name;?>
                </h2>
                <?=$block->dsc;?>
            </div>
            <div class="col-md-5 <?=(($key % 2 == 1) ? 'col-md-pull-7' : '')?>">
                <img class="featurette-image img-responsive center-block" src="<?=$block->getImagePath();?>" alt="<?=$block->name;?>">
            </div>
        </div>
        <hr class="featurette-divider">
        <?php if(count($about)-1 < $key):?>

            <hr class="featurette-divider">

        <?php endif;?>

        <?php endforeach; ?>
    <article class="about today">
        <h2>День донора сегодня</h2>
        <p>В 2018 году День донора представляет собой самую большую донорскую ячейку в Санкт-Петербурге. Это организованная
            команда волонтеров-единомышленников, готовая вывести донорство и типирование в городе на новый уровень.
            </p>
        <img src="/web/files/about/volunteers.jpg" class="about-image">
    </article>
</div>
