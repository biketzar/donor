<?php
use yii\helpers\Url;
use app\components\SiteHelper;

$page = SiteHelper::getCurrentPage();
if(!isset($this->params['breadcrumbs']))
    $this->params['breadcrumbs'] = [];
$this->params['breadcrumbs'] = array_merge($this->params['breadcrumbs'], SiteHelper::getCurrentPageBreadcrumbs(false));

$metaData = SiteHelper::metaData($this, [$page], [
    'title' => \Yii::t('app', 'Site map'),
    'meta_title' => \Yii::t('app', 'Site map'),
    'meta_description' => '',
    'meta_keywords' => '',
    ]);
echo '<h1>'.$metaData['title'].'</h1>';
?>

<?php 
function writeTree($list)
{
    $text = '';
    if(count($list))
    {
        $text .= '<ul>';
        
        foreach($list as $item)
        {
            $liclass = array();
            if(isset($item['items']) && count($item['items']))
                $liclass[] = 'bg';
            if($item['url'] == '<none>')
                $text .= '<li'.(count($liclass)?' class="'.implode(' ',$liclass).'"':'').'><span>'.$item['label'].'</span>';
            else
                $text .= '<li'.(count($liclass)?' class="'.implode(' ',$liclass).'"':'').'><a href="'.Url::toRoute($item['url']).'">'.$item['label'].'</a>';
            
            if(isset($item['items']))
                $text .= writeTree($item['items']);
            echo '</li>';
        }
        $text .= '</ul>';
    }
    return $text;
}
echo writeTree($pages);
?>