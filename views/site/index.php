<?php
/* @var $this yii\web\View */
$this->title = 'День донора в СПбПУ | Санкт-Петербургский Политехнический Университет Петра Великого';
$this->registerMetaTag(['name' => 'keywords', 'content' => 'День донора, регистрация, политех, СПбПУ']);
$this->registerMetaTag(['name' => 'description', 'content' => 'Заходите на сайт акции, регистрируйтесь и присоединяйтесь к добровольцам Политеха. Акция будет проходить: с 9 по 21 октября на выезде с возможностью сдачи крови на типирование; с 10 по 13 октября в Экспоцентре Главного учебного корпуса. ']);

?>
<!-- Carousel
        ================================================== -->
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
                <li data-target="#myCarousel" data-slide-to="2"></li>
                <!--<li data-target="#myCarousel" data-slide-to="5"></li>-->
                <?php //<li data-target="#myCarousel" data-slide-to="6"></li> ?>
            </ol>
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <img class="first-slide" src="web/images/black_hole.png" alt="Сайт целиком посвящен Дню донора в Политехе">
                    <div class="container">
                        <div class="carousel-caption">
                            <h1 class="my">День донора Политех</h1>
                            <!--<p class="my">Сайт целиком посвящен Дню донора в Политехе.</p>-->
                            <!--<p>Если все еще сомневаетесь &#8212 посмотрите.</p>-->
                            <!--<p><a class="btn btn-lg btn-primary" href="https://www.youtube.com/watch?v=91pLnWBV9J4" role="button" target="_blank">Смотреть</a></p>-->
                        </div>
                    </div>
                </div>
                <!--div class="item">
                    <img class="second-slide" src="web/images/slider_vk.png" alt="Second slide">
                    <div class="container">
                        <div class="carousel-caption">
                            <h1>Группа в контакте</h1>
                            <p>Фото + новости.</p>
                            <p><a class="btn btn-lg btn-primary" href="https://www.vk.com/polydonor" role="button" target="_blank">Открыть</a></p>
                        </div>
                    </div>
                </div-->
                <!--<div class="item">
                    <img class="third-slide" src="web/images/slider_counter.png" alt="День донора СПБПУ - счетчик сдач крови">
                    <div class="container">
                        <div class="carousel-caption">
                            <h1><?=$donateCount?> сделали добро</h1>
                            <p>Это счетчик сдач крови в нашем ВУЗе.</p>
                        </div>
                    </div>
                </div>-->
				<?php /*
                <div class="item">
                    <img class="fourth-slide" src="web/images/slider_banner.png" alt="Fourth slide">
                    <div class="container">
                        <div class="carousel-caption">
                            
                            <p><a class="btn btn-lg btn-primary" href="user/register" role="button" target="_blank" rel="nofollow">Записаться</a></p>
                            <!--<p>Если все еще сомневаетесь &#8212 посмотрите.</p>-->
                            <!--<p><a class="btn btn-lg btn-primary" href="https://www.youtube.com/watch?v=91pLnWBV9J4" role="button" target="_blank">Смотреть</a></p>-->
                        </div>
                    </div>
                </div> */?>
                <!--<div class="item">
                    <img class="fifth-slide" src="web/images/slider_stat.png" alt="День донора Политех - статистика">
                    <div class="container">
                        <div class="carousel-caption">-->
                            
                            <!--<p><a class="btn btn-lg btn-primary" href="https://www.vk.com/polydonor" role="button" target="_blank">Записаться</a></p>-->
                            <!--<p>Если все еще сомневаетесь &#8212 посмотрите.</p>-->
                            <!--<p><a class="btn btn-lg btn-primary" href="https://www.youtube.com/watch?v=91pLnWBV9J4" role="button" target="_blank">Смотреть</a></p>-->
                        <!--</div>
                    </div>
                </div>-->
                <div class="item">
                    <img class="sixth-slide" src="web/images/black_hole.png" alt="День донора Политех - донорам предлагается">
                    <div class="container">
                        <!--<div class="carousel-caption">-->
                            
                            <!--<p><a class="btn btn-lg btn-primary" href="https://www.vk.com/polydonor" role="button" target="_blank">Записаться</a></p>-->
                            <!--<p>Если все еще сомневаетесь &#8212 посмотрите.</p>-->
                            <!--<p><a class="btn btn-lg btn-primary" href="https://www.youtube.com/watch?v=91pLnWBV9J4" role="button" target="_blank">Смотреть</a></p>-->
                        <!--</div>-->
                    </div>
                </div>
                <div class="item">
                    <img class="seventh-slide" src="web/images/black_hole.png" alt="День донора Политех - о донорстве">
                    <div class="container">
                        <!--<div class="carousel-caption">-->
                            
                            <!--<p><a class="btn btn-lg btn-primary" href="https://www.vk.com/polydonor" role="button" target="_blank">Записаться</a></p>-->
                            <!--<p>Если все еще сомневаетесь &#8212 посмотрите.</p>-->
                            <!--<p><a class="btn btn-lg btn-primary" href="https://www.youtube.com/watch?v=91pLnWBV9J4" role="button" target="_blank">Смотреть</a></p>-->
                        <!--</div>-->
                    </div>
                </div>
                <!--<div class="item">
                    <img class="sixth-slide" src="web/images/black_hole.png" alt="Sixth slide">
                    <div class="container">
                        <div class="carousel-caption">
                            
                        </div>
                    </div>
                </div>-->
                <!--<div class="item">
                    <img class="fourth-slide" src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" alt="Fourth slide">
                    <div class="container">
                        <div class="carousel-caption none-back-png">
                            <h1>Видео о донорстве</h1>
                            <p>В производстве.</p>
                            <p>Если все еще сомневаетесь &#8212 посмотрите.</p>
                            <p><a class="btn btn-lg btn-primary" href="https://www.youtube.com/watch?v=91pLnWBV9J4" role="button" target="_blank">Смотреть</a></p>
                        </div>
                    </div>
                </div>-->
                <!--<div class="item">
                    <img class="fifth-slide" src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" alt="Fifth slide">
                    <div class="container">
                        <div class="carousel-caption none-back-png">
                            <h1>Видео о типировании</h1>
                            <p>В производстве.</p>
                            <p><a class="btn btn-lg btn-primary" href="https://www.youtube.com/watch?v=91pLnWBV9J4" role="button" target="_blank">Смотреть</a></p>
                        </div>
                    </div>
                </div>-->
            </div>
            <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev" rel="nofollow">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next" rel="nofollow">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div><!-- /.carousel -->
        <div class="container">
            <div class="row">
                <div class="col-sm-offset-2 col-sm-4">
                    <h3>Противопоказания</h3>

                    <p>Перед регистрацией на День донора обязательно ознакомьтесь с разделом противопоказания к донорству.</p>

                    <p><a class="btn btn-default" href="/site/contraind" rel="nofollow">Перейти &raquo;</a></p>
                    
                </div>
                <div class="col-sm-offset-0 col-sm-4">
                    <h3>Регистрация</h3>

                    <p>Для участия в мероприятии необходимо зарегистрироваться. В процессе регистрации можно будет выбрать удобное время для сдачи крови.</p>

                    <p><a class="btn btn-default" href="/user/login/" rel="nofollow">Перейти &raquo;</a></p>
                </div>
            </div>
        </div>

