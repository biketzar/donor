<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\HtmlPurifier;
use app\components\SiteHelper;
use yii\helpers\Url;
use yii\widgets\ListView;

$page = SiteHelper::getCurrentPage();

if(!isset($this->params['breadcrumbs']))
    $this->params['breadcrumbs'] = [];
$this->params['breadcrumbs'] = array_merge($this->params['breadcrumbs'], SiteHelper::getCurrentPageBreadcrumbs(false));

$metaData = SiteHelper::metaData($this, [$page], [
    'title' => \Yii::t('app', 'Search results'),
    'meta_title' => \Yii::t('app', 'Search results'),
    'meta_description' => '',
    'meta_keywords' => '',
    ]);
echo '<h1>'.$metaData['title'].'</h1>';
?>
<?php $form = ActiveForm::begin(['id' => 'search-form', 'method' =>'get', 'action' => Url::current(['query' => null])]); ?>
    <?php echo $form->errorSummary($model); ?>
    <?php /* echo $form->field($model, 'query')*/ ?>
    <?php echo Html::activeTextInput($model, 'query', ['class'=>'form-control', 'name' => 'query']); ?>
    <div class="form-group">
        <?= Html::submitButton(\yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
    </div>
<?php ActiveForm::end(); ?>


<?php 
if (count($dataProvider->getModels()) > 0): 
    
?>
    <div class="search-query"><?php echo \yii::t('app', 'Your search query');?> <span>"<?php echo $model->query;?>"</span></div>
<?php 
echo ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => '_search_result',
    'options' => ['class' => 'search-result-list'],
    
]);
        /*echo '<ul>';
	foreach ($result as $val): 
            
	
            
            if(isset($val['value']['link']))
            {
                //для нового способа вывода результатов
                echo '<li><a href="'.$val['value']['link'].'">'.$val['value']['title'].'</a></li>';
            } else {
                
                //для старого способа вывода результатов
                switch(get_class($val['value'])){
			case 'Good':
				$this->renderPartial('_good', array('model' => $val['value']));
				break;
			case 'GoodCategory':
				$this->renderPartial('_good_cat', array('model' => $val['value']));
				break;
			case 'Page':
				$this->renderPartial('_page', array('model' => $val['value']));
				break;
			default:
				$this->renderPartial('_default', array('model' => $val['value']));
				break;
		}
            }
		
	
	endforeach; 
        echo '</ul>';*/
elseif(!empty($model->query) && !$model->hasErrors()):
    echo '<h3>'.\Yii::t('app', 'No search results').'</h3>';
endif; 
?>

