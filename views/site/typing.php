<?php
use yii\helpers\Html;
$this->registerMetaTag(['name' => 'keywords', 'content' => 'День Донора Политех, Типирование костного мозга, СПбПУ']);
$this->registerMetaTag(['name' => 'description', 'content' => 'День Донора Политех, Выставочный комплекс, 11-13 октября 2016']);
/* @var $this yii\web\View */
$this->title = 'Типирование костного мозга';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <div class="jumbotron">
        <div class="container">
            <h1><?=$this->title?></h1>
        </div>
    </div>
    <p>
	Для прохождения типирования костного мозга необходимо заполнить анкету и сдать дополнительно 9 мл крови. Для сравнения донорство крови подразумевает сдачу от 350 до 450 мл крови. Так как сдача на типирование будет проходить одновременно с донорством крови, донор не получит дополнительного дискомфорта от лишнего укола.
	</p><p>
	После обследования 9 мл крови информация заносится в федеральную базу. При совпадении с типом костного мозга больного раком человека донору предлагается (можно отказаться без объяснения причин) сдать костный мозг, что на данный момент совершенно безболезненно. Вероятность совпадения типов с другим человеком очень мала, поэтому к Вашей помощи могут прибегнуть через много лет, либо она совсем не потребуется.
</p><p>
	В нашей стране донорство костного мозга по развитию значительно отстает от донорства крови. Для сравнения в России база доноров костного мозга насчитывает всего 50 тысяч человек, в то время как в США в базе состоит более 10 миллионов. Такое слабое развитие заставляет родителей больных деток тратить миллионы рублей на подбор донора за рубежом.
</p><p>
	Итак, донорство костного мозга:
	<ul>
		<li>Безболезненно</li>
		<li>Безвозмездно</li>
		<li>Очень нужно в нашей стране</li>
		<li>Даёт возможность спасти жизнь человеку <span style="color:#c52c32">❤</span></li>
	</ul>
    </p>
</div>
