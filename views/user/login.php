<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\SiteHelper;
use yii\bootstrap\Alert;

$page = SiteHelper::getCurrentPage();
$this->title = 'Личный кабинет';
$this->registerMetaTag([
        'name' => 'ROBOTS',
        'content' => 'NOINDEX,NOFOLLOW'
    ]);

//echo '<h1>'.$metaData['title'].'</h1>';

//if(\Yii::$app->session->hasFlash('register'))
//{
//    echo Alert::widget([
//        'options' => ['class' => 'alert-success'],
//        'body' => \Yii::$app->session->getFlash('register'),
//    ]);
//}
?>
<div class="container">
    <div class="jumbotron">
        <div class="container">
            <?php $text = \app\models\TextPlaceContent::getTextBlock(3); ?>
            <h1><?=$text['name'];?></h1>
            <?=$text['text'];?>
            <?php $form = yii\bootstrap\ActiveForm::begin([
                    'id'=>strtolower($model->formName()).'-form',
                    'options' => ['class' => 'form-horizontal'],
                    'enableAjaxValidation' => true,
                    'action' => '/user/login/'
                    //'enableClientValidation' => true,
            ]); ?>
                <div class="errorSummary" style="color:red">
                    <?=($model->hasErrors('password') ? $model->getFirstError('password') : '');?>
                </div>
                <?php
                if(\Yii::$app->session->hasFlash('recovery-password-success'))
                {
                    echo Alert::widget([
                        'options' => ['class' => 'alert-success'],
                        'body' => \Yii::$app->session->getFlash('recovery-password-success'),
                    ]);
                }
                ?>
                <div class="form-group">
                    <div class="col-sm-offset-0 col-sm-2">
                        <?= Html::activeLabel($model, 'email') ?>
                        <?= Html::activeTextInput($model, 'email', ['class'=>"form-control ",
                                                                      'required'=>'required',
                                                                      'type'=>'email',
                                                                       'value'=>'',
                            ]) ?>
                     </div>
                     <div class="col-sm-offset-0 col-sm-2">
                        <?= Html::activeLabel($model, 'password') ?>
                        <?= Html::activePasswordInput($model, 'password', ['class'=>"form-control ",
                                                                      'required'=>'required',
                                                                      'placeholder'=>'',
                                                                        'value'=>'',
                            ]) ?>
                     </div>
                </div>
                <div class="form-group-clear"></div>
                <?= Html::submitButton('Войти', ['class' => 'btn btn-primary']) ?>
                <a href="/user/register/" class="btn btn-primary">Регистрация</a>
                <br/>
                <div class="form-group">
                    <div class="col-sm-offset-0 col-sm-3">
                        <a href="/user/forget-password/">Восстановление пароля</a>
                    </div>
                </div>
            <?php yii\bootstrap\ActiveForm::end() ?>
        </div>
    </div>
</div>