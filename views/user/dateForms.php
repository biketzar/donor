<?php
use yii\helpers\Html;
use app\components\SiteHelper;
use yii\bootstrap\Alert;
$this->registerMetaTag([
        'name' => 'ROBOTS',
        'content' => 'NOINDEX,NOFOLLOW'
    ]);
$list = $models;
$models = [];
foreach($list as $model)
{
   $models[$model->priority] = $model;
}

$text = \app\models\TextPlaceContent::getTextBlock(5); ?>
            <h1>Выбор времени сдачи</h1>
            <p class="lead">Обязательно проверьте пункт <strong>"Сдача на выезде"</strong> в личном кабинете, 
            если выбор отсутствует</p>
            <div class="errorSummary">
                <?php if(\Yii::$app->session->hasFlash('date-form-error'))
                    {
                        echo Alert::widget([
                            'options' => ['class' => 'alert-error'],
                            'body' => \Yii::$app->session->getFlash('date-form-error'),
                        ]);
                    }
                ?>
                <?php if(\Yii::$app->session->hasFlash('date-form-success'))
                    {
                        echo Alert::widget([
                            'options' => ['class' => 'alert-success'],
                            'body' => \Yii::$app->session->getFlash('date-form-success'),
                        ]);
                    }
                ?>
            </div>
            <?php
                if(isset($models[1]))
                {
                    $model = $models[1];
                }else{
                    $model = new \app\models\Eventday();
                }
                ?>
                <?php $form = yii\bootstrap\ActiveForm::begin([
                       'id'=>strtolower($model->formName()).'-form',
                       'options' => ['class' => 'form-horizontal'],
                       'enableAjaxValidation' => true,
                       //'enableClientValidation' => true,
                       ]); ?>
                    <?= Html::activeHiddenInput($model, 'priority', ['name'=>'priority[0]', 'value'=>1]) ?>
                   <div class="form-group">
                       <div class="col-sm-offset-0 col-sm-2">
                           <p>Дата и время</p>
                       </div>
                       <div class="col-sm-offset-0 col-sm-2">
                            <?= Html::activeDropDownList($model, 'dtime', $model->getDtimeItems(), ['class'=>"form-control ",
                                                                          //'required'=>'required',
                                                                        'name'=>'dtime[0]'
                                ]) ?>
                         </div>
                   </div>
               <?php yii\bootstrap\ActiveForm::end();
            ?>
            <button class="btn btn-primary" onclick="saveAllDate();">Записаться</button>