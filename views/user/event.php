<?php
use yii\helpers\Html;
use app\components\SiteHelper;
use yii\bootstrap\Alert;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

$page = SiteHelper::getCurrentPage();
$this->title = 'Время сдачи';
if(!isset($this->params['breadcrumbs']))
    $this->params['breadcrumbs'] = [];
$this->params['breadcrumbs'] = array_merge($this->params['breadcrumbs'], SiteHelper::getCurrentPageBreadcrumbs(false));

$metaData = SiteHelper::metaData($this, [$page], [
    'title' => 'Выбор времени сдачи',
    'meta_title' => 'Выбор времени сдачи',
    'meta_description' => '',
    'meta_keywords' => '',
    ]);
$this->registerMetaTag([
        'name' => 'ROBOTS',
        'content' => 'NOINDEX,NOFOLLOW'
    ]);
?> 
<div class="container">
    <div class="jumbotron">
        <div class="container">
            <?=$forms?>
        </div>
    </div>
</div>
