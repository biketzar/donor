<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\SiteHelper;
use yii\bootstrap\Alert;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

$page = SiteHelper::getCurrentPage();
$this->title = 'Востановление пароля';
if(!isset($this->params['breadcrumbs']))
    $this->params['breadcrumbs'] = [];
$this->params['breadcrumbs'] = array_merge($this->params['breadcrumbs'], SiteHelper::getCurrentPageBreadcrumbs(false));

$metaData = SiteHelper::metaData($this, [$page], [
    'title' => \Yii::t('app', 'Recovery of password'),
    'meta_title' => \Yii::t('app', 'Recovery of password'),
    'meta_description' => '',
    'meta_keywords' => '',
    ]);
	$this->registerMetaTag([
        'name' => 'ROBOTS',
        'content' => 'NOINDEX,NOFOLLOW'
    ]);
//echo '<h1>'.$metaData['title'].'</h1>';
?>
<div class="container">
    <div class="jumbotron">
        <div class="container">
            <?php $text = \app\models\TextPlaceContent::getTextBlock(4); ?>
            <h1><?=$text['name'];?></h1>
            <?=$text['text'];?>
            
            <?php
            if(\Yii::$app->session->hasFlash('recovery-password-success'))
            {
                echo Alert::widget([
                    'options' => ['class' => 'alert-success'],
                    'body' => \Yii::$app->session->getFlash('recovery-password-success'),
                ]);
            }
            if(\Yii::$app->session->hasFlash('recovery-password-error'))
            {
                echo Alert::widget([
                    'options' => ['class' => 'alert-error'],
                    'body' => \Yii::$app->session->getFlash('recovery-password-error'),
                ]);
            }
            ?>
            
           <?php if($model && !\Yii::$app->session->hasFlash('recovery-password-success')):
            ?>
            <?php $form = yii\bootstrap\ActiveForm::begin([
                    'id'=>strtolower($model->formName()).'-form',
                    'options' => ['class' => 'form-horizontal'],
                    'enableAjaxValidation' => true,
                    'action' => '',
                    //'enableClientValidation' => true,
            ]); ?>
                <div class="errorSummary">
                    
                    <?=($model->hasErrors('password') ? $model->getFirstError('password') : '');?>
                </div>
            <div class="form-group">
                <div class="col-sm-offset-0 col-sm-2">
                    <?= Html::activeLabel($model, 'password') ?>
                    <?= Html::activePasswordInput($model, 'password', ['class'=>"form-control ".($model->hasErrors('password') ? 'errorInput' : ''),
                                                                  'required'=>'required',
                                                                  'placeholder'=>'',
                                                                    'value'=>'',
                                                                    'title'=>$model->getFirstError('password'),
                        ]) ?>
                 </div>
                 <div class="col-sm-offset-0 col-sm-2">
                    <?= Html::activeLabel($model, 'password_repeat') ?>
                    <?= Html::activePasswordInput($model, 'password_repeat', ['class'=>"form-control ".($model->hasErrors('password') ? 'errorInput' : ''),
                                                                  'placeholder'=>'',
                                                                  'value'=>'',
                                                                  'title'=>$model->getFirstError('password'),
                        ]) ?>
                 </div>
            </div>
                <div class="form-group-clear"></div>
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
                <br/>
            <?php yii\bootstrap\ActiveForm::end() ?>
            <?php endif;?>
        </div>
    </div>
</div>


