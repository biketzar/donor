<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\SiteHelper;
use yii\bootstrap\Alert;

$page = SiteHelper::getCurrentPage();

if(!isset($this->params['breadcrumbs']))
    $this->params['breadcrumbs'] = [];
$this->params['breadcrumbs'] = array_merge($this->params['breadcrumbs'], SiteHelper::getCurrentPageBreadcrumbs(false));

$metaData = SiteHelper::metaData($this, [$page], [
    'title' => \Yii::t('app', 'Registration confirmarion'),
    'meta_title' => \Yii::t('app', 'Registration confirmarion'),
    'meta_description' => '',
    'meta_keywords' => '',
    ]);
echo '<h1>'.$metaData['title'].'</h1>';
$this->registerMetaTag([
        'name' => 'ROBOTS',
        'content' => 'NOINDEX,NOFOLLOW'
    ]);

if(\Yii::$app->session->hasFlash('registration-confirm-success'))
{
    echo Alert::widget([
        'options' => ['class' => 'alert-success'],
        'body' => \Yii::$app->session->getFlash('registration-confirm-success'),
    ]);
}
if(\Yii::$app->session->hasFlash('registration-confirm-error'))
{
    echo Alert::widget([
        'options' => ['class' => 'alert-error'],
        'body' => \Yii::$app->session->getFlash('registration-confirm-error'),
    ]);
}
?>
