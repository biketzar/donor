<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\SiteHelper;
use yii\bootstrap\Alert;

$page = SiteHelper::getCurrentPage();

if(!isset($this->params['breadcrumbs']))
    $this->params['breadcrumbs'] = [];
$this->params['breadcrumbs'] = array_merge($this->params['breadcrumbs'], SiteHelper::getCurrentPageBreadcrumbs(false));

$metaData = SiteHelper::metaData($this, [$page], [
    'title' => 'Редактирование',
    'meta_title' => \Yii::t('app', 'Registration'),
    'meta_description' => '',
    'meta_keywords' => '',
    ]);
//echo '<h1>'.$metaData['title'].'</h1>';

//if(\Yii::$app->session->hasFlash('register-edit'))
//{
//    echo Alert::widget([
//        'options' => ['class' => 'alert-success'],
//        'body' => \Yii::$app->session->getFlash('register'),
//    ]);
//}
$this->title = 'Редактирование';
$this->registerMetaTag([
        'name' => 'ROBOTS',
        'content' => 'NOINDEX,NOFOLLOW'
    ]);
?>

<div class="container">
    <div class="jumbotron">
        <div class="container">
            <?php $text = \app\models\TextPlaceContent::getTextBlock(11); ?>
            <h1><?=$text['name'];?></h1>
            <?=$text['text'];?>
            <?php $form = yii\bootstrap\ActiveForm::begin([
                    'id'=>strtolower($model->formName()).'-form',
                    'options' => ['class' => 'form-horizontal'],
                    'enableAjaxValidation' => true,
                    //'enableClientValidation' => true,
            ]); ?>
			<?php
			if(\Yii::$app->session->hasFlash('register-edit'))
			{
			    echo Alert::widget([
			        'options' => ['class' => 'alert-success'],
			        'body' => \Yii::$app->session->getFlash('register-edit'),
			    ]);
			}
			?>
            <div class="errorSummary">
                <?=HTML::errorSummary($model)?>
            </div>
                <div class="form-group">
                     <div class="col-sm-offset-0 col-sm-2">
                        <?= Html::activeLabel($model, 'surname') ?>
                        <?= Html::activeTextInput($model, 'surname', ['class'=>"form-control ".($model->hasErrors('surname') ? 'errorInput' : ''),
                                                                      'required'=>'required',
                                                                      'placeholder'=>'Иванов',
                                                                        'title'=>$model->getFirstError('surname'),]) ?>
                     </div>
                     <div class="col-sm-offset-0 col-sm-2">
                        <?= Html::activeLabel($model, 'name') ?>
                        <?= Html::activeTextInput($model, 'name', ['class'=>"form-control ".($model->hasErrors('name') ? 'errorInput' : ''),
                                                                      'required'=>'required',
                                                                      'placeholder'=>'Иван',
                                                                        'title'=>$model->getFirstError('name'),]) ?>
                     </div>
                     <div class="col-sm-offset-0 col-sm-2">
                        <?= Html::activeLabel($model, 'pname') ?>
                        <?= Html::activeTextInput($model, 'pname', ['class'=>"form-control ".($model->hasErrors('pname') ? 'errorInput' : ''),
                                                                      'placeholder'=>'Иванович',
                                                                    'title'=>$model->getFirstError('pname'),]) ?>
                     </div>
                </div>
                <div class="form-group-clear"></div>
                <div class="form-group">
                     <div class="col-sm-offset-0 col-sm-2">
                        <?= Html::activeLabel($model, 'phone') ?>
                        <?= Html::activeTextInput($model, 'phone', ['class'=>"form-control ".($model->hasErrors('phone') ? 'errorInput' : ''),
                                                                      'required'=>'required',
                                                                      'placeholder'=>'+7(999)999-9999',
                                                                        'title'=>$model->getFirstError('phone'),]) ?>
                     </div>
                     <div class="col-sm-offset-0 col-sm-2">
                        <?= Html::activeLabel($model, 'gradebook') ?>
                        <?= Html::activeTextInput($model, 'gradebook', ['class'=>"form-control ".($model->hasErrors('gradebook') ? 'errorInput' : ''),
                                                                      'required'=>'required',
                                                                      'placeholder'=>'8 цифр',
                                                                        'title'=>$model->getFirstError('gradebook'),]) ?>
                     </div>
                     <div class="col-sm-offset-0 col-sm-2">
                        <?= Html::activeLabel($model, 'group_num') ?>
                        <?= Html::activeTextInput($model, 'group_num', ['class'=>"form-control ".($model->hasErrors('group_num') ? 'errorInput' : ''),
                                                                      'placeholder'=>'53504/1',
                                                                        'title'=>$model->getFirstError('group_num'),]) ?>
                     </div>
                </div>
                <div class="form-group-clear"></div>
				<div class="form-group">
                     <div class="col-sm-offset-0 col-sm-2">
                        <?= Html::activeLabel($model, 'department_id') ?>
                        <?= Html::activeDropDownList($model, 'department_id',
											$model->getDepartments(),
											['class'=>"form-control ".($model->hasErrors('department_id') ? 'errorInput' : ''),
                                                                      'required'=>'required',
                                                                        'title'=>$model->getFirstError('department_id'),]) ?>
                     </div>
                     <div class="col-sm-offset-0 col-sm-2">
                        <?= Html::activeLabel($model, 'is_budget') ?>
                        <?= Html::activeDropDownList($model, 'is_budget', [ '1'=>'Бюджет','0'=> 'Контракт',], ['class'=>"form-control ".($model->hasErrors('is_budget') ? 'errorInput' : ''),
                                                                      'required'=>'required',
                                                                        'title'=>$model->getFirstError('is_budget'),]) ?>
                     </div>
					 <div class="col-sm-offset-0 col-sm-2">
                        <?= Html::activeLabel($model, 'inn', ['title'=>'Идентификационный номер налогоплательщика']) ?>
                        <?= Html::activeTextInput($model, 'inn', ['class'=>"form-control ".($model->hasErrors('inn') ? 'errorInput' : ''),
                                                                      'required'=>'required',
                                                                      'placeholder'=>'XXXXXXXXXXXX',
																	  'maxlength'=>12,
																	  'pattern' => '[0-9]{12}',
                                                                        'title'=>$model->getFirstError('inn'),
                            ]) ?>
                     </div>
                </div>
                <div class="form-group-clear"></div>
                <div class="form-group">
                     <div class="col-sm-offset-0 col-sm-2">
                        <?= Html::activeLabel($model, 'snils', ['title'=>'Страховой номер индивидуального лицевого счёта']) ?>
                        <?= Html::activeTextInput($model, 'snils', ['class'=>"form-control ".($model->hasErrors('snils') ? 'errorInput' : ''),
                                                                      'required'=>'required',
                                                                      'placeholder'=>'XXX-XXX-XXX YY',
                                                                      'title'=>$model->getFirstError('snils'),
                            ]) ?>
                     </div>
                     <div class="col-sm-offset-0 col-sm-2">
                        <?= Html::activeLabel($model, 'pass_series') ?>
                        <?= Html::activeTextInput($model, 'pass_series', ['class'=>"form-control ".($model->hasErrors('pass_series') ? 'errorInput' : ''),
                                                                      'required'=>'required',
                                                                      'placeholder'=>'XXXX',
																	  'maxlength'=>4,
																	  'pattern' => '[0-9]{4}',
                                                                        'title'=>$model->getFirstError('pass_series'),
                            ]) ?>
                     </div>
                     <div class="col-sm-offset-0 col-sm-2">
                        <?= Html::activeLabel($model, 'pass_id') ?>
                        <?= Html::activeTextInput($model, 'pass_id', ['class'=>"form-control ".($model->hasErrors('pass_id') ? 'errorInput' : ''),
                                                                      'placeholder'=>'YYYYYY',
																	  'required'=>'required',
																	  'maxlength'=>6,
																	  'pattern' => '[0-9]{6}',
                                                                      'title'=>$model->getFirstError('pass_id'),
                            ]) ?>
                     </div>
                </div>
				<div class="form-group-clear"></div>
                <div class="form-group">
                     <div class="col-sm-offset-0 col-sm-2">
                        <?= Html::activeLabel($model, 'issued', ['title'=>'Страховой номер индивидуального лицевого счёта']) ?>
                        <?= Html::activeTextArea($model, 'issued', ['class'=>"form-control ".($model->hasErrors('issued') ? 'errorInput' : ''),
                                                                      'required'=>'required',
                                                                      'title'=>$model->getFirstError('issued'),
                            ]) ?>
                     </div>
                     <div class="col-sm-offset-0 col-sm-2">
                        <?= Html::activeLabel($model, 'issued_date') ?>
                        <?= Html::activeTextInput($model, 'issued_date', ['class'=>"form-control ".($model->hasErrors('issued_date') ? 'errorInput' : ''),
                                                                      'required'=>'required',
                                                                      'placeholder'=>'01.01.1970',
																	  'pattern' => '[0-3][0-9]\.[0-1][0-9]\.[1-2][0-9][0-9][0-9]',
																	  'maxlength'=>10,
                                                                        'title'=>$model->getFirstError('issued_date'),
                            ]) ?>
                     </div>
                     <div class="col-sm-offset-0 col-sm-2">
                        <?= Html::activeLabel($model, 'subdiv_code') ?>
                        <?= Html::activeTextInput($model, 'subdiv_code', ['class'=>"form-control ".($model->hasErrors('subdiv_code') ? 'errorInput' : ''),
                                                                      'placeholder'=>'XXX-YYY',
																	  'required'=>'required',
																	  'maxlength'=>16,
                                                                      'title'=>$model->getFirstError('subdiv_code'),
                            ]) ?>
                     </div>
                </div>
				<div class="form-group-clear"></div>
                <div class="form-group">
                     <div class="col-sm-offset-0 col-sm-2">
                        <?= Html::activeLabel($model, 'birth_date') ?>
                        <?= Html::activeTextInput($model, 'birth_date', ['class'=>"form-control ".($model->hasErrors('birth_date') ? 'errorInput' : ''),
                                                                      'required'=>'required',
                                                                      'placeholder'=>'01.01.1970',
																	  'pattern' => '[0-3][0-9]\.[0-1][0-9]\.[1-2][0-9][0-9][0-9]',
																	  'maxlength'=>10,
                                                                      'title'=>$model->getFirstError('birth_date'),
                            ]) ?>
                     </div>
                     <div class="col-sm-offset-0 col-sm-2" >
                        <?= Html::activeLabel($model, 'birth_place') ?>
                        <?= Html::activeTextArea($model, 'birth_place', ['class'=>"form-control ".($model->hasErrors('birth_place') ? 'errorInput' : ''),
                                                                      'required'=>'required',
                                                                       'title'=>$model->getFirstError('birth_place'),
																	   
                            ]) ?>
                     </div>
                     <div class="col-sm-offset-0 col-sm-2" style="width: 210px;">
                        <?= Html::activeLabel($model, 'reg_addr',['title'=>'Адрес временной регистрации или прописки в Санкт-Петербурге']) ?>
                        <?= Html::activeTextArea($model, 'reg_addr', ['class'=>"form-control ".($model->hasErrors('reg_addr') ? 'errorInput' : ''),
																	  'required'=>'required',
                                                                      'title'=>$model->getFirstError('reg_addr'),
                            ]) ?>
                     </div>
                </div>
                <div class="form-group-clear"></div>
                <div class="form-group">
                    <div class="col-sm-offset-0 col-sm-2">
                        <?= Html::activeLabel($model, 'sex') ?>
                        <?= Html::activeDropDownList($model, 'sex', [ '0'=>'Мужской','1'=> 'Женский',], ['class'=>"form-control ".($model->hasErrors('sex') ? 'errorInput' : ''),
                                                                      'required'=>'required',
                                                                        'title'=>$model->getFirstError('sex'),]) ?>
                     </div>
                    <div class="col-sm-offset-0 col-sm-2">
                        <?= Html::activeLabel($model, 'weight') ?>
                        <?= Html::activeDropDownList($model, 'weight', \app\models\Donorlite::getWeightItems(), ['class'=>"form-control ".($model->hasErrors('sex') ? 'errorInput' : ''),
                                                                      'required'=>'required',
                                                                        'title'=>$model->getFirstError('weight'),]) ?>
                     </div>
                    <div class="col-sm-offset-0 col-sm-2">
                        <?= Html::activeLabel($model, 'typing') ?>
                        <?= Html::activeDropDownList($model, 'typing', [ '0'=>'Не согласен','1'=> 'Согласен',], ['class'=>"form-control ".($model->hasErrors('sex') ? 'errorInput' : ''),
                                                                      'required'=>'required',
                                                                        'title'=>$model->getFirstError('typing'),]) ?>
                     </div>
                </div>
                <div class="form-group-clear"></div>
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary', 'required'=>'required', 'name'=>'save']) ?>
            <?php yii\bootstrap\ActiveForm::end() ?>
        </div>
    </div>
</div>