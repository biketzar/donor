<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\SiteHelper;
use yii\bootstrap\Alert;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

$page = SiteHelper::getCurrentPage();
$this->title = 'Восстановление пароля';
if(!isset($this->params['breadcrumbs']))
    $this->params['breadcrumbs'] = [];
$this->params['breadcrumbs'] = array_merge($this->params['breadcrumbs'], SiteHelper::getCurrentPageBreadcrumbs(false));

$metaData = SiteHelper::metaData($this, [$page], [
    'title' => \Yii::t('app', 'Forget password'),
    'meta_title' => \Yii::t('app', 'Forget password'),
    'meta_description' => '',
    'meta_keywords' => '',
    ]);
	$this->registerMetaTag([
        'name' => 'ROBOTS',
        'content' => 'NOINDEX,NOFOLLOW'
    ]);
//echo '<h1>'.$metaData['title'].'</h1>';
//if(\Yii::$app->session->hasFlash('forget-password-success'))
//{
//    echo Alert::widget([
//        'options' => ['class' => 'alert-success'],
//        'body' => \Yii::$app->session->getFlash('forget-password-success'),
//    ]);
//}
//if(\Yii::$app->session->hasFlash('forget-password-error'))
//{
//    echo Alert::widget([
//        'options' => ['class' => 'alert-error'],
//        'body' => \Yii::$app->session->getFlash('forget-password-error'),
//    ]);
//}
?>
<div class="container">
    <div class="jumbotron">
        <div class="container">
            <?php $text = \app\models\TextPlaceContent::getTextBlock(4); ?>
            <h1><?=$text['name'];?></h1>
            <?=$text['text'];?>
            <?php $form = yii\bootstrap\ActiveForm::begin([
                    'id'=>strtolower($model->formName()).'-form',
                    'options' => ['class' => 'form-horizontal'],
                    'enableAjaxValidation' => true,
                    //'enableClientValidation' => true,
            ]); ?>
                <div class="errorSummary">
                    <?php
                    if(\Yii::$app->session->hasFlash('forget-password-success'))
                    {
                        echo Alert::widget([
                            'options' => ['class' => 'alert-success'],
                            'body' => \Yii::$app->session->getFlash('forget-password-success'),
                        ]);
                    }
                    if(\Yii::$app->session->hasFlash('forget-password-error'))
                    {
                        echo Alert::widget([
                            'options' => ['class' => 'alert-error'],
                            'body' => \Yii::$app->session->getFlash('forget-password-error'),
                        ]);
                    }
                    ?>
                    <?=($model->hasErrors('password') ? $model->getFirstError('password') : '');?>
                </div>
            <?php if(!\Yii::$app->session->hasFlash('forget-password-success')){?>
            <div class="form-group">
                <div class="col-sm-offset-0 col-sm-2">
                        <?= Html::activeLabel($model, 'email') ?>
                        <?= Html::activeTextInput($model, 'email', ['class'=>"form-control ",
                                                                      'required'=>'required',
                                                                      'type'=>'email',
                                                                      'value'=>'',
                            ]) ?>
                     </div>
                    </div>
                <div class="form-group-clear"></div>
                <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary']) ?>
                <br/>
            <?php }?>
            <?php yii\bootstrap\ActiveForm::end() ?>
        </div>
    </div>
</div>
