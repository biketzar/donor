<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\SiteHelper;
use yii\bootstrap\Alert;
$page = SiteHelper::getCurrentPage();
$this->title = 'Личный кабинет';
if(!isset($this->params['breadcrumbs']))
    $this->params['breadcrumbs'] = [];
$this->params['breadcrumbs'] = array_merge($this->params['breadcrumbs'], SiteHelper::getCurrentPageBreadcrumbs(false));

$metaData = SiteHelper::metaData($this, [$page], [
    'title' => 'Личный кабинет',
    'meta_title' => 'Личный кабинет',
    'meta_description' => '',
    'meta_keywords' => '',
    ]);
	$this->registerMetaTag([
        'name' => 'ROBOTS',
        'content' => 'NOINDEX,NOFOLLOW'
    ]);
?>
<div class="container">
    <div class="jumbotron">
        <div class="container">
            <?php $text = \app\models\TextPlaceContent::getTextBlock(7); ?>
            <h1>Личный кабинет</h1>
            <p class='lead'>Здесь отображается информация о вашей донорской активности, а также есть возможность изменить время сдачи</p>
            <p class='lead'>За несколько дней до сдачи с Вами по телефону свяжется волонтер, чтобы сообщить дополнительную информацию </p>
            <p>
				<a href="/user/addinfo/" class="btn btn-primary btn-lg" role="button">Редактирование</a>
				<a href="/user/logout/?k=<?=md5(time())?>" class="btn btn-primary btn-lg" role="button">Выйти</a>
			</p> 
            <?php if(\Yii::$app->session->hasFlash('date-form-error'))
                    {
                        echo \yii\bootstrap\Alert::widget([
                            'options' => ['class' => 'alert-error'],
                            'body' => \Yii::$app->session->getFlash('date-form-error'),
                        ]);
                    }
                ?>            
                        
        </div>
    </div>
            
<div class="container container-fluid">
    <div class="col-sm-offset-0 col-sm-6">
        <h2 class="sub-header">Донор крови</h2>
        <div class="table-responsive">
            <table class="table table-striped">
                <tbody>
                    <tr>
                        <td>Дата и время сдачи</td>
                        <td>
            <?php
            $donorlist = \app\models\Donorlist::find()->where('id_donor=:id_donor AND approved=1', [':id_donor' => $donor->id])->one();
            $dstatus = 'Не утверждено';
            if($donorlist && $donorlist->times)
            {
                $dstatus = date('d.m.Y H:i', strtotime($donorlist->times->dtime));
            }
            echo $dstatus;
            ?>
                            
                        </td>
                    </tr>
                    <tr>
                        <td>Кровоотдач</td>
                        <td><?=count($donates)?></td>
                    </tr>
                    <tr>
                        <td>Последняя кровоотдача</td>
                        <td><?=count($donates)>0 ? date('m.Y', strtotime($donates[0]->date)) : '-'?></td>
                    </tr>
                
                </tbody>
            </table>
        </div>
    </div>
    
</div>
<hr>

<div class="table-responsive container container-fluid">
    <h2 class="sub-header">Время сдачи</h2>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Дата и время</th>
            </tr>
        </thead>
        <tbody>
            
            
            <?php
            $prName = ['1'=>'Первый','2'=>'Второй','3'=>'Третий',];
            foreach($eventDay as $event)
            {
                ?>
                <tr>
                    <td><?=(isset($prName[$event->priority]) ? $prName[$event->priority] : $event->priority)?></td>
                    <td>
                        <?=$event->getDateTime('d.m.Y H:i');?> <br/>
                        <?php if(
                                ($donor->typing == 0 && $donor->weight == 1 && $event->times->typing == 1)
                                || (!($donor->typing == 0 && $donor->weight == 1) && $event->times->typing == 0)) { ?>
                        <span style="color:red">
                             Время выбрано некорректно - необходимо выбрать другое время
                        </span>
                        <?php } ?> 
                    </td>
                </tr>
                <?php
            }
            ?>
        </tbody>
    </table>
    <p>        
        <a href="/user/datetime/" class="btn btn-primary" role="button">Изменить</a>
    </p>
</div>

</div>
<?php $form = yii\bootstrap\ActiveForm::begin([
                    'id'=>'hidden-form-yes',
                    'options' => ['class' => 'hiden'],
                    'action' => '/user/login/'
            ]); ?>
<?php yii\bootstrap\ActiveForm::end() ?>