<?php
namespace app\components;

class UrlManager extends \yii\web\UrlManager
{
    public function createUrl($params)
    {
        $addLang = false;
        
        if(isset($params['lang']))
        {
            $addLang = $params['lang'];
            unset($params['lang']);
        }
        $url = parent::createUrl($params);
        return MultilangHelper::addLangToUrl($url, $addLang);
        
    }
    public function parseRequest($request)
    {
        $url = $request->getPathInfo();
        $url = MultilangHelper::processLangInUrl($url);
        $url = trim($url, '/');
        $request->setPathInfo($url);
        return parent::parseRequest($request);
    }
}
?>
