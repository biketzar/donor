<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
namespace app\components;

use yii\behaviors\TimestampBehavior;

use yii\db\BaseActiveRecord;
//use yii\db\ActiveQuery;

class CommonActiveRecord extends \yii\db\ActiveRecord
{
    public $filterVars = [];
    
    public static function find()
    {
        $query = new CommonActiveQuery(get_called_class());
        return $query;
    }
    
    public function beforeDelete()
    {
        parent::beforeDelete();
        if($this->hasAttribute('visible'))
        {
            $this->visible = 0;
            $this->update(true, array('visible'));
            
            $this->setOldAttributes(null);
            $this->afterDelete();
            
            return false;
        } else {
            return true;
        }
    }
    public function init()
    {
        parent::init();
        
        if($this->getScenario() == 'insert' || $this->getScenario() == 'update')
        {
            $this->loadDefaultValues();
            
            if($this->isNewRecord && $this->hasAttribute('ordering')){
                $query = static::getDb()->createCommand('SELECT MAX(ordering) as max FROM '.$this->tableName().' WHERE visible=1');
                $this->ordering = intval($query->queryScalar())+10;
            }
        }
    }
    
    public function afterFind() {
        parent::afterFind();
        
    }
    public function searchQuery($data)
    {
        $className = $this->className();
        $formName = $this->formName();
        
        if(isset($data[$formName]))
            $data = $data[$formName];
        $query = $className::find();
        
        // load the search form data and validate
        if (!$this->validate()) {
            return $query;
        }
        
        $privateModelAdmin = \Yii::createObject(['class' => 'app\modules\privatepanel\components\PrivateModelAdmin'],[$this]);
        
        foreach ($this->activeAttributes() as $param)
        {
            if(isset($data[$param]))
            {
                /*if($relation = $this->getRelation($param, false))
                {
                    
                } else {*/
                    if($this->hasAttribute($param) && isset($data[$param]))
                    {
                        if($privateModelAdmin->getField($param))
                            $query->andFilterWhere($privateModelAdmin->getField($param)->getFilterWhere($this->$param));
                        else
                            $query->andFilterWhere([$this->tableName().'.'.$param => $this->$param]);
                    }
                //}
            }
        }
        $getParams = \Yii::$app->request->getQueryParams(); 
        
        $query = $this->parseArrayToQuery($query, $data, $privateModelAdmin, false);
        $query = $this->parseArrayToQuery($query, $getParams, $privateModelAdmin, false);
        
        $query = $this->parseArrayToQuery($query, $data, $privateModelAdmin, true);
        $query = $this->parseArrayToQuery($query, $getParams, $privateModelAdmin, true);
        
        /*foreach($data as $param=>$value)
        {
            if(isset($value))
            {
                if($relation = $this->getRelation($param, false))
                {
                    if(!$relation->multiple)
                    {
                        foreach($relation->link as $a=>$b)
                        {
                            $query->andFilterWhere([$b => $value]);
                            $this->$b = $value;
                        }
                    } else {
                        if($relation->via && $relation->via->link)
                        {
                            $modelClass = $relation->modelClass;
                            $m = new $modelClass;
                            foreach($relation->link as $a=>$b)
                                $query->andFilterWhere([$m->tableName().'.'.$a => $value]);

                            $query->innerJoinWith($param);

                            $this->filterVars[$param] = $value;
                        } else {
                            foreach($relation->link as $a=>$b)
                                $query->andFilterWhere([$b => $value]);

                            $query->innerJoinWith($param);
                        }
                    }
                } else {
                    
                }
            }
        }*/
        
        /*if(isset($getParams))
        {
            foreach($getParams as $param=>$value)
            {
                if(isset($value))
                {
                    if($relation = $this->getRelation($param, false))
                    {
                        
                    } else {
                        if($this->hasAttribute($param) && $f = $privateModelAdmin->getField($param)->getFilterWhere($value))
                        {
                            $query->andFilterWhere($f);
                            $this->$param = $value;
                        }
                        
                    }
                }
            }
            foreach($getParams as $param=>$value)
            {
                if(isset($value))
                {
                    if($relation = $this->getRelation($param, false))
                    {
                        if(!$relation->multiple)
                        {
                            foreach($relation->link as $a=>$b)
                            {
                                $query->andFilterWhere([$b => $value]);
                                $this->$b = $value;
                            }
                            
                        } else {
                            
                            if($relation->via && $relation->via->link)
                            {
                                $modelClass = $relation->modelClass;
                                $m = new $modelClass;
                                foreach($relation->link as $a=>$b)
                                    $query->andFilterWhere([$m->tableName().'.'.$a => $value]);

                                $query->innerJoinWith($param);
                            } else {
                                foreach($relation->link as $a=>$b)
                                    $query->andFilterWhere([$b => $value]);

                                $query->innerJoinWith($param);
                            }
                            
                        }
                        
                    } else {
                        
                    }
                }
            }
        }*/
        
        /*$getParams = \Yii::$app->request->get($formName); 
        
        if(isset($getParams))
        {
            foreach($getParams as $param=>$value)
            {
                
                if(isset($value))
                {
                    if($relation = $this->getRelation($param, false))
                    {
                        
                    } else {
                        if($this->hasAttribute($param) && $f = $privateModelAdmin->getField($param)->getFilterWhere($value))
                        {
                            $query->andFilterWhere($f);
                            $this->$param = $value;
                        }
                        
                    }
                }
            }
            foreach($getParams as $param=>$value)
            {
                if(isset($value))
                {
                    if($relation = $this->getRelation($param, false))
                    {
                        if(!$relation->multiple)
                        {
                            foreach($relation->link as $a=>$b)
                            {
                                $query->andFilterWhere([$b => $value]);
                                $this->$b = $value;
                            }
                        } else {
                            if($relation->via && $relation->via->link)
                            {
                                $modelClass = $relation->modelClass;
                                $m = new $modelClass;
                                foreach($relation->link as $a=>$b)
                                    $query->andFilterWhere([$m->tableName().'.'.$a => $value]);

                                $query->innerJoinWith($param);
                                
                                $this->filterVars[$param] = $value;
                            } else {
                                foreach($relation->link as $a=>$b)
                                    $query->andFilterWhere([$b => $value]);

                                $query->innerJoinWith($param);
                            }
                        }
                    } else {
                        
                    }
                }
            }
        }*/
        
        return $query;
    }
    
    protected function parseArrayToQuery($query, $data, $privateModelAdmin, $parse_relations = false)
    {
        if(isset($data))
        {
            foreach($data as $param=>$value)
            {
                if(!empty($value))
                {
                    if(!$parse_relations && $this->hasAttribute($param) && $f = $privateModelAdmin->getField($param)->getFilterWhere($value))
                    {
                        $query->andFilterWhere($f);
                        $this->$param = $value;
                    }
                    
                    if($parse_relations && $relation = $this->getRelation($param, false))
                    {
                        if(!$relation->multiple)
                        {
                            foreach($relation->link as $a=>$b)
                            {
                                $query->andFilterWhere([$b => $value]);
                                $this->$b = $value;
                            }
                            
                        } else {
                            if($relation->via && $relation->via->link)
                            {
                                $modelClass = $relation->modelClass;
                                $m = new $modelClass;
                                foreach($relation->link as $a=>$b)
                                    $query->andFilterWhere([$m->tableName().'.'.$a => $value]);

                                $query->innerJoinWith($param);
                                $this->filterVars[$param] = $value;
                            } else {
                                foreach($relation->link as $a=>$b)
                                    $query->andFilterWhere([$b => $value]);

                                $query->innerJoinWith($param);
                            }
                            
                        }
                    } 
                }
            }
        }
        return $query;
    }
    
    /*public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord && $this->hasAttribute('create_time')) {
                $this->create_time = date('Y-m-d H:i:s');
            }
            if (!$this->isNewRecord && $this->hasAttribute('update_time')) {
                $this->update_time = date('Y-m-d H:i:s');
            }
            return true;
        }
        return false;
    }*/
    
    public function behaviors()
    {
        $insert_attrs = [];
        $update_attrs = [];
        if($this->hasAttribute('create_time'))
            $insert_attrs[] = 'create_time';
        if($this->hasAttribute('update_time'))
        {
            $insert_attrs[] = 'update_time';
            $update_attrs[] = 'update_time';
        }
        
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    BaseActiveRecord::EVENT_BEFORE_INSERT => $insert_attrs,
                    BaseActiveRecord::EVENT_BEFORE_UPDATE => $update_attrs,
                ],
                'value' => function($event){
                    return date('Y-m-d H:i:s');
                }
            ],
        ];
    }
    
    public function getAttributeLabel($attribute)
    {
        if(!$this->getBehavior('ml'))
        {
            return parent::getAttributeLabel($attribute);
        } else {
            $labels = $this->attributeLabels();
            if(isset($labels[$attribute]))
                return $labels[$attribute];
            foreach(\Yii::$app->params['translatedLanguages'] as $l=>$lang):
                if(preg_match("/_".$l."$/si", $attribute))
                {
                    $attribute = substr($attribute, 0, strlen($attribute) - strlen('_'.$l));
                    return isset($labels[$attribute]) ? $labels[$attribute] .' ('.$lang.')' : $this->generateAttributeLabel($attribute).' ('.$lang.')';
                }
            endforeach;
            return $this->generateAttributeLabel($attribute);
        }
    }
    
}
class CommonActiveQuery extends \yii\db\ActiveQuery
{
    protected $use_visible = true;
    
    public $defaultOrder = null;
    
    public function createCommand($db = null)
    {
        $className = $this->modelClass;
        $model = \yii::createObject($className);
        
        if($model->hasAttribute('visible') && $this->use_visible)
            $this->andWhere([$model->tableName().'.visible' => 1]);
        
        if($this->defaultOrder)
        {
            if(!isset($this->orderBy))
            {
                $this->orderBy($this->defaultOrder);
            } else {
                foreach($this->defaultOrder as $k=>$v)
                {
                    if(!isset($this->orderBy[$k]))
                    {
                        $this->addOrderBy([$k => $v]);
                    }
                }
            }
        }
        
        return parent::createCommand($db);
    }
    public function allVisible()
    {
        $this->use_visible = false;
        return $this;
    }
    public function withTranslation()
    {
        $className = $this->modelClass;
        $model = new $className;
        
        if($model->getBehavior('ml'))
        {
            if($model->getBehavior('ml')->strict)
                $this->innerJoinWith('i18N');
            else
                $this->joinWith('i18N');
        }
        return $this;
    }
    /*public function visible($state = 1)
    {
        return $this->andWhere(['visible' => $state]);
    }*/
    public function published($state = 1)
    {
        $className = $this->modelClass;
        $model = \yii::createObject($className);
        return $this->andWhere([$model->tableName().'.published' => $state]);
    }
}
?>
