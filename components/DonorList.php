<?php
namespace app\components;

class DonorList
{
    public function add()
    {
        set_time_limit(0);
        $idTimeList = \app\models\Times::find()
                //->where('typing=1')
                ->orderBy('dtime ASC')->asArray()->all();
        $timeIds = ['in' => [], 'out' => []];
        $donorList = [];
        $outDonorList = [];
        foreach($idTimeList as $time)
        {
            if($time['typing'] == 0)
            {
                $donorList[$time['id']] = $this->getDonorList($time['id']);
                $timeIds['in'][] = $time['id'];
            }
            else
            {
                $timeIds['out'][] = $time['id'];
            }
            $outDonorList[$time['id']] = $this->getOutDonorList($time['id']);
        }
        $inlist = \app\models\Donorlist::find()->select('id_donor')->column();
        $dbDonorVipList = \app\models\Donorlite::find()
                ->where('is_vip=1')
                ->andWhere(['NOT IN', 'id', $inlist])
                ->orderBy('create_time ASC')->asArray()->all();
        $returnData = $this->addToDonorList($donorList, $outDonorList, $dbDonorVipList, $idTimeList, $timeIds);
        $donorList = $returnData[0];
        $outDonorList = $returnData[1];
        
        $donorFromHistory = \app\models\Donorhistory::find()->select('id_donor')->column();
        $dbDonorList = \app\models\Donorlite::find()
                ->where('is_vip!=1 and donorstatus!=7')
                ->andWhere(['NOT IN', 'id', $donorFromHistory])
                ->andWhere(['NOT IN', 'id', $inlist])
                ->orderBy('create_time ASC')->asArray()->all();
        $returnData = $this->addToDonorList($donorList, $outDonorList, $dbDonorList, $idTimeList, $timeIds);
        $donorList = $returnData[0];
        $outDonorList = $returnData[1];
        $dbDonorList = \app\models\Donorlite::find()
                ->where('is_vip!=1 and donorstatus!=7')
                ->andWhere(['IN', 'id', $donorFromHistory])
                ->andWhere(['NOT IN', 'id', $inlist])
                ->orderBy('create_time ASC')->asArray()->all();
        $returnData = $this->addToDonorList($donorList, $outDonorList, $dbDonorList, $idTimeList, $timeIds);
        $donorList = $returnData[0];
        $outDonorList = $returnData[1];
        foreach($donorList as $timeKey => $childList)
        {
            foreach($childList as $donorId)
            {
                $model = \app\models\Donorlist::findOne(['id_donor' => $donorId]);
                if(!$model)
                {
                    $model = new \app\models\Donorlist;
                    $model->id_donor = $donorId;
                    $model->approved = 0;
                    $model->save();
                }
                else{continue;}
                if($model->approved != 1)//если не утверждено время
                {
                    if($model->type != 0 || $model->date_id != $timeKey)
                    {
                        $model->type = 0;
                        $model->date_id = $timeKey;
                        $model->save();
                    }
                }
            }
        } 
        foreach($outDonorList as $timeKey => $childList)
        {
            foreach($childList as $donorId)
            {
                $model = \app\models\Donorlist::findOne(['id_donor' => $donorId]);
                if(!$model)
                {
                    $model = new \app\models\Donorlist;
                    $model->id_donor = $donorId;
                    $model->approved = 0;
                    $model->save();
                }else{continue;}
                if($model->approved != 1)//если не утверждено время
                {
                    if($model->type != 1 || $model->date_id != $timeKey)
                    {
                        $model->type = 1;
                        $model->date_id = $timeKey;
                        $model->save();
                    }
                }
            }
        }
       
    }
    
    /**
     * добавление доноров в список
     * @param array $donorList списко основных доноров
     * @param array $outDonorList список выездных доноров
     * @param array $dbDonorList список доноров, которых надо распределить
     * @param array $idTimeList массив со временем
     * @return array ($donorList, $outDonorList)
     */
    private function addToDonorList($donorList, $outDonorList, $dbDonorList, $idTimeList, $timeIds)
    {
        $priorityList = [1,2,3];
        //foreach($priorityList as $priority)
        {
            foreach($dbDonorList as $key => $donor)
            {
                if($this->inList($donor['id'], $donorList, $outDonorList))
                {
                    unset($dbDonorList[$key]);
                    continue;
                }
                $timeForDonor = $this->getTimeForDonor(
                        (int)$donor['id'], 
                        $idTimeList, 
                        $donor['typing'] == 0 && $donor['weight'] == 1 ? $donorList : $outDonorList, 
                        $donor['typing'] == 0 && $donor['weight'] == 1 ? $timeIds['in'] : $timeIds['out']
                        );
                if($timeForDonor !== FALSE)
                {
                    //var_dump($this->checkCount($donorList), $donor['weight'], $donor['typing']);
                    if($this->checkCount($donorList) !== FALSE 
                            && $donor['weight'] == 1 
                            && $donor['typing'] == 0
                            )
                    {
                        $donorList[$timeForDonor][] = (int)$donor['id'];
                    }
                    else
                    {
                        $outDonorList[$timeForDonor][] = (int)$donor['id'];
                    }
                    unset($dbDonorList[$key]);
                }
            }
        }
        return [$donorList, $outDonorList];
    }
    
    /**
     * 
     * @param int $donorId id донора
     * @param int $priority приоритет
     * @param array $idTimeList - список с доступным временем
     * @return boolean|int id выбранного времени или false
     */
    private function getTimeForDonor($donorId, $idTimeList, $donorList, $timeIds)
    {
        $models = $this->getTimeById($donorId, $timeIds);
        if($models)
        {
            $first = reset($models);
            $min = count($donorList[$first['date_id']]);
            $dateId = $first['date_id'];
            foreach($models as $model)
            {
                if($min > count($donorList[$model['date_id']]))
                {
                    $min = count($donorList[$model['date_id']]);
                    $dateId = $model['date_id'];
                }
            }
            return $dateId;
        }
        return FALSE;
    }
    
    /**
     * 
     * @staticvar array $timelist
     * @param int $id донора
     * @param array $timeIds список времени
     * @return array
     */
    private function getTimeById($id, $timeIds)
    {
        static $timelist;
        if(!$timeIds)
        {
            return [];
        }
        $key = implode('_', $timeIds);
        if(!is_array($timelist))
        {
            $timelist = array();
        }
        if(array_key_exists($key, $timelist))
        {
            return array_key_exists($id, $timelist[$key]) ? $timelist[$key][$id] : [];
        }
        $timelist[$key] = array();
        $models = \app\models\Eventday::find()
                ->select('date_id, id_donor')
                //->where('id_donor=:id_donor', [':id_donor' => $id])
                ->andWhere(['in', 'date_id', $timeIds])
                ->orderBy('priority ASC')
                ->asArray()
                ->all();
        foreach($models as $model)
        {
            $timelist[$key][$model['id_donor']][] = $model;
        }
        return array_key_exists($id, $timelist[$key]) ? $timelist[$key][$id] : [];
    }
    
    //private function getTimeForDonorAll()
    
    /**
     * проверка - можно ди добавить донора в основной список
     * @param type $donorList
     * @return type
     */
    private function checkCount($donorList)
    {
        $max = (int)\app\components\ConstantHelper::getValue('max-donor-count', 400);
        $counter = 0;
        foreach($donorList as $childList)
        {
            $counter += count($childList);
        }
        return true;//$max > $counter;
    }
    
    /**
     * есть ли $donorId в списках
     * @param int $donorId
     * @param type $donorList
     * @param type $outDonorList
     * @return boolean
     */
    private function inList($donorId, $donorList, $outDonorList)
    {
        $donorId = (int)$donorId;
        foreach($donorList as $childList)
        {
            if(in_array($donorId, $childList))
            {
                return TRUE;
            }
        }
        foreach($outDonorList as $childList)
        {
            if(in_array($donorId, $childList))
            {
                return TRUE;
            }
        }
        return FALSE;
    }
    
    private function getDonorList($timeId)
    {
        $donorList = [];
        $rows = \app\models\Donorlist::find()
                ->select('id_donor')
                ->where('date_id=:date_id and approved!=2 and type=0', [':date_id'=>$timeId])
                ->asArray()
                ->all();
        foreach($rows as $row)
        {
            $donorList[] = (int)$row['id_donor'];
        }
        return $donorList;
    }
    
    private function getOutDonorList($timeId)
    {
        $outDonorList = [];
        $rows = \app\models\Donorlist::find()
                ->select('id_donor')
                ->where('date_id=:date_id and approved!=2 and type=1', [':date_id'=>$timeId])
                ->asArray()
                ->all();
        foreach($rows as $row)
        {
            $outDonorList[] = (int)$row['id_donor'];
        }
        return $outDonorList;
    }
}