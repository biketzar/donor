<?php
namespace app\components;

use yii\web\ForbiddenHttpException;

class AccessControl extends \yii\filters\AccessControl
{
    public function beforeAction($action)
    {
        
        return parent::beforeAction($action);
    }
    
    /**
     * Denies the access of the user.
     * The default implementation will redirect the user to the login page if he is a guest;
     * if the user is already logged, a 403 HTTP exception will be thrown.
     * @param  yii\web\User $user the current user
     * @throws yii\web\ForbiddenHttpException if the user is already logged in.
     */
    /*protected function denyAccess($user)
    {
        if ($user->getIsGuest()) {
            $user->loginRequired();
        } else {
            throw new ForbiddenHttpException(\Yii::t('yii', 'You are not allowed to perform this action.'));
        }
    }*/
}
?>
