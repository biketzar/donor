<?php
namespace app\components;

use yii\helpers\Url;
use yii\helpers\Html;

class SiteHelper extends \yii\base\Object
{
    public static function translit($str)
    {
            $tr = array(
                    "А"=>"A","Б"=>"B","В"=>"V","Г"=>"G",
                    "Д"=>"D","Е"=>"E","Ж"=>"J","З"=>"Z","И"=>"I",
                    "Й"=>"Y","К"=>"K","Л"=>"L","М"=>"M","Н"=>"N",
                    "О"=>"O","П"=>"P","Р"=>"R","С"=>"S","Т"=>"T",
                    "У"=>"U","Ф"=>"F","Х"=>"H","Ц"=>"TS","Ч"=>"CH",
                    "Ш"=>"SH","Щ"=>"SCH","Ъ"=>"","Ы"=>"YI","Ь"=>"",
                    "Э"=>"E","Ю"=>"YU","Я"=>"YA","а"=>"a","б"=>"b",
                    "в"=>"v","г"=>"g","д"=>"d","е"=>"e","ж"=>"j",
                    "з"=>"z","и"=>"i","й"=>"y","к"=>"k","л"=>"l",
                    "м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
                    "с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"h",
                    "ц"=>"ts","ч"=>"ch","ш"=>"sh","щ"=>"sch","ъ"=>"y",
                    "ы"=>"yi","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya",
       "."=>"-"," "=>"-","?"=>"-","/"=>"-","\\"=>"-",
       "*"=>"-",":"=>"-","*"=>"-","\""=>"","<"=>"-",
       ">"=>"-","|"=>"-", " "=>'-'
            );
            return strtr($str,$tr);
    }
    
    public static function metaData($view, $objects, $defaults)
    {
        $title = '';
        $meta_title = '';
        $meta_description = '';
        $meta_keywords = '';
        
        $page_id = \Yii::$app->request->get('page_id', 0);
        
        if($page_id)
        {
            $page = \app\models\Page::find()->where('id=:id', [':id' => $page_id])->published()->one();
            if($page)
            {
                $objects[] = $page;
            }
        }
        
        foreach($objects as $object)
        {
            if($object instanceof \yii\db\ActiveRecord)
            {
                if($object->getRelation('metadata', false) && $object->metadata)
                {
                    if(!$title && $object->metadata->title)
                        $title = $object->metadata->title;
                    if(!$meta_title && $object->metadata->meta_title)
                        $meta_title = $object->metadata->meta_title;
                    if(!$meta_description && $object->metadata->meta_description)
                        $meta_description = $object->metadata->meta_description;
                    if(!$meta_keywords && $object->metadata->meta_keywords)
                        $meta_keywords = $object->metadata->meta_keywords;
                }
            }
        }
        
        if(!$title && isset($defaults['title']))
            $title = $defaults['title'];
        if(!$meta_title && isset($defaults['meta_title']))
            $meta_title = $defaults['meta_title'];
        if(!$meta_description && isset($defaults['meta_description']))
            $meta_description = $defaults['meta_description'];
        if(!$meta_keywords && isset($defaults['meta_keywords']))
            $meta_keywords = $defaults['meta_keywords'];
        
        $view->title = $meta_title;
        $view->registerMetaTag(['name' => 'description', 'content' => $meta_description], 'description');
        $view->registerMetaTag(['name' => 'keywords', 'content' => $meta_keywords], 'keywords');
        
        return [
            'title' => $title,
            'meta_title' => $meta_title,
            'meta_description' => $meta_description,
            'meta_keywords' => $meta_keywords,
        ];
    }
    
    public static function getCurrentPage($refresh = false)
    {
        static $result = null;
        if(!$result || $refresh)
        {
            $page_id = \Yii::$app->request->get('page_id', 0);
        
            if($page_id)
            {
                $result = \app\models\Page::find()->where('id=:id', [':id' => $page_id])->published()->one();
            }
        }
        return $result;
    }
    public static function getCurrentPageBreadcrumbs($last = false, $page = null)
    {
        static $result = null;
        
        if(!$result)
        {
            $path = [];
            if(empty($page))
                $page = static::getCurrentPage();
            if($page)
            {
                //if(!$last)
                    $path[] = ['label' => $page->name, 'url' => Url::to(['/site/page', 'page_id' => $page->id])];
                $parent = $page;
                while($parent = $parent->parent)
                {
                    $path[] = ['label' => $parent->name, 'url' => Url::to(['/site/page', 'page_id' => $parent->id])];
                }
                $path = array_reverse($path);
            } 
            $result = $path;
        }
        
        $t = $result;
        
        if(!$last)
            unset($t[count($t)-1]['url']);
        
        return $t;
    }
    
    public static function getMenuItemUrl($page)
    {
        static $urls = [];
        
        $result = '';
        if($page)
        {
            if(isset($urls[$page->id]))
                return $urls[$page->id];
            $prefix = static::getMenuItemUrl($page->parent);
            $result = (($prefix)?$prefix.'/':'').$page->alias;
            $urls[$page->id] = $result;
        }
        return $result;
    }
    
    protected static function addMenuItem(\yii\db\ActiveRecord $page, $menu_id = 0, $use_module_methods = false)
    {
        $result = [];
        
        $url = static::getPageRoute($page);
        
        
        /*if(\yii::$app->user->can($url))
        {*/
            $items = [];
            
            foreach($page->activeChildren as $child)
            {
                if(!$menu_id || $menu_id > 0 && in_array($menu_id, $child->getMenuIds()))
                {
                    $items = array_merge($items, static::addMenuItem($child, 0, $use_module_methods));
                    
                }
            }
            
            $moduleID = \app\models\Page::getHandlerModule($page->handler);
            
            if($use_module_methods)
            {
                
                if($moduleID)
                {
                    $module = \Yii::$app->getModule($moduleID);
                    if(method_exists($module, 'getSiteMenu'))
                    {
                        $items = array_merge($items, $module->getSiteMenu($page->id, $page->handler, $page->handler_data));
                        
                    }
                }
            }
            
            $result[] = [
                'label' => $page->name, 
                //'url' => ['/'.$url], 
                'url' => $url, 
                'items' => $items,
                'addoptions' => ['class' => 'nav__element-'.$moduleID],
                //'active' => $active || $has_active_child,
                ];
        //}
            
        return $result;
    }
    /*
     * get page url of object app\models\Page
     */
    public static function getPageRoute(\app\models\Page $page)
    {
        if($page->route)
        {
            if(Url::isRelative($page->route))
                $url = ['/'.$page->route];
            else
                $url = $page->route;
        } else 
            $url = ['/'.$page->handler, 'page_id' => $page->id];
        return $url;
    }
    public static function getMenuItems($type, $lang = null, $use_module_methods = false)
    {
        $menu = \app\models\MenuType::find()->where('alias=:id', [':id' => $type])->published()->one();

        if(!$menu)
        {
            $menu = \app\models\MenuType::find()->where('id=:id', [':id' => $type])->published()->one();
        }
        if(!$menu)
        {
            return [];
        }
        
        $cache = \yii::$app->cache;
        $key = 'sitehelper-menu-'.json_encode([$type, $lang, $use_module_methods, (int)\Yii::$app->user->getId()]);
        
        $data = $cache->get($key);
        $data = false;
        if ($data === false) 
        {
            $result = [];
            $subresult = [];
            $active = false;
            $pages = $menu->getActivePages($lang);
            foreach($pages as $page)
            {
                $result = array_merge($result, static::addMenuItem($page, $menu->id, $use_module_methods));
            }
            $cache->set($key, $result, 5*60);
            
        } else {
            $result = $data;
        }
        $active = false;
        list($result, $active) = static::setActiveMenuItem($result);
        return $result;
    }
    
    private static function setActiveMenuItem($items)
    {
        if(!is_array($items))
            return [$items, false];
        $has_active = false;
        foreach($items as $k => $item)
        {
            $current = trim(\yii::$app->request->getPathInfo(), '/');
            $menu = trim(Url::to($item['url']), '/');
            
            $active = ($current == $menu || (strpos($current, $menu) === 0 && substr(str_replace($menu, '', $current), 0, 1) == '/'))?true:false;
            
            $child_active = false;
            list($items[$k]['items'], $child_active) = static::setActiveMenuItem($items[$k]['items']);
            $items[$k]['active'] = $active || $child_active;
            $has_active = $has_active || $items[$k]['active'];
            
            
        }
        
        return [$items, $has_active];
    }
    
    public static function getPageIdByHandler($handler, $handler_data = [])
    {
        
        $result = null;
        $query = \app\models\Page::find()->where('handler=:handler', [':handler' => $handler])->published();
        if(!count($handler_data))
            $result = $query->one();
        else {
            $items = $query->all();
            if(count($items))
                $result = $items[0];
            foreach($items as $item)
            {
                $intersect = array_intersect($item->handler_data, $handler_data);
                
                if($handler_data == $intersect)
                {
                    $result = $item;
                    break;
                }
            }
        }
        
        return ($result)?$result->id:0;
    }
    
    public static function formatFileSize($bytes)
    {
        $bytes = floatval($bytes);
        $arBytes = array(
            0 => array(
                "UNIT" => "TB",
                "VALUE" => pow(1024, 4)
            ),
            1 => array(
                "UNIT" => "GB",
                "VALUE" => pow(1024, 3)
            ),
            2 => array(
                "UNIT" => "MB",
                "VALUE" => pow(1024, 2)
            ),
            3 => array(
                "UNIT" => "KB",
                "VALUE" => 1024
            ),
            4 => array(
                "UNIT" => "B",
                "VALUE" => 1
            ),
        );

        foreach($arBytes as $arItem)
        {
            if($bytes >= $arItem["VALUE"])
            {
                $result = $bytes / $arItem["VALUE"];
                $result = str_replace(".", "," , strval(round($result, 2)))." ".$arItem["UNIT"];
                break;
            }
        }
        
        return $result;
    }
    
    public static function strtolower($text)
    {
        if(function_exists('mb_strtolower'))
        {
            return mb_strtolower($text, \yii::$app->charset);
        } else {
            return strtolower($text);
        }
    }
    
    public static function checkFolderPath($path)
    {
        if(!is_dir($path))
        {
            $path_parts = str_replace(\Yii::getAlias('@webroot').'/', '', $path);
            $path_parts = explode('/',$path_parts);
            $path_temp = '';
            for($i = 0; $i<count($path_parts); $i++)
            {
                $path_temp.= $path_parts[$i].'/';
                if(!file_exists(\Yii::getAlias('@webroot').'/'.$path_temp))
                {
                    mkdir(\Yii::getAlias('@webroot').'/'.$path_temp, 0755);
                }
            }
        }
    }
    
    public static function processText($text)
    {
        //добавляет ссылки с классом fancybox на картинки с классом preview
        if(preg_match_all ("/(<img[^>]*class=['|\"]([^'|\"]+)['|\"][^>]*>)/i", $text, $images))
        {
            foreach($images[0] as $k => $imagetext)
            {
                $classes = explode(' ', $images[2][$k]);
                if(in_array('preview', $classes))
                {
                    $newimagetext = $imagetext;
                    $src = false;
                    $srctext = false;
                    $width = -1;
                    $height = -1;
                    $aoptions = ['class'=>'fancybox'];
                    if(preg_match("/src=['|\"]([^'|\"]+)['|\"]/i", $imagetext, $m))
                    {
                        $src = $m[1];
                        $srctext = $m[0];
                    } else {
                        continue;
                    }
                    if(preg_match("/alt=['|\"]([^'|\"]+)['|\"]/i", $imagetext, $m))
                    {
                        $aoptions['title'] = $m[1];
                    }
                    if(preg_match("/title=['|\"]([^'|\"]+)['|\"]/i", $imagetext, $m))
                    {
                        $aoptions['title'] = $m[1];
                    }
                    if(preg_match("/width=['|\"]([^'|\"]+)['|\"]/i", $imagetext, $m))
                    {
                        $width = $m[1];
                    }
                    if(preg_match("/height=['|\"]([^'|\"]+)['|\"]/i", $imagetext, $m))
                    {
                        $height = $m[1];
                    }
                    
                    if($width !== false || $height !== false)
                    {
                        $previewsrc = ImageHelper::thumbnail($src, $width, $height);
                        $newimagetext = str_replace($srctext, 'src="'.$previewsrc.'"', $newimagetext);
                    }
                    $newimagetext = Html::a($newimagetext, $src, $aoptions);
                    $text = str_replace($imagetext, $newimagetext, $text);
                }
                
            }
            
        }
        //убирает вложенные ссылки, если ссылка на fancybox попала внутрь другой ссылки
        if(preg_match_all ("/(<a[^>]*>)([^~]*)(<a[^>]*class=['|\"][^'|\"]*fancybox[^'|\"]*['|\"][^>]*>)([^~]*)(<\/a>)([^~]*)(<\/a>)/i", $text, $invalidlinks))
        {
            foreach($invalidlinks[0] as $k => $invalidlink)
            {
                $text = str_replace($invalidlink, $invalidlinks[1][$k].$invalidlinks[2][$k].
                        $invalidlinks[4][$k].$invalidlinks[6][$k].$invalidlinks[7][$k], $text);
            }
            
        }
        return $text;
    }
    
    public static function formatDate($date, $format = 'd F Y')
    {
        if (extension_loaded('intl')) {
            return \Yii::$app->formatter->asDate($date, 'php:'.$format);
        }
        $date = strtotime($date);
        $result = date($format, $date);
        $result = str_replace([
            'January', 'February', 'March',
            'April', 'May', 'June',
            'July', 'August', 'September',
            'October', 'November', 'December',
        ], [
            \Yii::t('common', 'January_rod'), \Yii::t('common', 'February_rod'), \Yii::t('common', 'March_rod'),
            \Yii::t('common', 'April_rod'), \Yii::t('common', 'May_rod'), \Yii::t('common', 'June_rod'),
            \Yii::t('common', 'July_rod'), \Yii::t('common', 'August_rod'), \Yii::t('common', 'September_rod'),
            \Yii::t('common', 'October_rod'), \Yii::t('common', 'November_rod'), \Yii::t('common', 'December_rod'),
        ], $result);
        return $result;
    }
}
?>
