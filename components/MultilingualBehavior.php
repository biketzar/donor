<?php
/**
 * MultilingualBehavior class file
 *
 * @author guillemc, Frédéric Rocheron<frederic.rocheron@gmail.com>
 */
/**
 * MultilingualBehavior handles active record model translation.
 *
 * This behavior allow you to create multilingual models and to use them (almost) like normal models.
 * For each model, translations have to be stored in a separate table of the database (ex: PostLang or ProductLang),
 * which allow you to easily add or remove a language without modifying your database.
 * 
 * Here an example of base 'post' table :
 * <pre>
 * CREATE TABLE IF NOT EXISTS `post` (
 *   `id` int(11) NOT NULL AUTO_INCREMENT,
 *   `title` varchar(255) NOT NULL,
 *   `content` TEXT NOT NULL,
 *   `created_at` datetime NOT NULL,
 *   `updated_at` datetime NOT NULL,
 *   `enabled` tinyint(1) NOT NULL DEFAULT '1',
 *   PRIMARY KEY (`id`)
 * ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
 * </pre>
 * 
 * And his associated translation table (configured as default), assuming translated fields are 'title' and 'content':
 * <pre>
 * CREATE TABLE IF NOT EXISTS `postLang` (
 *   `l_id` int(11) NOT NULL AUTO_INCREMENT,
 *   `post_id` int(11) NOT NULL,
 *   `lang_id` varchar(6) NOT NULL,
 *   `l_title` varchar(255) NOT NULL,
 *   `l_content` TEXT NOT NULL,
 *   PRIMARY KEY (`l_id`),
 *   KEY `post_id` (`post_id`),
 *   KEY `lang_id` (`lang_id`)
 * ) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
 * 
 * ALTER TABLE `postLang`
 *   ADD CONSTRAINT `postlang_ibfk_1` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
 * </pre>
 * 
 * Attach this behavior to the model (Post in the example).
 * (Everything that is commented is default values)
 * <pre>
 * public function behaviors() {
 *     return array(
 *         'ml' => array(
 *             'class' => 'application.models.behaviors.MultilingualBehavior',
 *             //'langClassName' => 'PostLang',
 *             //'langTableName' => 'postLang',
 *             //'langForeignKey' => 'post_id',
 *             //'langField' => 'lang_id',
 *             'localizedAttributes' => array('title', 'content'), //attributes of the model to be translated
 *             //'localizedPrefix' => 'l_',
 *             'languages' => Yii::app()->params['translatedLanguages'], // array of your translated languages. Example : array('fr' => 'Français', 'en' => 'English')
 *             'defaultLanguage' => Yii::app()->params['defaultLanguage'], //your main language. Example : 'fr'
 *             //'localizedRelation' => 'i18nPost',
 *             //'multilangRelation' => 'multilangPost',
 *             //'forceOverwrite' => false,
 *             //'forceDelete' => true, 
 *             //'dynamicLangClass' => true, //Set to true if you don't want to create a 'PostLang.php' in your models folder
 *         ),
 *     );
 * }
 * </pre>
 * 
 * In order to retrieve translated models by default, add this function in the model class:
 * <pre>
 * public function defaultScope()
 * {
 *     return $this->ml->localizedCriteria();
 * }
 * </pre>
 * 
 * You also can modify the loadModel function of your controller as guillemc suggested in a previous post :
 * <pre>
 * public function loadModel($id, $ml=false) {
 *     if ($ml) {
 *         $model = Post::model()->multilang()->findByPk((int) $id);
 *     } else {
 *         $model = Post::model()->findByPk((int) $id);
 *     }
 *     if ($model === null)
 *         throw new CHttpException(404, 'The requested post does not exist.');
 *     return $model;
 * }
 * </pre>
 * 
 * and use it like this in the update action :
 * <pre>
 * public function actionUpdate($id) {
 *     $model = $this->loadModel($id, true);
 *     ...
 * }
 * </pre>
 * 
 * Here is a very simple example for the form view : 
 * 
 * <?php foreach (Yii::app()->params['translatedLanguages'] as $l => $lang) :
 *     if($l === Yii::app()->params['defaultLanguage']) $suffix = '';
 *     else $suffix = '_'.$l;
 *     ?>
 * <fieldset>
 *     <legend><?php echo $lang; ?></legend>
 *     <div class="row">
 *     <?php echo $form->labelEx($model,'slug'); ?>
 *     <?php echo $form->textField($model,'slug'.$suffix,array('size'=>60,'maxlength'=>255)); ?>
 *     <?php echo $form->error($model,'slug'.$suffix); ?>
 *     </div>
 * 
 *     <div class="row">
 *     <?php echo $form->labelEx($model,'title'); ?>
 *     <?php echo $form->textField($model,'title'.$suffix,array('size'=>60,'maxlength'=>255)); ?>
 *     <?php echo $form->error($model,'title'.$suffix); ?>
 *     </div>
 * </fieldset>
 * 
 * <?php endforeach; ?>
 * 
 * 
 * To enable search on translated fields, you can modify the search() function in the model like this :
 * Example: $model = Post::model()->localized('fr')->findByPk((int) $id);
 * 
 * public function search()
 * {
 *     $criteria=new CDbCriteria;
 *     
 *     //...
 *     //here your criteria definition
 *     //...
 * 
 *     return new CActiveDataProvider($this, array(
 *         'criteria'=>$this->ml->modifySearchCriteria($criteria),
 *         //instead of
 *         //'criteria'=>$criteria,
 *     ));
 * }
 * 
 * Warning: the modification of the search criteria is based on a simple str_replace so it may not work properly under certain circumstances.
 * 
 * It's also possible to retrieve languages translation of two or more related models in one query.
 * Example for a Page with a "articles" HAS_MANY relation : 
 * $model = Page::model()->multilang()->with('articles', 'articles.multilangArticle')->findByPk((int) $id);
 * echo $model->articles[0]->content_en;
 * 
 * With this method it's possible to make multi model forms like it's explained here @link http://www.yiiframework.com/wiki/19/how-to-use-a-single-form-to-collect-data-for-two-or-more-models/
 * 
 * 28/03/2012:
 * It's now possible to modify language when retrieving data with the localized relation.
 * Example: $model = Post::model()->localized('en')->findByPk((int) $id);
 * 
 * 
 */

namespace app\components;

use yii\base\Behavior;

class MultilingualBehavior extends Behavior
{
    /**
     * @var string the name of translation model class. Default to '[base model class name]Lang'.
     * Example: for a base model class named 'Post', the translation model class name is 'PostLang'. 
     */
    public $langClassName;
    
    /**
     * @var string the name of the translation table. Default to '[base model table name]Lang'.
     * Example: for a base model table named 'post', the translation model table name is 'postLang'. 
     */
    public $langTableName;
    
    /**
     * @var string the name of the foreign key field of the translation table related to base model table. Default to '[base model table name]_id'.
     * Example: for a base model table named post', the translation model table name is 'post_id'. 
     */
	public $langForeignKey = 'owner_id';
    
    /**
     * @var string the name of the lang field of the translation table. Default to 'lang_id'.
     */
	public $langField = 'lang_id';
    
    /**
     * @var array the attributes of the base model to be translated
     * Example: array('title', 'content')
     */
	public $localizedAttributes;
    
    /**
     * @var string the prefix of the localized attributes in the lang table. Here to avoid collisions in queries.
     * In the translation table, the columns corresponding to the localized attributes have to be name like this: 'l_[name of the attribute]'
     * and the id column (primary key) like this : 'l_id'
     * Default to 'l_'.
     */
        public $localizedPrefix = 'l_';
    
    /**
     * @var array the languages to use.
     * It can be a simple array: array('fr', 'en')
     * or an associative array: array('fr' => 'Français', 'en' => 'English')
     *  For associatives assray, only the keys will be used.
     */
	public $languages;
    
    /**
     * @var string the default language.
     * Example: 'en'.
     */
	public $defaultLanguage;
    
    /**
     * @var string the scenario corresponding to the creation of the model. Default to 'insert'.
     */
	//public $createScenario = 'insert';
    
    /**
     * @var string the name the relation that is used to get translated attributes values for the current language.
     * Default to 'multilang[base model class name]'.
     * Example: for a base model class named 'Post', the relation name is 'multilangPost'. 
     */
	//public $localizedRelation;

	/**
     * @var string the name the relation that is used to all translations for all translated attributes.
     * Used to have access to all translations at once, for example when you want to display form to update a model
     * Every translation for an attribute can be accessed like this: $model->[name of the attribute]_[language code] (example: $model->title_en, $model->title_fr).
     * Default to 'i18n[base model class name]'.
     * Example: for a base model table named post', the relation name is 'i18nPost'. 
     */
    /*public $multilangRelation;

    
    public $localizedRelationStrict;
    public $multilangRelationStrict;*/
    /**
     * @var boolean wether to force overwrite of the default language value with translated value even if it is empty.
     * Used only for {@link localizedRelation}.
     * Default to false. 
     */
    public $forceOverwrite=false;

    /**
     * @var boolean wether to force deletion of the associated translations when a base model is deleted.
     * Not needed if using foreign key with 'on delete cascade'.
     * Default to true. 
     */
    public $forceDelete = true;
    
    public $strict = false;
    //public $strictField = false;
    
	/**
     * @var boolean wether to dynamically create translation model class.
     * If true, the translation model class will be generated on runtime with the use of the eval() function so no additionnal php file is needed.
     * See {@link createLangClass()}
     * Default to true. 
     */
    public $dynamicLangClass = true; 

    private $_langAttributes = [];
    
    private $_notDefaultLanguage = false;
    
    public function getI18N($lang = false)
    {
        if(!$lang)
            $lang = \Yii::$app->language;
        $owner = $this->owner;
        $pk = $this->owner->primaryKey();
        
        return $owner->hasMany($this->langClassName, [$this->langForeignKey => $pk[0]])
                ->where('lang_id="'.$lang.'" AND class = :class', 
                [':class'=>$owner::className()]);
    }
    /*public function getI18NStrict($lang = false)
    {
        if(!$lang)
            $lang = \Yii::$app->language;
        $owner = $this->owner;
        $owner->hasMany($this->langClassName, [$this->langForeignKey => $this->owner->primaryKey])
                ->where($this->localizedRelationStrict . '.lang_id="'.$lang.'" AND '.$this->langClassName.'.class = :class', 
                [':class'=>$owner::className()]);
    }*/
    public function getMultilang()
    {
        $owner = $this->owner;
        $pk = $this->owner->primaryKey();
        return $owner->hasMany($this->langClassName, [$this->langForeignKey => $pk[0]])
                ->where('class = :class', 
                [':class'=>$owner->className()]);
    }
    /*public function getMultilangStrict()
    {
        $owner = $this->owner;
        $owner->hasMany($this->langClassName, [$this->langForeignKey => $this->owner->primaryKey])
                ->where($this->langClassName.'.class = :class', 
                [':class'=>$owner::className()]);
    }*/
    
    public function events()
    {
        return [
            \yii\db\ActiveRecord::EVENT_AFTER_UPDATE => 'afterSave',
            \yii\db\ActiveRecord::EVENT_AFTER_INSERT => 'afterSave',
            \yii\db\ActiveRecord::EVENT_AFTER_FIND => 'afterFind',
            \yii\db\ActiveRecord::EVENT_INIT => 'modelInit',
            \yii\db\ActiveRecord::EVENT_BEFORE_VALIDATE => 'beforeValidate',
        ];
    }
            
    /**
     * Attach the behavior to the model.
     * @param model $owner 
     */
    public function attach($owner) {
        
	parent::attach($owner);
        $owner_classname = $owner->className();
        $table_name_chunks = explode('.', $owner->tableName());
        $simple_table_name = str_replace(array('{{', '}}', '%'), '', array_pop($table_name_chunks));
        
        if (!isset($this->langClassName)) {
                $this->langClassName = $owner->formName() . 'Lang';
        }
        if (!isset($this->langTableName)) {
                $this->langTableName = 'translation';
        }
        if(!$this->languages)
            $this->languages = isset(\Yii::$app->params['translatedLanguages'])?\Yii::$app->params['translatedLanguages']:['ru', 'en'];
        
        if(!$this->defaultLanguage)
            $this->defaultLanguage = isset(\Yii::$app->params['defaultLanguage'])?\Yii::$app->params['defaultLanguage']:'ru';
        
        /*if (!isset($this->localizedRelation)) {
                $this->localizedRelation = 'i18n' . str_replace('\\', '', $owner_classname);
        }
        $this->localizedRelationStrict = $this->localizedRelation.'Strict';
        if (!isset($this->multilangRelation)) {
                $this->multilangRelation = 'multilang' . str_replace('\\', '', $owner_classname);
        }
        $this->multilangRelationStrict = $this->multilangRelation.'Strict';    
	if (!isset($this->langForeignKey)) {
            $this->langForeignKey = $simple_table_name . '_id';
        }*/
        if ($this->dynamicLangClass) {
                $this->createLangClass();
        }
                
        if (array_values($this->languages) !== $this->languages) { //associative array
                $this->languages = array_keys($this->languages);
        }
        $validators = [];
        foreach ($this->languages as $l) 
        {
            $iterator = $this->owner->getValidators()->getIterator();
            while($iterator->valid()) {
                
                $validator = $iterator->current();
                foreach($this->localizedAttributes as $attr) 
                {
                    if(in_array($attr, $validator->attributes)) 
                    {
                        if($validator instanceof \yii\validators\RequiredValidator)
                        {
                            $new_validator = \yii\validators\Validator::createValidator( 'required', $this->owner, $attr. '_' . $l, ['on' => ['insert', 'update']]);
                            $validators[] = $new_validator;
                            //$this->owner->getValidators()->append($new_validator);
                        } //else if($this->forceOverwrite && $l != $this->defaultLanguage) {
                        //var_dump($attr. '_' . $l);
                            $new_validator = \yii\validators\Validator::createValidator( 'safe', $this->owner, $attr. '_' . $l, []);
                            $validators[] = $new_validator;
                            //$this->owner->getValidators()->append($validator);
                        //}
                    }
                }

                $iterator->next();
            }
            
        }
        foreach($validators  as $validator)
            $this->owner->getValidators()->append($validator);
        /*$validator = Validator::createValidator( 'safe', $this->_model, $this->_field.'_attr');
        $this->_model->getValidators()->append($validator);
        $rules = $owner->rules();
        $validators = $owner->getValidatorList(); 
        foreach ($this->languages as $l) {
			foreach($this->localizedAttributes as $attr) {
				foreach($rules as $rule) {
                                        $rule_attributes = array_map('trim', explode(',', $rule[0]));
                                        if(in_array($attr, $rule_attributes)) {
						if ($rule[1] !== 'required' || ($this->forceOverwrite && $l != $this->defaultLanguage)) {
							$validators->add(CValidator::createValidator($rule[1], $owner, $attr . '_' . $l, array_slice($rule, 2)));
						}
						else if($rule[1] === 'required') {
							//We add a safe rule in case the attribute has only a 'required' validation rule assigned
							//and forceOverWrite == false
							$validators->add(CValidator::createValidator('safe', $owner, $attr . '_' . $l, array_slice($rule, 2)));
						}
					}
				}
			}
        }*/
    }
    
    /**
     * Dynamically create the translation model class with the use of the eval() function so no additionnal php file is needed. 
     * The translation model class created is a basic active record model corresponding to the translations table.
     * It include a BELONG_TO relation to the base model which allow advanced usage of the translation model like conditional find on translations to retrieve base model.
     */
    public function createLangClass() {
        if(!class_exists($this->langClassName, false)) {
            $owner = $this->owner;
            $owner_classname = $owner->className();
            
            eval("class {$this->langClassName} extends \yii\db\ActiveRecord
            {
                public static function tableName()
                {
                    return '{{%{$this->langTableName}}}';
                }
                    
                public function getOwner()
                {
                    return \$this->hasOne($owner_classname, ['{$this->langForeignKey}' =>'id']);
                    //return array('$owner_classname' => array(self::BELONGS_TO, '$owner_classname', '{$this->langForeignKey}'));
                }
            }");
        }
    }
	
    /**
     * 
     * @return model 
     */
    /**
     * Named scope to use {@link localizedRelation}
     * @param string $lang the lang the retrieved models will be translated (default to current application language)
     * @return model 
     */
	public function localized($lang = null) {
            $owner = $this->getOwner();
            if($lang != null && $lang != \Yii::$app->language && in_array($lang, $this->languages)) {
                $this->createLocalizedRelation($owner, $lang);
                $this->_notDefaultLanguage = true;
            }
            /*$owner->getDbCriteria()->mergeWith(
                    $this->localizedCriteria()
            );
            */  
            return $owner;
	}
    
    /**
     * Named scope to use {@link multilangRelation}
     * @return model 
     */
    /*public function multilang() {
        $owner = $this->getOwner();
		$owner->getDbCriteria()->mergeWith(
			$this->multilangCriteria()
		);
		return $owner;
	}
    */
    /**
     * Array of criteria to use {@link localizedRelation}
     * @return array 
     */
    public function localizedCriteria() {
        
        
        if($this->strict && Yii::app()->language != $this->defaultLanguage)
        {
            
            return array(
                'with'=>array(
                    $this->localizedRelationStrict => array(
                        'joinType'=>'INNER JOIN',
                        'condition'=>($this->strictField)?'('.$this->localizedRelationStrict.'.l_value<>"" AND '.$this->localizedRelationStrict.'.l_field="'.$this->strictField.'")':'('.$this->localizedRelationStrict.'.l_value<>"")',
                        'together'=>true,
                    ),
                    $this->localizedRelation => array(
                    ),
                ),
                //'localized'=>array(),
            );
        } else {
            
            return array(
                'with'=>array(
                    $this->localizedRelation => array(
                    ),
                ),
                //'localized'=>array(),
            );
        }
    }
    
    /**
     * Array of criteria to use {@link multilangRelation}
     * @return array 
     */
    public function multilangCriteria() {
        if($this->strict && Yii::app()->language != $this->defaultLanguage)
        {
            return array(
                'with'=>array(
                    $this->multilangRelationStrict => array(
                        'joinType'=>'INNER JOIN',
                        'condition'=>($this->strictField)?'('.$this->multilangRelationStrict.'.l_value<>"" AND '.$this->multilangRelationStrict.'.l_field="'.$this->strictField.'")':'('.$this->multilangRelationStrict.'.l_value<>"")',
                        //'condition'=>($this->strictField)?'('.$this->multilangRelation.'.l_value<>"" AND '.$this->multilangRelation.'.l_field="'.$this->strictField.'")':'('.$this->multilangRelation.'.l_value<>"")',
                        'together'=>true,

                    ),
                    $this->multilangRelation => array(
                    ),
                ),
            );
        
        } else {
            return array(
                'with'=>array(
                    $this->multilangRelation => array(
                    ),
                ),
                //'localized'=>array(),
            );
        }
    }
    
    /**
     * Wether the attribute exists
     * @param string $name the name of the attribute
     * @return boolean 
     */
	public function hasLangAttribute($name) {
            return array_key_exists($name, $this->_langAttributes);
	}

    /**
     * @param string $name the name of the attribute
     * @return string the attribute value 
     */
	public function getLangAttribute($name) {
		return $this->hasLangAttribute($name) ? $this->_langAttributes[$name] : null;
	}

    /**
     * @param string $name the name of the attribute
     * @param string $value the value of the attribute
     */
	public function setLangAttribute($name, $value) {
		$this->_langAttributes[$name] = $value;
	}

    /**
     * @param CEvent $event 
     */
	/*public function afterConstruct($event) {
		$owner = $this->getOwner();
                //if ($owner->scenario==$this->createScenario) {
                	$langclass = new $this->langClassName;
			foreach ($this->languages as $lang) {
				foreach ($this->localizedAttributes as $field) {
                                        //$ownerfield = $this->localizedPrefix . $field;
                                        $record = $langclass->find("`class`=:class AND {$this->langForeignKey}=:owner_id AND `l_field`=:field AND `{$this->langField}`=:lang", array('class'=>get_class($owner), 'owner_id'=>$owner->getPrimaryKey(), 'field'=>$field, 'lang'=>$lang));
                                        $ownerfield = 'l_value';
                                        $this->setLangAttribute($field. '_' . $lang, ($record)?$record->$ownerfield:'');
                }
            }
		//}
	}*/
    /**
     * Modify passed criteria by replacing conditions on base attributes with conditions on translations.
     * Allow to make search on model translated values.
     * @param CDbCriteria $event 
     */
    public function modifySearchCriteria(CDbCriteria $criteria) {
        $owner = $this->getOwner();
        $criteria_array = $criteria->toArray();
        
        foreach($this->localizedAttributes as $attribute) {
           //if(!empty($owner->$attribute)) {
                //$criteria_array['condition'] = str_replace($attribute . ' ', $this->localizedPrefix . $attribute . ' ', $criteria_array['condition']);
                $criteria_array['condition'] = str_replace(get_class($owner).'.'.$attribute . ' ', ' `'.$this->localizedRelation.'`.l_field="'.$attribute.'" AND `'.$this->localizedRelation.'`.l_value ', $criteria_array['condition']);
            //}
        }
        $criteria_array['together'] = true;
        $criteria = new CDbCriteria($criteria_array);
        
        return $criteria;
    }
    
    public function modelInit()
    {
        foreach ($this->languages as $lang)
        {
            foreach ($this->localizedAttributes as $field)
            {
                $this->setLangAttribute($field. '_' . $lang, null);
            }
        }
    }
    /*public function beforeFind($event) {
        $owner = $this->getOwner();
       $criteria = $owner->ml->modifySearchCriteria($owner->getDbCriteria());
       if(get_class($owner) == 'NewsArticle')
        {
        //var_dump($criteria);
        var_dump($criteria);
        }
    }*/
    
    /**
     * @param CEvent $event 
     */
    public function afterFind($event) 
    {
        $clang = explode('-', \yii::$app->language);
        $clang = $clang[0];
        
        if(in_array($clang, $this->languages))
            $activeLang = $clang;
        else
            $activeLang = $this->defaultLanguage;
        
        $update_fields = [];
        foreach($this->owner->getBehaviors() as $behavior)
        {
            if($behavior instanceof \yii\behaviors\TimestampBehavior)
            {
                foreach($behavior->attributes[\yii\db\BaseActiveRecord::EVENT_BEFORE_UPDATE] as $attr)
                    if($this->owner->hasAttribute($attr))
                        $update_fields[] = $attr;

            }
        }
        foreach ($this->languages as $lang)
        {
            $rels = [];
            
            if($this->owner->getRelation('I18N', false))
            {
                if(count($update_fields))
                {
                    $cache = \yii::$app->cache;
                    $key = self::className().'-afterFind-'.$lang.'-'.$this->owner->className().'-'.serialize($this->owner->primaryKey);
                    
                    $rels = $cache->get($key);
                    //$rels = false;
                    if ($rels === false) 
                    {
                        $rels = $this->owner->getI18N($lang)->all();
                        $expression = [];
                        foreach($update_fields as $update_field)
                            $expression[] = $this->owner->db->schema->quoteColumnName($update_field);
                        
                        //$dependency = new \yii\caching\ExpressionDependency(['expression' => '"'.implode('.', $expression).'"']);
                        $sql = 'SELECT '.implode(', ', $expression).' FROM '.$this->owner->db->schema->quoteTableName($this->owner->tableName()).' WHERE 1 ';
                        foreach($this->owner->getPrimaryKey(true) as $pkkey => $pkvalue)
                        {
                            $sql .= ' AND '.$this->owner->db->schema->quoteColumnName($pkkey).' = '.$this->owner->db->schema->quoteValue($pkvalue).' ';
                        }

                        $dependency = new \yii\caching\DbDependency(['sql' => $sql]);
                        $cache->set($key, $rels, 3600, $dependency);
                        
                    } else {
                        
                    }
                } else {
                    $rels = $this->owner->getI18N($lang)->all();
                }
                foreach ($this->localizedAttributes as $field)
                {
                    $this->setLangAttribute($field. '_' . $lang, isset($this->owner->$field)?$this->owner->$field:'');

                    foreach($rels as $rel)
                    {
                        if($rel->l_field == $field)
                        {
                            $this->setLangAttribute($field. '_' . $lang, $rel->l_value);

                            if ($lang == $activeLang && (isset($this->owner->$field) && (!empty($rel->l_value) || $this->forceOverwrite))) 
                                $this->owner->$field = $rel->l_value;
                        }
                    }
                }
            }
        }
            //$owner = $this->getOwner();
            /*
    if ($owner->hasRelated($this->multilangRelation)) {
                    $related = $owner->getRelated($this->multilangRelation);
                    foreach ($this->languages as $lang)
                            foreach ($this->localizedAttributes as $field)
                                    $this->setLangAttribute($field. '_' . $lang, isset($related[$lang][$this->localizedPrefix . $field]) ? $related[$lang][$this->localizedPrefix . $field] : null);
            } else if ($owner->hasRelated($this->localizedRelation)) {
                    $related = $owner->getRelated($this->localizedRelation);
                    //if ($row = current($related)) {
                            foreach ($this->localizedAttributes as $field)
                            {
                                foreach ($related as $rel)
                                {
                                    if($rel->l_field == $field)
                                    {
                                        if (isset($owner->$field) && (!empty($rel->l_value) || $this->forceOverwrite)) $owner->$field = $rel->l_value;
                                        break;
                                    }
                                }
                                    //if (isset($owner->$field) && (!empty($row[$this->localizedPrefix . $field]) || $this->forceOverwrite)) $owner->$field = $row[$this->localizedPrefix . $field];
                            }

                    //}*/
        /*if($this->_notDefaultLanguage) {
            $this->createLocalizedRelation($owner, Yii::app()->language);
            $this->_notDefaultLanguage = false;
        }
            }*/

    }
    
    public function beforeValidate($event)
    {
        if($this->owner->getScenario() == 'insert' || $this->owner->getScenario() == 'update')
        {
            $lang = $this->defaultLanguage;
            
            foreach ($this->localizedAttributes as $field) 
            {
                $value = $this->getLangAttribute($field . '_'.$lang);
                
                if(isset($value))
                {
                    $this->owner->$field = $value;
                } else {
                }

                
            }
        }
        
        return true;
    }

    /**
     * @param CModelEvent $event 
     */
	public function afterSave($event) {
            
		$main_owner = $this->owner;
		$ownerPk = $main_owner->getPrimaryKey();
		$rs = array();
                $langClassName = $this->langClassName;
                
                foreach ($this->languages as $lang) {
            	
                    foreach ($this->localizedAttributes as $field) {
                        $translation = $langClassName::find()->where($this->langField.'=:lang_id AND class=:class AND '.$this->langForeignKey.'=:owner AND l_field=:field',
                                [':lang_id' => $lang, ':class' => $main_owner->className(), ':owner'=>$ownerPk, ':field'=>$field])->one();
                        $value = $this->getLangAttribute($field . '_'.$lang);
                        
                        if($translation)
                        {
                            $translation->l_value = $value;
                        } else {
                            $translation = new $langClassName;
                            $translation->lang_id = $lang;
                            $translation->class = $main_owner->className();
                            $translation->owner_id = $ownerPk;
                            $translation->l_field = $field;
                            $translation->l_value = $value;
                        }
                        
                        $translation->save();
                    }
			
		}
	}
        
        

        /**
     * @param CEvent $event 
     */
	public function afterDelete($event) {
            if ($this->forceDelete) 
            {
                $main_owner = $this->owner;
                $ownerPk = $main_owner->getPrimaryKey();
                $langClassName = $this->langClassName;
                $langClassName->deleteAll([
                    $this->langForeignKey => ':owner',
                    $this->class => ':class',
                ],[':owner' => $ownerPk, 'class' => $main_owner->className()]);
            }
	}
	
	public function __get($name) {
            if ($this->hasLangAttribute($name))
                return $this->getLangAttribute($name);
            else {
                return $this->owner->__get($name);
            }                                                    
	}

	public function __set($name, $value) {
            if ($this->hasLangAttribute($name))
                return $this->setLangAttribute($name, $value);
            else {
                return $this->owner->__set($name, $value);
            }                                         
	}
	
	public function __isset($name){
            if (!$this->owner->__isset($name))
                return ($this->hasLangAttribute($name));
            else {
                return true;
            } 
	}
        
        public function canGetProperty($name, $checkVars = true)
	{
            return parent::canGetProperty($name, $checkVars) or $this->hasLangAttribute($name);
	}
	
	public function canSetProperty($name, $checkVars = true)
	{
		return parent::canSetProperty($name, $checkVars) or $this->hasLangAttribute($name);
	}
	
	public function hasProperty($name, $checkVars = true)
	{
		return parent::hasProperty($name, $checkVars) or $this->hasLangAttribute($name);
	}
        
}
