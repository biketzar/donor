<?php
namespace app\components;

use yii\web\UrlRuleInterface;
use yii\base\Object;

class PageUrlRule extends Object implements UrlRuleInterface
{

    //static $pages = [];
    static $pageObjects = [];
    
    public function createUrl($manager, $route, $params)
    {
        /*if(!count(self::$pages))
        {
            $pageObjects = \app\models\Page::find()->published()->asArray()->all();
            foreach($pageObjects as $p)
            {
                self::$pages[$p['id']] = $p;
            }
        }
        */
        
        if(!count(self::$pageObjects))
        {
            $pageObjects = \app\models\Page::find()->published()->all();
            foreach($pageObjects as $p)
            {
                self::$pageObjects[$p->id] = $p;
            }
        }
        
        $parts = explode('/', trim($route, '/'));
        //$modules = array_keys(\yii::$app->getModules());
        
        if($parts[0] == 'private' || $parts[0] == 'privateuser')
            return false;
        
        if (!$manager->enablePrettyUrl) 
            return false;
        /*if ($route === 'car/index') {
            if (isset($params['manufacturer'], $params['model'])) {
                return $params['manufacturer'] . '/' . $params['model'];
            } elseif (isset($params['manufacturer'])) {
                return $params['manufacturer'];
            }
        }*/
        
        static $urls = [];
        
        $cacheKey = serialize([$route, $params]);
        
        if(isset($urls[$cacheKey]))
            return $urls[$cacheKey];
        
        if($route == '/site/page' && isset($params['id']))
        {
            $params['page_id'] = $params['id'];
            unset($params['id']);
        }
        $pageObject = null;
        
        if(!empty($params['page_id']))
        {
            $page_id = $params['page_id'];
            unset($params['page_id']);
            if(isset(self::$pageObjects[$page_id]))
                $pageObject = self::$pageObjects[$page_id];
            else {
                $pageObject = \app\models\Page::find()->where('`id`=:id', [':id'=> $page_id])->published()->limit(1)->one();
                self::$pageObjects[$page_id] = $pageObject;
            }
            
            if($pageObject)
            {
                if (($result = $this->createUrlPage($pageObject, $manager, $route, $params)) !== false) {
                    $urls[$cacheKey] = $result;
                    return $result;
                }
            } 
        } 
        /*if(!$pageObject && \yii::$app->request->get('page_id')) 
        {
            $page_id = \yii::$app->request->get('page_id');
            $pageObject = \app\models\Page::find()->where('`id`=:id', [':id'=> $page_id])->published()->limit(1)->one();
            if($pageObject)
            {
                if (($result = $this->createUrlPage($pageObject, $manager, $route, $params)) !== false) {
                    $urls[$cacheKey] = $result;
                    return $result;
                }
            } 
            
        }
        */
        if(!$route)
            return '';
        
        $pageObjects = \app\models\Page::find()->where('`handler`=:handler', [':handler'=> $route])->published()->all();
        
        if(isset($params['page_id']))  unset($params['page_id']);
        foreach($pageObjects as $pageObject)
        {
            try {
                if(count($this->diffArray($pageObject->handler_data, $params)) == 0)
                {
                    if (($result = $this->createUrlPage($pageObject, $manager, $route, $params)) !== false) {
                        $urls[$cacheKey] = $result;
                        return $result;
                    }
                }
            } catch(Exception $e) {
                
            }
        }
        
        return false;
        
        //return false;  // this rule does not apply
    }
    
    private function diffArray($a, $b)
    {
        $result = [];
        foreach($a as $k=>$v)
        {
            if(!isset($b[$k]))
            {
                $result[$k] = $v;
                continue;
            }
            if(is_array($v) && is_array($b[$k]))
            {
                $c = $this->diffArray($a[$k], $b[$k]);
                if(count($c))
                {
                    $result[$k] = $v;
                }
                continue;
            }
            if(gettype($a) !== gettype($b))
            {
                $result[$k] = $v;
                continue;
            }
            if($v != $b[$k])
                $result[$k] = $v;
        }
        return $result;
    }
    
    protected function createUrlPage($pageObject, $manager, $route, $params)
    {
        static $urls = [];
        static $prefixes = [];
        
        $cacheKey = serialize([$pageObject->id, $route, $params]);
        
        if(isset($urls[$cacheKey]))
            return $urls[$cacheKey];
        
        if(isset($params['page_id']))
            unset($params['page_id']);
        $rules = \app\models\Page::getHandlerRules($pageObject->handler);
        
        $prefix = '';
        /*if(isset($prefixes[$pageObject['id']]))
            $prefix = $prefixes[$pageObject['id']];
        else {
            $parent = $pageObject;
            while($parent = $parent->parent)
            {
                $prefix = $parent->alias.(($prefix)?'/'.$prefix:'');
            }
            $prefixes[$pageObject->id] = $prefix;
        }*/
        $parent = $pageObject->parent_id;
        while(isset(self::$pageObjects[$parent]))
        {
            $prefix = self::$pageObjects[$parent]->alias.(($prefix)?'/'.$prefix:'');
            $parent = self::$pageObjects[$parent]->parent_id;
        }
        
        /*if(!count($rules))
        {
            $prefix = (($prefix)?$prefix.'/':'').$pageObject->alias;
            //$url = $pageObject->alias;
            $url = '';
            if (!empty($params) && ($query = http_build_query($params)) !== '') {
                $url .= '?' . $query;
            }
            $urls[$cacheKey] = $prefix.(($url)?'/'.$url:'');
            return $urls[$cacheKey];
        }
        */
        
        foreach ($rules as $key => $rule) {
            if (is_string($rule)) {
                $rule = ['route' => $rule];
                if (preg_match("/^((?:($verbs),)*($verbs))\\s+(.*)$/", $key, $matches)) {
                    $rule['verb'] = explode(',', $matches[1]);
                    // rules that do not apply for GET requests should not be use to create urls
                    if (!in_array('GET', $rule['verb'])) {
                        $rule['mode'] = UrlRule::PARSING_ONLY;
                    }
                    $key = $matches[4];
                }
                $rule['pattern'] = $key;
            }
            if (is_array($rule)) {
                $rule = \Yii::createObject(array_merge($manager->ruleConfig, $rule));
            }
            if (!$rule instanceof UrlRuleInterface) {
                throw new InvalidConfigException('URL rule class must implement UrlRuleInterface.');
            }
            $params['pageObject'] = $pageObject;
            if (($result = $rule->createUrl($manager, $route, $params)) !== false) {

                $urls[$cacheKey] = (($prefix)?$prefix.'/':'').(($result)?$result:'');
                return $urls[$cacheKey];
            }
        }
        /*if(count($rules))
        {*/
        $prefix = (($prefix)?$prefix.'/':'').$pageObject->alias;
        //$url = $pageObject->alias;
        $url = '';
        if(isset($params['pageObject']))
            unset($params['pageObject']);
        if (!empty($params) && ($query = http_build_query($params)) !== '') {
            $url .= '?' . $query;
        }
        $urls[$cacheKey] = $prefix.(($url)?'/'.$url:'');
        return $urls[$cacheKey];
        /*}
        
        return false;*/
    }

    public function parseRequest($manager, $request)
    {
        $pathInfo = $request->getPathInfo();
        $newpathInfo = $pathInfo;
        $parts = $pathInfo;
        $parts = explode('/', $parts);
        
        if($pathInfo == 'sitemap.xml')
        {
            return ['site/sitemap-xml', []];
        }
        $pageObject = null;
        
        foreach($parts as $k => $part)
        {
            $pageObject2 = \app\models\Page::find()->where('`alias`=:alias', [':alias'=> $part]);
			
            if($pageObject)
                $pageObject2->andWhere('parent_id=:parent_id', [':parent_id' => $pageObject->id]);
            else
                $pageObject2->andWhere('parent_id=0 OR parent_id IS NULL');
            
            $pageObject2 = $pageObject2->published()->limit(1)->one();
			 
            if($pageObject2)
            {
                $pageObject = $pageObject2;
                $newpathInfo = array_slice($parts, $k+1, count($parts) - 1);
                $newpathInfo = implode('/', $newpathInfo);
                
            } else {
                break;
            }
        }
        
        if(!$pageObject)
        {
            return [$pathInfo, []];
        }
        $getParams = \yii::$app->request->getQueryParams();
        $getParams['page_id'] = $pageObject->id;
        
        $rules = \app\models\Page::getHandlerRules($pageObject->handler);
        
        foreach($pageObject->handler_data as $key => $value)
        {
            $getParams[$key] = $value;
        }
        \yii::$app->request->setQueryParams($getParams);
        
        if(count($rules))
        {
            /*$newpathInfo = $pathInfo;
            $newpathInfo = explode('/', $newpathInfo);
            $newpathInfo = array_slice($newpathInfo, 1);*/
            $request->setPathInfo($newpathInfo);
            $verbs = 'GET|HEAD|POST|PUT|PATCH|DELETE|OPTIONS';
            foreach ($rules as $key => $rule) {
                if (is_string($rule)) {
                    $rule = ['route' => $rule];
                    if (preg_match("/^((?:($verbs),)*($verbs))\\s+(.*)$/", $key, $matches)) {
                        $rule['verb'] = explode(',', $matches[1]);
                        // rules that do not apply for GET requests should not be use to create urls
                        if (!in_array('GET', $rule['verb'])) {
                            $rule['mode'] = UrlRule::PARSING_ONLY;
                        }
                        $key = $matches[4];
                    }
                    $rule['pattern'] = $key;
                }
                if (is_array($rule)) {
                    $rule = \Yii::createObject(array_merge($manager->ruleConfig, $rule));
                }
                if (!$rule instanceof UrlRuleInterface) {
                    throw new InvalidConfigException('URL rule class must implement UrlRuleInterface.');
                }
                
                if (($result = $rule->parseRequest($manager, $request)) !== false) {
                    
                    $request->setPathInfo($pathInfo);
                    return $result;
                }
            }
            $request->setPathInfo($pathInfo);
            return false;
        } else {
            if($newpathInfo == '')
                return [$pageObject->handler, ['id' => $pageObject->id]];
            else {
                return false; //404 error for Page handler
            }
        }
        
        return false;  // this rule does not apply
    }
}
?>