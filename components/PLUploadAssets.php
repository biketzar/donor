<?php
namespace app\components;


use yii\web\AssetBundle;

class PLUploadAssets extends AssetBundle{
    public $basePath = '@webroot2/js/pluploader/';
    public $baseUrl = '@web2/js/pluploader/';
    public $css = [
        //'http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/themes/smoothness/jquery-ui.min.css',
        'js/jquery.ui.plupload/css/jquery.ui.plupload.css',
   ];
    public $js = [
        //'http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js',
        'js/plupload.full.min.js',
        'js/jquery.ui.plupload/jquery.ui.plupload.min.js',
        'js/i18n/ru.js',
    ];
    public $depends = array(
            'yii\web\JqueryAsset',
            'yii\jui\JuiAsset'
    );

    /*public function init()
    {
            $this->sourcePath = '@web/js/pluploader/';
            parent::init();
    }*/
    public function getFlashUrl()
    {
        return $this->baseUrl.'js/Moxie.swf';
    }
    public function getSilverLightUrl()
    {
        return $this->baseUrl.'js/Moxie.xap';
    }
} 