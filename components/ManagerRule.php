<?php
namespace app\components;

use app\components\ActionRuleRule;

/**
 * Checks if authorID matches user passed via params
 */
class ManagerRule extends ActionRule
{
    public $name = 'manager_rule';

    /**
     * @param string|integer $user the user ID.
     * @param Item $item the role or permission that this rule is associated with
     * @param array $params parameters passed to ManagerInterface::checkAccess().
     * @return boolean a value indicating whether the rule permits the role or permission it is associated with.
     */
    public function execute($user, $item, $params)
    {
        return true;
    }
    public function allows($action, $user, $request)
    {
        return true;
    }
    
}
?>
