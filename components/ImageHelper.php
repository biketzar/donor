<?php
namespace app\components;

use \yii\imagine\Image;
use Imagine\Image\Point;

class ImageHelper extends \yii\base\Object
{
    const THUMBNAIL_RESIZE = 1;
    const THUMBNAIL_RESIZE_AND_CROP = 2;
    const THUMBNAIL_FORCE_ASPECT_RATIO = 3;
    
    /**
     * Adds a watermark to an existing image.
     * @param string $source the image file path or path alias.
     * @param string $watermark the file path or path alias of the watermark image.
     * @param mixed $position the starting point. This may be an array with two elements representing `x` and `y` coordinates. This may be a text in format `[x position (left, right, center, %, px)] [y position (top, bottom, middel, %, px)`,
     * @return boolean success of operation
     */
    static function watermark($source, $watermark, $destination, $position = 'right bottom')
    {
        if(!$imgSource = Image::getImagine()->open(\Yii::getAlias($source)))
        {
            \Yii::$app->session->addFlash('error', \Yii::t('app', 'Incorrect source image'));
            return false;
        }
        if(!$imgWatermark = Image::getImagine()->open(\Yii::getAlias($watermark)))
        {
            \Yii::$app->session->addFlash('error', \Yii::t('app', 'Incorrect watermark image'));
            return false;
        }
        $imgSourceSize = $imgSource->getSize();
        $sWidth = $imgSourceSize->getWidth();
        $sHeight = $imgSourceSize->getHeight();

        $imgWatermarkSize = $imgWatermark->getSize();
        $wWidth = $imgWatermarkSize->getWidth();
        $wHeight = $imgWatermarkSize->getHeight();
        
        $x = 0;
        $y = 0;
        if(!is_array($position))
        {
            if(!preg_match("/([\w\d]+)[\s]+([\w\d]+)/si", $position, $m))
            {
                \Yii::$app->session->addFlash('error', \Yii::t('app', 'Incorrect position'));
                return false;
            }
            if($m[1] == 'right')
                $x = $sWidth - $wWidth;
            elseif($m[1] == 'center')
                $x = ($sWidth - $wWidth) / 2;
            elseif(strpos($m[1], '%') !== false)
            {
                $m[1] = substr('%', '', $m[1]);
                $m[1] = floatval($m[1]);
                $x = $sWidth * $m[1] / 100;
            } else 
                $x = $m[1];
            if($m[2] == 'bottom')
                $y = $sHeight - $wHeight;
            elseif($m[2] == 'middle')
                $y = ($sHeight - $wHeight) / 2;
            elseif(strpos($m[1], '%') !== false)
            {
                $m[2] = substr('%', '', $m[2]);
                $m[2] = floatval($m[2]);
                $y = $sHeight * $m[2] / 100;
            } else 
                $y = $m[2];
        } else {
            $x = floatval($position[0]);
            $y = floatval($position[1]);
        }
        if($x < 0)
            $x = 0;
        if($y < 0)
            $y = 0;
        if (!$imgSourceSize->contains($imgWatermarkSize, new Point($x, $y))) {
            if (!$imgSourceSize->contains($imgWatermarkSize, new Point(0, 0))) {
                $imgWatermark = $imgWatermark->copy();
                $ratio = min($sWidth/$wWidth, $sHeight/$wHeight);
                
                $imgWatermarkSize = $imgWatermarkSize->scale($ratio);
                $imgWatermark->resize($imgWatermarkSize);
                $x = 0;
                $y = 0;
            } else {
                $x = $sWidth - $wWidth;
                $y = $sHeight - $wHeight;
            }
        }
        
        if($img = $imgSource->paste($imgWatermark, new Point($x, $y)))
        {
            $img->save($destination);
        }
        
    }
    static function thumbnail($source, $width = -1, $height = -1, $type = self::THUMBNAIL_RESIZE, $options = [])
    {
        $source = '..'.str_replace(\Yii::getAlias('@web2'), '', $source);
        return \Yii::getAlias('@web2').'/phpthumb/phpThumb.php?src='.$source.(($width >=0)?'&w='.$width:'').(($height >=0)?'&h='.$height:'')
            .(($type == self::THUMBNAIL_RESIZE_AND_CROP)?'&zc=1':'')
            .(($type == self::THUMBNAIL_FORCE_ASPECT_RATIO)?'&far=1':'')
            .'&q=100'
            ;
    }
}
?>
