<?php
namespace app\components;

class MultilangHelper extends \yii\base\Object
{
    public static function enabled()
    {
        
        return 
            isset(\yii::$app->params['translatedLanguages']) && count(\yii::$app->params['translatedLanguages']) > 1;
    }
 
    public static function suffixList()
    {
        $list = array();
        $enabled = self::enabled();
        $langs = isset(\yii::$app->params['translatedLanguages'])?\yii::$app->params['translatedLanguages']:[];
        $default = (isset(\yii::$app->params['defaultLanguage']))?\yii::$app->params['defaultLanguage']:\yii::$app->language;
        foreach ($langs as $lang => $name)
        {
            if ($lang === $default) {
                $suffix = '';
                $list[$suffix] = $enabled ? $name : '';
            } else {
                $suffix = '_' . $lang;
                $list[$suffix] = $name;
            }
        }
 
        return $list;
    }
 
    public static function processLangInUrl($url)
    {
        if (self::enabled())
        {
            //$url = str_replace(\yii\helpers\Url::home(), '', $url);
            
            $domains = explode('/', ltrim($url, '/'));
            
            $langs = isset(\yii::$app->params['translatedLanguages'])?\yii::$app->params['translatedLanguages']:[];
            $default = (isset(\yii::$app->params['defaultLanguage']))?\yii::$app->params['defaultLanguage']:\yii::$app->language;
            
            $isLangExists = in_array($domains[0], array_keys($langs));
            $isDefaultLang = $domains[0] == $default;
            
            //if ($isLangExists && !$isDefaultLang)
            if ($isLangExists)
            {
                $lang = array_shift($domains);
                $cookies = \yii::$app->response->cookies;
                
                // add a new cookie to the response to be sent
                $cookies->add(new \yii\web\Cookie([
                    'name' => 'language',
                    'value' => $lang,
                ]));
                /*$cookie=new CHttpCookie('language', $lang);
                Yii::app()->request->cookies['language'] = $cookie;
                Yii::app()->setLanguage($lang);*/
                \yii::$app->language = $lang;
                
            } else {
                /*if(!isset($_SERVER['HTTP_REFERER']))
                {
                    if(Yii::app()->request->cookies['language'])
                    {
                        if(Yii::app()->request->cookies['language'] != Yii::app()->params['defaultLanguage'])
                        {
                            Yii::app()->setLanguage(Yii::app()->request->cookies['language']);
                            Yii::app()->request->redirect(Yii::app()->createAbsoluteUrl(self::addLangToUrl($url)));
                            return;
                        }
                    }
                } 
                Yii::app()->setLanguage(Yii::app()->params['defaultLanguage']);
                $cookie=new CHttpCookie('language', Yii::app()->params['defaultLanguage']);
                Yii::app()->request->cookies['language'] = $cookie;*/
                
            }
 
            $url = implode('/', $domains);
        }
        
        return $url;
    }
 
    public static function addLangToUrl($url, $addLang = false)
    {
        if (self::enabled())
        {
            $baseUrl = \yii::$app->urlManager->showScriptName || !\yii::$app->urlManager->enablePrettyUrl ? \yii::$app->urlManager->getScriptUrl() : \yii::$app->urlManager->getBaseUrl();
            
            $url = str_replace($baseUrl, '', $url);
            
            $domains = explode('/', ltrim($url, '/'));
            
            $clang = explode('-', \yii::$app->language);
            $clang = $clang[0];
            
            $lang = ($addLang)?$addLang:$clang;
            
            $langs = isset(\yii::$app->params['translatedLanguages'])?\yii::$app->params['translatedLanguages']:[];
            $default = (isset(\yii::$app->params['defaultLanguage']))?\yii::$app->params['defaultLanguage']:\yii::$app->language;
            
            $isHasLang = in_array($domains[0], array_keys($langs));
            $isDefaultLang = $lang == $default;
            
            if ($isHasLang && $isDefaultLang)
                array_shift($domains);
 
            if (!$isHasLang && !$isDefaultLang)
                array_unshift($domains, $lang);
            $r = [];
            foreach($domains as $d)
            {
                if(trim($d) !== '')
                    $r[] = $d;
            }
            $domains = $r;
            $url = $baseUrl.'/'.implode('/', $domains);
        }
 
        return $url;
    }
}
?>
