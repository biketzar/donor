<?php
namespace app\components;

use yii\helpers\Url;

class Sitemap extends \yii\base\Object
{
    public static function getSiteMap($update = false)
    {
        $lang = \yii::$app->language;
        $lang = explode('-', $lang);
        $lang = $lang[0];
        $cache = \yii::$app->cache;
        $key = 'app\components\Sitemap-map-'.$lang;
        $data = $cache->get($key);
        $data = false;
        if ($data === false || $update) 
        {
            $result = self::generateSiteMap(0, $lang);
            $cache->set($key, $result, 60*60);
            
            
            
        } else {
            $result = $data;
        }
        
        return $result;
    }
    public static function getSiteMapXml($update = false)
    {
        if(!file_exists(\yii::getAlias('@runtime').'/sitemap.xml') || time() - filemtime(\yii::getAlias('@runtime').'/sitemap.xml') > 24*60*60 || $update)
        {
            $pages = self::generateSiteMap();
			$pages = array_merge([['url'=>'/', 'items'=>[], 'label'=>'']], $pages);
            $text = array();
            $text[] = '<?xml version="1.0" encoding="UTF-8"?>';
            $text[] = '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
            $text = array_merge($text, static::writeSiteXmlTree($pages));
            $text[] = '</urlset>';

            $text = implode("\n", $text);
            file_put_contents(\yii::getAlias('@runtime').'/sitemap.xml', $text);
            return $text;
        } else {
            return file_get_contents(\yii::getAlias('@runtime').'/sitemap.xml');
        }
    }
    public static function generateSiteMap($parent = 0, $lang = null)
    {
        $pages = \app\models\Page::find()->andWhere("inmap=1")->published()->orderBy('ordering');
        if($parent)
            $pages->andWhere('parent_id=:parent_id', [':parent_id'=> $parent]);
        else
            $pages->andWhere('parent_id=0 OR parent_id IS NULL');
        
        $pages = $pages->all();
        
        $results = [];

        foreach($pages as $page)
        {
            if(!empty($lang) && !in_array($lang, $page->lang))
                continue;
            $url = \app\components\SiteHelper::getPageRoute($page);
            $result = [
                'url' => $url,
                'label' => $page->name,
                'items' => [],
            ];
            
            $result['items'] = \app\models\Page::getSitemapByHandler($page->id, $page->handler, $page->handler_data);
                    
            $result['items'] = array_merge($result['items'], static::generateSiteMap($page->id, $lang));
            $results[] = $result;
        }
        return $results;
    }
    
    protected static function writeSiteXmlTree($list)
    {
        $text = array();
        if(count($list))
        {
            foreach($list as $item)
            {
                if($item['url'] != '<none>')
                {
                    $text[] = '<url>';
                     //$text[] = "\t".'<loc>'.Yii::app()->createAbsoluteUrl($item['link']).'</loc>';
                     $text[] = "\t".'<loc>'.trim(Url::to($item['url'], true), '/')."/".'</loc>';
                    //$text[] = "\t".'<lastmod>'.date('Y-m-d').'</lastmod>';
                    //$text[] = "\t".'<changefreq>monthly</changefreq>';
                    //$text[] = "\t".'<priority>0.8</priority>';
                    $text[] = '</url>';
                }
                if(isset($item['items']))
                    $text = array_merge($text, static::writeSiteXmlTree($item['items']));

            }

        }
        return $text;
    }
}
?>
