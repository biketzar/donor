<?php
namespace app\components;

use yii\helpers\Url;

class Search extends \yii\base\Object
{
    public static function getResults($form, $lang = null)
    {
        $pages = \app\models\Page::find()->where("insearch=1");
        
        $pages = $pages->published()->all();
        
        $results = [];

        foreach($pages as $page)
        {
            if(!empty($lang) && !in_array($lang, $page->lang))
                continue;
            $results = array_merge($results, 
                    \app\models\Page::getSearchResultsByHandler($form, $page->id, $page->handler, $page->handler_data));
            
        }
        return $results;
    }
}
?>
