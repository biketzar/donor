<?php

namespace app\components;

use yii\rbac\Item;
use yii\db\Query;
use yii\base\InvalidConfigException;

class DbManager extends \yii\rbac\DbManager
{
    static $_roles;
    
    static $_permissions;
    
    public $exceptPermissionOfSuperadmin = ['view_prices_user'];
    
    /*public function checkAccess($userId, $permissionName, $params = [])
    {
        $assignments = $this->getAssignments($userId);
        if(isset($assignments['superadmin']))
            return true;
        return parent::checkAccess($userId, $permissionName, $params);
        
    }
    */
    protected function checkAccessRecursive($user, $itemName, $params, $assignments)
    {
        /*$items = $this->getItems(Item::TYPE_PERMISSION);
        
        while (!isset($items[$itemName])) {
            $itemName = str_replace('/*', '', $itemName);
            $parts = explode('/', $itemName);
            if(count($parts) < 2)
                break;
            $parts = array_slice($parts, 0, count($parts)-1);
            $itemName = implode('/',$parts).'/*';
            
        }
        
        return parent::checkAccessRecursive($user, $itemName, $params, $assignments);*/
        static $items = [];
        $key = serialize([$user, $itemName, $params, $assignments]);
        if(isset($items[$key]))
            return $items[$key];
        if(strpos($itemName, '/') === 0)
        {
            while (($item = $this->getItem($itemName)) === null) {
                $parts = str_replace('/*', '', $itemName);
                $parts = explode('/', $parts);
                if(count($parts) < 2)
                    break;
                $parts = array_slice($parts, 0, count($parts)-1);

                $itemName = implode('/',$parts).'/*';
            }
        
        } else {
            while (($item = $this->getItem($itemName)) === null) {
                $parts = str_replace('-*', '', $itemName);
                $parts = explode('-', $parts);
                if(count($parts) < 2)
                    break;
                $parts = array_slice($parts, 0, count($parts)-1);

                $itemName = implode('-',$parts).'';


            }
        }
        if(!$item)
        {
            $items[$key] = (isset($assignments['superadmin']) && !in_array($itemName, $this->exceptPermissionOfSuperadmin))?true:false;
            return $items[$key];
        }
        if ($this->executeRule($user, $item, $params)) {
            //$assignments = $this->getAssignments($userId);
            
            if(isset($assignments['superadmin']) && !in_array($itemName, $this->exceptPermissionOfSuperadmin))
            {
                //return true;
                $items[$key] = true;
                return $items[$key];
            }
        }
        //$result = parent::checkAccessRecursive($user, $itemName, $params, $assignments);
        if (($item = $this->getItem($itemName)) === null) {
            $items[$key] = false;
            return $items[$key];
        }

        \Yii::trace($item instanceof Role ? "Checking role: $itemName" : "Checking permission: $itemName", __METHOD__);
        
        if (!$this->executeRule($user, $item, $params)) {
            //return false;
            $items[$key] = false;
            return $items[$key];
        }
        
        if (isset($assignments[$itemName]) || in_array($itemName, $this->defaultRoles)) {
            //return true;
            $items[$key] = true;
            return $items[$key];
        }
        if(isset($assignments['superadmin']) && in_array($itemName, $this->exceptPermissionOfSuperadmin))
        {
        } else {
            $query = new Query;
            $parents = $query->select(['parent'])
                ->from($this->itemChildTable)
                ->where(['child' => $itemName])
                ->column($this->db);
            foreach ($parents as $parent) {
                if ($this->checkAccessRecursive($user, $parent, $params, $assignments)) {
                    //return true;
                    $items[$key] = true;
                    return $items[$key];
                }
            }
        }
        $items[$key] = false;
        return $items[$key];
        
    }
    
    protected function executeRule($user, $item, $params)
    {
        static $rules = [];
        if ($item->ruleName === null) {
            return true;
        }
        if(!isset($rules[$item->ruleName]))
        {
            
             $rules[$item->ruleName] = $this->getRule($item->ruleName);
        } else {
            
        }
        $rule = $rules[$item->ruleName];
        
        if ($rule instanceof \yii\rbac\Rule) {
            return $rule->execute($user, $item, $params);
        } else {
            throw new InvalidConfigException("Rule not found: {$item->ruleName}");
        }
    }
    
    protected function getItem($name)
    {
        static $items = [];
        if (array_key_exists($name, $items)) {
            return $items[$name];
        }
        
        $row = (new Query)->from($this->itemTable)
            ->where(['name' => $name])
            ->one($this->db);

        if ($row === false) {
            $items[$name] = null;
            return null;
        }

        if (!isset($row['data']) || ($data = @unserialize($row['data'])) === false) {
            $row['data'] = null;
        }
        $items[$name] = $this->populateItem($row);
        
        return $items[$name];
    }
    
    public function getAssignments($userId)
    {
        if (empty($userId)) {
            //return [];
            return ['guest' => $this->getItem('guest')];
        }
        
        static $items = [];
        if (!empty($items[$userId])) {
            return $items[$userId];
        }
        $query = (new Query)
            ->from($this->assignmentTable)
            ->where(['user_id' => (string) $userId]);

        $assignments = [];
        foreach ($query->all($this->db) as $row) {
            $assignments[$row['item_name']] = new \yii\rbac\Assignment([
                'userId' => $row['user_id'],
                'roleName' => $row['item_name'],
                'createdAt' => $row['created_at'],
            ]);
        }
        
        $items[$userId] = $assignments;
        
        return $assignments;
    }
    
    /*public function getAccessLevels()
    {
        return $this->getItems(\app\components\AccessLevel::TYPE_ACCESS_LEVEL);
    }*/
    
    /*protected function populateItem($row)
    {
        if($row['type'] == \app\components\AccessLevel::TYPE_ACCESS_LEVEL)
        {
            $class = \app\components\AccessLevel::className();
            if (!isset($row['data']) || ($data = @unserialize($row['data'])) === false) {
                $data = null;
            }
        
            return new $class([
                'name' => $row['name'],
                'type' => $row['type'],
                'description' => $row['description'],
                'ruleName' => $row['rule_name'],
                'data' => $data,
                'createdAt' => $row['created_at'],
                'updatedAt' => $row['updated_at'],
            ]);
        }
        return parent::populateItem($row);
    }*/
    
    /*public function getRoleAccessLevels($role)
    {
        static $resultral = [];
        if(isset($resultral[$role]))
            return $resultral[$role];
        $childrenList = $this->getChildrenList();
        $result = [];
        
        $this->getChildrenRecursive($role, $childrenList, $result);
        if (empty($result)) {
            return [];
        }
        
        $query = (new Query)->from($this->itemTable)->where([
            'type' => \app\components\AccessLevel::TYPE_ACCESS_LEVEL,
            'name' => array_keys($result),
        ]);
        $levels = [];
        foreach ($query->all($this->db) as $row) {
            $levels[$row['name']] = $this->populateItem($row);
        }
        $resultral[$role] = $levels;
        return $levels;
    }
    
    public function getRoleAccessLevelsStrict($role)
    {
        static $resultrals = [];
        if(isset($resultrals[$role]))
            return $resultrals[$role];
        $childrenList = $this->getChildrenList();
        $result = isset($childrenList[$role])?$childrenList[$role]:[];
        
        //$this->getChildrenRecursive($role, $childrenList, $result);
        if (empty($result)) {
            return [];
        }
        
        $query = (new Query)->from($this->itemTable)->where([
            'type' => \app\components\AccessLevel::TYPE_ACCESS_LEVEL,
            'name' => array_values($result),
        ]);
        $levels = [];
        foreach ($query->all($this->db) as $row) {
            $levels[$row['name']] = $this->populateItem($row);
        }
        $resultrals[$role] = $levels;
        return $levels;
    }
    */
    protected function findParentsRecursive($parent, $row, $childrenList, $type = null)
    {
        $result = [];
        if(isset($childrenList[$parent]))
        {
            foreach($childrenList[$parent] as $c)
            {
                $children = ((isset($childrenList[$c]))?($childrenList[$c]):[]);
                if(in_array($row, $children))
                {
                    $object = $this->getItem($parent);
                    $object2 = $this->getItem($c);
                    
                    if($type && $object instanceof $type)
                        $result[$parent] = $object;
                    elseif($type && $object2 instanceof $type)
                        $result[$c] = $object2;
                    elseif(!isset($type))
                        $result[$parent] = $object;
                    else
                        $result[$c] = $object2;
                } else
                    $result = array_merge($result, $this->findParentsRecursive($c, $row, $childrenList, $type));
            }
            //$result = array_unique($result);
        } else {
            
        }
        return $result;
    }
    
    /*public function getRoleAccessLevelsMap($role)
    {
        static $resultralm = [];
        if(isset($resultralm[$role]))
            return $resultralm[$role];
        $childrenList = $this->getChildrenList();
        
        $strict_children = (isset($childrenList[$role]))?$childrenList[$role]:[];
        
        $result = [];
        
        $this->getChildrenRecursive($role, $childrenList, $result);
        if (empty($result)) {
            return [];
        }
        
        $query = (new Query)->from($this->itemTable)->where([
            'type' => \app\components\AccessLevel::TYPE_ACCESS_LEVEL,
            'name' => array_keys($result),
        ]);
        $levels = [];
        foreach ($query->all($this->db) as $row) {
             
             if(in_array($row['name'], $strict_children))
                 $levels[$row['name']] = [$role => $this->getItem($role)];
             else {
                 $levels[$row['name']] = $this->findParentsRecursive($role, $row['name'], $childrenList);
             }
        }
        $resultralm[$role] = $levels;
        return $levels;
    }
    */
    /*public function getAccessLevelPermissionsMap($level)
    {
        static $resultalpm = [];
        if(isset($resultalpm[$level]))
            return $resultalpm[$level];
        $childrenList = $this->getChildrenList();
        
        $strict_children = (isset($childrenList[$level]))?$childrenList[$level]:[];
        
        $result = [];
        
        $this->getChildrenRecursive($level, $childrenList, $result);
        if (empty($result)) {
            return [];
        }
        
        $query = (new Query)->from($this->itemTable)->where([
            'type' => \app\components\AccessLevel::TYPE_PERMISSION,
            'name' => array_keys($result),
        ]);
        $levels = [];
        foreach ($query->all($this->db) as $row) {
             
             if(in_array($row['name'], $strict_children))
                 $levels[$row['name']] = [$level => $this->getItem($level)];
             else {
                 $levels[$row['name']] = $this->findParentsRecursive($level, $row['name'], $childrenList, 'item');
             }
        }
        $resultalpm[$level] = $levels;
        return $levels;
    }
    */
    public function getRolePermissionsMap($role)
    {
        static $result = [];
        if(isset($result[$role]))
            return $result[$role];
        $childrenList = $this->getChildrenList();
        
        $strict_children = (isset($childrenList[$role]))?$childrenList[$role]:[];
        
        $rows = self::getChildrenOfType($role);
        
        $items = [];
        
        foreach($rows as $row)
        {
            if(in_array($row->name, $strict_children))
                 $items[$row->name] = [$role => $this->getItem($role)];
             else {
                 $items[$row->name] = $this->findParentsRecursive($role, $row->name, $childrenList);
             }
        }
        
        
        
        $result[$role] = $items;
        
        return $items;
    }
    /*public function getParentRolesByRoleStrict($role)
    {
        static $resultpals = [];
        if(isset($resultpals[$role]))
            return $resultpals[$role];
        
        //$this->getChildrenRecursive($levelName, $childrenList, $result);
        
        $query = (new Query)->from($this->itemChildTable)->where([
            'child' => $role,
        ]);
        $result = [];
        foreach ($query->all($this->db) as $row) {
            $result[] = $row['parent'];
        }
        if (empty($result)) {
            return [];
        }
        $query = (new Query)->from($this->itemTable)->where([
            'type' => Item::TYPE_ROLE,
            'name' => array_values($result),
        ]);
        $roles = [];
        foreach ($query->all($this->db) as $row) {
            $permissions[$row['name']] = $this->populateItem($row);
        }
        $roles[$role] = $roles;
        
        return $roles;
    }
    */
    public function findItem($name)
    {
        return $this->getItem($name);
    }
    
    public function getUsersByRole($role)
    {
        if (empty($role)) {
            return [];
        }

        $query = (new Query)->select('a.user_id')
            ->from(['a' => $this->assignmentTable])
            ->where(['a.item_name' => (string) $role])
            ;

        $users = [];
        foreach ($query->all($this->db) as $row) {
            $users[] = $row['user_id'];
        }
        return $users;
    }
    
    /*public function getAccessLevel($name)
    {
        $item = $this->getItem($name);
        return $item instanceof Item && $item->type == \app\components\AccessLevel::TYPE_ACCESS_LEVEL ? $item : null;
    }
    */
    public function getRoles()
    {
        if(is_array(self::$_roles))
            return self::$_roles;
        
        $items = parent::getRoles();
        
        self::$_roles = $items;
        return self::$_roles;
    }
    
    public function getPermissions()
    {
        if(is_array(self::$_permissions))
            return self::$_permissions;
        
        $items = parent::getPermissions();
        
        self::$_permissions = $items;
        return self::$_permissions;
    }
    
    public function getChildrenOfTypeDirect($parent, $type = Item::TYPE_PERMISSION)
    {
        static $result = [];
        if(isset($result[$parent]))
            return $result[$parent];
        $childrenList = $this->getChildrenList();
        $children = (isset($childrenList[$parent])?$childrenList[$parent]:[]);
        
        if (empty($children)) {
            return [];
        }
        $query = (new Query)->from($this->itemTable)->where([
            'type' => $type,
            'name' => array_values($children),
        ]);
        $items = [];
        foreach ($query->all($this->db) as $row) {
            $items[$row['name']] = $this->populateItem($row);
        }
        $result[$parent] = $items;
        return $items;
    }
    public function getChildrenOfType($parent, $type = Item::TYPE_PERMISSION)
    {
        static $result = [];
        if(isset($result[$parent]))
            return $result[$parent];
        $childrenList = $this->getChildrenList();
        $children = [];
        
        $this->getChildrenRecursive($parent, $childrenList, $children);
        if (empty($children)) {
            return [];
        }
        
        $query = (new Query)->from($this->itemTable)->where([
            'type' => $type,
            'name' => array_keys($children),
        ]);
        $items = [];
        foreach ($query->all($this->db) as $row) {
            $items[$row['name']] = $this->populateItem($row);
        }
        $result[$parent] = $items;
        return $items;
    }
    public function getParentOfTypeDirect($child, $type = Item::TYPE_PERMISSION)
    {
        static $result = [];
        if(isset($result[$child]))
            return $result[$child];
        /*$childrenList = $this->getChildrenList();
        $result = (isset($childrenList[$permission])?$childrenList[$permission]:[]);*/
        //$this->getChildrenRecursive($levelName, $childrenList, $result);
        
        $query = (new Query)->from($this->itemChildTable)->where([
            'child' => $child,
        ]);
        $parents = [];
        foreach ($query->all($this->db) as $row) {
            $parents[] = $row['parent'];
        }
        if (empty($parents)) {
            return [];
        }
        $query = (new Query)->from($this->itemTable)->where([
            'type' => $type,
            'name' => array_values($parents),
        ]);
        $items = [];
        foreach ($query->all($this->db) as $row) {
            $items[$row['name']] = $this->populateItem($row);
        }
        $result[$child] = $items;
        
        return $items;
    }
    
}
?>
