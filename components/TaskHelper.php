<?php
namespace app\components;

class TaskHelper
{
    public function start()
    {
        $this->donorList();
    }
    
    /**
     * автоматическая остановка регистрации
     */
    protected function donorList()
    {
        echo 'start';
        $helper = new \app\components\DonorList();
        $helper->add();
        $timeModels = \app\models\Times::find()
                ->where(['hide' => 0])
                ->andWhere('max_donor_count>0')
                ->all();
        foreach($timeModels as $timeModel)
        {
            if(\app\models\Donorlist::find()
                    ->where(['date_id' => $timeModel->id])
                ->count() >= $timeModel->max_donor_count)
            {
                $timeModel->hide = 1;
                $timeModel->save();
            }
        }
    }
}