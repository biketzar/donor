<?php
namespace app\components;

use \app\models\Constants;
class ConstantHelper extends \yii\base\Component
{
    protected $_data = [];
    
    public function __construct($config = [])
    {
        /*if (!empty($config)) {
            Yii::configure($this, $config);
        }*/
        parent::__construct($config);
        $container = \Yii::$container;
        $object = $container->setSingleton('app\components\ConstantHelper', []);
    }
    
    public function init()
    {
        $this->initData();
    }
    
    public function refresh()
    {
        $this->initData(true);
    }
    
    protected function initData($refresh =  false)
    {
        if(!count($this->_data) || $refresh)
        {
            $models = Constants::find()->all();
            foreach ($models as $item)
            {
                switch ($item->type)
                {
                    case Constants::TYPE_FILE:
                        $this->_data[trim($item->name)] = $item->getValuePath();
                        break;
                    case Constants::TYPE_CHECKBOX:
                        $this->_data[trim($item->name)] = $item->value?true:false;
                        break;
                    default:
                        $this->_data[trim($item->name)] = $item->value;
                }

            }
            unset($models);
        }
    }
    
    public static function getValue($name, $default = null)
    {
        $container = \Yii::$container;
        
        $object = $container->get('app\components\ConstantHelper', []);
        
        return $object->get($name, $default);
    }
    
    public function __get($name)
    {
        return $this->get($name);
    }
    public function get($name, $default = null)
    {
        if(isset($this->_data[$name]))
            return $this->_data[$name];
        else
            return $default;
    }
    public static function setValue($name, $value)
    {
        
        return \Yii::$app->constants->set($name, $value);
    }
    
    public function __set($name, $value)
    {
        $this->set($name, $value);
    }
    public function set($name, $value)
    {
        if(\Yii::$app->user->can('private.constants.update'))
        {
            $object = Constants::find()->where(['name' => $name])->one();
            
            if($object)
            {
                $object->value = $value;
                
                if($object->save())
                {
                    $this->refresh();
                    return true;
                } else {
                    \Yii::$app->session->addFlash('error', \Yii::t('app', 'Error was occupied'));
                }
            } else {
                \Yii::$app->session->addFlash('error', \Yii::t('app', 'Unknown constant'));
            }
        } else {
            \Yii::$app->session->addFlash('error', \Yii::t('app', 'No rights for this action'));
        }
        return false;
    }
}
?>
