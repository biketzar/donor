<?php
//namespace app\components;
define('CACHE_PATH', $_SERVER['DOCUMENT_ROOT'].'/web/assets/cache2/files/');
define('LAST_UPDATE_FILE', $_SERVER['DOCUMENT_ROOT'].'/web/assets/cache2/settings.txt');
class PageCache
{
    private $exceptions = array('/private', '/user', 'captcha');
    private $cacheTime = 3600;
    
    public function init($cache_time = 3600)
    {
        $this->cacheTime = $cache_time;
    }
    
    /*
     * метод определяет, выводить данные из кэша или нет
     * * return bool
     */
    public function checkPage($request)
    {
        //если есть параметры - кэш не используется
        if(count($_POST)>0 && count($_GET)>0)
        {
            return FALSE;
        }
        $time = $this->getLastUpdateTime();
        $fileName = $this->getCacheFileName($request);
        if(file_exists($fileName) && $time!==FALSE)
        {
            if(filemtime($fileName)>$time && (filemtime($fileName) + $this->cacheTime) > time() && $this->checkException($request) === FALSE)
            {
                return TRUE;
            }
            else
            {
                return FALSE;
            }
        }
        else
        {
            return FALSE;
        }
    }
    
    /*
     * поиск исключений
     * если подстрока попадётся в REQUEST_URI, то кэширование не включается
     * return bool
     */
    private function checkException($request)
    {
        foreach($this->exceptions as $exception)
        {
            if(strripos($request, $exception) !== FALSE)
            {
                return TRUE;
            }
        }
        return FALSE;
    }
    
    /*
     * возвращает имя закэшированного файла
     * return string
     */
    private function getCacheFileName($request)
    {
        return CACHE_PATH.md5($request.'1121');
    }
    
    /*
     * возвращает timestamp последнего изменения в админке
     * если файл кэша изменён до этого времени, следовательно не используем его
     * * return int/bool
     */
    private function getLastUpdateTime()
    {
        if(file_exists(LAST_UPDATE_FILE))
        {            
            $time =  file_get_contents(LAST_UPDATE_FILE);
            if($time)
            {
                return $time;
            }
            else
            {
                return FALSE;
            }
        }
    }
    
    /*
     * проверка существоания директории
     * если нет, то создание директории
     * return bool
     */
    private function checkDirectory($dir)
    {
        $dirArray = explode('/', trim($dir,'/'));
        $dirPath = '/';
        foreach($dirArray as $dir_name)
        {
            $dirPath .= $dir_name.'/';
            if(!file_exists($_SERVER['DOCUMENT_ROOT'].$dirPath) && strpos($dir_name, '.')===FALSE)
            {
                if(mkdir($_SERVER['DOCUMENT_ROOT'].$dirPath, 0774) === FALSE)
                {
                    return FALSE;
                };
            }
        }
        return TRUE;
    }
    
    /*
     * запись страницы в файл
     * params string data - записываемые данные
     * params string request
     * return bool
     */
    public function cacheInFile($data, $request)
    {
        if($this->checkDirectory(str_replace($_SERVER['DOCUMENT_ROOT'], '', CACHE_PATH)) === FALSE && $this->checkException($request)===TRUE)
        {
            return FALSE;
        };
        $fileName = $this->getCacheFileName($request);
        $file = fopen($fileName, 'w');
        if($file !== FALSE)
        {
            if (flock($file, LOCK_EX | LOCK_NB)) {
                fwrite($file, $data);
                flock($file, LOCK_UN);
                return TRUE;
            }
        }
        return FALSE;
    }
    
    /*
     * чтение закешированного файла
     * params - string request
     * return string/bool 
     */
    public function getCacheFile($request)
    {
        $fileName = $this->getCacheFileName($request);
        $file = fopen($fileName, 'r');
        if($file !== FALSE && filesize($fileName)>0)
        {
            if (flock($file, LOCK_SH | LOCK_NB)) {
                $data = fread($file, filesize($fileName));
                flock($file, LOCK_UN);
                return $data;
            }
        }
        return FALSE;
    }
    
    /*
     * запись в файл timestamp последнего изменения в админке 
     * вызывается в htdocs\modules\privatepanel\components\PrivateModelAdmin.php
     * params int timestamp
     * return bool
     */    
    public function saveLastUpdateTime($time)
    {
        if($this->checkDirectory(str_replace($_SERVER['DOCUMENT_ROOT'], '', LAST_UPDATE_FILE)) === FALSE)
        {
            return FALSE;
        };
        file_put_contents(LAST_UPDATE_FILE, $time);
        return TRUE;
    }
    
}

