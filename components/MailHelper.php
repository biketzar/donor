<?php
namespace app\components;

use yii\helpers\Url;

class MailHelper extends \yii\base\Object
{
    public $to = [];
    public $from = null;
    public $fromName = null;
    public $subject = '';
    public $content = '';
    public $params = [];
    public $files = [];
    //public $template = '@app/views/mail/mail';
    public $template = '@app/views/mail/simple.php';
    public $mailTemplate = '';
    public $unsubscribeLink;
    
    public function __construct($config = array()) {
        parent::__construct($config);
        if(!$this->from)
            $this->from = \Yii::$app->constants->get('mail_from');
        
    }
    
    public function send()
    {
        if($this->mailTemplate)
        {
            $mailTemplates = \app\models\MailTemplate::find()->where('alias=:alias', [':alias' => $this->mailTemplate])->all();
            
            $origContent = $this->content;
            
            foreach($mailTemplates as $mailTemplate)
            {
                $this->content = $origContent.$mailTemplate->content;
                if($mailTemplate->from && !$this->from)
                    $this->from = $mailTemplate->from;
                if($mailTemplate->subject && !$this->subject)
                    $this->subject = $mailTemplate->subject;                
                return $this->sendOne();
            }
        } else {
            return $this->sendOne();
        }
        
        
    }
    
    public function sendOne()
    {
        $params = [];
        if(trim($this->content))
        {
            $params['message'] = $this->replaceParams($this->content);
            
        }
        if($this->unsubscribeLink)
        {
          $params['unsubscribeLink'] = $this->unsubscribeLink;
        }
            
        if($this->template)
            $mailer = \Yii::$app->mailer->compose($this->template, $params);
        else
            $mailer = \Yii::$app->mailer->compose(null, $params);
        
        if(!is_array($this->to))
            $mailer->setTo($this->to);
        else {
            foreach($this->to as $k => $to)
            {
                if($k == 0)
                    $mailer->setTo($to);
                else
                    $mailer->addTo($to);
            }
        }
        if($this->from)
            $mailer->setFrom($this->from, $this->fromName);
        
        $this->subject = $this->replaceParams($this->subject);
        
        $mailer->setSubject($this->subject);
        $mailer->setReplyTo(ConstantHelper::getValue('reply-to', 'borovov.alexandr@gmail.com'));
        foreach($this->files as $k => $file)
        {
            $options = [];
            if(!is_numeric($k))
                $options['fileName'] = $k;
            $filePath = str_replace(\yii::getAlias('@webroot'), '', $file);
            $filePath = str_replace(\yii::getAlias('@web'), '', $filePath);
            $mailer->attach(\yii::getAlias('@webroot').$filePath, $options);
        }
        
        if($body = $mailer->getSwiftMessage()->getBody())
        {
            $mailer->getSwiftMessage()->setBody($this->proccessText($mailer, $body));
        } else {
            $parts = $mailer->getSwiftMessage()->getChildren();
            foreach ($parts as $key => $part) {
                if (!($part instanceof \Swift_Mime_Attachment)) {
                    $part->setBody($this->proccessText($mailer, $part->getBody()));
                }
            }
        }
        
        
        return $mailer->send();
        
    }
    
    protected function replaceParams($text)
    {
        $project_url = \Yii::$app->constants->get('project_url');
        if(!$project_url && \yii::$app->urlManager->hostInfo)
            $project_url = \yii::$app->urlManager->hostInfo;
        if(!$project_url && isset($_SERVER["HTTP_HOST"]))
            $project_url = 'http://'.$_SERVER["HTTP_HOST"];
        $text = str_replace('[WWW]', $project_url, $text);
        
        foreach($this->params  as $k=>$v)
        {
            $text = str_replace('['.$k.']', $v, $text);
        }
        return $text;
    }

    protected function proccessText($mailer, $text)
    {
        $text = $this->replaceParams($text);
        
        $images = array();
        $text = preg_replace("/&amp;/i","&",$text);
        
        
        if (preg_match_all ("/<a.[^>]*href[^=]*=['\"]+([^>]*)['\"]+[^>]*>/i", $text, $links)) {
                foreach($links[0] as $k => $link)
                {
                    $newlink = $link;
                    if(Url::isRelative($links[1][$k]))
                        $newlink = str_replace($links[1][$k], Url::to($links[1][$k], true), $link);
                    
                    $text = str_replace($link, $newlink, $text);
                }
        }
        
        if (preg_match_all ("/<img.[^>]*>/i", $text, $images)) {
                $images = $images[0];
        }
        
        if (preg_match_all ("/background:[\s]*url\(['\"]?(.[^ '\"]*)(['\"])[\s]*\)?/i", $text, $backgrounds)) {
                $images = array_merge($images,$backgrounds[0]);
        }
        
        $filelist = Array();
        for ($i = 0; $i < sizeof($images); $i++ ) {
            
                if (is_array($images[$i])) continue;	
                
                $filelist[] = static::ProcessImage($mailer, $images[$i], $text);
        }
        /*for ($i=0; $i< sizeof($filelist); $i++) {
                $file_text = $filelist[$i][0];
                $file_name = preg_replace("/^\.\.\/files/i","files", $file_text);
                $file_name = preg_replace("/^\.\./i","", $file_name);
                $file_name = preg_replace("/^\/files/i","files", $file_name);
        }
        */
        
        return $text;
            
    }
    
    
	/**
	 * Функция обработки изображений в теле письма
	 * @param img_text строка картинки из регулярного выражения
	 * @param text тело письма
	 * @return array (путь к изображению, mime тип)
	 */
	private static function ProcessImage ($mailer, $img_text, &$text) {
            $temp_img = array();
            $root = $_SERVER['DOCUMENT_ROOT'];
            if(preg_match("/<img[^>]*src=\"?(.[^ \"]*)\"?[^>]*>/i", $img_text, $temp_img)){
                    $path = $temp_img[1];
                    if(strpos($path,'http') === false)
                    {
                        /*if($path[0] != '/')
                            $path = '/'.$path;
                        */
                        if(strpos($path, \yii::getAlias('@web2')) !== false)
                            $path = str_replace(\yii::getAlias('@web2'), \yii::getAlias('@webroot2'), $path);
                        else
                            $path = \yii::getAlias('@webroot'.$path);
                        //$path = Url::to($path, true);
                        //$text = str_replace('src="'.$temp_img[1].'"', 'src=\''.$path.'\'', $text);
                        
                        if(file_exists($path))
                            $text = str_replace('src="'.$temp_img[1].'"', 'src="'.$mailer->embed($path).'"', $text);
                        else
                            $text = str_replace($temp_img[0], '', $text);
                        //$text = str_replace('src="'.$temp_img[1].'"', 'src="'.$mailer->embed($path).'"', $text);
                    }
                    $size = @getimagesize($path);
                    return array($path, $size['mime']);
            }
            $size = @getimagesize($path);
            if(preg_match("/background:[\s]*url\(['\"]?(.[^ '\"]*)(['\"])[\s]*\)?/i", $img_text, $temp_img)){
                    $path = $temp_img[1];
                    if(strpos($path,'http') === false)
                    {
                        if($path[0] != '/')
                            $path = '/'.$path;
                        $path = Url::to($path, true);
                        $text = str_replace($temp_img[0], 'background: url('.$temp_img[2].$path.$temp_img[2].')', $text);
                    }
                    $size = @getimagesize($path);
                    return array($path, $size['mime']);
            }

            /*if(preg_match("/src=\"?(.[^ \"]*)\"?/i", $img_text, $temp_img)){
                    $path = preg_replace("/http:\/\/.[^\/]*\//i","",$temp_img[1]);
                    $source_path = $path;			
                    $temp_img = array();
                    $small_width = (preg_match("/width=\"?(\d*)\"?/i", $img_text, $temp_img)) ? $temp_img[1] : 100;
                    $small_height = (preg_match("/height=\"?(\d*)\"?/i", $img_text, $temp_img)) ? $temp_img[1] : 100;
            }

            $quality = 0;
            if(preg_match("/\.jpg$/i", $path)) {
                    $CreateFunction = 'ImageCreateFromJPEG';
                    $SaveFunction = 'imageJpeg';
                    $TypeHeader = 'jpeg';	
                    $Extension = 'jpg';
                    $quality = 100;
            } elseif(preg_match("/\.png$/i", $path)) {
                    $CreateFunction = 'ImageCreateFromPng';
                    $SaveFunction = 'imagePng';
                    $TypeHeader = 'png';			
                    $Extension = 'png';
            }elseif(preg_match("/\.gif$/i", $path)){			
                    $CreateFunction = 'ImageCreateFromGif';
                    $SaveFunction = 'imageGif';
                    $TypeHeader = 'gif';			
                    $Extension = 'gif';
            }else{
                    return array($path, false);
            }

            $path = preg_replace("/^\.\.\/files/i","files", $path);
            $path = preg_replace("/^\/files/i","files", $path);
            $path = $root.'/'.$path;

            if(!$size = @getimagesize($path)){
                    return array($path, false);
            }
            $width = $size[0];
            $height = $size[1];	
            if($small_width<$width && $small_height<$height){
                    $small_path = preg_replace("/\.".$Extension."/i", "s{$small_width}.".$Extension, $path);
                    if (!file_exists($small_path)) {
                            $src = $CreateFunction ($path);
                            $x = 0;
                            $y = 0; 
                            if (($small_width/$small_height)>($width/$height)){
                                    //$dx = $width;
                                    $dy = $width*$small_height/$small_width;
                                    $y = round(($height/2) - $dy/2);
                                    $height = (round($dy) < $height) ? round($dy): $height;
                            } else {
                                    //$dy = $height;
                                    $dx = $height*$small_width/$small_height;
                                    $x = round(($width/2) - $dx/2);
                                    $width = (round($dx) < $width) ? round($dx): $width;
                            }
                            if($Extension == 'jpg'){			
                                    $dst = imagecreatetruecolor($small_width, $small_height);
                                    imagecopyresampled ($dst, $src, 0, 0, $x, $y, $small_width, $small_height, $width, $height);			
                            }else{ 
                                    $dst = imagecreate ($small_width, $small_height);			
                                    $black = ImageColorAllocate ($dst, 0, 0, 0);
                                    ImageColorTransparent($dst, $black);			
                                    imagecopyresized ($dst, $src, 0, 0, $x, $y, $small_width, $small_height, $width, $height);			
                            } 
                            $SaveFunction($dst, $small_path, $quality);	
                    }
            }else{
                    $small_path = $path;	
            }
            $small_path = str_replace($root.'/', '', $small_path);
            $text = str_replace($source_path, $small_path, $text);
            
            
            return array($small_path, $size['mime']);*/
            return [];
    }
    /*
    
    Yii::$app->mailer->compose()
    ->setFrom('from@domain.com')
    ->setTo('to@domain.com')
    ->setSubject('Message subject')
    ->setTextBody('Plain text content')
    ->setHtmlBody('<b>HTML content</b>')
    ->send();
    // Attach file from local file system:
$message->attach('/path/to/source/file.pdf');

// Create attachment on-the-fly
$message->attachContent('Attachment content', ['fileName' => 'attach.txt', 'contentType' => 'text/plain']);
Yii::$app->mailer->compose('embed-email', ['imageFileName' => '/path/to/image.jpg'])*/
}
?>
