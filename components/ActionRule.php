<?php
namespace app\components;

use yii\rbac\Rule;

use yii\web\User;
use yii\di\Instance;


/**
 * Checks if authorID matches user passed via params
 */
class ActionRule extends Rule
{
    public $name = 'action_rule';

    private $_user = 'user';
    
    /**
     * @param string|integer $user the user ID.
     * @param Item $item the role or permission that this rule is associated with
     * @param array $params parameters passed to ManagerInterface::checkAccess().
     * @return boolean a value indicating whether the rule permits the role or permission it is associated with.
     */
    public function execute($user, $item, $params)
    {
        return true;
    }
    
    public function allows($action, $user, $request)
    {
        $authManager = \yii::$app->authManager;
        $assignments = $authManager->getAssignments($user->id);
        if(isset($assignments['superadmin']))
            return true;
        $actionId = $action->getUniqueId();
        
        return $user->can('/'.$actionId);
    }
    
    public function getUser()
    {
        if (!$this->_user instanceof User) {
            $this->_user = Instance::ensure($this->_user, User::className());
        }
        return $this->_user;
    }
    
    
    
}
?>
