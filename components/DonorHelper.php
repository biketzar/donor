<?php
namespace app\components;
use \yii\helpers\ArrayHelper;
use \yii\helpers\Html;

class DonorHelper
{
    
    public static function getResult($action, $data)
    {
        $instance = new DonorHelper();
        switch($action)
        {
            case 'get-donors':
                return $instance->getDonors($data);
            case 'set-bloodtype':
                return $instance->setBloodType($data);
            case 'set-need-docs':
                return $instance->setNeedDocs($data);
            case 'set-donated':
                return $instance->setDonated($data);
        }
    }
    
    /**
     * список доноров в указанном времени
     * @param array $data
     * @return array
     */
    public function getDonors($data)
    {
        $time = ArrayHelper::getValue($data, 'time');
        if(!$time)
        {
            return ['error' => 'Выбрано некорректное время'];
        }
        $timeModel = \app\models\Times::findOne(['id' => $time]);
        if(!$timeModel)
        {
            return ['error' => 'Выбрано некорректное время'];
        }
        $donorInfoModels = \app\models\Donorlist::find()
                ->where('approved=1')
                ->andWhere(['date_id' => $timeModel->id])
                ->orderBy('id ASC')
                ->with('donor')
                ->all();
        $rows = [];
        $donorList = [];
        $counter = 1;
        foreach($donorInfoModels as $donorInfoModel)
        {
            $col = [];
            $donorList[] = $donorInfoModel->donor->attributes;
            $col[] = $counter++;
            $col[] = "{$donorInfoModel->donor->surname} {$donorInfoModel->donor->name} {$donorInfoModel->donor->pname}";
            $col[] = \app\models\Donorhistory::find()->where(['id_donor' => $donorInfoModel->donor->id])->limit(1)->count() ? 'В' : 'П';
            $col[] = $donorInfoModel->donor->typing ? 'Т' : '';
            $col[] = $donorInfoModel->donor->phone;
            $col[] = Html::dropDownList('blood_type', $donorInfoModel->donor->blood_type, ['' => '---'] + \app\models\Donorlite::getBloodTypeList())
                    .Html::tag('SPAN', ArrayHelper::getValue(\app\models\Donorlite::getBloodTypeList(), $donorInfoModel->donor->blood_type), ['class' => 'show-print']);
            $isDonated = (bool)\app\models\Donorhistory::find()->where(['id_donor' => $donorInfoModel->donor->id, 'date_id' => $timeModel->id])->limit(1)->count();
            $col[] = Html::dropDownList('donated', $isDonated ? 1 : 0, ['0' => 'Не сдал', '1' => 'Сдал'])
                    .Html::tag('SPAN', $isDonated ? 'Сдал' : ' ', ['class' => 'show-print']);
            $needDocs = $this->getNeedDocsByDonor($donorInfoModel->donor->id, $timeModel->id);
            $docList = ArrayHelper::map(\app\models\Documents::find()->asArray()->all(), 'id', 'name');
            $col[] = Html::dropDownList('docs[]', $needDocs, $docList, ['class' => 'need-docs-elems', 'multiple' => TRUE])
                    .Html::tag('SPAN', implode('; ', array_map(function($elem) use($docList) {return ArrayHelper::getValue($docList, $elem);}, $needDocs)), ['class' => 'show-print']);
            $col[] = ' ';
            $rows[] = $col;
        }
        return [
            'error' => false,
            'rows' => $rows,
            'donorList' => $donorList,
            'time' => date('d.m H:i', strtotime($timeModel->dtime)),
            'timeId' => $timeModel->id,
            ];
    }
    
    private function getNeedDocsByDonor($donorId, $timeId)
    {
        return (new \yii\db\Query())->from('yii2_link_doc')
                ->where(['donor_id' => $donorId, 'time_id' => $timeId])
                ->column();
    }
    
    /**
     * запись группы крови
     * @param array $data
     * @return array
     */
    public function setBloodType($data)
    {
        $donor = \app\models\Donorlite::findOne(['id' => ArrayHelper::getValue($data, 'donorId')]);
        if(!$donor)
        {
            return ['error' => 'Донор не найден'];
        }
        if(!array_key_exists(ArrayHelper::getValue($data, 'bloodType'), \app\models\Donorlite::getBloodTypeList()))
        {
            return ['error' => '"Гр.крови" - недопустимое значение'];
        }
        $donor->blood_type = ArrayHelper::getValue($data, 'bloodType');
        if(!$donor->save())
        {
            $error = '';
            foreach($donor->getErrors() as $errors)
            {
                $error .= "\n".implode("\n", $errors);
            }
            return [
                'error' => $error
            ];
        }
        return [
            'error' => false
            ];
    }
    
    public function setNeedDocs($data)
    {
        \Yii::$app->db->createCommand('DELETE FROM yii2_link_doc WHERE donor_id=:donor_id AND time_id=:time_id', 
                [
                    ':donor_id' => ArrayHelper::getValue($data, 'donorId'),
                    ':time_id' => ArrayHelper::getValue($data, 'timeId')
                    ])
                ->execute();
        foreach(ArrayHelper::getValue($data, 'needDocs', []) as $docId)
        {
            \Yii::$app->db->createCommand('INSERT INTO yii2_link_doc (doc_id, donor_id, time_id) VALUES(:doc_id, :donor_id, :time_id)', 
                [
                    ':donor_id' => ArrayHelper::getValue($data, 'donorId'),
                    ':time_id' => ArrayHelper::getValue($data, 'timeId'),
                    ':doc_id' => $docId
                    ])
                ->execute();
        }
        return [
            'error' => false
            ];
    }
    
    public function setDonated($data)
    {
        $timeModel = \app\models\Times::findOne(['id' => ArrayHelper::getValue($data, 'timeId')]);
        if(!$timeModel)
        {
            return ['error' => 'Выбрано некорректное время'];
        }
        $isDonated = (bool)ArrayHelper::getValue($data, 'donated');
        if($isDonated)
        {
            $historyModel = \app\models\Donorhistory::find()
                    ->where([
                        'id_donor' => ArrayHelper::getValue($data, 'donorId'), 
                        'date_id' => $timeModel->id])->one();
            if(!$historyModel)
            {
                $historyModel = new \app\models\Donorhistory();
                $historyModel->setScenario('insert');
            }
            $historyModel->id_donor = ArrayHelper::getValue($data, 'donorId');
            $historyModel->date_id = $timeModel->id;
            $historyModel->is_politeh = $timeModel->typing ? 0 : 1;
            if(!$historyModel->save())
            {
                $error = '';
                foreach($historyModel->getErrors() as $errors)
                {
                    $error .= "\n".implode("\n", $errors);
                }
                return [
                    'error' => $error
                ];
            }
        }
        else
        {
            $historyModel = \app\models\Donorhistory::find()
                    ->where([
                        'id_donor' => ArrayHelper::getValue($data, 'donorId'), 
                        'date_id' => $timeModel->id])->one();
            if($historyModel)
            {
                $historyModel->delete();
            }
        }
        return [
            'error' => false
            ];
    }
}