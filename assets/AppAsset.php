<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot2';
    public $baseUrl = '@web2';
    public $css = [
        //'css/site.css',
        'css/texts.css',
        'css/contraind_table.css',
        'css/navbar.css',
        'css/event_styles.css',
        'css/upButton.css'
    ];
    public $js = [
        //'js/site.js'
        'js/upButton.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
    ];
}
