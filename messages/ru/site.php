<?php
return [
    'Menu layout Choice' => 'Выбор шаблона оформления',
    'User settings' => 'Настройки',
    'Horizontal menu layout' => 'Горизонтальное меню',
    'Vertical menu layout' => 'Вертикальное меню',
    'Color Theme Choice' => 'Цветовые схемы',
    'Green color theme' => 'Зеленая',
    'Blue color theme' => 'Голубая',
    'Logotype image' => 'Логотип',
    'Download file' => 'Загрузить файл',
    'Add' => 'Добавить',
    'Delete' => 'Удалить',
    'File is not selected' => 'Файл не выбран',
    'Background Theme images' => 'Пользовательские темы',
    'Blue background theme' => 'Голубая',
    'Standart' => 'Стандарт',
    
];
?>
